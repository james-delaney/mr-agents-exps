import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
#from gym_grid_wireless.envs import PathPowerWirelessEnvV1 as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw
from gym_grid_wireless.envs.path_power_wireless_env import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates
from gym_grid_wireless.multi_radio_actions import Actions, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv

'''
TIME_STEP_INTERVAL = 0.2
PATH_LEN = 20000

FILE_DIR = 'gym_grid_wireless/envs/'#os.path.dirname(__file__) + '/'

RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]


#RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [7,15,18,21,24], [4,27,54,107,215], 'psk', 4), #Radio(0, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]

#RADIO_CFG = [   Radio(0, 2400, -84, 1000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240]),
#               Radio(1, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215])]
                


#RADIO_CFG = [   Radio(0, 915, -101, 200, [7,7,7,7,7], [4,4,4,4,4]), #Radio(0, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400, -84, 1000, [5,5,5,5,5], [7.8,7.82,7.82,7.82,7.82])]


DST_NODE_X = 100
DST_NODE_Y = 100


#PATH_FNAME = 'm_7500.csv'#'center_20000.csv'
#ENV_FNAME = 'polys_n_3.2_4.5.csv'#'polys_c_2.0_3.5.csv'
#PLE_FNAME = 'PLE_m_7500_polys_n_3.2_4.5.csv'#'PLE_center_20000_polys_c_2.0_3.5.csv'
#SHD_FNAME = 'SHD_m_7500_polys_n_3.2_4.5.csv'

PATH_FNAME = 'q_20000.csv'
ENV_FNAME = 'polys_p_3.5_5.csv'
PLE_FNAME = 'PLE_q_20000_polys_q_3.5_5.csv'
SHD_FNAME = 'SHD_q_20000_polys_q_3.5_5.csv'

#env = gym.make("grid-wireless-v0", update_interval=0.5, god_agent=False)
#env = gym.make("linear-wireless-v0", update_interval=0.5, god_agent=False)
env = gym.make("path-power-wireless-v2", update_interval=TIME_STEP_INTERVAL, radio_cfg=RADIO_CFG, agent_path_fname=PATH_FNAME, env_fname=ENV_FNAME, ple_fname=PLE_FNAME, shd_fname=SHD_FNAME, god_agent=False)
#env = gym.make("path-power-wireless-v0", update_interval=0.5, radio_cfg=RADIO_CFG, agent_path_fname='a_1.csv', env_fname='polys_1.5_4.5.csv', god_agent=False)

def plt_env_path(radio_use_trace, outage_trace):
    env_polygons = []
    env_PLE = []
    path_points = []
    path_count = 0
    path_len = 0
    env_PLE = []
    PLE_points = []
    path_points_x = []
    path_points_y = []

    print('loading path')
    with open(FILE_DIR+PATH_FNAME, 'r', newline='') as file:
        lines = csv.reader(file, delimiter=',')
        for line in lines:
            #print([int(line[0]), int(line[1])])
            path_points.append([int(line[0]), int(line[1])])
            path_points_x.append(int(line[0]))
            path_points_y.append(int(line[1]))
    
    
    print('loading polygons')
    with open(FILE_DIR+ENV_FNAME, 'r') as file:
        lines = csv.reader(file, delimiter=',')
        env = next(lines)
        
        line=next(lines)
        while line:
        #for line in lines:
            p = []
            
            for i in range(1, len(line), 2):
                p.append([float(line[i]), float(line[i+1])])
            poly  = Polygon(p)
            
            env_polygons.append(poly)
            env_PLE.append(float(line[0]))

            line = next(lines, None)
        
        # Comment out plot of environment and path
        
        fig = plt.figure(1)
        fig.suptitle('Mobile Node Trajectory and Adaptive Radio Selection\nQ-Learning')
        gs = gridspec.GridSpec(16, 10)

        ax = plt.subplot2grid((16,20), (0,17), colspan=1, rowspan=16)
        ax2 = plt.subplot2grid((16,20), (0,0), colspan=16, rowspan=16)
    
        norm = mpl.colors.Normalize(vmin=2.0, vmax=6.0, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in env_polygons:
            ax2.fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(env_PLE[env_polygons.index(p)]))


        cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})



        #ax2.plot(path_points_x, path_points_y, 'r', label="Mobile node path")
        
        r0_points_x = []
        r0_points_y = []
        r1_points_x = []
        r1_points_y = []
        nr_points_x = []
        nr_points_y = []

        r0_dn_points_x = []
        r0_dn_points_y = []
        r1_dn_points_x = []
        r1_dn_points_y = []
        
        for i in range(0, PATH_LEN-1):
            #if i % 1000 == 0:
                #print("count: " + str(i)) 
            if radio_use_trace[i] == 0:
                #print("num " + str(i) + "r0")
                r0_points_x.append(path_points_x[i])
                r0_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='y')
            elif radio_use_trace[i] == 1:
                #print("num " + str(i) + "r1")
                r1_points_x.append(path_points_x[i])
                r1_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='c')
            else:
                #print("num " + str(i) + "nr")
                nr_points_x.append(path_points_x[i])
                nr_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='r')

            if outage_trace[i] == 1:
                r0_dn_points_x.append(path_points_x[i])
                r0_dn_points_y.append(path_points_y[i])
            elif outage_trace[i] == 2:
                r1_dn_points_x.append(path_points_x[i])
                r1_dn_points_y.append(path_points_y[i])
        
        print(len(r0_dn_points_x), len(r1_dn_points_x))
        #print(radio_use_trace)

        ax2.plot(r0_points_x, r0_points_y, 'om', markersize=1, c='m', label='802.15.4 Radio')
        ax2.plot(r1_points_x, r1_points_y, 'oc', markersize=1, c='c', label='802.11 Radio')
        
        ax2.plot(r0_dn_points_x, r0_dn_points_y, 'ob', markersize=1, c='b', label='802.15.4 Radio down')
        ax2.plot(r1_dn_points_x, r1_dn_points_y, 'og', markersize=1, c='g', label='802.11 Radio down')
        ax2.plot(nr_points_x, nr_points_y, 'or', markersize=1, c='r', label='Radio off')

        ax2.plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")

        ax2.set_xlabel('X coordinate, m')
        ax2.set_ylabel('Y coordinate, m')
        #ax2.set_xticks([])
        #ax2.set_yticks([])
        lg = ax2.legend(prop={'size':8}, loc=4)
        lg.legendHandles[0]._legmarker.set_markersize(5)
        lg.legendHandles[1]._legmarker.set_markersize(5)
        lg.legendHandles[2]._legmarker.set_markersize(5)
        lg.legendHandles[3]._legmarker.set_markersize(5)
        lg.legendHandles[4]._legmarker.set_markersize(5)
        lg.legendHandles[5]._legmarker.set_markersize(5)
        plt.xlim(0, 500)
        plt.ylim(0, 500)
        
        plt.axis([0,500,0,500])

def find_optimum_action(action_space):
    

    max_r = 0
    max_r_action = 0
    for a in action_space:
        
        if (a >= Actions.RADIO_0_TX_POW_0 and a <= Actions.RADIO_0_TX_POW_4):
            radio_id = 0
        elif (a >= Actions.RADIO_1_TX_POW_0 and a <= Actions.RADIO_1_TX_POW_4):
            radio_id = 1
        elif (a == Actions.NO_TX):
            radio_id = 0

        obs,reward,d,i = env.test_step(a)
        #r = env.calc_reward(radios[radio_id], False, a, radios[radio_id-1], None)
        #print(a, r)
        r = 0.5*reward[0] + 0.5*reward[1]
        if r > max_r:
            if obs.get_link_status():
                max_r = r
                max_r_action = a

    return max_r_action


def run_optimum_agent():
    
    action_enum = Actions
    action_space = spaces.Discrete(len(action_enum))


    env.reset()
    next_action = 0
    done = False
    count = 0
    while not done:
        
        obs, reward, done, info = env.step(next_action)

        
        if (obs is not None):
            curr_radio_id = obs.get_curr_radio_id()
            radios = obs.get_radios()    
        else:
            pass
        
        if not done:
            
            next_action = find_optimum_action(action_enum)
            if (next_action == Actions.NO_TX):
                print(count)
        #if (reward == 0):    
            #print(count, next_action)
        count += 1
        #time.sleep(0.01)


run_optimum_agent()

pkt_loss_rate = env.recorder.data['pkt_loss_rate']
cum_power = env.recorder.data['tot_pwr']
cum_step_reward = env.recorder.data['cum_step_reward']

pkt_loss_rate = np.array(pkt_loss_rate)
tot_power = np.array(cum_power)
mean_pkt_loss = np.mean(pkt_loss_rate)
pkt_loss_std = np.std(pkt_loss_rate)
mean_power = np.mean(tot_power)
power_std = np.std(tot_power)

print('mean pkt loss: ' + str(mean_pkt_loss) + ' std: ' + str(pkt_loss_std))
print('mean pwr cons: ' + str(mean_power) + ' std: ' + str(power_std))
print('cum reward: ' + str(env.recorder.data['cum_reward']))

print('mean reward: ' + str(np.mean(env.recorder.data['cum_reward'])) + ' std: ' + str(np.std(env.recorder.data['cum_reward'])))


radio_trace = env.recorder.data['radio_use_trace']
outage_trace = env.recorder.data['outage_trace']
radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
dist_step = env.recorder.data['dist_step'][0]

plt_env_path(radio_trace[0], outage_trace[0])


print(len(radio_0_pwr_trace[0]))

#### Plot Radio Power level and distance vs no. of steps
fig = plt.figure()
fig, ax1 =plt.subplots()
plt.xlabel('Steps')
r0 = ax1.scatter(range(0,PATH_LEN), radio_0_pwr_trace[0], c='m', s=1.5, label='802.15.4 Radio')
r1 = ax1.scatter(range(0,PATH_LEN), radio_1_pwr_trace[0], c='c', s=1.5, label='802.11 Radio')
ax1.set_ylim([0.5,5.5])
ax1.set_ylabel('Radio Power Level')

ax2 = ax1.twinx()
l2 = ax2.plot(range(0,PATH_LEN), dist_step, c='r', label='Node distance')

ax2.set_ylim([0, 450])
ax2.set_ylabel('Node distance, m')
#ax1.legend(loc='lower right')

#lns = [r0,r1,l2]
#labs = [l.get_label() for l in lns]
#a1.legend(lns, labs, loc='lower right')
fig.legend(loc='lower right', bbox_to_anchor=(1,0), bbox_transform=ax1.transAxes)

fig.suptitle('Radio Power Level and Node Distance vs. Steps\nSARSA')

plt.show()
'''

class OptimalAgent():
    
    def __init__(self, env):
        self.num_components = 0
        self.component_weights = None
        
        self.env = env
        
        self.action_space = self.env.action_space
        self.action_enum = self.env.action_enum
        self.n_actions = len(self.action_enum)
        
        self.is_init = False
        
        self.env.recorder.add_data_point('multi_obj_reward')
        
    def cfg_agent(self, num_components, component_weights):
        self.num_components = num_components
        self.component_weights = np.array(component_weights)
    
    def _init_agent(self):
        self.is_init = True
    
    def simulate_run(self, re_init=False):
        self._init_agent()
        
        self.env.reset()
        next_action = 0
        done = False
        count = 0
        while not done:
            
            obs, reward, done, info = self.env.step(next_action)
    
            '''
            if (obs is not None):
                curr_radio_id = obs.get_curr_radio_id()
                radios = obs.get_radios()    
            else:
                pass
            '''
            
            if not done:
                next_action = self._find_optimal_action(count)
                #if (next_action == Actions.NO_TX):
                #    print(count)
                
            mo_reward = np.sum(reward * self.component_weights)
            self.env.recorder.record_data('multi_obj_reward', mo_reward)
            count += 1
            
        
    def _find_optimal_action(self, count):
        max_r = -1
        max_r_action = 0
        rs = []
        for a in range(self.n_actions):
            
            if (a >= Actions.RADIO_0_TX_POW_0 and a <= Actions.RADIO_0_TX_POW_4):
                radio_id = 0
            elif (a >= Actions.RADIO_1_TX_POW_0 and a <= Actions.RADIO_1_TX_POW_4):
                radio_id = 1
            elif (a == Actions.NO_TX):
                
                radio_id = 0

            obs,reward,d,i = self.env.test_step(a)
            
            
            # calculate multi-objective reward by weighted sum of rewards in reward vector
            r = 0
            for ri in range(self.num_components):
                r += self.component_weights[ri]*reward[ri]
            
            '''
            if (count > 13000) and (count < 13005):
                print(obs.get_curr_radio_id(), obs.radios[0].get_fade_margin_to_dst(), obs.radios[0].get_tx_pow_step(), obs.radios[1].get_fade_margin_to_dst(), obs.radios[1].get_tx_pow_step())
                print(count, a, '%.2f\t[%.2f, %.2f]' % (r, reward[0], reward[1]))
                print(count, a, obs.radios[obs.get_curr_radio_id()].get_link_status(), obs.radios[obs.get_curr_radio_id()].curr_ber, r, max_r)
            '''
            # if this action will return the highest reward, update the best action/reward
            if r > max_r:
                
                if obs.radios[obs.get_curr_radio_id()].get_link_status():
                    max_r = r
                    max_r_action = a
                elif a == Actions.NO_TX:
                    max_r = r
                    max_r_action = a

        '''
        if (count > 13000) and (count < 13005):
            print('opt='+str(max_r_action))
        '''
        return max_r_action

class OptimalSOAgent():
    
    def __init__(self, env):
        self.num_components = 0
        self.component_weights = None
        
        self.env = env
        
        self.action_space = self.env.action_space
        self.action_enum = self.env.action_enum
        self.n_actions = len(self.action_enum)
        
        self.is_init = False
        
        
    def cfg_agent(self):
        pass
    
    def _init_agent(self):
        self.is_init = True
    
    def simulate_run(self, re_init=False):
        self._init_agent()
        
        self.env.reset()
        next_action = 0
        done = False
        count = 0
        while not done:
            
            obs, reward, done, info = self.env.step(next_action)
    
            '''
            if (obs is not None):
                curr_radio_id = obs.get_curr_radio_id()
                radios = obs.get_radios()    
            else:
                pass
            '''
            
            if not done:
                next_action = self._find_optimal_action(count)
                #if (next_action == Actions.NO_TX):
                #    print(count)

            count += 1
            
        
    def _find_optimal_action(self, count):
        max_r = -1
        max_r_action = 0
        rs = []
        for a in range(self.n_actions):
            
            if (a >= Actions.RADIO_0_TX_POW_0 and a <= Actions.RADIO_0_TX_POW_4):
                radio_id = 0
            elif (a >= Actions.RADIO_1_TX_POW_0 and a <= Actions.RADIO_1_TX_POW_4):
                radio_id = 1
            elif (a == Actions.NO_TX):
                
                radio_id = 0

            obs,reward,d,i = self.env.test_step(a)
            
            
            # calculate multi-objective reward by weighted sum of rewards in reward vector
            r = reward
            
            '''
            if (count > 13000) and (count < 13005):
                print(obs.get_curr_radio_id(), obs.radios[0].get_fade_margin_to_dst(), obs.radios[0].get_tx_pow_step(), obs.radios[1].get_fade_margin_to_dst(), obs.radios[1].get_tx_pow_step())
                print(count, a, '%.2f\t[%.2f, %.2f]' % (r, reward[0], reward[1]))
                print(count, a, obs.radios[obs.get_curr_radio_id()].get_link_status(), obs.radios[obs.get_curr_radio_id()].curr_ber, r, max_r)
            '''
            # if this action will return the highest reward, update the best action/reward
            if r > max_r:
                
                if obs.radios[obs.get_curr_radio_id()].get_link_status():
                    max_r = r
                    max_r_action = a
                elif a == Actions.NO_TX:
                    max_r = r
                    max_r_action = a

        '''
        if (count > 13000) and (count < 13005):
            print('opt='+str(max_r_action))
        '''
        return max_r_action