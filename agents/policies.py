import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import random

DECAY_PWR_STEPS = 0.0
DECAY_EXP_STEPS = 0.2
DECAY_LINEAR_STEPS = 0.3

def decay_pwr_steps(epsilon_init, epsilon, n_step):
    return epsilon**n_step

def decay_exp_steps(epsilon_init, epsilon, n_step):
    return np.exp(-2*n_step)

def decay_linear_steps(epsilon_init, epsilon, n_step):
    return -0.001*epsilon

def exp_td_err(lr, td_err, sigma):
    num  = -1 * np.abs(lr * td_err)
    
    return np.exp(num/sigma)

def func_eps(lr, td_err, sigma):
    e = exp_td_err(lr, td_err, sigma)

    return ((1 - e)/(1 + e))

class Policy():
    
    def __init__(self, action_space, policy_id, name):
        self.action_space = action_space
        self.policy_id = policy_id
        self.name = name
      
    def __float__(self):
        return float(self.policy_id)
        
    def get_action(self, q, n_step, s, tde):
        pass
        
class MOPolicy():
    
    def __init__(self, name):
        self.name = name
        
    def get_action(self, wq, n_step):
        pass

class EGreedyPolicy(Policy):
    
    def __init__(self, action_space, epsilon):
        super().__init__(action_space, 0, 'eps-greedy')
        self.epsilon = epsilon
        
    def get_action(self, q, n_step, s, tde):
        if (np.random.uniform(0, 1) < self.epsilon): 
            a = self.action_space.sample()
        else: 
            a = np.argmax(q) 
        
        return a
    
class ScheduleEGreedyPolicy(Policy):
    
    def __init__(self, action_space, epsilon, epsilon_min, schedule):
        super().__init__(action_space, schedule, 'schedule-eps-greedy')
        self.epsilon_init = epsilon
        self.epsilon = 0
        self.epsilon_min = epsilon_min
        if schedule == DECAY_PWR_STEPS:
            self.schedule  = decay_pwr_steps
        elif schedule == DECAY_EXP_STEPS:
            self.schedule = decay_exp_steps
        elif schedule == DECAY_LINEAR_STEPS:
            self.schedule = decay_linear_steps
            
        
    def get_action(self, q, n_step, s, tde):
        if (np.random.uniform(0, 1) < self.epsilon):
            a = self.action_space.sample()
        else:
            a = np.argmax(q)
        
        if self.epsilon < self.epsilon_min:
            self.epsilon = self.epsilon_min
        else:
            self.epsilon = self.schedule(self.epsilon_init, self.epsilon, n_step)    
        
        return a

class AdaptiveEGreedyPolicy(Policy):
    
    def __init__(self, action_space, n_states, lr, sigma):
        super().__init__(action_space, 1, 'adaptive-VDBE')
        self.eps_s = np.ones(n_states)
        self.lr = lr
        self.sigma = sigma
        self.dlt = 1/action_space.n
        
    def get_action(self, q_s, n_step, s, tde):
        if (np.random.uniform(0, 1) < self.eps_s[s]):
            a = self.action_space.sample()
        else:
            a = np.argmax(q_s)
            #print(n_step, a)
        #print(n_step, self.eps_s[s])
        self.eps_s[s] = self.dlt * func_eps(self.lr, tde, self.sigma) + (1 - self.dlt) * self.eps_s[s]
        
        return a

class SoftmaxPolicy(Policy):
    
    def __init__(self, action_space, tau):
        super().__init__(action_space, 3, 'softmax')
        self.tau = tau
        
    def get_action(self, q, n_step, s, tde):
        num = np.exp(q/self.tau)
        den = np.sum(np.exp(q/self.tau))
        
        p_a = num/den
        
        return random.choices(range(self.action_space.n), weights=p_a, k=1)[0]

                
            
        