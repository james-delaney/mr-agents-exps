import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly, ControlActions
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv

def exp_td_err(lr, td_err, sigma):
    num  = -1 * np.abs(lr * td_err)
    
    return np.exp(num/sigma)

def func_eps(lr, td_err, sigma):
    e = exp_td_err(lr, td_err, sigma)

    return ((1 - e)/(1 + e))

class GmAgent():

    DECAY_EPS      = 0
    ADPTV_EPS      = 1

    ALG_QL         = 0
    ALG_SARSA      = 1
    ALG_EXP_SARSA  = 2

    def __init__(self, env):
        self.alpha = 0
        self.gamma = 0
        self.epsilon = 0
        self.sigma = 0

        self.q = None

        self.rl_alg = 0
        self.num_components = 0
        self.component_weights = None
        self.epsilon_schedule = 1
        self.rand_expl = False

        self.env = env
        self.states_obj = None

        self.action_space = self.env.action_space
        self.action_enum = self.env.action_enum
        self.n_actions = len(self.action_enum)

        self.is_init = False

        self.env.recorder.add_data_point('multi_obj_reward')
        self.env.recorder.add_data_point('step_weights0')
        self.env.recorder.add_data_point('step_weights1')
        self.env.recorder.add_data_point('td_err')

    def cfg_agent(self, rl_alg, state_space, states_obj, num_components, w1_comp_val, alpha, gamma, epsilon_schedule, rand_expl, sigma, epsilon_min, policy):
        
        self.rl_alg = rl_alg
        self.state_space = state_space
        self.states_obj = states_obj
        self.num_components = num_components
        self.component_weights = np.array([w1_comp_val, 1-w1_comp_val])
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon_schedule = epsilon_schedule
        self.rand_expl = rand_expl
        self.sigma = sigma
        self.epsilon_min = epsilon_min
        self.policy = policy

        self.state_table_size = len(self.state_space)

    def _init_agent(self):
        self.q = np.zeros([self.num_components, self.state_table_size, self.env.action_space.n])
        
        if (self.epsilon_schedule == self.DECAY_EPS):
            self.epsilon = 0.95
        else:
            self.epsilon = np.ones(self.state_table_size)
        
        self.is_init = True

    def simulate_run(self, re_init=False):
        # initialise agent if not already done or if we should re-init every run
        if self.is_init and re_init:
            self._init_agent()
        elif not self.is_init:
            self._init_agent()

        #print(self.component_weights)
        s = self.env.reset()
        #s = int(self.state_space.RADIO_OFF)
        s = 0
        done = False
        r_sum = np.zeros(self.num_components, dtype=float)
        avg_reward = np.zeros(self.num_components, dtype=float)
        n_steps = 0 
        #component_weights = np.array(component_weights).reshape(1,num_components)
        r_bar = 0       # avg reward for r-learning
        dlt = 1/self.n_actions
        sigma = self.sigma
        td_err = np.zeros(self.num_components, dtype=float)
        wtde = 0
        gm_q = np.zeros(self.n_actions)

        avg_td_err = 0
        last_avg_td_err = 0
        last_weight_val = 0 

        a = self.action_space.sample()
        while not done: 
            n_steps += 1
            # take action from policy
            
            '''
            # if we are using regular e-greedy or adaptive e-greedy
            if self.epsilon_schedule == self.DECAY_EPS:
                eps_s = self.epsilon
            else:
                eps_s = self.epsilon[s]        

            '''
            '''
            if (np.random.uniform(0, 1) < eps_s): 
                a = self.action_space.sample()
            else: 
                a = np.argmax(gm_q)
            '''
            # take step 
            obs,r,done,info = self.env.step(list(self.action_enum)[a])
            self.states_obj.update_obs(obs.get_radio_id(), obs)
            s_prime  = self.states_obj.get_state(obs.get_radio_id(), a)

            w_q = np.array([w * self.q[idx,:,:] for idx,w in enumerate(self.component_weights)])
            gm_q = [np.sum(w_q[:, s_prime, a]) for a in range(self.n_actions)]
            gm_q = np.array(gm_q)
            '''
            if (np.random.uniform(0, 1) < eps_s): 
                a_prime = self.action_space.sample()
            else: 
                a_prime = np.argmax(gm_q) 
            '''
            wtde = np.dot(self.component_weights, td_err)
            a_prime = self.policy.get_action(gm_q, n_steps, s_prime, wtde)
            
            for i in range(self.num_components):
                if self.rl_alg == 'q_learning': 
                    a_prime = np.random.choice(np.where(self.q[i,s_prime] == max(self.q[i,s_prime]))[0])
                    td_err[i] = (r[i] + self.gamma*self.q[i,s_prime,a_prime] - self.q[i,s,a])
                    self.q[i,s,a] = self.q[i,s,a] + self.alpha * td_err[i]
                elif self.rl_alg == 'sarsa': 
                    #a_prime = np.argmax(np.cumsum(pi[s_prime,:]) > np.random.random())
                    
                    if self.epsilon_schedule == self.DECAY_EPS:
                        eps_s = self.epsilon
                    else:
                        eps_s = self.epsilon[s_prime] 
                    '''
                    if (np.random.uniform(0, 1) < eps_s): 
                        a_prime = self.action_space.sample()
                    else: 
                        a_prime = np.argmax(self.q[i, s_prime, :])
                    '''
                    #a_prime = np.argmax(self.q[i, s_prime, :])
                    td_err[i] = (r[i] + self.gamma*self.q[i,s_prime,a_prime ] - self.q[i,s,a])
                    new_q = self.q[i,s,a] + self.alpha * td_err[i]
                    '''
                    if (n_steps > 13360) and n_steps < 13380 and i == 1:
                        print(i, s, s_prime)
                        print(i, new_q)
                        print(i, self.q[i,s])
                        print(i, self.q[i, s_prime])

                        print([np.argmax(self.q[i,s]) for s in range(self.state_table_size)])
                    '''
                    self.q[i,s,a] = new_q
                elif self.rl_alg == 'expected_sarsa':
                    q[i,s,a] = q[i,s,a] + self.alpha * (r[i] + self.gamma* np.dot(pi[i,s_prime,:],self.q[i,s_prime,:]) - self.q[i,s,a])
                elif self.rl_alg == 'r_learning':
                    a_prime = np.argmax(np.cumsum(pi[i,s_prime,:]) > np.random.random())
                    delt = r - r_bar + self.q[i,s_prime, a_prime] - self.q[i,s, a]
                    r_bar = r_bar + beta*delt
                    self.q[i,s,a] = self.q[i,s,a] + self.alpha
                else: 
                    raise Exception("Invalid method provided")
                
                
            #wtde = np.dot(self.component_weights, td_err)
            # compute greatest mass for q
            
            #w_q = np.array([w * self.q[idx,:,:] for idx,w in enumerate(self.component_weights)])
            #gm_q = [np.sum(w_q[:, s, a]) for a in range(self.n_actions)]
            '''
            # epsilon decay or state/td error-based adaptive decay
            if self.epsilon_schedule == self.DECAY_EPS:
                #if (self.epsilon < 0.01):
                #    self.epsilon = 0.01
                if (self.epsilon < self.epsilon_min):
                    self.epsilon = self.epsilon_min
                else:
                    self.epsilon = 0.5**n_steps
                    #epsilon = 0.1
            else:
                tde = np.dot(self.component_weights, td_err)
                self.epsilon[s] = dlt * func_eps(self.alpha, tde, sigma) + (1 - dlt) * self.epsilon[s]
                #if (n_steps > 12500):
                #    print(n_steps, self.epsilon[s])
            '''
            a = a_prime
            s = s_prime

            self.env.recorder.record_data('td_err', td_err)
            mo_reward = np.sum(r * self.component_weights)
            self.env.recorder.record_data('multi_obj_reward', mo_reward)
            '''
            if n_steps < WINDOW_SIZE+1:
                avg_td_err = avg_td_err + (1/n_steps)*(td_err - avg_td_err)
            else:
                window_td_err = self.env.recorder.data['td_err'][self.env.recorder.curr_run][len(self.env.recorder.data['td_err'][self.env.recorder.curr_run])-WINDOW_SIZE-1:-1]
                avg_td_err = np.sum(window_td_err)/WINDOW_SIZE
            '''
            '''
            if n_steps % 100 == 0:
                print(avg_td_err)
                step_w_ctl_v2(component_weights, avg_td_err, last_avg_td_err, last_weight_val)
                print(component_weights)
            #    step_weight_controller(component_weights)
            '''
            #if n_steps == 5000:
            #    component_weights = [0.2, 0.8]

            # periodically reset state-based epsilon value
            if n_steps % 1000 == 0:
                if self.rand_expl == True:
                    if (self.epsilon_schedule == self.ADPTV_EPS):
                        for s in range(len(self.epsilon)):
                            self.epsilon[s] = 0.5
                    else:
                        self.epsilon = 0.95



            self.env.recorder.record_data('step_weights0', self.component_weights[0])
            self.env.recorder.record_data('step_weights1', self.component_weights[1])
            
        #return r_sum, avg_reward, d_q
        return done
