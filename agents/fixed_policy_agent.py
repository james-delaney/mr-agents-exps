import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly, ControlActions
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv


class FixedPolicyAgent():

    MAX_PWR         = 0
    MIN_PWR         = 1
    MIN_PWR_CON     = 2 # only use 802.15.4 radio at mid power
    MAX_BR          = 3 # use 802.11 at max power until no signal
    BALANCE         = 4 # 

    def __init__(self, env):
        self.policy = None
        
        self.env = env
        self.states_obj = None
        
        self.env.action_enum = Actions
        self.env.action_space = spaces.Discrete(len(self.env.action_enum))
        
        self.action_space = self.env.action_space
        self.action_enum = self.env.action_enum
        self.n_actions = len(self.action_enum)

        

        self.is_init = False


    def cfg_agent(self, policy):
        
        self.policy = policy

    def _init_agent(self):
        self.is_init = True

    def policy_max_pwr(obs):
        pass
    
    def policy_min_pwr(obs):
        pass
    
    def policy_min_pwr_cons(obs):
        pass
    
    def policy_max_br(obs):
        pass
    
    def policy_balance(self, obs):
        curr_radio = obs.get_radios()[obs.get_curr_radio_id()]
        alt_radio = obs.get_radios()[obs.get_curr_radio_id()-1]
        
        if curr_radio.get_bit_rate() > alt_radio.get_bit_rate():
            if curr_radio.get_link_status():
                a = Actions.RADIO_1_TX_POW_4
            elif alt_radio.get_link_status():
                a = Actions.RADIO_0_TX_POW_3
            else:
                a = Actions.RADIO_1_TX_POW_4
        else:
            if alt_radio.get_link_status():
                a = Actions.RADIO_1_TX_POW_4
            elif curr_radio.get_link_status():
                a = Actions.RADIO_0_TX_POW_3
            else:
                a = Actions.RADIO_1_TX_POW_4
        
        return a

    def simulate_run(self, re_init=False):
        # initialise agent if not already done or if we should re-init every run
        if self.is_init and re_init:
            self._init_agent()
        elif not self.is_init:
            self._init_agent()
        
        self.env.reset()
        
        done = False
        '''
        if self.policy == self.MAX_PWR:
            a = Actions.RADIO_0_TX_POW_4
        elif self.policy == self.MIN_PWR:
            a = Actions.RADIO_0_TX_POW_0
        '''
        a = Actions.RADIO_0_TX_POW_3
        
        while not done:
            
            obs,r,done,info = self.env.step(list(self.action_enum)[a])
            
            '''
            if (obs.get_radio_id() == 1) and (obs.get_fade_margin_to_dst() < 3):
                if self.policy == self.MIN_PWR:    
                    a = Actions.RADIO_0_TX_POW_0
                elif self.policy == self.MAX_PWR:
                    a = Actions.RADIO_0_TX_POW_4
            elif (obs.get_radio_id() == 0) and (obs.get_fade_margin_to_dst() > 11):
                if self.policy == self.MIN_PWR:
                    a = Actions.RADIO_1_TX_POW_0
                elif self.policy == self.MAX_PWR:
                    a = Actions.RADIO_1_TX_POW_4
            else:
                a = a
            '''
            if self.policy == self.BALANCE:
                a = self.policy_balance(obs)
            
        return done


