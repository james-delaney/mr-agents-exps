import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl


class FuzzyController():
    
    def __init__(self, fuzzy_set):
        self.fuzzy_set = fuzzy_set
        
    
    # takes a dictionary of inputs that matches the named inputs to the fuzzy set/ctl sys
    def compute_output(self, inputs):
        for k in inputs.keys():
            self.fuzzy_set.ctl_sys_sim.input[k] = inputs[k]
        
        #print(inputs)
        self.fuzzy_set.ctl_sys_sim.compute()
        
        return self.fuzzy_set.get_output_membership()


class FuzzySet():
    
    def __init__(self, set_id):
        self.set_id = set_id
        self.is_init = False
        
        self.inputs = {}
        self.outputs = {}
        self.rules = []
    
    
        self.ctl_sys = None
        self.ctl_sys_sim = None
    
    def __float__(self):
        return float(self.set_id)
    
    def simulate(self):
        self.ctl_sys = ctrl.ControlSystem(self.rules)
        
        self.ctl_sys_sim = ctrl.ControlSystemSimulation((self.ctl_sys))
        
        return self.ctl_sys_sim
    
    def get_inputs(self):
        return self.inputs
        
    def get_outputs(self):
        return self.outputs
    
    def get_rules(self):
        return self.rules
    
    def get_output_membership(self):
        outps = []
        # loop through the defined outputs (consequents)
        for k in self.outputs.keys():
            # each output has its own membership function - interpolate membership for each part of it and output a list 
            out = [fuzz.interp_membership(self.outputs[k].universe, self.outputs[k][o].mf, self.ctl_sys_sim.output[k]) for o in self.outputs[k].terms.keys()]
            
            # take the argmax of list of interpolated mf - argmax is the selected action for this output
            outps.append(np.argmax(out))
        
        return outps
    
class RadioPwrCtlFuzzySet(FuzzySet):
    
    def __init__(self):
        self.inputs = {'snr_802154':    None,
                       'snr_80211':     None,
                       'tx_pow_802154': None,
                       'tx_pow_80211':  None
                      }
    
        self.outputs = {'tx_pow_ctl_802154': None,
                        'tx_pow_ctl_80211': None
                       }
        
        self.rules = []
        
        
        self.setup_inputs()
        self.setup_outputs()
        self.setup_rules()
    
        self.is_init = True
        
    def setup_inputs(self):
        
        self.inputs['snr_802154']            = ctrl.Antecedent(np.arange(-20,100,1), 'snr_802154')
        self.inputs['snr_80211']             = ctrl.Antecedent(np.arange(-20,100,1), 'snr_80211')
        
        self.inputs['tx_pow_802154']         = ctrl.Antecedent(np.arange(0,7,1), 'tx_pow_802154')
        self.inputs['tx_pow_80211']           = ctrl.Antecedent(np.arange(0,7,1), 'tx_pow_80211')

        self.inputs['snr_802154']['none'] = fuzz.trapmf(self.inputs['snr_802154'].universe, [-40, -40, 3.65, 3.65])
        self.inputs['snr_802154']['low'] = fuzz.trapmf(self.inputs['snr_802154'].universe, [0, 3.65, 5, 7])
        self.inputs['snr_802154']['low_med'] = fuzz.trimf(self.inputs['snr_802154'].universe, [4, 7, 10])
        self.inputs['snr_802154']['med'] = fuzz.trimf(self.inputs['snr_802154'].universe, [8, 13, 18])
        self.inputs['snr_802154']['med_high'] = fuzz.trimf(self.inputs['snr_802154'].universe, [15, 20, 25])
        self.inputs['snr_802154']['high'] = fuzz.trapmf(self.inputs['snr_802154'].universe, [22, 27, 100, 100])
        
        '''
        self.inputs['snr_80211']['none'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [-40, -40, 3.65, 3.65])
        self.inputs['snr_80211']['low'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [0, 3.65, 5, 7])
        self.inputs['snr_80211']['low_med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [4, 7, 10])
        self.inputs['snr_80211']['med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [8, 13, 18])
        self.inputs['snr_80211']['med_high'] = fuzz.trimf(self.inputs['snr_80211'].universe, [15, 20, 25])
        self.inputs['snr_80211']['high'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [22, 27, 100, 100])
        '''
        self.inputs['snr_80211']['none'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [-40, -40, 10, 10])
        self.inputs['snr_80211']['low'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [10, 10, 12, 14])
        self.inputs['snr_80211']['low_med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [11, 14, 17])
        self.inputs['snr_80211']['med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [15, 20, 25])
        self.inputs['snr_80211']['med_high'] = fuzz.trimf(self.inputs['snr_80211'].universe, [22, 27, 32])
        self.inputs['snr_80211']['high'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [29, 34, 100, 100])
        
        
        self.inputs['tx_pow_802154']['off'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [-0.5,0,0.5])
        self.inputs['tx_pow_802154']['1'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [0.5,1,1.5])
        self.inputs['tx_pow_802154']['2'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [1.5,2,2.5])
        self.inputs['tx_pow_802154']['3'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [2.5,3,3.5])
        self.inputs['tx_pow_802154']['4'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [3.5,4,4.5])
        self.inputs['tx_pow_802154']['5'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [4.5,5,5.5])
        
        
        self.inputs['tx_pow_80211']['off'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [-0.5,0,0.5])
        self.inputs['tx_pow_80211']['1'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [0.5,1,1.5])
        self.inputs['tx_pow_80211']['2'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [1.5,2,2.5])
        self.inputs['tx_pow_80211']['3'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [2.5,3,3.5])
        self.inputs['tx_pow_80211']['4'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [3.5,4,4.5])
        self.inputs['tx_pow_80211']['5'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [4.5,5,5.5])


    def setup_outputs(self):
        self.outputs['tx_pow_ctl_802154']    = ctrl.Consequent(np.arange(0,4.5,0.5), 'tx_pow_ctl_802154', defuzzify_method='bisector')
        self.outputs['tx_pow_ctl_80211']     = ctrl.Consequent(np.arange(0,4.5,0.5), 'tx_pow_ctl_80211', defuzzify_method='bisector')
        
        self.outputs['tx_pow_ctl_802154']['off'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [0,0.5,1])
        self.outputs['tx_pow_ctl_802154']['inc'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [1,1.5,2])
        self.outputs['tx_pow_ctl_802154']['dec'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [2,2.5,3])
        self.outputs['tx_pow_ctl_802154']['nc'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [3,3.5,4])
        
        
        self.outputs['tx_pow_ctl_80211']['off'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [0,0.5,1])
        self.outputs['tx_pow_ctl_80211']['inc'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [1,1.5,2])
        self.outputs['tx_pow_ctl_80211']['dec'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [2,2.5,3])
        self.outputs['tx_pow_ctl_80211']['nc'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [3,3.5,4])
    
    def setup_rules(self):
        # 802.15.4 rules
        rule1 = ctrl.Rule(self.inputs['snr_802154']['none'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc']) # covers first column of SNR/P_tx rules
        rule2 = ctrl.Rule(self.inputs['snr_802154']['none'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['off']) # covers first column of SNR/P_tx rules
        
        rule3 = ctrl.Rule(self.inputs['snr_802154']['low'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule4 = ctrl.Rule(self.inputs['snr_802154']['low'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['nc'])
        
        rule5 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (~self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule6 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        
        rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        rule10 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule11 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule12 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        rule13 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['off'])
        rule14 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule15 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        # 802.11 rules
        rule16 = ctrl.Rule(self.inputs['snr_80211']['none'] & (~self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['inc']) # covers first column of SNR/P_tx rules
        rule17 = ctrl.Rule(self.inputs['snr_80211']['none'] & (self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['off']) # covers first column of SNR/P_tx rules
        
        rule18 = ctrl.Rule(self.inputs['snr_80211']['low'] & (~self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule19 = ctrl.Rule(self.inputs['snr_80211']['low'] & (self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['nc'])
        
        rule20 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (~self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule21 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        
        rule22 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule23 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule24 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule25 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule26 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule27 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule28 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['off'])
        rule29 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule30 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
    
        self.rules = [rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10, 
                    rule11, rule12, rule13, rule14, rule15, rule16, rule17, rule18, rule19, rule20,
                    rule21, rule22, rule23, rule24, rule25, rule26, rule27, rule28, rule29, rule30
                   ]

class MultiRadioFuzzySet(FuzzySet):
    
    def __init__(self, set_id, rulebase='balanced'):
        super().__init__(set_id)
        self.inputs = {'snr_802154':    None,
                       'snr_80211':     None,
                       'tx_pow_802154': None,
                       'tx_pow_80211':  None
                      }
    
        self.outputs = {'tx_pow_ctl_802154': None,
                        'tx_pow_ctl_80211': None
                       }
        
        self.rules = []
        
        
        self.setup_inputs()
        self.setup_outputs()
        
        if rulebase == 'balanced':
            self.setup_rules()
        elif rulebase == 'power':
            self.setup_min_pwr_rules()
        elif rulebase == 'bitrate':
            self.setup_max_br_rules()
    
        self.is_init = True
    
    def build_snr_mf(self, input_var, min_snr, max_snr, cutoff, overlap, width):
        dist = width-overlap
        low = cutoff + dist
        low_med = low + 2*dist
        med = low_med + 2*dist
        med_high = med + 2*dist
        high = med_high + 2*dist
        
        
        #print('low', [low-width, low, low+width])
        #print('low_med', [low_med-width, low_med, low_med+width])
        
        input_var['none'] = fuzz.trapmf(input_var.universe, [min_snr, min_snr, cutoff, cutoff])
        input_var['low'] = fuzz.trimf(input_var.universe, [low-width, low, low+width])
        input_var['low_med'] = fuzz.trimf(input_var.universe, [low_med-width, low_med, low_med+width])
        input_var['med'] = fuzz.trimf(input_var.universe, [med-width, med, med+width])
        input_var['med_high'] = fuzz.trimf(input_var.universe, [med_high-width, med_high, med_high+width])
        input_var['high'] = fuzz.trapmf(input_var.universe, [high-width, high, max_snr, max_snr])
      
    def setup_inputs(self):
        
        self.inputs['snr_802154']            = ctrl.Antecedent(np.arange(-20,150,0.01), 'snr_802154')
        self.inputs['snr_80211']             = ctrl.Antecedent(np.arange(-20,150,0.01), 'snr_80211')
        
        self.inputs['tx_pow_802154']         = ctrl.Antecedent(np.arange(0,7,0.01), 'tx_pow_802154')
        self.inputs['tx_pow_80211']           = ctrl.Antecedent(np.arange(0,7,0.01), 'tx_pow_80211')

        min_802154 = -40
        max_802154 = 150
        cutoff_802154 = 3.65
        overlap_802154 = 1
        width_802154 = 5
        
        #cutoff_802154 = 0
        #overlap_802154 = 4
        #width_802154 = 5
        
        
        #arbitrary set
        # cutoff = 0
        # overlap = 4
        # width = 5
        
        
        min_80211 = -40
        max_80211 = 150
        cutoff_80211 = -0.5
        overlap_80211 = 2
        width_80211 = 5
        
        #cutoff_80211 = 0
        #overlap_80211 = 4
        #width_80211 = 5

        self.inputs['snr_802154']            = ctrl.Antecedent(np.arange(-20,150,0.01), 'snr_802154')
        self.inputs['snr_80211']             = ctrl.Antecedent(np.arange(-20,150,0.01), 'snr_80211')
        
        self.inputs['tx_pow_802154']         = ctrl.Antecedent(np.arange(0,7,1), 'tx_pow_802154')
        self.inputs['tx_pow_80211']           = ctrl.Antecedent(np.arange(0,7,1), 'tx_pow_80211')

        self.build_snr_mf(self.inputs['snr_802154'], min_802154, max_802154, cutoff_802154, overlap_802154, width_802154)
        self.build_snr_mf(self.inputs['snr_80211'], min_80211, max_80211, cutoff_80211, overlap_80211, width_80211)
        '''
        self.inputs['snr_802154']['none'] = fuzz.trapmf(self.inputs['snr_802154'].universe, [-40, -40, 3.65, 3.65])
        self.inputs['snr_802154']['low'] = fuzz.trapmf(self.inputs['snr_802154'].universe, [0, 3.65, 5, 7])
        self.inputs['snr_802154']['low_med'] = fuzz.trimf(self.inputs['snr_802154'].universe, [4, 7, 10])
        self.inputs['snr_802154']['med'] = fuzz.trimf(self.inputs['snr_802154'].universe, [8, 13, 18])
        self.inputs['snr_802154']['med_high'] = fuzz.trimf(self.inputs['snr_802154'].universe, [15, 20, 25])
        self.inputs['snr_802154']['high'] = fuzz.trapmf(self.inputs['snr_802154'].universe, [22, 27, 100, 100])
        
        
        self.inputs['snr_80211']['none'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [-40, -40, 3.65, 3.65])
        self.inputs['snr_80211']['low'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [0, 3.65, 5, 7])
        self.inputs['snr_80211']['low_med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [4, 7, 10])
        self.inputs['snr_80211']['med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [8, 13, 18])
        self.inputs['snr_80211']['med_high'] = fuzz.trimf(self.inputs['snr_80211'].universe, [15, 20, 25])
        self.inputs['snr_80211']['high'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [22, 27, 100, 100])
        
        self.inputs['snr_80211']['none'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [-40, -40, 10, 10])
        self.inputs['snr_80211']['low'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [7, 10, 12, 14])
        self.inputs['snr_80211']['low_med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [11, 14, 17])
        self.inputs['snr_80211']['med'] = fuzz.trimf(self.inputs['snr_80211'].universe, [15, 20, 25])
        self.inputs['snr_80211']['med_high'] = fuzz.trimf(self.inputs['snr_80211'].universe, [22, 27, 32])
        self.inputs['snr_80211']['high'] = fuzz.trapmf(self.inputs['snr_80211'].universe, [29, 34, 100, 100])
        '''
        
        self.inputs['tx_pow_802154']['off'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [-0.5,0,0.5])
        self.inputs['tx_pow_802154']['1'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [0.5,1,1.5])
        self.inputs['tx_pow_802154']['2'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [1.5,2,2.5])
        self.inputs['tx_pow_802154']['3'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [2.5,3,3.5])
        self.inputs['tx_pow_802154']['4'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [3.5,4,4.5])
        self.inputs['tx_pow_802154']['5'] = fuzz.trimf(self.inputs['tx_pow_802154'].universe, [4.5,5,5.5])
        
        
        self.inputs['tx_pow_80211']['off'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [-0.5,0,0.5])
        self.inputs['tx_pow_80211']['1'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [0.5,1,1.5])
        self.inputs['tx_pow_80211']['2'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [1.5,2,2.5])
        self.inputs['tx_pow_80211']['3'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [2.5,3,3.5])
        self.inputs['tx_pow_80211']['4'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [3.5,4,4.5])
        self.inputs['tx_pow_80211']['5'] = fuzz.trimf(self.inputs['tx_pow_80211'].universe, [4.5,5,5.5])


    def setup_outputs(self):
        self.outputs['tx_pow_ctl_802154']    = ctrl.Consequent(np.arange(0,6,0.5), 'tx_pow_ctl_802154', defuzzify_method='bisector')
        self.outputs['tx_pow_ctl_80211']     = ctrl.Consequent(np.arange(0,6,0.5), 'tx_pow_ctl_80211', defuzzify_method='bisector')
        
        self.outputs['tx_pow_ctl_802154']['off'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [-0.5,0.5,1.5])
        self.outputs['tx_pow_ctl_802154']['inc'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [0.5,1.5,2.5])
        self.outputs['tx_pow_ctl_802154']['dec'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [1.5,2.5,3.5])
        self.outputs['tx_pow_ctl_802154']['nc'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [2.5,3.5,4.5])
        self.outputs['tx_pow_ctl_802154']['sw'] = fuzz.trimf(self.outputs['tx_pow_ctl_802154'].universe, [3.5,4.5,5.5])
        
        self.outputs['tx_pow_ctl_80211']['off'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [-0.5,0.5,1.5])
        self.outputs['tx_pow_ctl_80211']['inc'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [0.5,1.5,2.5])
        self.outputs['tx_pow_ctl_80211']['dec'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [1.5,2.5,3.5])
        self.outputs['tx_pow_ctl_80211']['nc'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [2.5,3.5,4.5])
        self.outputs['tx_pow_ctl_80211']['sw'] = fuzz.trimf(self.outputs['tx_pow_ctl_80211'].universe, [3.5,4.5,5.5])
        
    def setup_rules(self):
        '''
        # 802.15.4 rules
        rule1 = ctrl.Rule(self.inputs['snr_802154']['none'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc']) # covers first column of SNR/P_tx rules
        rule2 = ctrl.Rule(self.inputs['snr_802154']['none'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['off']) # covers first column of SNR/P_tx rules
        
        rule3 = ctrl.Rule(self.inputs['snr_802154']['low'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule4 = ctrl.Rule(self.inputs['snr_802154']['low'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['nc'])
        
        rule5 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (~self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule6 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        
        '''
        #rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        #rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        #rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        '''
        rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1'] | self.inputs['tx_pow_802154']['2']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
            
        rule10 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule11 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule12 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        rule13 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['off'])
        rule14 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule15 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        # 802.11 rules
        rule16 = ctrl.Rule(self.inputs['snr_80211']['none'] & (~self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['inc']) # covers first column of SNR/P_tx rules
        rule17 = ctrl.Rule(self.inputs['snr_80211']['none'] & (self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['sw']) # covers first column of SNR/P_tx rules
        
        rule18 = ctrl.Rule(self.inputs['snr_80211']['low'] & (~self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule19 = ctrl.Rule(self.inputs['snr_80211']['low'] & (self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['nc'])
        
        rule20 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (~self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule21 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        
        rule22 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule23 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule24 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule25 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule26 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule27 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule28 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['off'])
        rule29 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule30 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        '''
        # 802.15.4 rules
        rule1 = ctrl.Rule(self.inputs['snr_802154']['none'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc']) # covers first column of SNR/P_tx rules
        rule2 = ctrl.Rule(self.inputs['snr_802154']['none'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['off']) # covers first column of SNR/P_tx rules
        
        rule3 = ctrl.Rule(self.inputs['snr_802154']['low'] & (~self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule4 = ctrl.Rule(self.inputs['snr_802154']['low'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        
        rule5 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (~self.inputs['tx_pow_802154']['off'] & ~self.inputs['tx_pow_802154']['1'] & ~self.inputs['tx_pow_802154']['2']), self.outputs['tx_pow_ctl_802154']['dec'])
        rule6 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule31 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['1'] | self.inputs['tx_pow_802154']['2']), self.outputs['tx_pow_ctl_802154']['sw'])
        '''
        #rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        #rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        #rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        '''
        rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] |self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
            
        rule10 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule11 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule12 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        rule13 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['off'])
        rule14 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule15 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        # 802.11 rules
        rule16 = ctrl.Rule(self.inputs['snr_80211']['none'] & (~self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['inc']) # covers first column of SNR/P_tx rules
        rule17 = ctrl.Rule(self.inputs['snr_80211']['none'] & (self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['sw']) # covers first column of SNR/P_tx rules
        
        rule18 = ctrl.Rule(self.inputs['snr_80211']['low'] & (~self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule19 = ctrl.Rule(self.inputs['snr_80211']['low'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        
        rule20 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule21 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule32 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule22 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule23 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule24 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule25 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule26 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule27 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule28 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['off'])
        rule29 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule30 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        self.rules = [rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10, 
                    rule11, rule12, rule13, rule14, rule15, rule16, rule17, rule18, rule19, rule20,
                    rule21, rule22, rule23, rule24, rule25, rule26, rule27, rule28, rule29, rule30,
                    rule31, rule32
                   ]
        
    def setup_min_pwr_rules(self):
        # 802.15.4
        rule1 = ctrl.Rule(self.inputs['snr_802154']['none'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc']) # covers first column of SNR/P_tx rules
        rule2 = ctrl.Rule(self.inputs['snr_802154']['none'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['off']) # covers first column of SNR/P_tx rules
        
        rule3 = ctrl.Rule(self.inputs['snr_802154']['low'] & (~self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule4 = ctrl.Rule(self.inputs['snr_802154']['low'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        
        rule5 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (~self.inputs['tx_pow_802154']['off'] & ~self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['dec'])
        rule6 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule31 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        
        rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] |self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
            
        rule10 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule11 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule12 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        rule13 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['off'])
        rule14 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule15 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        # 802.11 rules
        rule16 = ctrl.Rule(self.inputs['snr_80211']['none'] & (~self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['sw']) # covers first column of SNR/P_tx rules
        rule17 = ctrl.Rule(self.inputs['snr_80211']['none'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc']) # covers first column of SNR/P_tx rules
        
        rule18 = ctrl.Rule(self.inputs['snr_80211']['low'] & (~self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['sw'])
        rule19 = ctrl.Rule(self.inputs['snr_80211']['low'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        
        rule20 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule21 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['sw'])
        rule32 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule22 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule23 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['sw'])
        rule24 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule25 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule26 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['sw'])
        rule27 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule28 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['off']), self.outputs['tx_pow_ctl_80211']['off'])
        rule29 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['sw'])
        rule30 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['2'] | self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        self.rules = [rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10, 
                rule11, rule12, rule13, rule14, rule15, rule16, rule17, rule18, rule19, rule20,
                rule21, rule22, rule23, rule24, rule25, rule26, rule27, rule28, rule29, rule30,
                rule31, rule32
               ]
            
    def setup_max_br_rules(self):
        # 802.15.4 rules
        rule1 = ctrl.Rule(self.inputs['snr_802154']['none'] & (~self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['inc']) # covers first column of SNR/P_tx rules
        rule2 = ctrl.Rule(self.inputs['snr_802154']['none'] & (self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['off']) # covers first column of SNR/P_tx rules
        
        rule3 = ctrl.Rule(self.inputs['snr_802154']['low'] & (~self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['nc'])
        rule4 = ctrl.Rule(self.inputs['snr_802154']['low'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        
        rule5 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (~self.inputs['tx_pow_802154']['off'] & ~self.inputs['tx_pow_802154']['1'] & ~self.inputs['tx_pow_802154']['2']), self.outputs['tx_pow_ctl_802154']['dec'])
        rule6 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule31 = ctrl.Rule(self.inputs['snr_802154']['low_med'] & (self.inputs['tx_pow_802154']['1'] | self.inputs['tx_pow_802154']['2']), self.outputs['tx_pow_ctl_802154']['sw'])
        '''
        #rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        #rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        #rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        '''
        rule7 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule8 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule9 = ctrl.Rule(self.inputs['snr_802154']['med'] & (self.inputs['tx_pow_802154']['2'] |self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
            
        rule10 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['inc'])
        rule11 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule12 = ctrl.Rule(self.inputs['snr_802154']['med_high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        rule13 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['off']), self.outputs['tx_pow_ctl_802154']['off'])
        rule14 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['1']), self.outputs['tx_pow_ctl_802154']['sw'])
        rule15 = ctrl.Rule(self.inputs['snr_802154']['high'] & (self.inputs['tx_pow_802154']['2'] | self.inputs['tx_pow_802154']['3'] | self.inputs['tx_pow_802154']['4'] | self.inputs['tx_pow_802154']['5']), self.outputs['tx_pow_ctl_802154']['dec'])
        
        # 802.11 rules
        rule16 = ctrl.Rule(self.inputs['snr_80211']['none'] & (~self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['inc']) # covers first column of SNR/P_tx rules
        rule17 = ctrl.Rule(self.inputs['snr_80211']['none'] & (self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['sw']) # covers first column of SNR/P_tx rules
        
        rule18 = ctrl.Rule(self.inputs['snr_80211']['low'] & (~self.inputs['tx_pow_80211']['off'] & ~self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule19 = ctrl.Rule(self.inputs['snr_80211']['low'] & (self.inputs['tx_pow_80211']['off'] | self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['inc'])
        
        rule20 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['off'] | self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule21 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['2']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule32 = ctrl.Rule(self.inputs['snr_80211']['low_med'] & (self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule22 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['off'] | self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule23 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['2']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule24 = ctrl.Rule(self.inputs['snr_80211']['med'] & (self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule25 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['off'] | self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['inc'])
        rule26 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['2']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule27 = ctrl.Rule(self.inputs['snr_80211']['med_high'] & (self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        rule28 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['off'] | self.inputs['tx_pow_80211']['1']), self.outputs['tx_pow_ctl_80211']['off'])
        rule29 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['2']), self.outputs['tx_pow_ctl_80211']['nc'])
        rule30 = ctrl.Rule(self.inputs['snr_80211']['high'] & (self.inputs['tx_pow_80211']['3'] | self.inputs['tx_pow_80211']['4'] | self.inputs['tx_pow_80211']['5']), self.outputs['tx_pow_ctl_80211']['dec'])
        
        self.rules = [rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10, 
                    rule11, rule12, rule13, rule14, rule15, rule16, rule17, rule18, rule19, rule20,
                    rule21, rule22, rule23, rule24, rule25, rule26, rule27, rule28, rule29, rule30,
                    rule31, rule32
                   ]