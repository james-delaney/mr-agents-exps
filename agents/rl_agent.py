import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly, ControlActions
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv

def exp_td_err(lr, td_err, sigma):
    num  = -1 * np.abs(lr * td_err)
    
    return np.exp(num/sigma)

def func_eps(lr, td_err, sigma):
    e = exp_td_err(lr, td_err, sigma)

    return ((1 - e)/(1 + e))


class RLAgent():
    
    DECAY_EPS       = 0
    ADPTV_EPS       = 1
    
    ALG_QL          = 0
    ALG_SARSA       = 1
    ALG_EXP_SARSA   = 2
    
    def __init__(self, env):
        self.alpha = 0
        self.gamma = 0
        self.epsilon = 0
        
        self.q = None
        
        self.rl_alg = 0
        self.epsilon_schedule = 1
        self.rand_expl = False
        
        self.env = env
        self.states_obj = None
        
        self.action_space = self.env.action_space
        self.action_enum  = self.env.action_enum
        self.n_actions = len(self.action_enum)
        
        self.is_init = False
        
        self.env.recorder.add_data_point('td_err')
        
    def cfg_agent(self, rl_alg, state_space, states_obj, alpha, gamma, epsilon_schedule, rand_expl, policy):
        self.rl_alg = rl_alg
        self.state_space = state_space
        self.states_obj = states_obj
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon_schedule = epsilon_schedule
        self.rand_expl = rand_expl   
        self.policy = policy
        
        self.state_table_size = len(self.state_space)
        
    def _init_agent(self):
        self.q = np.zeros([self.state_table_size, self.env.action_space.n])
        
        if (self.epsilon_schedule == self.DECAY_EPS):
            self.epsilon = 0.95
        else:
            self.epsilon = np.ones(self.state_table_size)
            
        self.is_init = True

    def simulate_run(self, re_init=False):
        # initialise agent if not already done or if we should re-init every run
        if self.is_init and re_init:
            self._init_agent()
        elif not self.is_init:
            self._init_agent()
            
        
        s = self.env.reset()
        s = 0
        done = False
        #r_sum = np.zeros(self.num_components, dtype=float)
        #avg_reward = np.zeros(self.num_components, dtype=float)
        n_steps = 0 
        r_bar = 0       # avg reward for r-learning
        dlt = 1/self.n_actions
        sigma = 0.08
        td_err = 0

        avg_td_err = 0
        last_avg_td_err = 0
        last_weight_val = 0 

        a = self.action_space.sample()
        while not done:
            n_steps += 1
            
            '''
            # if we are using regular e-greedy or adaptive e-greedy
            if self.epsilon_schedule == self.DECAY_EPS:
                eps_s = self.epsilon
            else:
                eps_s = self.epsilon[s]
            
            if self.rl_alg == 'q_learning':
                if (np.random.uniform(0, 1) < eps_s):
                    a = self.actions_space.sample()
                else:
                    a = np.argmax(self.q[s])
            '''
            
            obs,r,done,info = self.env.step(list(self.action_enum)[a])
            self.states_obj.update_obs(obs.get_radio_id(), obs)
            s_prime = self.states_obj.get_state(obs.get_radio_id(), a)
            
            a_prime = self.policy.get_action(self.q[s_prime], n_steps, s_prime, td_err)
                
            if self.rl_alg == 'q_learning':
                a_prime = np.random.choice(np.where(self.q[s_prime] == max(self.q[s_prime]))[0])
                td_err = (r + self.gamma*self.q[s_prime,a_prime] - self.q[s,a])
                self.q[s,a] = self.q[s,a] + self.alpha * td_err
                
            elif self.rl_alg == 'sarsa':
                '''
                if (np.random.uniform(0, 1) < eps_s):
                    a_prime = self.action_space.sample()
                else:
                    a_prime = np.argmax(self.q[s_prime])
                '''
                
                td_err = r + self.gamma*self.q[s_prime, a_prime] - self.q[s,a]
                self.q[s,a] = self.q[s,a] + self.alpha * td_err
                
                a = a_prime
            
            elif self.rl_alg == 'expected_sarsa':
                raise Exception('Not implemented')
            else:
                raise Exception('Invalid method provided')
            
            '''
            # epsilon decay or state/td error-based adaptive decay
            if self.epsilon_schedule == self.DECAY_EPS:
                if (self.epsilon < 0.01):
                    self.epsilon = 0.01
                else:
                    self.epsilon = 0.5**n_steps
                    #epsilon = 0.1
            else:
                tde = td_err
                self.epsilon[s] = dlt * func_eps(self.alpha, tde, sigma) + (1 - dlt) * self.epsilon[s]
                #if (n_steps > 12500):
                #    print(n_steps, self.epsilon[s])
            
            '''
            s = s_prime
            
            self.env.recorder.record_data('td_err', td_err)
            
            if n_steps % 1000 == 0:
                if self.rand_expl == True:
                    if (self.epsilon_schedule == self.ADPTV_EPS):
                        for s in range(len(self.epsilon)):
                            self.epsilon[s] = 0.5
                    else:
                        self.epsilon = 0.95
            
        return done