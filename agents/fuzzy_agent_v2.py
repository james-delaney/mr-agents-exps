import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]

#import gym

import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
import gym_grid_wireless.envs
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw
from gym_grid_wireless.envs.path_power_wireless_env import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States
from gym_grid_wireless.multi_radio_actions import Actions, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly, ControlActions
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import sys
from os import path

TIME_STEP_INTERVAL = 0.2
PATH_LEN = 20000

#FILE_DIR = 'gym_grid_wireless/envs/'#os.path.dirname(__file__) + '/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'


RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)
                Radio(1, 2400000000, 20000000, -97, 11000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]


#RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [7,15,18,21,24], [4,27,54,107,215], 'psk', 4), #Radio(0, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]

#RADIO_CFG = [   Radio(0, 2400, -84, 1000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240]),
#               Radio(1, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215])]
                


#RADIO_CFG = [   Radio(0, 915, -101, 200, [7,7,7,7,7], [4,4,4,4,4]), #Radio(0, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400, -84, 1000, [5,5,5,5,5], [7.8,7.82,7.82,7.82,7.82])]


DST_NODE_X = 100
DST_NODE_Y = 100

'''
PATH_FNAME = 'm_7500.csv'#'center_20000.csv'
ENV_FNAME = 'polys_n_3.2_4.5.csv'#'polys_c_2.0_3.5.csv'
PLE_FNAME = 'PLE_m_7500_polys_n_3.2_4.5.csv'#'PLE_center_20000_polys_c_2.0_3.5.csv'
SHD_FNAME = 'SHD_m_7500_polys_n_3.2_4.5.csv'
'''

PATH_FNAME = 'q_20000.csv'
ENV_FNAME = 'polys_q_3.5_5.csv'
PLE_FNAME = 'PLE_q_20000_polys_q_3.5_5.csv'
SHD_FNAME = 'SHD_q_20000_polys_q_3.5_5.csv'

'''
PATH_FNAME = 'q_20000.csv'#'center_20000.csv'
ENV_FNAME = 'polys_q_3.0_4.5.csv'#'polys_c_2.0_3.5.csv'
PLE_FNAME = 'PLE_q_20000_polys_q_3.0_4.5.csv'#'PLE_center_20000_polys_c_2.0_3.5.csv'
SHD_FNAME = 'SHD_q_20000_polys_q_3.0_4.5.csv'
'''

#env = gym.make("grid-wireless-v0", update_interval=0.5, god_agent=False)
#env = gym.make("linear-wireless-v0", update_interval=0.5, god_agent=False)
#env = gym.make("path-power-wireless-v2", update_interval=TIME_STEP_INTERVAL, radio_cfg=RADIO_CFG, agent_path_fname=PATH_FNAME, env_fname=ENV_FNAME, ple_fname=PLE_FNAME, shd_fname=SHD_FNAME, god_agent=True)
#env = gym.make("path-power-wireless-v0", update_interval=0.5, radio_cfg=RADIO_CFG, agent_path_fname='a_1.csv', env_fname='polys_1.5_4.5.csv', god_agent=False)

#state = env.reset()
#renderer = env.render('human')


NUM_RUNS = 1

def plt_env_path(radio_use_trace, outage_trace):
    env_polygons = []
    env_PLE = []
    path_points = []
    path_count = 0
    path_len = 0
    env_PLE = []
    PLE_points = []
    path_points_x = []
    path_points_y = []

    print('loading path')
    with open(FILE_DIR+PATH_FNAME, 'r', newline='') as file:
        lines = csv.reader(file, delimiter=',')
        for line in lines:
            #print([int(line[0]), int(line[1])])
            path_points.append([int(line[0]), int(line[1])])
            path_points_x.append(int(line[0]))
            path_points_y.append(int(line[1]))
    
    
    print('loading polygons')
    with open(FILE_DIR+ENV_FNAME, 'r') as file:
        lines = csv.reader(file, delimiter=',')
        env = next(lines)
        
        line=next(lines)
        while line:
        #for line in lines:
            p = []
            
            for i in range(1, len(line), 2):
                p.append([float(line[i]), float(line[i+1])])
            poly  = Polygon(p)
            
            env_polygons.append(poly)
            env_PLE.append(float(line[0]))

            line = next(lines, None)
        
        # Comment out plot of environment and path
        
        fig = plt.figure(1)
        fig.suptitle('Mobile Node Trajectory and Adaptive Radio Selection\nFuzzy Power Control')
        gs = gridspec.GridSpec(16, 10)

        ax = plt.subplot2grid((16,20), (0,17), colspan=1, rowspan=16)
        ax2 = plt.subplot2grid((16,20), (0,0), colspan=16, rowspan=16)
    
        norm = mpl.colors.Normalize(vmin=2.0, vmax=6.0, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in env_polygons:
            ax2.fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(env_PLE[env_polygons.index(p)]))


        cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})



        #ax2.plot(path_points_x, path_points_y, 'r', label="Mobile node path")
        
        r0_points_x = []
        r0_points_y = []
        r1_points_x = []
        r1_points_y = []
        nr_points_x = []
        nr_points_y = []

        r0_dn_points_x = []
        r0_dn_points_y = []
        r1_dn_points_x = []
        r1_dn_points_y = []
        
        for i in range(0, PATH_LEN-1):
            #if i % 1000 == 0:
            #    print("count: " + str(i)) 
            if radio_use_trace[i] == 0:
                #print("num " + str(i) + "r0")
                r0_points_x.append(path_points_x[i])
                r0_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='y')
            elif radio_use_trace[i] == 1:
                #print("num " + str(i) + "r1")
                r1_points_x.append(path_points_x[i])
                r1_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='c')
            else:
                #print("num " + str(i) + "nr")
                nr_points_x.append(path_points_x[i])
                nr_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='r')

            if outage_trace[i] == 1:
                r0_dn_points_x.append(path_points_x[i])
                r0_dn_points_y.append(path_points_y[i])
            elif outage_trace[i] == 2:
                r1_dn_points_x.append(path_points_x[i])
                r1_dn_points_y.append(path_points_y[i])
        
        print(len(r0_dn_points_x), len(r1_dn_points_x))
        #print(radio_use_trace)

        ax2.plot(r0_points_x, r0_points_y, 'om', markersize=1, c='m', label='802.15.4 Radio')
        ax2.plot(r1_points_x, r1_points_y, 'oc', markersize=1, c='c', label='802.11 Radio')
        
        ax2.plot(r0_dn_points_x, r0_dn_points_y, 'ob', markersize=1, c='b', label='802.15.4 Radio down')
        ax2.plot(r1_dn_points_x, r1_dn_points_y, 'og', markersize=1, c='g', label='802.11 Radio down')
        ax2.plot(nr_points_x, nr_points_y, 'or', markersize=1, c='r', label='Radio off')

        ax2.plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")

        ax2.set_xlabel('X coordinate, m')
        ax2.set_ylabel('Y coordinate, m')
        #ax2.set_xticks([])
        #ax2.set_yticks([])
        lg = ax2.legend(prop={'size':8}, loc=4)
        lg.legendHandles[0]._legmarker.set_markersize(5)
        lg.legendHandles[1]._legmarker.set_markersize(5)
        lg.legendHandles[2]._legmarker.set_markersize(5)
        lg.legendHandles[3]._legmarker.set_markersize(5)
        lg.legendHandles[4]._legmarker.set_markersize(5)
        lg.legendHandles[5]._legmarker.set_markersize(5)
        plt.xlim(0, 500)
        plt.ylim(0, 500)
        
        plt.axis([0,500,0,500])

class FuzzyMultiRadioAgent():

    def __init__(self):
        self.init_fuzzy_set()

    def init_fuzzy_set(self):
        sig_qual_802154 = ctrl.Antecedent(np.arange(-20,100,1), 'snr_802154')
        sig_qual_80211 = ctrl.Antecedent(np.arange(-20,100,1), 'snr_80211')
        
        sig_qual_802154['none'] = fuzz.trapmf(sig_qual_802154.universe, [-40, -40, 3.65, 3.65])
        sig_qual_802154['low'] = fuzz.trapmf(sig_qual_802154.universe, [0, 3.65, 5, 7])
        sig_qual_802154['low_med'] = fuzz.trimf(sig_qual_802154.universe, [4, 7, 10])
        sig_qual_802154['med'] = fuzz.trimf(sig_qual_802154.universe, [8, 13, 18])
        sig_qual_802154['med_high'] = fuzz.trimf(sig_qual_802154.universe, [15, 20, 25])
        sig_qual_802154['high'] = fuzz.trapmf(sig_qual_802154.universe, [22, 27, 100, 100])
        
        '''
        sig_qual_80211['none'] = fuzz.trapmf(sig_qual_80211.universe, [-40, -40, 3.65, 3.65])
        sig_qual_80211['low'] = fuzz.trapmf(sig_qual_80211.universe, [0, 3.65, 5, 7])
        sig_qual_80211['low_med'] = fuzz.trimf(sig_qual_80211.universe, [4, 7, 10])
        sig_qual_80211['med'] = fuzz.trimf(sig_qual_80211.universe, [8, 13, 18])
        sig_qual_80211['med_high'] = fuzz.trimf(sig_qual_80211.universe, [15, 20, 25])
        sig_qual_80211['high'] = fuzz.trapmf(sig_qual_80211.universe, [22, 27, 100, 100])
        '''
        sig_qual_80211['none'] = fuzz.trapmf(sig_qual_80211.universe, [-40, -40, 10, 10])
        sig_qual_80211['low'] = fuzz.trapmf(sig_qual_80211.universe, [7, 10, 12, 14])
        sig_qual_80211['low_med'] = fuzz.trimf(sig_qual_80211.universe, [11, 14, 17])
        sig_qual_80211['med'] = fuzz.trimf(sig_qual_80211.universe, [15, 20, 25])
        sig_qual_80211['med_high'] = fuzz.trimf(sig_qual_80211.universe, [22, 27, 32])
        sig_qual_80211['high'] = fuzz.trapmf(sig_qual_80211.universe, [29, 34, 100, 100])
        
        
        tx_pow_802154 = ctrl.Antecedent(np.arange(0,7,1), 'tx_pow_802154')
        tx_pow_80211 = ctrl.Antecedent(np.arange(0,7,1), 'tx_pow_80211')
        
        tx_pow_802154['off'] = fuzz.trimf(tx_pow_802154.universe, [-0.5,0,0.5])
        tx_pow_802154['1'] = fuzz.trimf(tx_pow_802154.universe, [0.5,1,1.5])
        tx_pow_802154['2'] = fuzz.trimf(tx_pow_802154.universe, [1.5,2,2.5])
        tx_pow_802154['3'] = fuzz.trimf(tx_pow_802154.universe, [2.5,3,3.5])
        tx_pow_802154['4'] = fuzz.trimf(tx_pow_802154.universe, [3.5,4,4.5])
        tx_pow_802154['5'] = fuzz.trimf(tx_pow_802154.universe, [4.5,5,5.5])
        
        
        tx_pow_80211['off'] = fuzz.trimf(tx_pow_80211.universe, [-0.5,0,0.5])
        tx_pow_80211['1'] = fuzz.trimf(tx_pow_80211.universe, [0.5,1,1.5])
        tx_pow_80211['2'] = fuzz.trimf(tx_pow_80211.universe, [1.5,2,2.5])
        tx_pow_80211['3'] = fuzz.trimf(tx_pow_80211.universe, [2.5,3,3.5])
        tx_pow_80211['4'] = fuzz.trimf(tx_pow_80211.universe, [3.5,4,4.5])
        tx_pow_80211['5'] = fuzz.trimf(tx_pow_80211.universe, [4.5,5,5.5])
        
        
        self.tx_pow_ctl_802154 = ctrl.Consequent(np.arange(0,4.5,0.5), 'tx_pow_ctl_802154', defuzzify_method='bisector')
        self.tx_pow_ctl_80211 = ctrl.Consequent(np.arange(0,4.5,0.5), 'tx_pow_ctl_80211', defuzzify_method='bisector')
        
        self.tx_pow_ctl_802154['off'] = fuzz.trimf(self.tx_pow_ctl_802154.universe, [0,0.5,1])
        self.tx_pow_ctl_802154['inc'] = fuzz.trimf(self.tx_pow_ctl_802154.universe, [1,1.5,2])
        self.tx_pow_ctl_802154['dec'] = fuzz.trimf(self.tx_pow_ctl_802154.universe, [2,2.5,3])
        self.tx_pow_ctl_802154['nc'] = fuzz.trimf(self.tx_pow_ctl_802154.universe, [3,3.5,4])
        
        
        self.tx_pow_ctl_80211['off'] = fuzz.trimf(self.tx_pow_ctl_80211.universe, [0,0.5,1])
        self.tx_pow_ctl_80211['inc'] = fuzz.trimf(self.tx_pow_ctl_80211.universe, [1,1.5,2])
        self.tx_pow_ctl_80211['dec'] = fuzz.trimf(self.tx_pow_ctl_80211.universe, [2,2.5,3])
        self.tx_pow_ctl_80211['nc'] = fuzz.trimf(self.tx_pow_ctl_80211.universe, [3,3.5,4])
        
        radio_sel_802154 = ctrl.Consequent(np.arange(0,1.1,0.1), 'radio_802154')
        radio_sel_80211 = ctrl.Consequent(np.arange(0,1.1,0.1), 'radio_80211')
        
        radio_pwr_lvl_802154 = ctrl.Consequent(np.arange(-0.5,6,0.5), 'pwr_lvl_802154')
        radio_pwr_lvl_80211 = ctrl.Consequent(np.arange(-0.5,6,0.5), 'pwr_lvl_80211')
        
        
        radio_pwr_lvl_802154['off'] = fuzz.trimf(radio_pwr_lvl_802154.universe, [-0.5,0,0.5])
        radio_pwr_lvl_802154['r0_0'] = fuzz.trimf(radio_pwr_lvl_802154.universe, [0.5,1,1.5])
        radio_pwr_lvl_802154['r0_1'] = fuzz.trimf(radio_pwr_lvl_802154.universe, [1.5,2,2.5])
        radio_pwr_lvl_802154['r0_2'] = fuzz.trimf(radio_pwr_lvl_802154.universe, [2.5,3,3.5])
        radio_pwr_lvl_802154['r0_3'] = fuzz.trimf(radio_pwr_lvl_802154.universe, [3.5,4,4.5])
        radio_pwr_lvl_802154['r0_4'] = fuzz.trimf(radio_pwr_lvl_802154.universe, [4.5,5,5.5])
        
        
        radio_pwr_lvl_80211['off'] = fuzz.trimf(radio_pwr_lvl_80211.universe, [-0.5,0,0.5])
        radio_pwr_lvl_80211['r1_0'] = fuzz.trimf(radio_pwr_lvl_80211.universe, [0.5,1,1.5])
        radio_pwr_lvl_80211['r1_1'] = fuzz.trimf(radio_pwr_lvl_80211.universe, [1.5,2,2.5])
        radio_pwr_lvl_80211['r1_2'] = fuzz.trimf(radio_pwr_lvl_80211.universe, [2.5,3,3.5])
        radio_pwr_lvl_80211['r1_3'] = fuzz.trimf(radio_pwr_lvl_80211.universe, [3.5,4,4.5])
        radio_pwr_lvl_80211['r1_4'] = fuzz.trimf(radio_pwr_lvl_80211.universe, [4.5,5,5.5])
        
        radio_pwr_sel = ctrl.Consequent(np.arange(-0.5,11,0.5), 'radio_pwr_sel')
        
        radio_pwr_sel['off'] = fuzz.trimf(radio_pwr_sel.universe, [-0.5,0,0.5])
        radio_pwr_sel['r0_0'] = fuzz.trimf(radio_pwr_sel.universe, [0.5,1,1.5])
        radio_pwr_sel['r0_1'] = fuzz.trimf(radio_pwr_sel.universe, [1.5,2,2.5])
        radio_pwr_sel['r0_2'] = fuzz.trimf(radio_pwr_sel.universe, [2.5,3,3.5])
        radio_pwr_sel['r0_3'] = fuzz.trimf(radio_pwr_sel.universe, [3.5,4,4.5])
        radio_pwr_sel['r0_4'] = fuzz.trimf(radio_pwr_sel.universe, [4.5,5,5.5])
        radio_pwr_sel['r1_0'] = fuzz.trimf(radio_pwr_sel.universe, [5.5,6,6.5])
        radio_pwr_sel['r1_1'] = fuzz.trimf(radio_pwr_sel.universe, [6.5,7,7.5])
        radio_pwr_sel['r1_2'] = fuzz.trimf(radio_pwr_sel.universe, [7.5,8,8.5])
        radio_pwr_sel['r1_3'] = fuzz.trimf(radio_pwr_sel.universe, [8.5,9,9.5])
        radio_pwr_sel['r1_4'] = fuzz.trimf(radio_pwr_sel.universe, [9.5,10,10.5])

        # 802.15.4 rules
        rule1 = ctrl.Rule(sig_qual_802154['none'] & (~tx_pow_802154['5']), self.tx_pow_ctl_802154['inc']) # covers first column of SNR/P_tx rules
        rule2 = ctrl.Rule(sig_qual_802154['none'] & (tx_pow_802154['5']), self.tx_pow_ctl_802154['off']) # covers first column of SNR/P_tx rules
        
        rule3 = ctrl.Rule(sig_qual_802154['low'] & (~tx_pow_802154['5']), self.tx_pow_ctl_802154['inc'])
        rule4 = ctrl.Rule(sig_qual_802154['low'] & (tx_pow_802154['5']), self.tx_pow_ctl_802154['nc'])
        
        rule5 = ctrl.Rule(sig_qual_802154['low_med'] & (~tx_pow_802154['off']), self.tx_pow_ctl_802154['nc'])
        rule6 = ctrl.Rule(sig_qual_802154['low_med'] & (tx_pow_802154['off']), self.tx_pow_ctl_802154['inc'])
        
        rule7 = ctrl.Rule(sig_qual_802154['med'] & (tx_pow_802154['off']), self.tx_pow_ctl_802154['inc'])
        rule8 = ctrl.Rule(sig_qual_802154['med'] & (tx_pow_802154['1']), self.tx_pow_ctl_802154['nc'])
        rule9 = ctrl.Rule(sig_qual_802154['med'] & (tx_pow_802154['2'] | tx_pow_802154['3'] | tx_pow_802154['4'] | tx_pow_802154['5']), self.tx_pow_ctl_802154['dec'])
        
        rule10 = ctrl.Rule(sig_qual_802154['med_high'] & (tx_pow_802154['off']), self.tx_pow_ctl_802154['inc'])
        rule11 = ctrl.Rule(sig_qual_802154['med_high'] & (tx_pow_802154['1']), self.tx_pow_ctl_802154['nc'])
        rule12 = ctrl.Rule(sig_qual_802154['med_high'] & (tx_pow_802154['2'] | tx_pow_802154['3'] | tx_pow_802154['4'] | tx_pow_802154['5']), self.tx_pow_ctl_802154['dec'])
        
        rule13 = ctrl.Rule(sig_qual_802154['high'] & (tx_pow_802154['off']), self.tx_pow_ctl_802154['off'])
        rule14 = ctrl.Rule(sig_qual_802154['high'] & (tx_pow_802154['1']), self.tx_pow_ctl_802154['nc'])
        rule15 = ctrl.Rule(sig_qual_802154['high'] & (tx_pow_802154['2'] | tx_pow_802154['3'] | tx_pow_802154['4'] | tx_pow_802154['5']), self.tx_pow_ctl_802154['dec'])
        
        # 802.11 rules
        rule16 = ctrl.Rule(sig_qual_80211['none'] & (~tx_pow_80211['5']), self.tx_pow_ctl_80211['inc']) # covers first column of SNR/P_tx rules
        rule17 = ctrl.Rule(sig_qual_80211['none'] & (tx_pow_80211['5']), self.tx_pow_ctl_80211['off']) # covers first column of SNR/P_tx rules
        
        rule18 = ctrl.Rule(sig_qual_80211['low'] & (~tx_pow_80211['5']), self.tx_pow_ctl_80211['inc'])
        rule19 = ctrl.Rule(sig_qual_80211['low'] & (tx_pow_80211['5']), self.tx_pow_ctl_80211['nc'])
        
        rule20 = ctrl.Rule(sig_qual_80211['low_med'] & (~tx_pow_80211['off']), self.tx_pow_ctl_80211['nc'])
        rule21 = ctrl.Rule(sig_qual_80211['low_med'] & (tx_pow_80211['off']), self.tx_pow_ctl_80211['inc'])
        
        rule22 = ctrl.Rule(sig_qual_80211['med'] & (tx_pow_80211['off']), self.tx_pow_ctl_80211['inc'])
        rule23 = ctrl.Rule(sig_qual_80211['med'] & (tx_pow_80211['1']), self.tx_pow_ctl_80211['nc'])
        rule24 = ctrl.Rule(sig_qual_80211['med'] & (tx_pow_80211['2'] | tx_pow_80211['3'] | tx_pow_80211['4'] | tx_pow_80211['5']), self.tx_pow_ctl_80211['dec'])
        
        rule25 = ctrl.Rule(sig_qual_80211['med_high'] & (tx_pow_80211['off']), self.tx_pow_ctl_80211['inc'])
        rule26 = ctrl.Rule(sig_qual_80211['med_high'] & (tx_pow_80211['1']), self.tx_pow_ctl_80211['nc'])
        rule27 = ctrl.Rule(sig_qual_80211['med_high'] & (tx_pow_80211['2'] | tx_pow_80211['3'] | tx_pow_80211['4'] | tx_pow_80211['5']), self.tx_pow_ctl_80211['dec'])
        
        rule28 = ctrl.Rule(sig_qual_80211['high'] & (tx_pow_80211['off']), self.tx_pow_ctl_80211['off'])
        rule29 = ctrl.Rule(sig_qual_80211['high'] & (tx_pow_80211['1']), self.tx_pow_ctl_80211['nc'])
        rule30 = ctrl.Rule(sig_qual_80211['high'] & (tx_pow_80211['2'] | tx_pow_80211['3'] | tx_pow_80211['4'] | tx_pow_80211['5']), self.tx_pow_ctl_80211['dec'])

        self.radio_ctrl_sys = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, 
                                        rule10, rule11, rule12, rule13,rule14, rule15,rule16, rule17, rule18,
                                        rule19, rule20, rule21, rule22, rule23, rule24, rule25, rule26, rule27,
                                        rule28, rule29, rule30])
        '''
        self.radio_ctrl_sys = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, 
                                         rule10, rule11, rule12, rule13,rule14, rule15,rule16, rule17, rule18,
                                         rule19, rule20, rule21, rule22, rule23, rule24])
        '''

        self.radio_sel = ctrl.ControlSystemSimulation(self.radio_ctrl_sys)


    def compute_output(self, snr_0, snr_1, tx_pow_0, tx_pow_1):
        self.radio_sel.input['snr_802154'] = snr_0
        self.radio_sel.input['snr_80211'] = snr_1
        
        self.radio_sel.input['tx_pow_802154'] = tx_pow_0
        self.radio_sel.input['tx_pow_80211'] = tx_pow_1

        #print(snr_0, tx_pow_0)
        #print(snr_1, tx_pow_1)

        self.radio_sel.compute()
        #print(snr_0, snr_1)
        #print(self.radio_sel.output['radio_802154'], self.radio_sel.output['radio_80211'])
        a=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['off'].mf, self.radio_sel.output['tx_pow_ctl_802154'])
        b=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['inc'].mf, self.radio_sel.output['tx_pow_ctl_802154'])
        c=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['dec'].mf, self.radio_sel.output['tx_pow_ctl_802154'])
        d=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['nc'].mf, self.radio_sel.output['tx_pow_ctl_802154'])

        pwr_sel_802154 = np.argmax([a,b,c,d])
        
        a=fuzz.interp_membership(self.tx_pow_ctl_80211.universe, self.tx_pow_ctl_80211['off'].mf, self.radio_sel.output['tx_pow_ctl_80211'])
        b=fuzz.interp_membership(self.tx_pow_ctl_80211.universe, self.tx_pow_ctl_80211['inc'].mf, self.radio_sel.output['tx_pow_ctl_80211'])
        c=fuzz.interp_membership(self.tx_pow_ctl_80211.universe, self.tx_pow_ctl_80211['dec'].mf, self.radio_sel.output['tx_pow_ctl_80211'])
        d=fuzz.interp_membership(self.tx_pow_ctl_80211.universe, self.tx_pow_ctl_80211['nc'].mf, self.radio_sel.output['tx_pow_ctl_80211'])
        
        pwr_sel_80211 = np.argmax([a,b,c,d])
        
            
        return (pwr_sel_802154, pwr_sel_80211)

        '''
        if (self.radio_sel.output['radio_802154'] > self.radio_sel.output['radio_80211']):
            a=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['off'].mf, self.radio_sel.output['pwr_lvl_802154'])
            b=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['inc'].mf, self.radio_sel.output['pwr_lvl_802154'])
            c=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['dec'].mf, self.radio_sel.output['pwr_lvl_802154'])
            d=fuzz.interp_membership(self.tx_pow_ctl_802154.universe, self.tx_pow_ctl_802154['nc'].mf, self.radio_sel.output['pwr_lvl_802154'])
            
            return [0,d]
        else:
            a=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['low'].mf, self.radio_sel.output['pwr_lvl_80211'])
            b=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['med'].mf, self.radio_sel.output['pwr_lvl_80211'])
            c=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['high'].mf, self.radio_sel.output['pwr_lvl_80211'])
            #print(a,b,c)
            if (a > b and a > c):
                d = 1
            elif (b > a and b > c):
                d = 2
            elif (c > b and c > b):
                d = 3
            else:
                d = -1
            return [1,d]
        '''
        
def run_fuzzy_agent():

    agent = FuzzyMultiRadioAgent()

    action_1 = Actions.RADIO_1_TX_POW_0
    action_1 = ControlActions.RADIO_SWITCH
    env.reset()
    
    n_steps = 0

    done = False
    while not done:
        obs, reward, done, info = env.step(action_1)
        
        #print('reward ' + str(reward))
        if (obs is not None):
            curr_radio_id = obs.get_curr_radio_id()
            radios = obs.get_radios()    
        else:
            pass
        
        tx_pow_0 = 2
        tx_pow_1 = 2

        if radios[0].get_tx_pow_step() == 0:
            tx_pow_0 = 1
        elif radios[0].get_tx_pow_step() == 1:
            tx_pow_0 = 2
        elif radios[0].get_tx_pow_step() == 2:
            tx_pow_0 = 3
        elif radios[0].get_tx_pow_step() == 3:
            tx_pow_0 = 4
        elif radios[0].get_tx_pow_step() == 4:
            tx_pow_0 = 5
        elif action_1 == ControlActions.RADIO_OFF:
            tx_pow_0 = 0

        if radios[1].get_tx_pow_step() == 0:
            tx_pow_1 = 1
        elif radios[1].get_tx_pow_step() == 1:
            tx_pow_1 = 2
        elif radios[1].get_tx_pow_step() == 2:
            tx_pow_1 = 3
        elif radios[1].get_tx_pow_step() == 3:
            tx_pow_1 = 4
        elif radios[1].get_tx_pow_step() == 4:
            tx_pow_1 = 5
        elif action_1 == ControlActions.RADIO_OFF:
            tx_pow_1 = 0
            
            
        radio_pwr = agent.compute_output(radios[0].get_snr(), radios[1].get_snr(), tx_pow_0, tx_pow_1)[1]
        
        
        if n_steps >= 8560 and n_steps <= 8610:
            print('%d:   %.2f   %.2e     %d %d %d' % (n_steps, radios[1].get_snr(), radios[1].curr_ber, tx_pow_1, radios[1].get_link_status(), radio_pwr))
        
        if radio_pwr == 0:
            action_1 = ControlActions.RADIO_OFF
        elif radio_pwr == 1:
            action_1 = ControlActions.RADIO_PWR_INC
        elif radio_pwr == 2:
            action_1 = ControlActions.RADIO_PWR_DEC
        elif radio_pwr == 3:
            action_1 = ControlActions.RADIO_PWR_STB
        
        '''
        if radios[1].get_link_status():    
        #if radio_select[0] == 1:
            radio_select = 1
        else:
            radio_select = 0

        if (radio_select == 0):
            
            if (radio_pwr == 1):
                action_1 = Actions.RADIO_0_TX_POW_0
            elif (radio_pwr == 2):
                action_1 = Actions.RADIO_0_TX_POW_2
            elif (radio_pwr == 3):
                #print(0, 4)
                action_1 = Actions.RADIO_0_TX_POW_4
            
            #action_1 = Actions.RADIO_0_TX_POW_4
        else:
            
            if (radio_pwr == 1):
                action_1 = Actions.RADIO_1_TX_POW_0
            elif (radio_pwr == 2):
                action_1 = Actions.RADIO_1_TX_POW_2
            elif (radio_pwr == 3):
                #print(1,4)
                action_1 = Actions.RADIO_1_TX_POW_4
            
            #action_1 = Actions.RADIO_1_TX_POW_4
        '''
        n_steps += 1

PLE_list = [2.5,2.75,3.0,3.25,3.5,3.75,4.0,4.25,4.5,4.75,5.0]
PLE_list = [0]
data = []
for i in range(0, len(PLE_list)):
    env = gym.make("path-power-wireless-v2", update_interval=TIME_STEP_INTERVAL, radio_cfg=RADIO_CFG, agent_path_fname=PATH_FNAME, env_fname=ENV_FNAME, ple_fname=PLE_FNAME, shd_fname=SHD_FNAME, god_agent=True)
    env.PLE = PLE_list[i]

    env.action_space = spaces.Discrete(len(ControlActions))
    env.action_enum = ControlActions

    print('Running fuzzy agent for ' + str(NUM_RUNS) + ' runs at PLE = ' + str(PLE_list[i]))

    for i in range(0, NUM_RUNS):
        print('Run ' + str(i+1) + ' of ' + str(NUM_RUNS),end='\r', flush=True)
        run_fuzzy_agent()
        

    pkt_loss_rate = env.recorder.data['pkt_loss_rate']
    cum_power = env.recorder.data['tot_pwr']
    step_reward = env.recorder.data['step_reward']

    pkt_loss_rate = np.array(pkt_loss_rate)
    tot_power = np.array(cum_power)
    mean_pkt_loss = np.mean(pkt_loss_rate)
    pkt_loss_std = np.std(pkt_loss_rate)
    mean_power = np.mean(tot_power)
    power_std = np.std(tot_power)

    print('mean pkt loss: ' + str(mean_pkt_loss) + ' std: ' + str(pkt_loss_std))
    print('mean pwr cons: ' + str(mean_power) + ' std: ' + str(power_std))

    data.append([mean_pkt_loss, pkt_loss_std, mean_power, power_std])

print(data)
#print(env.recorder.data['radio_use_trace'])
#print(env.recorder.data['outage_trace'][0])
radio_trace_ = env.recorder.data['radio_use_trace']
outage_trace_ = env.recorder.data['outage_trace']
plt_env_path(radio_trace_[0], outage_trace_[0])

plt.savefig('traj_env_q.png', dpi=600)


radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
dist_step = env.recorder.data['dist_step'][0]

fig = plt.figure()
fig, ax1 =plt.subplots()
plt.xlabel('Steps')
r0 = ax1.scatter(range(0,PATH_LEN), radio_0_pwr_trace[NUM_RUNS-1], c='m', s=1.5, label='802.15.4 Radio')
r1 = ax1.scatter(range(0,PATH_LEN), radio_1_pwr_trace[NUM_RUNS-1], c='c', s=1.5, label='802.11 Radio')
ax1.set_ylim([0.5,5.5])
ax1.set_ylabel('Radio Power Level')

ax2 = ax1.twinx()
l2 = ax2.plot(range(0,PATH_LEN), dist_step, c='r', label='Node distance')

ax2.set_ylim([0, 450])
ax2.set_ylabel('Node distance, m')


fig.suptitle('Radio Power Level and Node Distance vs. Steps')
plt.savefig('pwr_trace_env_q.png', dpi=600)
plt.show()
