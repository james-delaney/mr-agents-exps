import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly, ControlActions
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv

from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.fuzzy_ctl import FuzzyController, FuzzySet

class FuzzyMultiRadioAgent():

   

    def __init__(self, env):
        self.env = env
        self.states_obj = None

        self.action_space = self.env.action_space
        self.action_enum = self.env.action_enum
        self.n_actions = len(self.action_enum)

        self.is_init = False

    def cfg_agent(self, fuzzy_set, input_map):
        self.fuzzy_set = fuzzy_set
        self.fuzzy_set.simulate()
        
        self.input_map = input_map


    def _init_agent(self):
        self.fuzzy_controller = FuzzyController(self.fuzzy_set)
        
        self.is_init = True

    def simulate_run(self, re_init=False):
        # initialise agent if not already done or if we should re-init every run
        if self.is_init and re_init:
            self._init_agent()
        elif not self.is_init:
            self._init_agent()

        #print(self.component_weights)
        s = self.env.reset()
        
        done = False
        
        n_steps = 0 

        avg_td_err = 0
        last_avg_td_err = 0
        last_weight_val = 0 

        a = self.action_space.sample()
        a = ControlActions.RADIO_SWITCH
        while not done: 
            
            
            # take step 
            obs,r,done,info = self.env.step(list(self.action_enum)[a])
            radios = obs.get_radios()
            
            set_inputs = {}
            for k in self.input_map.keys():
                set_inputs[k] = eval(self.input_map[k])
            
            action_idx = self.fuzzy_controller.compute_output(set_inputs)
            
            curr_radio_id = obs.get_curr_radio_id()
            #curr_radio_id = 1
            #if n_steps >= 10000 and n_steps <= 10050:
            #    print(n_steps, set_inputs, action_idx)
            #a = list(self.action_enum)[action_idx[curr_radio_id]]
            a = action_idx[curr_radio_id]
            
            n_steps += 1
        #return r_sum, avg_reward, d_q
        return done
