import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F


class ReplayMemory():
    
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []

    def push(self, transition):
        self.memory.append(transition)
        if len(self.memory) > self.capacity:
            del self.memory[0]

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

class DQN(nn.Module):
    
    def __init__(self, num_inputs, num_outputs, hidden_layer_size):
        nn.Module.__init__(self)
        self.l1 = nn.Linear(num_inputs, hidden_layer_size)
        self.l2 = nn.Linear(hidden_layer_size, hidden_layer_size)
        self.l3 = nn.Linear(hidden_layer_size, num_outputs)
        
    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x =  self.l3(x)
        return x
        
class DQNAgent():
    
    def __init__(self, env):
        self.alpha = 0
        self.gamma = 0
        self.epsilon = 0
        
        self.num_components = 0
        self.component_weights = None
        self.epsilon_schedule = 1
        self.rand_expl = False
        
        self.env = env
        self.states_obj = None
        
        self.action_space = self.env.action_space
        self.action_enum = self.env.action_enum
        self.n_actions = len(self.actions_enum)
        
        self.is_init = False
        
    def cfg_agent(self, state_space, states_obj, num_components, w1_comp_val, alpha, gamma, epsilon_schedule):
        self.state_space = state_space
        self.states_obj = states_obj
        self.num_components = num_components
        self.component_weights = [w1_comp_val, 1-w1_comp_val]
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon_schedule
        
    def _init_agent(self):
        