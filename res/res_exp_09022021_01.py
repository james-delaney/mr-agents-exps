import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from res_util import opt_policy_rmse_at_step

DATA_DIR = '../exps/data/'

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_1454.hdf5' # for env q, w1=0.45
#exp_exploration_fname = 'test_radio_data_exploration_16102020_1552.hdf5' # with a = a_prime change in algorithm

#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_w_sweep_01122020_1723.hdf5'
exp_gm_agent_fname = 'radio_data_exp_adaptive_gm_agent_w_sweep_10022021_1134.hdf5'
exp_gm_agent_opt_fname = 'radio_data_exp_optimal_front_14022021_1122.hdf5'

#exp_optimal_fname = 'radio_data_exp_optimal_front_01122020_1040.hdf5'
exp_rl_agent_fname = 'radio_data_exp_adaptive_rl_agent_rw_sweep_04022021_1139.hdf5'
exp_rl_agent_opt_fname = 'radio_data_exp_so_optimal_front_14022021_1123.hdf5'

data_file_gm_agent = h5py.File(DATA_DIR+exp_gm_agent_fname, 'r')
data_file_rl_agent = h5py.File(DATA_DIR+exp_rl_agent_fname, 'r')

data_file_gm_agent_opt = h5py.File(DATA_DIR+exp_gm_agent_opt_fname, 'r')
data_file_rl_agent_opt = h5py.File(DATA_DIR+exp_rl_agent_opt_fname, 'r')

param_list_gm_agent = ['0.05', '0.50', '0.95']
param_list_rl_agent = ['1.00', '2.00', '0.00']
rw_fn_pref  = ['Minimised Power Consumption', 'Balanced Objective', 'Maximised Bitrate']

print(data_file_gm_agent['radio_data'].keys())
print(data_file_rl_agent['radio_data'].keys())

#print('Radio data histogram, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
#print(data_file.keys())

path_len = int(data_file_gm_agent['exp_cfg/path_len'][()])
#path_len = 2000

def radio_data_to_hist(r0, r1):
    r0_hist = []
    r1_hist = []
    for i in range(path_len):
        r0_hist.append(np.bincount(r0[:,i], minlength=6))
        r1_hist.append(np.bincount(r1[:,i], minlength=6))
        if i % 1000 == 0:
            sys.stdout.write('\r\tCalculating histogram done %d steps of %d' % (i, path_len))
            sys.stdout.flush()
            
    r0_hist = np.array(r0_hist)
    r1_hist = np.array(r1_hist)
    print('\tDone')
    print(r0_hist.shape)
    
    r0_hist_bin = []
    r1_hist_bin = []
    for i in range(int(path_len/50)):
        r0_hist_bin.append(np.mean(r0_hist[i*50:(i*50+50), :], axis=0))
        r1_hist_bin.append(np.mean(r1_hist[i*50:(i*50+50), :], axis=0))
    
    r0_hist_bin = np.array(r0_hist_bin)
    r1_hist_bin = np.array(r1_hist_bin)
    
    # stack up the histograms for the two radios' power levels (0-4 is radio 0, 5-9 is radio 1)
    cmb_pwr_lvl = np.vstack((r0_hist[:,1:].T, r1_hist[:,1:].T))
    return cmb_pwr_lvl

#param_list = ['0.90','0.95','1.00']

def gen_histograms():
    for p_gm, p_rl,rw_fn in zip(param_list_gm_agent, param_list_rl_agent, rw_fn_pref):
        #print('param=%d; pkt loss rate=%.2f' % (p, data_file['data/'+p+'/pkt_loss_rate']) )
        #print('param=%d; goodput=%.2f' % (p, data_file['data/'+p+'/goodput']) )
        r0_gm = data_file_gm_agent['radio_data/'+p_gm+'/radio_0_pwr_trace']
        r1_gm = data_file_gm_agent['radio_data/'+p_gm+'/radio_1_pwr_trace']
        
        cmb_pwr_lvl_gm = radio_data_to_hist(r0_gm, r1_gm)
        
        r0_rl = data_file_rl_agent['radio_data/'+p_rl+'/radio_0_pwr_trace']
        r1_rl = data_file_rl_agent['radio_data/'+p_rl+'/radio_1_pwr_trace']
        
        cmb_pwr_lvl_rl = radio_data_to_hist(r0_rl, r1_rl)
        
        
        fig, (ax1, ax2, axcb) = plt.subplots(1, 3, gridspec_kw={'width_ratios':[1,1,0.08]})
        ax1.get_shared_y_axes().join(ax2)
        
        #print(cmb_pwr_lvl.shape)
        #axcb= ax.figure.colorbar(ax.collections[0])
        h1 = sns.heatmap(cmb_pwr_lvl_rl, cmap='rocket_r', cbar=False, xticklabels=2500, ax=ax1)
        h1.invert_yaxis()
        h1.set_xlabel('Steps')
        h1.set_ylabel('Action, (Power levels)')
        h1.set_title('RL Agent')
        
        
        h2 = sns.heatmap(cmb_pwr_lvl_gm, cmap='rocket_r', xticklabels=2500, ax=ax2, cbar_ax = axcb)
        h2.invert_yaxis()
        h2.set_xlabel('Steps')
        #h2.set_ylabel('Action, (Power levels)')
        h2.set_title('MORL Agent')
        
        axcb.set_yticks([0, 20, 40, 60, 80, 100])
        axcb.set_yticklabels(['0', '0.2', '0.4', '0.6', '0.8', '1.0'])
        axcb.set_ylabel('Probability of selection')
        
        fig.suptitle(rw_fn)
        fig_fname = '09022021_01/09020201_01_%.2f.png' % (float(p_rl))
        plt.tight_layout()
        plt.savefig(fig_fname, dpi=600)
        

def calc_policy_mpe():
    print('Start calculating MPE for agents')
    mpe_gm = {}
    mpe_rl = {}
    for p_gm, p_rl,rw_fn in zip(param_list_gm_agent, param_list_rl_agent, rw_fn_pref):
        rmse_gm = opt_policy_rmse_at_step(data_file_gm_agent_opt['radio_data'], p_gm, data_file_gm_agent['radio_data'], p_gm, path_len)
        rmse_rl = opt_policy_rmse_at_step(data_file_rl_agent_opt['radio_data'], p_rl, data_file_rl_agent['radio_data'], p_rl, path_len)
        
        mpe_gm[p_gm] = [np.mean(rmse_gm), np.std(rmse_gm)]
        mpe_rl[p_rl] = [np.mean(rmse_rl), np.std(rmse_rl)]
        
    print()
    print('Mean Policy Error for Rw pref:')
    for p_gm, p_rl,rw_fn in zip(param_list_gm_agent, param_list_rl_agent, rw_fn_pref):
        print('\t%s' % (rw_fn))
        print('\t\t\tGM Agent:\t\t\t%.4f\t\t%.4f' % (mpe_gm[p_gm][0], mpe_gm[p_gm][1]))
        print('\t\t\tRL Agent:\t\t\t%.4f\t\t%.4f' % (mpe_rl[p_rl][0], mpe_rl[p_rl][1]))
        
        
        
calc_policy_mpe()

data_file_gm_agent.close()
data_file_rl_agent.close()

data_file_gm_agent_opt.close()
data_file_rl_agent_opt.close()