import csv
import numpy as np
import h5py

def load_res_data_descriptor(fname):
    data = []
    with open(fname, 'r') as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            data.append(np.array(row))
    
    return np.array(data)

def load_res_data(data_dir, data_desc, agent_name, exp_sfx, dt_idx=4):
    data = []
    radio_data = []
    for df in data_desc:
        data_fname = data_dir+'data_'+agent_name+'_'+exp_sfx+'_'+df[dt_idx]+'.hdf5'
        radio_data_fname = data_dir+'radio_data_'+agent_name+'_'+exp_sfx+'_'+df[dt_idx]+'.hdf5'
        data.append(h5py.File(data_fname, 'r'))
        radio_data.append(h5py.File(radio_data_fname, 'r'))
    
    return data, radio_data