from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize

from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv, gen_confusion_matrix
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
from thesis_util import *

import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 12,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


DATA_DIR = '../../exps/data/thesis_ch4/'


ENV_NAME = ['o', 'p', 'q']

def get_data_params(data_file_list):
    num_files = len(data_file_list)
    
    goodput = np.array([[data_file_list[i]['data/'+p+'/goodput'][()]/1000000 for p in data_file_list[i]['data'].keys()] for i in range(num_files)]).squeeze()
    plr = np.array([[data_file_list[i]['data/'+p+'/pkt_loss_rate'][()] for p in data_file_list[i]['data'].keys()] for i in range(num_files)]).squeeze()
    pwr = np.array([[data_file_list[i]['data/'+p+'/tot_pwr'][()] for p in data_file_list[i]['data'].keys()] for i in range(num_files)]).squeeze()
    
    return goodput, plr, pwr



opt_w_sweep_desc_fname = DATA_DIR+'mo/opt_agent_w_sweep.csv'

opt_w_sweep_data_desc = load_res_data_descriptor(opt_w_sweep_desc_fname)

opt_w_sweep_data, opt_w_sweep_radio_data = load_res_data(DATA_DIR+'mo/', opt_w_sweep_data_desc, 'opt_agent', 'w_sweep')

opt_goodput, opt_plr, opt_pwr = get_data_params(opt_w_sweep_data)



num = 21

env_o = np.full(num, 'o')
env_p = np.full(num, 'p')
env_q = np.full(num, 'q')
w1 = np.linspace(0,1,num)
goodput = np.full(num, 'goodput')
plr = np.full(num, 'plr')
pwr = np.full(num, 'pwr')

data = []
'''
data.append(np.vstack((w1, opt_goodput[0], goodput, env_o)).T)
data.append(np.vstack((w1, opt_plr[0], plr, env_o)).T)
data.append(np.vstack((w1, opt_pwr[0], pwr, env_o)).T)
data.append(np.vstack((w1, opt_goodput[1], goodput, env_p)).T)
data.append(np.vstack((w1, opt_plr[1], plr, env_p)).T)
data.append(np.vstack((w1, opt_pwr[1], pwr, env_p)).T)
data.append(np.vstack((w1, opt_goodput[2], goodput, env_q)).T)
data.append(np.vstack((w1, opt_plr[2], plr, env_q)).T)
data.append(np.vstack((w1, opt_pwr[2], pwr, env_q)).T)
'''

data.append(np.vstack((w1, opt_goodput[0], opt_plr[0], opt_pwr[0], env_o)).T)
data.append(np.vstack((w1, opt_goodput[1], opt_plr[1], opt_pwr[1], env_p)).T)
data.append(np.vstack((w1, opt_goodput[2], opt_plr[2], opt_pwr[2], env_q)).T)


'''
df = pd.DataFrame(data=np.vstack(data), columns=['w1', 'data', 'metric', 'env'])
df.explode('data')
df['data'] =df['data'].astype('float')
df.explode('w1')
df['w1'] = df['w1'].astype('float')
'''
df = pd.DataFrame(data=np.vstack(data), columns=['w1', 'goodput', 'plr', 'pwr', 'env'])
df.explode('w1')
df['w1'] = df['w1'].astype('float')
df.explode('goodput')
df['goodput'] = df['goodput'].astype('float')
df.explode('plr')
df['plr'] = df['plr'].astype('float')
df.explode('pwr')
df['pwr'] = df['pwr'].astype('float')

df_o = df.loc[(df['env'] == 'o') & (df['w1'] > 0.00) & (df['w1'] < 1.00)]
df_p = df.loc[(df['env'] == 'p')]
df_q = df.loc[(df['env'] == 'q')]


fig, ax = plt.subplots()
sns.scatterplot(data=df_o, x='pwr', y='goodput', ax=ax)
#ax.invert_yaxis()
ax.set_ylabel('Total data transferred, MB')
ax.set_xlabel('Power consumption, Wh')

ax.annotate(text='\(0.05 \leq w_1 \leq 0.20\)', xy=(0.0671,454), 
            xycoords='data', 
            xytext=(60,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.3', color='black',))

ax.annotate(text='\(w_1 = 0.25\)', xy=(0.0775,457), 
            xycoords='data', 
            xytext=(60,40), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.3', color='black',))

ax.annotate(text='\(0.30 \leq w_1 \leq 0.40\)', xy=(0.3112,526.2), 
            xycoords='data', 
            xytext=(-80,-60), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.5', color='black',))


ax.annotate(text='\(0.45 \leq w_1 \leq 0.95\)', xy=(0.3314,538.6), 
            xycoords='data', 
            xytext=(-180,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.5', color='black',))

ax.set_xticks([0.1,0.15,0.20, 0.25, 0.3])

#plt.tight_layout(0.4)
plt.savefig('out/fig_momr_res_mo_sweep_o.pdf', format='pdf', bbox_inches='tight')

fig, ax = plt.subplots()
sns.scatterplot(data=df_p, x='pwr', y='goodput', ax=ax)
#ax.invert_yaxis()
ax.set_ylabel('Total data transferred, MB')
ax.set_xlabel('Power consumption, Wh')

ax.annotate(text='\(0.05 \leq w_1 \leq 0.20\)', xy=(0.055,600), 
            xycoords='data', 
            xytext=(30,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.3', color='black',))

ax.annotate(text='\(w_1 = 0.25\)', xy=(0.112,626), 
            xycoords='data', 
            xytext=(30,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.3', color='black',))


ax.annotate(text='\(0.30 \leq w_1 \leq 0.40\)', xy=(0.516,766.6), 
            xycoords='data', 
            xytext=(-120,-60), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.5', color='black',))


ax.annotate(text='\(0.45 \leq w_1 \leq 0.95\)', xy=(0.558,785), 
            xycoords='data', 
            xytext=(-180,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.5', color='black',))

ax.set_xticks([0.1,0.2,0.3, 0.4, 0.5])
ax.set_xticklabels(['0.10', '0.20', '0.30', '0.40', '0.50'])
#plt.tight_layout(0.4)
plt.savefig('out/fig_momr_res_mo_sweep_p.pdf', format='pdf', bbox_inches='tight')

fig, ax = plt.subplots()
sns.scatterplot(data=df_q, x='pwr', y='goodput', ax=ax)
#ax.invert_yaxis()
ax.set_ylabel('Total data transferred, MB')
ax.set_xlabel('Power consumption, Wh')


ax.annotate(text='\(0.05 \leq w_1 \leq 0.20\)', xy=(0.066,600), 
            xycoords='data', 
            xytext=(60,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.3', color='black',))

ax.annotate(text='\(w_1 = 0.25\)', xy=(0.098,613), 
            xycoords='data', 
            xytext=(60,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.3', color='black',))

ax.annotate(text='\(0.30 \leq w_1 \leq 0.40\)', xy=(0.505,739), 
            xycoords='data', 
            xytext=(-120,-60), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.5', color='black',))


ax.annotate(text='\(0.45 \leq w_1 \leq 0.95\)', xy=(0.556,759), 
            xycoords='data', 
            xytext=(-180,-4), 
            textcoords='offset points', 
            arrowprops=dict(arrowstyle='-[, widthB=0.5', color='black',))

#plt.tight_layout(0.4)
ax.set_xticks([0.1,0.2,0.3, 0.4, 0.5])
ax.set_xticklabels(['0.10', '0.20', '0.30', '0.40', '0.50'])

plt.savefig('out/fig_momr_res_mo_sweep_q.pdf', format='pdf', bbox_inches='tight')

'''
for i in range(3):
    hist_min_pwr = get_radio_data_hist(opt_w_sweep_radio_data[i], 20000, '0.10')
    hist_int_pwr = get_radio_data_hist(opt_w_sweep_radio_data[i], 20000, '0.25')
    hist_max_br = get_radio_data_hist(opt_w_sweep_radio_data[i], 20000, '0.85')
    hist_bal = get_radio_data_hist(opt_w_sweep_radio_data[i], 20000, '0.40')
    
    plt_radio_data_hist(hist_min_pwr, 'out/fig_momr_res_mo_'+ENV_NAME[i]+'_min_pwr.pdf', num_runs=1)
    plt_radio_data_hist(hist_int_pwr, 'out/fig_momr_res_mo_'+ENV_NAME[i]+'_int_pwr.pdf', num_runs=1)
    plt_radio_data_hist(hist_max_br, 'out/fig_momr_res_mo_'+ENV_NAME[i]+'_max_br.pdf', num_runs=1)
    plt_radio_data_hist(hist_bal, 'out/fig_momr_res_mo_'+ENV_NAME[i]+'_bal.pdf', num_runs=1)

'''
