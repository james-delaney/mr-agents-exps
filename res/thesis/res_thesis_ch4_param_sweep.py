from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns

import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize
from thesis_util import *
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_actions, gen_confusion_matrix

import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


#plt.style.use('seaborn')
#plt.style.use('tex')

DATA_DIR = '../../exps/data/thesis_ch4/param/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = 'comms_letters_11022021/'


ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']



def get_data_params(data_file_list):
    num_files = len(data_file_list)
    
    goodput = np.array([[data_file_list[i]['data/'+p+'/goodput'][()]/1000000 for p in data_file_list[i]['data'].keys()] for i in range(num_files)]).squeeze()
    plr = np.array([[data_file_list[i]['data/'+p+'/pkt_loss_rate'][()] for p in data_file_list[i]['data'].keys()] for i in range(num_files)]).squeeze()
    pwr = np.array([[data_file_list[i]['data/'+p+'/tot_pwr'][()] for p in data_file_list[i]['data'].keys()] for i in range(num_files)]).squeeze()
    
    return goodput, plr, pwr


gm_agent_param_sweep_desc_fname = DATA_DIR+'gm_agent_param_sweep.csv'
gm_agent_param_sweep_w1_01_desc_fname = DATA_DIR+'gm_agent_param_sweep_w1_01.csv'

sweep_matrix_f1_data_fname = DATA_DIR+'f1_data.csv'
sweep_matrix_f1_w1_01_data_fname = DATA_DIR+'w1_01_f1_data.csv'

gm_agent_param_sweep_data_desc = load_res_data_descriptor(gm_agent_param_sweep_desc_fname)
gm_agent_param_sweep_w1_01_data_desc = load_res_data_descriptor(gm_agent_param_sweep_w1_01_desc_fname)

gm_agent_param_sweep_data, gm_agent_param_sweep_radio_data = load_res_data(DATA_DIR, gm_agent_param_sweep_data_desc, 'gm_agent', 'param_sweep', dt_idx=3)
gm_agent_param_sweep_w1_01_data, gm_agent_param_sweep_w1_01_radio_data = load_res_data(DATA_DIR, gm_agent_param_sweep_w1_01_data_desc, 'gm_agent', 'param_sweep_w1_01', dt_idx=3)


alpha = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
gamma = [0.1, 0.3, 0.6, 0.9]



'''

    - for each alpha/gamma pair, generate a confusion matrix
         - calculate precision/recall
         - mask out opt_actions
         - remove 0.0 values
         - calculate f1_score using precision/recall


'''

#
#
#
# w1 = 0.5

if not os.path.isfile(sweep_matrix_f1_data_fname):
    print('loading opt data')
    exp_optimal_o = 'radio_data_opt_agent_w1_08052021_1554.hdf5'
    data_file_opt_o = h5py.File(DATA_DIR+exp_optimal_o, 'r')
    
    opt_use_trace = data_file_opt_o['radio_data/0.50/radio_use_trace']
    opt_r0_pwr_trace = data_file_opt_o['radio_data/0.50/radio_0_pwr_trace']
    opt_r1_pwr_trace = data_file_opt_o['radio_data/0.50/radio_1_pwr_trace']
    
    print('getting opt actions')
    opt_actions = radio_data_to_actions(opt_use_trace, opt_r0_pwr_trace, opt_r1_pwr_trace, 20000, 1)
    
    data = []
    for g in range(len(gamma)):
        d_int = []
        for a in alpha:
            print('alpha: ',a, ' gamma: ', g)
            
            param_str = '%.2f' % (float(a))
            radio_use_trace  = gm_agent_param_sweep_radio_data[g]['radio_data/'+param_str+'/radio_use_trace']
            r0_pwr_trace = gm_agent_param_sweep_radio_data[g]['radio_data/'+param_str+'/radio_0_pwr_trace']
            r1_pwr_trace = gm_agent_param_sweep_radio_data[g]['radio_data/'+param_str+'/radio_1_pwr_trace']
            
            print('getting actions')
            actions = radio_data_to_actions(radio_use_trace, r0_pwr_trace, r1_pwr_trace, 20000, 50)
            matrix, report, mcc = gen_confusion_matrix(opt_actions, actions, 11)
            
            matrix = (matrix/np.sum(matrix, axis=1).reshape(-1,1))*100
            matrix = np.nan_to_num(matrix)
            
            opt_action_mask = np.array([1,1,1,1,1,0,0,0,1,0,0]).reshape(-1,1)
            matrix *= opt_action_mask
            
            precision = []
            recall = []
            for i in range(11):
                precision.append(matrix[i,i]/np.sum(matrix[:,i]))
                recall.append(matrix[i,i]/np.sum(matrix[i,:]))
                
            precision = precision*opt_action_mask.T
            recall = recall*opt_action_mask.T
            
            
            mean_p = np.mean(precision[precision>0.0])
            mean_r = np.mean(recall[recall>0.0])
            f1_a = (2 * (mean_p*mean_r))/(mean_p+mean_r)
            
            print('stats:')
            print('Precision:', mean_p)
            print('Recall: ', mean_r)
            print('F1-score: ', f1_a)
            
            d_int.append(f1_a)
        
        data.append(d_int)
    
    with open(sweep_matrix_f1_data_fname, 'w') as file:
        for g in range(len(gamma)):
            for a in range(len(alpha)):
                file.write('%.6f,' % (data[g][a]))
            file.write('\n')
        
    data = np.array(data)
else:
    with open(sweep_matrix_f1_data_fname, 'r') as file:
        data = []
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            data.append(np.array(row[:-1]).astype(float))

alpha = np.array(alpha)            
gamma_01 = np.full(len(alpha), r'$\gamma$=0.1')
gamma_03 = np.full(len(alpha), r'$\gamma$=0.3')
gamma_06 = np.full(len(alpha), r'$\gamma$=0.6')
gamma_09 = np.full(len(alpha), r'$\gamma$=0.9')

gamma_tbl = [gamma_01, gamma_03, gamma_06, gamma_09]
#for g in gamma:
    #gamma_tbl.append(np.full(len(alpha), r'$\gamma$=%.1f'%(g)))
#    gamma_tbl.append()
    
#gamma_tbl = np.array(gamma_tbl)

tbl = []
for i in range(len(gamma)):
    tbl.append(np.vstack((alpha, gamma_tbl[i], data[i])).T)


df = pd.DataFrame(data=np.vstack(tbl), columns=['alpha', 'gamma', 'f1'])
df.explode('alpha')
df['alpha'] = df['alpha'].astype('float')
df.explode('f1')
df['f1'] = df['f1'].astype('float')

fig, ax = plt.subplots()
ax =sns.lineplot(data=df, x='alpha', y='f1', hue='gamma', style='gamma', markers=True, ax=ax)
ax.set_xlabel(r'$\alpha$')
ax.set_xticks(np.linspace(0.1,1,10))
ax.set_xticklabels([str('%.1f'%l) for l in np.linspace(0.1,1,10)])
ax.set_xlim([0.1,1])
ax.set_ylabel('Mean F1-score')
ax.set_ylim([0,1])
ax.legend(loc=2, bbox_to_anchor=(0, 1)).set_title('')
plt.savefig('out/fig_momr_res_param_sweep_o_w1_05.pdf', )



for d, rd in zip(gm_agent_param_sweep_data, gm_agent_param_sweep_radio_data):
    d.close()
    rd.close()
    
   

#
#
#
# w1 = 0.1
if not os.path.isfile(sweep_matrix_f1_w1_01_data_fname):
    print('loading opt data')
    exp_optimal_o = 'radio_data_opt_agent_w1_01_09052021_1806.hdf5'
    data_file_opt_o = h5py.File(DATA_DIR+exp_optimal_o, 'r')
    
    opt_use_trace = data_file_opt_o['radio_data/0.10/radio_use_trace']
    opt_r0_pwr_trace = data_file_opt_o['radio_data/0.10/radio_0_pwr_trace']
    opt_r1_pwr_trace = data_file_opt_o['radio_data/0.10/radio_1_pwr_trace']
    
    print('getting opt actions')
    opt_actions = radio_data_to_actions(opt_use_trace, opt_r0_pwr_trace, opt_r1_pwr_trace, 20000, 1)
    
    data = []
    for g in range(len(gamma)):
        d_int = []
        for a in alpha:
            print('alpha: ',a, ' gamma: ', g)
            
            param_str = '%.2f' % (float(a))
            radio_use_trace  = gm_agent_param_sweep_w1_01_radio_data[g]['radio_data/'+param_str+'/radio_use_trace']
            r0_pwr_trace = gm_agent_param_sweep_w1_01_radio_data[g]['radio_data/'+param_str+'/radio_0_pwr_trace']
            r1_pwr_trace = gm_agent_param_sweep_w1_01_radio_data[g]['radio_data/'+param_str+'/radio_1_pwr_trace']
            
            print('getting actions')
            actions = radio_data_to_actions(radio_use_trace, r0_pwr_trace, r1_pwr_trace, 20000, 50)
            matrix, report, mcc = gen_confusion_matrix(opt_actions, actions, 11)
            
            matrix = (matrix/np.sum(matrix, axis=1).reshape(-1,1))*100
            matrix = np.nan_to_num(matrix)
            
            opt_action_mask = np.array([0,0,0,0,0,1,1,1,1,1,1]).reshape(-1,1)
            matrix *= opt_action_mask
            
            precision = []
            recall = []
            for i in range(11):
                precision.append(matrix[i,i]/np.sum(matrix[:,i]))
                recall.append(matrix[i,i]/np.sum(matrix[i,:]))
                
            precision = precision*opt_action_mask.T
            recall = recall*opt_action_mask.T
            
            
            mean_p = np.mean(precision[precision>0.0])
            mean_r = np.mean(recall[recall>0.0])
            f1_a = (2 * (mean_p*mean_r))/(mean_p+mean_r)
            
            print('stats:')
            print('Precision:', mean_p)
            print('Recall: ', mean_r)
            print('F1-score: ', f1_a)
            
            d_int.append(f1_a)
        
        data.append(d_int)
    
    with open(sweep_matrix_f1_w1_01_data_fname, 'w') as file:
        for g in range(len(gamma)):
            for a in range(len(alpha)):
                file.write('%.6f,' % (data[g][a]))
            file.write('\n')
        
    data = np.array(data)
else:
    with open(sweep_matrix_f1_w1_01_data_fname, 'r') as file:
        data = []
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            data.append(np.array(row[:-1]).astype(float))

alpha = np.array(alpha)            
gamma_01 = np.full(len(alpha), r'$\gamma$=0.1')
gamma_03 = np.full(len(alpha), r'$\gamma$=0.3')
gamma_06 = np.full(len(alpha), r'$\gamma$=0.6')
gamma_09 = np.full(len(alpha), r'$\gamma$=0.9')

gamma_tbl = [gamma_01, gamma_03, gamma_06, gamma_09]
#for g in gamma:
    #gamma_tbl.append(np.full(len(alpha), r'$\gamma$=%.1f'%(g)))
#    gamma_tbl.append()
    
#gamma_tbl = np.array(gamma_tbl)

tbl = []
for i in range(len(gamma)):
    tbl.append(np.vstack((alpha, gamma_tbl[i], data[i])).T)


df = pd.DataFrame(data=np.vstack(tbl), columns=['alpha', 'gamma', 'f1'])
df.explode('alpha')
df['alpha'] = df['alpha'].astype('float')
df.explode('f1')
df['f1'] = df['f1'].astype('float')

fig, ax = plt.subplots()
ax =sns.lineplot(data=df, x='alpha', y='f1', hue='gamma', style='gamma', markers=True, ax=ax)
ax.set_xlabel(r'$\alpha$')
ax.set_ylabel('Mean F1-score')
ax.set_xticks(np.linspace(0.1,1,10))
ax.set_xticklabels([str('%.1f'%l) for l in np.linspace(0.1,1,10)])
ax.set_xlim([0.1,1])
ax.set_ylim([0,1])
ax.legend(loc=1, bbox_to_anchor=(1, 0.85)).set_title('')
plt.savefig('out/fig_momr_res_param_sweep_o_w1_01.pdf', )



for d, rd in zip(gm_agent_param_sweep_w1_01_data, gm_agent_param_sweep_w1_01_radio_data):
    d.close()
    rd.close()