from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns

import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_actions, gen_confusion_matrix, csv_to_array
import copy
import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


#plt.style.use('seaborn')
#plt.style.use('tex')

DATA_DIR = '../exps/data/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = 'ch4_policy_eval/'


ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']

param_list_str = {'0.00':'Decayed Exploration Rate',
                      '1.00':'Adaptive Exploration Rate',}


def get_actions(env_name):
    opt_fname = 'opt_actions_'+env_name+'.csv'
    adaptive_fname = 'adaptive_actions_'+env_name+'.csv'
    decayed_fname = 'decayed_actions_'+env_name+'.csv'
    
    opt_actions = csv_to_array(DATA_EXP_DIR+opt_fname, int)
    adaptive_actions = csv_to_array(DATA_EXP_DIR+adaptive_fname, int)
    decayed_actions = csv_to_array(DATA_EXP_DIR+decayed_fname, int)
    
    return opt_actions, adaptive_actions, decayed_actions



def get_spec_actions(name):
    actions = csv_to_array(DATA_EXP_DIR+name+'.csv', int)
    return actions

def plt_conf_matrix(df, annot, fname=None):
    plt.figure()
    ax1 = sns.heatmap(df, annot=annot, fmt='s', vmin=0, vmax=100, cbar=False, cmap='rocket', annot_kws={"size":14, "fontweight":'bold'})
    for i in range(11):
        ax1.axhline(i, color='white', lw=2)
    #plt.title('Adaptive exploration')
    plt.xlabel('Learned Actions')
    plt.ylabel('Optimal Actions')
    plt.gca().invert_yaxis()
    cbar = ax1.figure.colorbar(ax1.collections[0])
    cbar.set_ticks([0,20,40,60,80,100])
    cbar.set_label('Rate of selection, \%')
    #plt.savefig('comms_letters_11022021/conf_adaptive.png', dpi=1200)
    plt.tight_layout(pad=0.4)
    if fname is not None:
        plt.savefig(fname, format='pdf', bbox_inches='tight')


def print_conf_stats(a, d, name_a=None, name_d=None):
    a_precision = []
    a_recall = []
    d_precision = []
    d_recall = []
    for i in range(11):
        a_precision.append(a[i,i]/np.sum(a[:,i]))
        a_recall.append(a[i,i]/np.sum(a[i,:]))
        d_precision.append(d[i,i]/np.sum(d[:,i]))
        d_recall.append(d[i,i]/np.sum(d[i,:]))
    
    a_precision = a_precision*opt_action_mask.T
    d_precision = d_precision*opt_action_mask.T
    a_recall = a_recall*opt_action_mask.T
    d_recall = d_recall*opt_action_mask.T
    
    mean_a_p = np.mean(a_precision[a_precision>0.0])
    mean_a_r = np.mean(a_recall[a_recall>0.0])
    mean_d_p = np.mean(d_precision[d_precision>0.0])
    mean_d_r = np.mean(d_recall[d_recall>0.0])
    f1_a = (2 * (mean_a_p*mean_a_r))/(mean_a_p+mean_a_r)
    f1_d = (2 * (mean_d_p*mean_d_r))/(mean_d_p+mean_d_r)
    
    if name_a is None: print('Adaptive stats:')
    else: print(name_a)
    print('Precision:', mean_a_p)
    print('Recall: ', mean_a_r)
    print('F1-score: ', f1_a)
    
    if name_d is None: print('Decayed stats:')
    else: print(name_d)
    print('Precision:', mean_d_p)
    print('Recall: ', mean_d_r)
    print('F1-score: ', f1_d)

'''
exp_adaptive_o = 'radio_data_exp_adaptive_gm_agent_exp_sweep_10032021_1009.hdf5' #radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1615.hdf5'
#exp_adaptive_o = 'radio_data_exp_adaptive_gm_agent_exp_sweep_17032021_1027.hdf5'
exp_adaptive_p = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1616.hdf5'
exp_adaptive_q = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'
exp_optimal_o = 'radio_data_exp_optimal_front_15022021_1435.hdf5'
exp_optimal_p = 'radio_data_exp_optimal_front_15022021_1436.hdf5'
exp_optimal_q = 'radio_data_exp_optimal_front_12022021_1433.hdf5'

exp_data_adaptive_env_o = 'data_exp_adaptive_gm_agent_exp_sweep_10032021_1009.hdf5' #data_exp_adaptive_gm_agent_exp_sweep_11022021_1615.hdf5'
#exp_data_adaptive_env_o = 'data_exp_adaptive_gm_agent_exp_sweep_17032021_1027.hdf5'
exp_data_adaptive_env_p = 'data_exp_adaptive_gm_agent_exp_sweep_11022021_1616.hdf5'
exp_data_adaptive_env_q = 'data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'


radio_data_file_adaptive_o = h5py.File(DATA_DIR+exp_adaptive_o, 'r')
radio_data_file_adaptive_p = h5py.File(DATA_DIR+exp_adaptive_p, 'r')
radio_data_file_adaptive_q = h5py.File(DATA_DIR+exp_adaptive_q, 'r')

radio_data_file_opt_o = h5py.File(DATA_DIR+exp_optimal_o, 'r')
radio_data_file_opt_p = h5py.File(DATA_DIR+exp_optimal_p, 'r')
radio_data_file_opt_q = h5py.File(DATA_DIR+exp_optimal_q, 'r')


opt_o_use_trace = radio_data_file_opt_o['radio_data/0.50/radio_use_trace']
opt_o_r0_pwr_trace = radio_data_file_opt_o['radio_data/0.50/radio_0_pwr_trace']
opt_o_r1_pwr_trace = radio_data_file_opt_o['radio_data/0.50/radio_1_pwr_trace']

print('getting opt actions')
opt_o_actions = radio_data_to_actions(opt_o_use_trace, opt_o_r0_pwr_trace, opt_o_r1_pwr_trace, 20000, 1)


adaptive_o_use_trace = radio_data_file_adaptive_o['radio_data/1.00/radio_use_trace']
adaptive_o_r0_pwr_trace = radio_data_file_adaptive_o['radio_data/1.00/radio_0_pwr_trace']
adaptive_o_r1_pwr_trace = radio_data_file_adaptive_o['radio_data/1.00/radio_1_pwr_trace']
decayed_o_use_trace = radio_data_file_adaptive_o['radio_data/0.00/radio_use_trace']
decayed_o_r0_pwr_trace = radio_data_file_adaptive_o['radio_data/0.00/radio_0_pwr_trace']
decayed_o_r1_pwr_trace = radio_data_file_adaptive_o['radio_data/0.00/radio_1_pwr_trace']

print('getting adaptive actions')
adaptive_o_actions = radio_data_to_actions(adaptive_o_use_trace, adaptive_o_r0_pwr_trace, adaptive_o_r1_pwr_trace, 20000, 100)
print('getting decayed actions')
decayed_o_actions = radio_data_to_actions(decayed_o_use_trace, decayed_o_r0_pwr_trace, decayed_o_r1_pwr_trace, 20000, 100)
'''


##
#
#
# w1 = 0.5

opt, adaptive, decayed = get_actions('q_0.50')
#print(opt.shape, adaptive.shape)
a_q, a_report_q, a_mcc_q = gen_confusion_matrix(opt[0], adaptive, 11)
d_q, d_report_q, d_mcc_q = gen_confusion_matrix(opt[0], decayed, 11)

opt, adaptive, decayed = get_actions('o_0.50')
#print(opt.shape, adaptive.shape)
a_o, a_report_o, a_mcc_o = gen_confusion_matrix(opt[0], adaptive, 11)
d_o, d_report_o, d_mcc_o = gen_confusion_matrix(opt[0], decayed, 11)

opt, adaptive, decayed = get_actions('p_0.50')
#print(opt.shape, adaptive.shape)
a_p, a_report_p, a_mcc_p = gen_confusion_matrix(opt[0], adaptive, 11)
d_p, d_report_p, d_mcc_p = gen_confusion_matrix(opt[0], decayed, 11)


adptv_mtx = a_q + a_o + a_p
decay_mtx = d_q + d_o + d_p
adptv_mtx = (adptv_mtx/np.sum(adptv_mtx, axis=1).reshape(-1,1))*100
adptv_mtx = np.nan_to_num(adptv_mtx)
decay_mtx = (decay_mtx/np.sum(decay_mtx, axis=1).reshape(-1,1))*100
decay_mtx = np.nan_to_num(decay_mtx)


df_adptv_mtx = pd.DataFrame(adptv_mtx, range(11), range(11))
df_decay_mtx = pd.DataFrame(decay_mtx, range(11), range(11))


labels = StringIO(u'''0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,1,0,0,0,0,0
                      0,0,0,0,0,0,1,0,0,0,0
                      0,0,0,0,0,0,0,1,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,1,0
                      0,0,0,0,0,0,0,0,0,0,1
                      ''')
                      
labels = StringIO(u'''0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,1,0,0,0,0,0
                      0,0,0,0,0,0,1,0,0,0,0
                      0,0,0,0,0,0,0,1,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      ''')                      
  

labels = pd.read_csv(labels, header=None)
annotations = labels.astype(str)
annot_adptv_mtx = np.where(annotations=='1', 'N/A', '      ')
annot_decay_mtx = np.where(annotations=='1', 'N/A', '      ')



opt_action_mask = np.array([1,1,1,1,1,0,0,0,1,1,1]).reshape(-1,1)
adptv_mtx *= opt_action_mask
decay_mtx *= opt_action_mask

for i in range(11):
    for j in range(11):
        if adptv_mtx[i,j] > 0.0:
            #annot_a[i,j] = r'$\textbf{{{:.1f}}}$'.format(a[i,j])
            #print(r'$\textbf{{{:.1f}}}$'.format(a[i,j]))
            annot_adptv_mtx[i,j] = '%.1f' % (adptv_mtx[i,j])
        
        if decay_mtx[i,j] > 0.0:
            #annot_d[i,j] = r'$\textbf{{{:.1f}}}$'.format(d[i,j])
            #annot_d[i,j] = r'$\\textbf{abc}$'
            annot_decay_mtx[i,j] = '%.1f' % (decay_mtx[i,j])

plt_conf_matrix(df_adptv_mtx, annot_adptv_mtx, fname='out/fig_momr_res_sim_perf_05_adptv.pdf')
plt_conf_matrix(df_decay_mtx, annot_decay_mtx, fname='out/fig_momr_res_sim_perf_05_decay.pdf')

print('w1=0.5 stats')
print_conf_stats(adptv_mtx, decay_mtx)

adptv_mtx_05 = copy.deepcopy(adptv_mtx)


#
#
#
# w1 = 0.10
opt, adaptive, decayed = get_actions('q_0.10')
#print(opt.shape, adaptive.shape)
a_q, a_report_q, a_mcc_q = gen_confusion_matrix(opt[0], adaptive, 11)
d_q, d_report_q, d_mcc_q = gen_confusion_matrix(opt[0], decayed, 11)

opt, adaptive, decayed = get_actions('o_0.10')
#print(opt.shape, adaptive.shape)
a_o, a_report_o, a_mcc_o = gen_confusion_matrix(opt[0], adaptive, 11)
d_o, d_report_o, d_mcc_o = gen_confusion_matrix(opt[0], decayed, 11)

opt, adaptive, decayed = get_actions('p_0.10')
#print(opt.shape, adaptive.shape)
a_p, a_report_p, a_mcc_p = gen_confusion_matrix(opt[0], adaptive, 11)
d_p, d_report_p, d_mcc_p = gen_confusion_matrix(opt[0], decayed, 11)


adptv_mtx = a_q + a_o + a_p
decay_mtx = d_q + d_o + d_p
adptv_mtx = (adptv_mtx/np.sum(adptv_mtx, axis=1).reshape(-1,1))*100
adptv_mtx = np.nan_to_num(adptv_mtx)
decay_mtx = (decay_mtx/np.sum(decay_mtx, axis=1).reshape(-1,1))*100
decay_mtx = np.nan_to_num(decay_mtx)


df_adptv_mtx = pd.DataFrame(adptv_mtx, range(11), range(11))
df_decay_mtx = pd.DataFrame(decay_mtx, range(11), range(11))


labels = StringIO(u'''1,0,0,0,0,0,0,0,0,0,0
                      0,1,0,0,0,0,0,0,0,0,0
                      0,0,1,0,0,0,0,0,0,0,0
                      0,0,0,1,0,0,0,0,0,0,0
                      0,0,0,0,1,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      ''')
                      
labels = pd.read_csv(labels, header=None)
annotations = labels.astype(str)
annot_adptv_mtx = np.where(annotations=='1', 'N/A', '      ')
annot_decay_mtx = np.where(annotations=='1', 'N/A', '      ')



opt_action_mask = np.array([0,0,0,0,0,1,1,1,1,1,1]).reshape(-1,1)
adptv_mtx *= opt_action_mask
decay_mtx *= opt_action_mask

for i in range(11):
    for j in range(11):
        if adptv_mtx[i,j] > 0.0:
            #annot_a[i,j] = r'$\textbf{{{:.1f}}}$'.format(a[i,j])
            #print(r'$\textbf{{{:.1f}}}$'.format(a[i,j]))
            annot_adptv_mtx[i,j] = '%.1f' % (adptv_mtx[i,j])
        
        if decay_mtx[i,j] > 0.0:
            #annot_d[i,j] = r'$\textbf{{{:.1f}}}$'.format(d[i,j])
            #annot_d[i,j] = r'$\\textbf{abc}$'
            annot_decay_mtx[i,j] = '%.1f' % (decay_mtx[i,j])

plt_conf_matrix(df_adptv_mtx, annot_adptv_mtx, fname='out/fig_momr_res_sim_perf_01_adptv.pdf')
plt_conf_matrix(df_decay_mtx, annot_decay_mtx, fname='out/fig_momr_res_sim_perf_01_decay.pdf')

print('w1=0.1 stats')
print_conf_stats(adptv_mtx, decay_mtx)

adptv_mtx_01 = copy.deepcopy(adptv_mtx)


#
#
#
# rl single obj - bal
rl_adptv_o_bal = get_spec_actions('rl_adptv_actions_o_bal')
opt_o_bal = get_spec_actions('opt_so_actions_o_bal')

a_o, report_o, a_mcc_o = gen_confusion_matrix(opt_o_bal[0], rl_adptv_o_bal, 11)

rl_adptv_p_bal = get_spec_actions('rl_adptv_actions_p_bal')
opt_p_bal = get_spec_actions('opt_so_actions_p_bal')

a_p, report_p, a_mcc_p = gen_confusion_matrix(opt_p_bal[0], rl_adptv_p_bal, 11)

rl_adptv_q_bal = get_spec_actions('rl_adptv_actions_q_bal')
opt_q_bal = get_spec_actions('opt_so_actions_q_bal')

a_q, report_q, a_mcc_q = gen_confusion_matrix(opt_q_bal[0], rl_adptv_q_bal, 11)


so_mtx = a_q + a_o + a_p
so_mtx = (so_mtx/np.sum(so_mtx, axis=1).reshape(-1,1))*100
so_mtx = np.nan_to_num(so_mtx)

opt_action_mask = np.array([1,1,1,1,1,0,0,0,1,1,1]).reshape(-1,1)
so_mtx *= opt_action_mask

print_conf_stats(adptv_mtx_05, so_mtx, 'MORL-adptv_05', 'RL-adptv_bal')


#
#
#
# rl single obj pwr
rl_adptv_o_pwr = get_spec_actions('rl_adptv_actions_o_pwr')
opt_o_pwr = get_spec_actions('opt_so_actions_o_pwr')

a_o, report_o, a_mcc_o = gen_confusion_matrix(opt_o_pwr[0], rl_adptv_o_pwr, 11)

rl_adptv_p_pwr = get_spec_actions('rl_adptv_actions_p_pwr')
opt_p_pwr = get_spec_actions('opt_so_actions_p_pwr')

a_p, report_p, a_mcc_p = gen_confusion_matrix(opt_p_pwr[0], rl_adptv_p_pwr, 11)

rl_adptv_q_pwr = get_spec_actions('rl_adptv_actions_q_pwr')
opt_q_pwr = get_spec_actions('opt_so_actions_q_pwr')

a_q, report_q, a_mcc_q = gen_confusion_matrix(opt_q_pwr[0], rl_adptv_q_pwr, 11)


so_mtx = a_q + a_o + a_p
so_mtx = (so_mtx/np.sum(so_mtx, axis=1).reshape(-1,1))*100
so_mtx = np.nan_to_num(so_mtx)

opt_action_mask = np.array([0,0,0,0,0,1,1,1,1,1,1]).reshape(-1,1)
so_mtx *= opt_action_mask

print_conf_stats(adptv_mtx_01, so_mtx, 'MORL-adptv_01', 'RL-adptv_pwr')
