from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize

from res_util import plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv, gen_confusion_matrix
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
from thesis_util import *



import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 12,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


DATA_DIR = '../../exps/data/thesis_ch3/policies/'


O = 0
Q = 1

def load_radio_data_for_param(radio_data_list, param):
    num_file = len(radio_data_list)
    data = []
    for rd in radio_data_list:
        radio_use_trace = rd['radio_data/'+param+'/radio_use_trace'][()]
        r0_pwr_trace = rd['radio_data/'+param+'/radio_0_pwr_trace'][()]
        r1_pwr_trace = rd['radio_data/'+param+'/radio_1_pwr_trace'][()]
        
        data.append([radio_use_trace, r0_pwr_trace, r1_pwr_trace])
        
    return data

def get_radio_data_list_hist(radio_data_list, path_len, param, items=None):
    hist = []
    if items == None:
        for rd in radio_data_list:
            hist.append(get_radio_data_hist(rd, path_len, param))
    else:
        for rdi in items:
            hist.append(get_radio_data_hist(radio_data_list[rdi], path_len, param))
            
    
    return hist

rl_agent_adptv_rw_sweep_desc_fname = DATA_DIR+'rl_adptv_eps/05052021/rl_agent_rw_sweep.csv'
rl_agent_decay_rw_sweep_desc_fname = DATA_DIR+'rl_decay_eps/rl_agent_rw_sweep.csv'
fuzzy_agent_tuned_set_sweep_desc_fname = DATA_DIR+'fuzzy_tuned_set/fuzzy_agent_set_sweep.csv'
opt_agent_obj_sweep_desc_fname = DATA_DIR+'opt/opt_agent_obj_sweep.csv'

rl_agent_adptv_rw_sweep_data_desc = load_res_data_descriptor(rl_agent_adptv_rw_sweep_desc_fname)
rl_agent_decay_rw_sweep_data_desc = load_res_data_descriptor(rl_agent_decay_rw_sweep_desc_fname)
fuzzy_agent_tuned_set_sweep_data_desc = load_res_data_descriptor(fuzzy_agent_tuned_set_sweep_desc_fname)
opt_agent_obj_sweep_data_desc = load_res_data_descriptor(opt_agent_obj_sweep_desc_fname)


rl_agent_adptv_rw_sweep_data, rl_agent_adptv_rw_sweep_radio_data = load_res_data(DATA_DIR+'rl_adptv_eps/05052021/', rl_agent_adptv_rw_sweep_data_desc, 'rl_agent', 'rw_sweep')
rl_agent_decay_rw_sweep_data, rl_agent_decay_rw_sweep_radio_data = load_res_data(DATA_DIR+'rl_decay_eps/', rl_agent_decay_rw_sweep_data_desc, 'rl_agent', 'rw_sweep')
fuzzy_agent_tuned_set_sweep_data, fuzzy_agent_tuned_set_sweep_radio_data = load_res_data(DATA_DIR+'fuzzy_tuned_set/', fuzzy_agent_tuned_set_sweep_data_desc, 'fuzzy_agent', 'set_sweep')
opt_agent_obj_sweep_data, opt_agent_obj_sweep_radio_data = load_res_data(DATA_DIR+'opt/', opt_agent_obj_sweep_data_desc, 'opt_agent', 'obj_sweep')

'''
#
# RL Adaptive
#
rl_agent_adptv_hist_br = get_radio_data_list_hist(rl_agent_adptv_rw_sweep_radio_data, 20000, '0.00', [1,5])
rl_agent_adptv_hist_pwr = get_radio_data_list_hist(rl_agent_adptv_rw_sweep_radio_data, 20000, '1.00', [1,5])
rl_agent_adptv_hist_bal = get_radio_data_list_hist(rl_agent_adptv_rw_sweep_radio_data, 20000, '2.00', [1,5])

plt_radio_data_hist(rl_agent_adptv_hist_br[O], fname='out/imr_res_policy_rl_adptv_o_br_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_pwr[O], fname='out/imr_res_policy_rl_adptv_o_pwr_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_bal[O], fname='out/imr_res_policy_rl_adptv_o_bal_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_br[Q], fname='out/imr_res_policy_rl_adptv_q_br_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_pwr[Q], fname='out/imr_res_policy_rl_adptv_q_pwr_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_bal[Q], fname='out/imr_res_policy_rl_adptv_q_bal_hard.pdf', num_runs=100)
#
#


#
# RL Decay
#
rl_agent_decay_hist_br = get_radio_data_list_hist(rl_agent_decay_rw_sweep_radio_data, 20000, '0.00', [1,5])
rl_agent_decay_hist_pwr = get_radio_data_list_hist(rl_agent_decay_rw_sweep_radio_data, 20000, '1.00', [1,5])
rl_agent_decay_hist_bal = get_radio_data_list_hist(rl_agent_decay_rw_sweep_radio_data, 20000, '2.00', [1,5])

plt_radio_data_hist(rl_agent_decay_hist_br[O], fname='out/imr_res_policy_rl_decay_o_br_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_decay_hist_pwr[O], fname='out/imr_res_policy_rl_decay_o_pwr_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_decay_hist_bal[O], fname='out/imr_res_policy_rl_decay_o_bal_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_decay_hist_br[Q], fname='out/imr_res_policy_rl_decay_q_br_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_decay_hist_pwr[Q], fname='out/imr_res_policy_rl_decay_q_pwr_hard.pdf', num_runs=100)
plt_radio_data_hist(rl_agent_decay_hist_bal[Q], fname='out/imr_res_policy_rl_decay_q_bal_hard.pdf', num_runs=100)
#
#


#
# Fuzzy - tuned
#
fuzzy_agent_hist_br = get_radio_data_list_hist(fuzzy_agent_tuned_set_sweep_radio_data, 20000, '2.00', [1,5])
fuzzy_agent_hist_pwr = get_radio_data_list_hist(fuzzy_agent_tuned_set_sweep_radio_data, 20000, '1.00', [1,5])
fuzzy_agent_hist_bal = get_radio_data_list_hist(fuzzy_agent_tuned_set_sweep_radio_data, 20000, '0.00', [1,5])

plt_radio_data_hist(fuzzy_agent_hist_br[O], fname='out/imr_res_policy_fuzzy_o_br_hard.pdf', num_runs=100)
plt_radio_data_hist(fuzzy_agent_hist_pwr[O], fname='out/imr_res_policy_fuzzy_o_pwr_hard.pdf', num_runs=100)
plt_radio_data_hist(fuzzy_agent_hist_bal[O], fname='out/imr_res_policy_fuzzy_o_bal_hard.pdf', num_runs=100)
plt_radio_data_hist(fuzzy_agent_hist_br[Q], fname='out/imr_res_policy_fuzzy_q_br_hard.pdf', num_runs=100)
plt_radio_data_hist(fuzzy_agent_hist_pwr[Q], fname='out/imr_res_policy_fuzzy_q_pwr_hard.pdf', num_runs=100)
plt_radio_data_hist(fuzzy_agent_hist_bal[Q], fname='out/imr_res_policy_fuzzy_q_bal_hard.pdf', num_runs=100)

#
#

#
#
#
opt_agent_hist_br = get_radio_data_list_hist(opt_agent_obj_sweep_radio_data, 20000, '0.00', [1,5])
opt_agent_hist_pwr = get_radio_data_list_hist(opt_agent_obj_sweep_radio_data, 20000, '1.00', [1,5])
opt_agent_hist_bal = get_radio_data_list_hist(opt_agent_obj_sweep_radio_data, 20000, '2.00', [1,5])

plt_radio_data_hist(opt_agent_hist_br[O], fname='out/imr_res_policy_opt_o_br_hard.pdf', num_runs=1)
plt_radio_data_hist(opt_agent_hist_pwr[O], fname='out/imr_res_policy_opt_o_pwr_hard.pdf', num_runs=1)
plt_radio_data_hist(opt_agent_hist_bal[O], fname='out/imr_res_policy_opt_o_bal_hard.pdf', num_runs=1)
plt_radio_data_hist(opt_agent_hist_br[Q], fname='out/imr_res_policy_opt_q_br_hard.pdf', num_runs=1)
plt_radio_data_hist(opt_agent_hist_pwr[Q], fname='out/imr_res_policy_opt_q_pwr_hard.pdf', num_runs=1)
plt_radio_data_hist(opt_agent_hist_bal[Q], fname='out/imr_res_policy_opt_q_bal_hard.pdf', num_runs=1)
#
#
'''


rl_agent_adptv_hist_br = get_radio_data_list_hist(rl_agent_adptv_rw_sweep_radio_data, 20000, '0.00', [1,5])
rl_agent_adptv_hist_pwr = get_radio_data_list_hist(rl_agent_adptv_rw_sweep_radio_data, 20000, '1.00', [1,5])
rl_agent_adptv_hist_bal = get_radio_data_list_hist(rl_agent_adptv_rw_sweep_radio_data, 20000, '2.00', [1,5])

plt_radio_data_hist(rl_agent_adptv_hist_br[O], fname=None, num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_pwr[O], fname=None, num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_bal[O], fname=None, num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_br[Q], fname=None, num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_pwr[Q], fname=None, num_runs=100)
plt_radio_data_hist(rl_agent_adptv_hist_bal[Q], fname=None, num_runs=100)

for d, rd in zip(rl_agent_adptv_rw_sweep_data, rl_agent_adptv_rw_sweep_radio_data):
    d.close()
    rd.close()

for d, rd in zip(rl_agent_decay_rw_sweep_data, rl_agent_decay_rw_sweep_radio_data):
    d.close()
    rd.close()
    
for d, rd, in zip(fuzzy_agent_tuned_set_sweep_data, fuzzy_agent_tuned_set_sweep_radio_data):
    d.close()
    rd.close()

for d, rd, in zip(opt_agent_obj_sweep_data, opt_agent_obj_sweep_radio_data):
    d.close()
    rd.close()

