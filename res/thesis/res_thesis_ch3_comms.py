from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize

from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv, gen_confusion_matrix
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
from thesis_util import *

import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 12,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


DATA_DIR = '../../exps/data/thesis_ch3/'



def get_data_params(data_file_list):
    num_files = len(data_file_list)
    goodput_mean = [data_file_list[i]['data/4.00/goodput'][()].mean()/1000000 for i in range(num_files)]
    plr_mean = [data_file_list[i]['data/4.00/pkt_loss_rate'][()].mean() for i in range(num_files)]
    pwr_mean = [data_file_list[i]['data/4.00/tot_pwr'][()].mean() for i in range(num_files)]
    
    
    goodput = np.array([data_file_list[i]['data/4.00/goodput'][()]/1000000 for i in range(num_files)]).squeeze()
    plr = np.array([data_file_list[i]['data/4.00/pkt_loss_rate'][()] for i in range(num_files)]).squeeze()
    pwr = np.array([data_file_list[i]['data/4.00/tot_pwr'][()] for i in range(num_files)]).squeeze()
    return goodput, plr, pwr


def extract_easy_data(data):
    return [[data[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]

def extract_hard_data(data):
    return [[data[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]


rl_agent_adptv_app_sweep_desc_fname = DATA_DIR+'rl_adptv_eps/rl_agent_app_sweep.csv'
rl_agent_decay_app_sweep_desc_fname = DATA_DIR+'rl_decay_eps/rl_agent_app_sweep.csv'
fuzzy_agent_tuned_app_sweep_desc_fname = DATA_DIR+'fuzzy_tuned_set/fuzzy_agent_app_sweep.csv'
fuzzy_agent_arb_app_sweep_desc_fname = DATA_DIR+'fuzzy_arb_set/fuzzy_agent_app_sweep.csv'
fixed_agent_app_sweep_desc_fname = DATA_DIR+'fixed2/fixed_agent_app_sweep.csv'

rl_agent_adptv_app_sweep_data_desc = load_res_data_descriptor(rl_agent_adptv_app_sweep_desc_fname)
rl_agent_decay_app_sweep_data_desc = load_res_data_descriptor(rl_agent_decay_app_sweep_desc_fname)
fuzzy_agent_tuned_app_sweep_desc = load_res_data_descriptor(fuzzy_agent_tuned_app_sweep_desc_fname)
fuzzy_agent_arb_app_sweep_desc = load_res_data_descriptor(fuzzy_agent_arb_app_sweep_desc_fname)
fixed_agent_app_sweep_desc = load_res_data_descriptor(fixed_agent_app_sweep_desc_fname)
#print(rl_agent_app_sweep_data_desc)

rl_agent_decay_app_sweep_data, rl_agent_decay_app_sweep_radio_data = load_res_data(DATA_DIR+'rl_decay_eps/', rl_agent_decay_app_sweep_data_desc, 'rl_agent', 'app_sweep')
rl_agent_adptv_app_sweep_data, rl_agent_adptv_app_sweep_radio_data = load_res_data(DATA_DIR+'rl_adptv_eps/', rl_agent_adptv_app_sweep_data_desc, 'rl_agent', 'app_sweep')
fuzzy_agent_tuned_app_sweep_data, fuzzy_agent_tuned_app_sweep_radio_data = load_res_data(DATA_DIR+'fuzzy_tuned_set/', fuzzy_agent_tuned_app_sweep_desc, 'fuzzy_agent', 'app_sweep')
fuzzy_agent_arb_app_sweep_data, fuzzy_agent_arb_app_sweep_radio_data = load_res_data(DATA_DIR+'fuzzy_arb_set/', fuzzy_agent_arb_app_sweep_desc, 'fuzzy_agent', 'app_sweep')
fixed_agent_app_sweep_data, fixed_agent_app_sweep_radio_data = load_res_data(DATA_DIR+'fixed2/', fixed_agent_app_sweep_desc, 'fixed_agent', 'app_sweep')

#print(rl_agent_app_sweep_data[0]['data/1.00/pkt_loss_rate'][()].mean())
rl_adptv_goodput, rl_adptv_plr, rl_adptv_pwr = get_data_params(rl_agent_adptv_app_sweep_data)
rl_decay_goodput, rl_decay_plr, rl_decay_pwr = get_data_params(rl_agent_decay_app_sweep_data)
fuzzy_tuned_goodput, fuzzy_tuned_plr, fuzzy_tuned_pwr = get_data_params(fuzzy_agent_tuned_app_sweep_data)
fuzzy_arb_goodput, fuzzy_arb_plr, fuzzy_arb_pwr = get_data_params(fuzzy_agent_arb_app_sweep_data)
fixed_goodput, fixed_plr, fixed_pwr = get_data_params(fixed_agent_app_sweep_data)

'''
rl_goodput_easy = [[rl_goodput[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]
rl_goodput_hard = [[rl_goodput[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]

rl_plr_easy = [[rl_plr[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]
rl_plr_hard = [[rl_plr[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]

rl_pwr_easy = [[rl_pwr[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]
rl_pwr_hard = [[rl_pwr[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]

fuzzy_goodput_easy = [[fuzzy_goodput[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]
fuzzy_goodput_hard = [[fuzzy_goodput[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]

fuzzy_plr_easy = [[fuzzy_plr[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]
fuzzy_plr_hard = [[fuzzy_plr[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]

fuzzy_pwr_easy = [[fuzzy_pwr[j] for j in range(i*4, (i*4)+4)] for i in range(0,6, 2)]
fuzzy_pwr_hard = [[fuzzy_pwr[j] for j in range(i*4, (i*4)+4)] for i in range(1,7, 2)]
'''

rl_adptv_goodput_easy = extract_easy_data(rl_adptv_goodput)
rl_decay_goodput_easy = extract_easy_data(rl_decay_goodput)
fuzzy_tuned_goodput_easy = extract_easy_data(fuzzy_tuned_goodput)
fuzzy_arb_goodput_easy = extract_easy_data(fuzzy_arb_goodput)
fixed_goodput_easy = extract_easy_data(fixed_goodput)

rl_adptv_goodput_hard = extract_hard_data(rl_adptv_goodput)
rl_decay_goodput_hard = extract_hard_data(rl_decay_goodput)
fuzzy_tuned_goodput_hard = extract_hard_data(fuzzy_tuned_goodput)
fuzzy_arb_goodput_hard = extract_hard_data(fuzzy_arb_goodput)
fixed_goodput_hard = extract_hard_data(fixed_goodput)

rl_adptv_plr_easy = extract_easy_data(rl_adptv_plr)
rl_decay_plr_easy = extract_easy_data(rl_decay_plr)
fuzzy_tuned_plr_easy = extract_easy_data(fuzzy_tuned_plr)
fuzzy_arb_plr_easy = extract_easy_data(fuzzy_arb_plr)
fixed_plr_easy = extract_easy_data(fixed_plr)

rl_adptv_plr_hard = extract_hard_data(rl_adptv_plr)
rl_decay_plr_hard = extract_hard_data(rl_decay_plr)
fuzzy_tuned_plr_hard = extract_hard_data(fuzzy_tuned_plr)
fuzzy_arb_plr_hard = extract_hard_data(fuzzy_arb_plr)
fixed_plr_hard = extract_hard_data(fixed_plr)

rl_adptv_pwr_easy = extract_easy_data(rl_adptv_pwr)
rl_decay_pwr_easy = extract_easy_data(rl_decay_pwr)
fuzzy_tuned_pwr_easy = extract_easy_data(fuzzy_tuned_pwr)
fuzzy_arb_pwr_easy = extract_easy_data(fuzzy_arb_pwr)
fixed_pwr_easy = extract_easy_data(fixed_pwr)

rl_adptv_pwr_hard = extract_hard_data(rl_adptv_pwr)
rl_decay_pwr_hard = extract_hard_data(rl_decay_pwr)
fuzzy_tuned_pwr_hard = extract_hard_data(fuzzy_tuned_pwr)
fuzzy_arb_pwr_hard = extract_hard_data(fuzzy_arb_pwr)
fixed_pwr_hard = extract_hard_data(fixed_pwr)

#rl_adptv_goodput_easy = np.array(rl_adptv_goodput_easy).squeeze()

num = 100
shd_std = np.array([0, 4, 7, 12])
env_o = np.full(num, 'o')
env_p = np.full(num, 'p')
env_q = np.full(num, 'q')
ple_easy = np.full(num, 'easy')
ple_hard = np.full(num, 'hard')
rl_adptv =  np.full(num, 'RL-adaptive')
rl_decay = np.full(num, 'RL-decayed')
fuzzy_tuned = np.full(num, 'Fuzzy-tuned')
fuzzy_arb = np.full(num, 'Fuzzy-arbitrary')
fixed = np.full(num, 'Static')
goodput = np.full(num, 'goodput')
plr = np.full(num, 'plr')
pwr = np.full(num, 'pwr')

data = []

for s in range(len(shd_std)):
    shd = np.full(100, shd_std[s])
    
    #data.append(np.vstack((shd, rl_adptv_goodput_easy[0][s], env_o, ple_easy, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_adptv_goodput_easy[0][s], env_o, ple_easy, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_adptv_goodput_easy[1][s], env_p, ple_easy, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_adptv_goodput_easy[2][s], env_q, ple_easy, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_decay_goodput_easy[0][s], env_o, ple_easy, rl_decay, goodput)).T)
    data.append(np.vstack((shd, rl_decay_goodput_easy[1][s], env_p, ple_easy, rl_decay, goodput)).T)
    data.append(np.vstack((shd, rl_decay_goodput_easy[2][s], env_q, ple_easy, rl_decay, goodput)).T)
    
    data.append(np.vstack((shd, rl_adptv_goodput_hard[0][s], env_o, ple_hard, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_adptv_goodput_hard[1][s], env_p, ple_hard, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_adptv_goodput_hard[2][s], env_q, ple_hard, rl_adptv, goodput)).T)
    data.append(np.vstack((shd, rl_decay_goodput_hard[0][s], env_o, ple_hard, rl_decay, goodput)).T)
    data.append(np.vstack((shd, rl_decay_goodput_hard[1][s], env_p, ple_hard, rl_decay, goodput)).T)
    data.append(np.vstack((shd, rl_decay_goodput_hard[2][s], env_q, ple_hard, rl_decay, goodput)).T)
    
    
    data.append(np.vstack((shd, fuzzy_tuned_goodput_easy[0][s], env_o, ple_easy, fuzzy_tuned, goodput)).T)
    data.append(np.vstack((shd, fuzzy_tuned_goodput_easy[1][s], env_p, ple_easy, fuzzy_tuned, goodput)).T)
    data.append(np.vstack((shd, fuzzy_tuned_goodput_easy[2][s], env_q, ple_easy, fuzzy_tuned, goodput)).T)
    data.append(np.vstack((shd, fuzzy_arb_goodput_easy[0][s], env_o, ple_easy, fuzzy_arb, goodput)).T)
    data.append(np.vstack((shd, fuzzy_arb_goodput_easy[1][s], env_p, ple_easy, fuzzy_arb, goodput)).T)
    data.append(np.vstack((shd, fuzzy_arb_goodput_easy[2][s], env_q, ple_easy, fuzzy_arb, goodput)).T)
    
    data.append(np.vstack((shd, fuzzy_tuned_goodput_hard[0][s], env_o, ple_hard, fuzzy_tuned, goodput)).T)
    data.append(np.vstack((shd, fuzzy_tuned_goodput_hard[1][s], env_p, ple_hard, fuzzy_tuned, goodput)).T)
    data.append(np.vstack((shd, fuzzy_tuned_goodput_hard[2][s], env_q, ple_hard, fuzzy_tuned, goodput)).T)
    data.append(np.vstack((shd, fuzzy_arb_goodput_hard[0][s], env_o, ple_hard, fuzzy_arb, goodput)).T)
    data.append(np.vstack((shd, fuzzy_arb_goodput_hard[1][s], env_p, ple_hard, fuzzy_arb, goodput)).T)
    data.append(np.vstack((shd, fuzzy_arb_goodput_hard[2][s], env_q, ple_hard, fuzzy_arb, goodput)).T)
    
   

    
    data.append(np.vstack((shd, rl_adptv_plr_easy[0][s], env_o, ple_easy, rl_adptv, plr)).T)
    data.append(np.vstack((shd, rl_adptv_plr_easy[1][s], env_p, ple_easy, rl_adptv, plr)).T)
    data.append(np.vstack((shd, rl_adptv_plr_easy[2][s], env_q, ple_easy, rl_adptv, plr)).T)
    data.append(np.vstack((shd, rl_decay_plr_easy[0][s], env_o, ple_easy, rl_decay, plr)).T)
    data.append(np.vstack((shd, rl_decay_plr_easy[1][s], env_p, ple_easy, rl_decay, plr)).T)
    data.append(np.vstack((shd, rl_decay_plr_easy[2][s], env_q, ple_easy, rl_decay, plr)).T)
    
    data.append(np.vstack((shd, rl_adptv_plr_hard[0][s], env_o, ple_hard, rl_adptv, plr)).T)
    data.append(np.vstack((shd, rl_adptv_plr_hard[1][s], env_p, ple_hard, rl_adptv, plr)).T)
    data.append(np.vstack((shd, rl_adptv_plr_hard[2][s], env_q, ple_hard, rl_adptv, plr)).T)
    data.append(np.vstack((shd, rl_decay_plr_hard[0][s], env_o, ple_hard, rl_decay, plr)).T)
    data.append(np.vstack((shd, rl_decay_plr_hard[1][s], env_p, ple_hard, rl_decay, plr)).T)
    data.append(np.vstack((shd, rl_decay_plr_hard[2][s], env_q, ple_hard, rl_decay, plr)).T)
    
    
    data.append(np.vstack((shd, fuzzy_tuned_plr_easy[0][s], env_o, ple_easy, fuzzy_tuned, plr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_plr_easy[1][s], env_p, ple_easy, fuzzy_tuned, plr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_plr_easy[2][s], env_q, ple_easy, fuzzy_tuned, plr)).T)
    data.append(np.vstack((shd, fuzzy_arb_plr_easy[0][s], env_o, ple_easy, fuzzy_arb, plr)).T)
    data.append(np.vstack((shd, fuzzy_arb_plr_easy[1][s], env_p, ple_easy, fuzzy_arb, plr)).T)
    data.append(np.vstack((shd, fuzzy_arb_plr_easy[2][s], env_q, ple_easy, fuzzy_arb, plr)).T)
    
    data.append(np.vstack((shd, fuzzy_tuned_plr_hard[0][s], env_o, ple_hard, fuzzy_tuned, plr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_plr_hard[1][s], env_p, ple_hard, fuzzy_tuned, plr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_plr_hard[2][s], env_q, ple_hard, fuzzy_tuned, plr)).T)
    data.append(np.vstack((shd, fuzzy_arb_plr_hard[0][s], env_o, ple_hard, fuzzy_arb, plr)).T)
    data.append(np.vstack((shd, fuzzy_arb_plr_hard[1][s], env_p, ple_hard, fuzzy_arb, plr)).T)
    data.append(np.vstack((shd, fuzzy_arb_plr_hard[2][s], env_q, ple_hard, fuzzy_arb, plr)).T)
    
    
    data.append(np.vstack((shd, rl_adptv_pwr_easy[0][s], env_o, ple_easy, rl_adptv, pwr)).T)
    data.append(np.vstack((shd, rl_adptv_pwr_easy[1][s], env_p, ple_easy, rl_adptv, pwr)).T)
    data.append(np.vstack((shd, rl_adptv_pwr_easy[2][s], env_q, ple_easy, rl_adptv, pwr)).T)
    data.append(np.vstack((shd, rl_decay_pwr_easy[0][s], env_o, ple_easy, rl_decay, pwr)).T)
    data.append(np.vstack((shd, rl_decay_pwr_easy[1][s], env_p, ple_easy, rl_decay, pwr)).T)
    data.append(np.vstack((shd, rl_decay_pwr_easy[2][s], env_q, ple_easy, rl_decay, pwr)).T)
    
    data.append(np.vstack((shd, rl_adptv_pwr_hard[0][s], env_o, ple_hard, rl_adptv, pwr)).T)
    data.append(np.vstack((shd, rl_adptv_pwr_hard[1][s], env_p, ple_hard, rl_adptv, pwr)).T)
    data.append(np.vstack((shd, rl_adptv_pwr_hard[2][s], env_q, ple_hard, rl_adptv, pwr)).T)
    data.append(np.vstack((shd, rl_decay_pwr_hard[0][s], env_o, ple_hard, rl_decay, pwr)).T)
    data.append(np.vstack((shd, rl_decay_pwr_hard[1][s], env_p, ple_hard, rl_decay, pwr)).T)
    data.append(np.vstack((shd, rl_decay_pwr_hard[2][s], env_q, ple_hard, rl_decay, pwr)).T)
    
    
    data.append(np.vstack((shd, fuzzy_tuned_pwr_easy[0][s], env_o, ple_easy, fuzzy_tuned, pwr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_pwr_easy[1][s], env_p, ple_easy, fuzzy_tuned, pwr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_pwr_easy[2][s], env_q, ple_easy, fuzzy_tuned, pwr)).T)
    data.append(np.vstack((shd, fuzzy_arb_pwr_easy[0][s], env_o, ple_easy, fuzzy_arb, pwr)).T)
    data.append(np.vstack((shd, fuzzy_arb_pwr_easy[1][s], env_p, ple_easy, fuzzy_arb, pwr)).T)
    data.append(np.vstack((shd, fuzzy_arb_pwr_easy[2][s], env_q, ple_easy, fuzzy_arb, pwr)).T)
    
    data.append(np.vstack((shd, fuzzy_tuned_pwr_hard[0][s], env_o, ple_hard, fuzzy_tuned, pwr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_pwr_hard[1][s], env_p, ple_hard, fuzzy_tuned, pwr)).T)
    data.append(np.vstack((shd, fuzzy_tuned_pwr_hard[2][s], env_q, ple_hard, fuzzy_tuned, pwr)).T)
    data.append(np.vstack((shd, fuzzy_arb_pwr_hard[0][s], env_o, ple_hard, fuzzy_arb, pwr)).T)
    data.append(np.vstack((shd, fuzzy_arb_pwr_hard[1][s], env_p, ple_hard, fuzzy_arb, pwr)).T)
    data.append(np.vstack((shd, fuzzy_arb_pwr_hard[2][s], env_q, ple_hard, fuzzy_arb, pwr)).T)
    
    
num = 50
#shd_std = np.array([0, 4, 7, 12])
env_o = np.full(num, 'o')
env_p = np.full(num, 'p')
env_q = np.full(num, 'q')
ple_easy = np.full(num, 'easy')
ple_hard = np.full(num, 'hard')
rl_adptv =  np.full(num, 'RL-adaptive')
rl_decay = np.full(num, 'RL-decayed')
fuzzy_tuned = np.full(num, 'Fuzzy-tuned')
fuzzy_arb = np.full(num, 'Fuzzy-arbitrary')
fixed = np.full(num, 'Static')
goodput = np.full(num, 'goodput')
plr = np.full(num, 'plr')
pwr = np.full(num, 'pwr')


for s in range(len(shd_std)):
    shd = np.full(50, shd_std[s])

    data.append(np.vstack((shd, fixed_goodput_easy[0][s], env_o, ple_easy, fixed, goodput)).T)
    data.append(np.vstack((shd, fixed_goodput_easy[1][s], env_p, ple_easy, fixed, goodput)).T)
    data.append(np.vstack((shd, fixed_goodput_easy[2][s], env_q, ple_easy, fixed, goodput)).T)
    data.append(np.vstack((shd, fixed_goodput_hard[0][s], env_o, ple_hard, fixed, goodput)).T)
    data.append(np.vstack((shd, fixed_goodput_hard[1][s], env_p, ple_hard, fixed, goodput)).T)
    data.append(np.vstack((shd, fixed_goodput_hard[2][s], env_q, ple_hard, fixed, goodput)).T)
    
    data.append(np.vstack((shd, fixed_plr_easy[0][s], env_o, ple_easy, fixed, plr)).T)
    data.append(np.vstack((shd, fixed_plr_easy[1][s], env_p, ple_easy, fixed, plr)).T)
    data.append(np.vstack((shd, fixed_plr_easy[2][s], env_q, ple_easy, fixed, plr)).T)
    data.append(np.vstack((shd, fixed_plr_hard[0][s], env_o, ple_hard, fixed, plr)).T)
    data.append(np.vstack((shd, fixed_plr_hard[1][s], env_p, ple_hard, fixed, plr)).T)
    data.append(np.vstack((shd, fixed_plr_hard[2][s], env_q, ple_hard, fixed, plr)).T)
    
    data.append(np.vstack((shd, fixed_pwr_easy[0][s], env_o, ple_easy, fixed, pwr)).T)
    data.append(np.vstack((shd, fixed_pwr_easy[1][s], env_p, ple_easy, fixed, pwr)).T)
    data.append(np.vstack((shd, fixed_pwr_easy[2][s], env_q, ple_easy, fixed, pwr)).T)
    data.append(np.vstack((shd, fixed_pwr_hard[0][s], env_o, ple_hard, fixed, pwr)).T)
    data.append(np.vstack((shd, fixed_pwr_hard[1][s], env_p, ple_hard, fixed, pwr)).T)
    data.append(np.vstack((shd, fixed_pwr_hard[2][s], env_q, ple_hard, fixed, pwr)).T)
    
'''
data.append(np.vstack((shd_std, rl_adptv_goodput_easy[0], env_o, ple_easy, rl_adptv, goodput)).T)
data.append(np.vstack((shd_std, rl_adptv_goodput_easy[1], env_p, ple_easy, rl_adptv, goodput)).T)
data.append(np.vstack((shd_std, rl_adptv_goodput_easy[2], env_q, ple_easy, rl_adptv, goodput)).T)
data.append(np.vstack((shd_std, rl_decay_goodput_easy[0], env_o, ple_easy, rl_decay, goodput)).T)
data.append(np.vstack((shd_std, rl_decay_goodput_easy[1], env_p, ple_easy, rl_decay, goodput)).T)
data.append(np.vstack((shd_std, rl_decay_goodput_easy[2], env_q, ple_easy, rl_decay, goodput)).T)

data.append(np.vstack((shd_std, rl_adptv_goodput_hard[0], env_o, ple_hard, rl_adptv, goodput)).T)
data.append(np.vstack((shd_std, rl_adptv_goodput_hard[1], env_p, ple_hard, rl_adptv, goodput)).T)
data.append(np.vstack((shd_std, rl_adptv_goodput_hard[2], env_q, ple_hard, rl_adptv, goodput)).T)
data.append(np.vstack((shd_std, rl_decay_goodput_hard[0], env_o, ple_hard, rl_decay, goodput)).T)
data.append(np.vstack((shd_std, rl_decay_goodput_hard[1], env_p, ple_hard, rl_decay, goodput)).T)
data.append(np.vstack((shd_std, rl_decay_goodput_hard[2], env_q, ple_hard, rl_decay, goodput)).T)


data.append(np.vstack((shd_std, fuzzy_tuned_goodput_easy[0], env_o, ple_easy, fuzzy_tuned, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_goodput_easy[1], env_p, ple_easy, fuzzy_tuned, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_goodput_easy[2], env_q, ple_easy, fuzzy_tuned, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_arb_goodput_easy[0], env_o, ple_easy, fuzzy_arb, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_arb_goodput_easy[1], env_p, ple_easy, fuzzy_arb, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_arb_goodput_easy[2], env_q, ple_easy, fuzzy_arb, goodput)).T)

data.append(np.vstack((shd_std, fuzzy_tuned_goodput_hard[0], env_o, ple_hard, fuzzy_tuned, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_goodput_hard[1], env_p, ple_hard, fuzzy_tuned, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_goodput_hard[2], env_q, ple_hard, fuzzy_tuned, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_arb_goodput_hard[0], env_o, ple_hard, fuzzy_arb, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_arb_goodput_hard[1], env_p, ple_hard, fuzzy_arb, goodput)).T)
data.append(np.vstack((shd_std, fuzzy_arb_goodput_hard[2], env_q, ple_hard, fuzzy_arb, goodput)).T)



data.append(np.vstack((shd_std, rl_adptv_plr_easy[0], env_o, ple_easy, rl_adptv, plr)).T)
data.append(np.vstack((shd_std, rl_adptv_plr_easy[1], env_p, ple_easy, rl_adptv, plr)).T)
data.append(np.vstack((shd_std, rl_adptv_plr_easy[2], env_q, ple_easy, rl_adptv, plr)).T)
data.append(np.vstack((shd_std, rl_decay_plr_easy[0], env_o, ple_easy, rl_decay, plr)).T)
data.append(np.vstack((shd_std, rl_decay_plr_easy[1], env_p, ple_easy, rl_decay, plr)).T)
data.append(np.vstack((shd_std, rl_decay_plr_easy[2], env_q, ple_easy, rl_decay, plr)).T)

data.append(np.vstack((shd_std, rl_adptv_plr_hard[0], env_o, ple_hard, rl_adptv, plr)).T)
data.append(np.vstack((shd_std, rl_adptv_plr_hard[1], env_p, ple_hard, rl_adptv, plr)).T)
data.append(np.vstack((shd_std, rl_adptv_plr_hard[2], env_q, ple_hard, rl_adptv, plr)).T)
data.append(np.vstack((shd_std, rl_decay_plr_hard[0], env_o, ple_hard, rl_decay, plr)).T)
data.append(np.vstack((shd_std, rl_decay_plr_hard[1], env_p, ple_hard, rl_decay, plr)).T)
data.append(np.vstack((shd_std, rl_decay_plr_hard[2], env_q, ple_hard, rl_decay, plr)).T)


data.append(np.vstack((shd_std, fuzzy_tuned_plr_easy[0], env_o, ple_easy, fuzzy_tuned, plr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_plr_easy[1], env_p, ple_easy, fuzzy_tuned, plr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_plr_easy[2], env_q, ple_easy, fuzzy_tuned, plr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_plr_easy[0], env_o, ple_easy, fuzzy_arb, plr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_plr_easy[1], env_p, ple_easy, fuzzy_arb, plr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_plr_easy[2], env_q, ple_easy, fuzzy_arb, plr)).T)

data.append(np.vstack((shd_std, fuzzy_tuned_plr_hard[0], env_o, ple_hard, fuzzy_tuned, plr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_plr_hard[1], env_p, ple_hard, fuzzy_tuned, plr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_plr_hard[2], env_q, ple_hard, fuzzy_tuned, plr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_plr_hard[0], env_o, ple_hard, fuzzy_arb, plr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_plr_hard[1], env_p, ple_hard, fuzzy_arb, plr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_plr_hard[2], env_q, ple_hard, fuzzy_arb, plr)).T)


data.append(np.vstack((shd_std, rl_adptv_pwr_easy[0], env_o, ple_easy, rl_adptv, pwr)).T)
data.append(np.vstack((shd_std, rl_adptv_pwr_easy[1], env_p, ple_easy, rl_adptv, pwr)).T)
data.append(np.vstack((shd_std, rl_adptv_pwr_easy[2], env_q, ple_easy, rl_adptv, pwr)).T)
data.append(np.vstack((shd_std, rl_decay_pwr_easy[0], env_o, ple_easy, rl_decay, pwr)).T)
data.append(np.vstack((shd_std, rl_decay_pwr_easy[1], env_p, ple_easy, rl_decay, pwr)).T)
data.append(np.vstack((shd_std, rl_decay_pwr_easy[2], env_q, ple_easy, rl_decay, pwr)).T)

data.append(np.vstack((shd_std, rl_adptv_pwr_hard[0], env_o, ple_hard, rl_adptv, pwr)).T)
data.append(np.vstack((shd_std, rl_adptv_pwr_hard[1], env_p, ple_hard, rl_adptv, pwr)).T)
data.append(np.vstack((shd_std, rl_adptv_pwr_hard[2], env_q, ple_hard, rl_adptv, pwr)).T)
data.append(np.vstack((shd_std, rl_decay_pwr_hard[0], env_o, ple_hard, rl_decay, pwr)).T)
data.append(np.vstack((shd_std, rl_decay_pwr_hard[1], env_p, ple_hard, rl_decay, pwr)).T)
data.append(np.vstack((shd_std, rl_decay_pwr_hard[2], env_q, ple_hard, rl_decay, pwr)).T)


data.append(np.vstack((shd_std, fuzzy_tuned_pwr_easy[0], env_o, ple_easy, fuzzy_tuned, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_pwr_easy[1], env_p, ple_easy, fuzzy_tuned, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_pwr_easy[2], env_q, ple_easy, fuzzy_tuned, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_pwr_easy[0], env_o, ple_easy, fuzzy_arb, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_pwr_easy[1], env_p, ple_easy, fuzzy_arb, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_pwr_easy[2], env_q, ple_easy, fuzzy_arb, pwr)).T)

data.append(np.vstack((shd_std, fuzzy_tuned_pwr_hard[0], env_o, ple_hard, fuzzy_tuned, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_pwr_hard[1], env_p, ple_hard, fuzzy_tuned, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_tuned_pwr_hard[2], env_q, ple_hard, fuzzy_tuned, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_pwr_hard[0], env_o, ple_hard, fuzzy_arb, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_pwr_hard[1], env_p, ple_hard, fuzzy_arb, pwr)).T)
data.append(np.vstack((shd_std, fuzzy_arb_pwr_hard[2], env_q, ple_hard, fuzzy_arb, pwr)).T)
'''
#print(np.vstack(data))

df = pd.DataFrame(data=np.vstack(data), columns=['shd-std', 'data', 'env', 'ple', 'agent', 'metric'])
df.explode('data')
df['data'] =df['data'].astype('float')
df.explode('shd-std')
df['shd-std'] = df['shd-std'].astype('float')
print(df.loc[(df['env'] == 'o') & (df['ple'] == 'easy')])

df_gp_o_easy = df.loc[(df['env'] == 'q') & (df['ple'] == 'easy') & (df['metric'] == 'goodput')]
df_gp_p_easy = df.loc[(df['env'] == 'o') & (df['ple'] == 'easy') & (df['metric'] == 'goodput')]
df_gp_q_easy = df.loc[(df['env'] == 'p') & (df['ple'] == 'easy') & (df['metric'] == 'goodput')]

df_plr_o_easy = df.loc[(df['env'] == 'o') & (df['ple'] == 'easy') & (df['metric'] == 'plr')]
df_plr_p_easy = df.loc[(df['env'] == 'p') & (df['ple'] == 'easy') & (df['metric'] == 'plr')]
df_plr_q_easy = df.loc[(df['env'] == 'q') & (df['ple'] == 'easy') & (df['metric'] == 'plr')]

#sns.lineplot(data=df_plr_p_easy, x='shd_std', y='data', hue='agent', style='agent', markers=True)

df_o_gp_easy = df.loc[(df['env'] == 'o') & (df['ple'] == 'easy') & (df['metric'] == 'goodput')]
df_o_plr_easy = df.loc[(df['env'] == 'o') & (df['ple'] == 'easy') & (df['metric'] == 'plr')]
df_o_pwr_easy = df.loc[(df['env'] == 'o') & (df['ple'] == 'easy') & (df['metric'] == 'pwr')]

df_o_gp_hard = df.loc[(df['env'] == 'o') & (df['ple'] == 'hard') & (df['metric'] == 'goodput')]
df_o_plr_hard = df.loc[(df['env'] == 'o') & (df['ple'] == 'hard') & (df['metric'] == 'plr')]
df_o_pwr_hard = df.loc[(df['env'] == 'o') & (df['ple'] == 'hard') & (df['metric'] == 'pwr')]

df_q_gp_easy = df.loc[(df['env'] == 'q') & (df['ple'] == 'easy') & (df['metric'] == 'goodput')]
df_q_plr_easy = df.loc[(df['env'] == 'q') & (df['ple'] == 'easy') & (df['metric'] == 'plr')]
df_q_pwr_easy = df.loc[(df['env'] == 'q') & (df['ple'] == 'easy') & (df['metric'] == 'pwr')]

df_q_gp_hard = df.loc[(df['env'] == 'q') & (df['ple'] == 'hard') & (df['metric'] == 'goodput')]
df_q_plr_hard = df.loc[(df['env'] == 'q') & (df['ple'] == 'hard') & (df['metric'] == 'plr')]
df_q_pwr_hard = df.loc[(df['env'] == 'q') & (df['ple'] == 'hard') & (df['metric'] == 'pwr')]


def plot_metric(df, xlabel, ylabel, fdir, fname):
    plt.figure(figsize=(5.8, 4.8))
    ax = sns.lineplot(data=df, x='shd-std', y='data', hue='agent', style='agent', markers=True, ci='sd')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend().set_title('')
    
    ax.set_xlim([0,12])
    
    plt.tight_layout(pad=0.4)
    plt.savefig(fdir+'/'+fname+'.pdf', format='pdf', bbox_inches='tight')
#o_gp_easy.axes[0,0].set_ylabel('Total Data Transferred, MB')
#o_gp_easy.axes[0,1].set_ylabel('Packet Loss Rate, %')

plot_metric(df_o_gp_easy, '$\sigma_s$', 'Total Data Transferred, MB', 'out', 'fig_imr_res_comms_sens_o_gp_easy')
plot_metric(df_o_plr_easy, '$\sigma_s$', 'Packet Loss Rate, \%', 'out', 'fig_imr_res_comms_sens_o_plr_easy')
plot_metric(df_o_pwr_easy, '$\sigma_s$', 'Power Consumption, Wh', 'out', 'fig_imr_res_comms_sens_o_pwr_easy')

plot_metric(df_o_gp_hard, '$\sigma_s$', 'Total Data Transferred, MB', 'out', 'fig_imr_res_comms_sens_o_gp_hard')
plot_metric(df_o_plr_hard, '$\sigma_s$', 'Packet Loss Rate, \%', 'out', 'fig_imr_res_comms_sens_o_plr_hard')
plot_metric(df_o_pwr_hard, '$\sigma_s$', 'Power Consumption, Wh', 'out', 'fig_imr_res_comms_sens_o_pwr_hard')

plot_metric(df_q_gp_easy, '$\sigma_s$', 'Total Data Transferred, MB', 'out', 'fig_imr_res_comms_sens_q_gp_easy')
plot_metric(df_q_plr_easy, '$\sigma_s$', 'Packet Loss Rate, \%', 'out', 'fig_imr_res_comms_sens_q_plr_easy')
plot_metric(df_q_pwr_easy, '$\sigma_s$', 'Power Consumption, Wh', 'out', 'fig_imr_res_comms_sens_q_pwr_easy')

plot_metric(df_q_gp_hard, '$\sigma_s$', 'Total Data Transferred, MB', 'out', 'fig_imr_res_comms_sens_q_gp_hard')
plot_metric(df_q_plr_hard, '$\sigma_s$', 'Packet Loss Rate, \%', 'out', 'fig_imr_res_comms_sens_q_plr_hard')
plot_metric(df_q_pwr_hard, '$\sigma_s$', 'Power Consumption, Wh', 'out', 'fig_imr_res_comms_sens_q_pwr_hard')


#df_q_easy = df.loc[(df['env'] == 'q') & (df['ple'] == 'easy')]
#sns.relplot(data=df_q_easy, x='shd-std', y='data', hue='agent', style='agent', col='metric', kind='line', ci='sd', facet_kws={'sharex':True, 'sharey':False})

'''
plt.figure()
plt.plot(shd_std, rl_adptv_goodput_easy[1], 'b-o', label='RL-adaptive')
plt.plot(shd_std, rl_decay_goodput_easy[1], 'r--o', label='RL-decay')
plt.plot(shd_std, fuzzy_tuned_goodput_easy[1], 'g-.o', label='Fuzzy-tuned')
plt.plot(shd_std, fuzzy_arb_goodput_easy[1], 'y:o', label='Fuzzy-arb')
plt.legend()
'''

'''
plt.figure(1)
plt.plot(shd_std, rl_goodput_easy[0], 'b-o', label='Long boundary')
plt.plot(shd_std, rl_goodput_easy[1], 'r-o', label='Near return')
plt.plot(shd_std, rl_goodput_easy[2], 'g-o', label='Linear return')

plt.plot(shd_std, fuzzy_goodput_easy[0], 'b--^', label='Long boundary')
plt.plot(shd_std, fuzzy_goodput_easy[1], 'r--^', label='Near return')
plt.plot(shd_std, fuzzy_goodput_easy[2], 'g--^', label='Linear return')
plt.legend()

plt.figure(2)
plt.plot(shd_std, rl_plr_easy[0], 'b-o', label='Long boundary')
plt.plot(shd_std, rl_plr_easy[1], 'r-o', label='Near return')
plt.plot(shd_std, rl_plr_easy[2], 'g-o', label='Linear return')

plt.plot(shd_std, fuzzy_plr_easy[0], 'b--^', label='Long boundary')
plt.plot(shd_std, fuzzy_plr_easy[1], 'r--^', label='Near return')
plt.plot(shd_std, fuzzy_plr_easy[2], 'g--^', label='Linear return')
plt.legend()

plt.figure(3)
plt.plot(shd_std, rl_pwr_easy[0], 'b-o', label='Long boundary')
plt.plot(shd_std, rl_pwr_easy[1], 'r-o', label='Near return')
plt.plot(shd_std, rl_pwr_easy[2], 'g-o', label='Linear return')

plt.plot(shd_std, fuzzy_pwr_easy[0], 'b--^', label='Long boundary')
plt.plot(shd_std, fuzzy_pwr_easy[1], 'r--^', label='Near return')
plt.plot(shd_std, fuzzy_pwr_easy[2], 'g--^', label='Linear return')
plt.legend()
'''

for d, rd in zip(rl_agent_adptv_app_sweep_data, rl_agent_adptv_app_sweep_radio_data):
    d.close()
    rd.close()

for d, rd in zip(rl_agent_decay_app_sweep_data, rl_agent_decay_app_sweep_radio_data):
    d.close()
    rd.close()
    
for d, rd, in zip(fuzzy_agent_tuned_app_sweep_data, fuzzy_agent_tuned_app_sweep_radio_data):
    d.close()
    rd.close()

for d, rd, in zip(fuzzy_agent_arb_app_sweep_data, fuzzy_agent_arb_app_sweep_radio_data):
    d.close()
    rd.close()