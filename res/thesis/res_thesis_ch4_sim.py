import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import os
from os import path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd



DATA_DIR = '../../exps/data/'
#DATA_DIR = '../../../exp_data/'

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = 'comms_letters_11022021/'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']




exp_adaptive_o = 'radio_data_exp_adaptive_gm_agent_exp_sweep_10032021_1009.hdf5' #radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1615.hdf5'
#exp_adaptive_o = 'radio_data_exp_adaptive_gm_agent_exp_sweep_17032021_1027.hdf5'
exp_adaptive_p = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1616.hdf5'
exp_adaptive_q = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'
exp_optimal_o = 'radio_data_exp_optimal_front_15022021_1435.hdf5'
exp_optimal_p = 'radio_data_exp_optimal_front_15022021_1436.hdf5'
exp_optimal_q = 'radio_data_exp_optimal_front_12022021_1433.hdf5'

exp_data_adaptive_env_o = 'data_exp_adaptive_gm_agent_exp_sweep_10032021_1009.hdf5' #data_exp_adaptive_gm_agent_exp_sweep_11022021_1615.hdf5'
#exp_data_adaptive_env_o = 'data_exp_adaptive_gm_agent_exp_sweep_17032021_1027.hdf5'
exp_data_adaptive_env_p = 'data_exp_adaptive_gm_agent_exp_sweep_11022021_1616.hdf5'
exp_data_adaptive_env_q = 'data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'


radio_data_file_adaptive_o = h5py.File(DATA_DIR+exp_adaptive_o, 'r')
radio_data_file_adaptive_p = h5py.File(DATA_DIR+exp_adaptive_p, 'r')
radio_data_file_adaptive_q = h5py.File(DATA_DIR+exp_adaptive_q, 'r')

radio_data_file_opt_o = h5py.File(DATA_DIR+exp_optimal_o, 'r')
radio_data_file_opt_p = h5py.File(DATA_DIR+exp_optimal_p, 'r')
radio_data_file_opt_q = h5py.File(DATA_DIR+exp_optimal_q, 'r')

def gen_histograms(data_file, data_file_opt, fname_base):
    param_list = data_file['radio_data'].keys()
    
    param_list_str = {'0.00':'Decayed Exploration Rate',
                      '1.00':'Adaptive Exploration Rate',}
    
    param_fname_sfx = {'0.00': '_decay',
                       '1.00': '_adptv'}
    
    exp_cfg_keys = data_file['exp_cfg'].keys()
    
    #for k in exp_cfg_keys:
    #    print(k, data_file['exp_cfg/'+k][()])
    
    path_len = int(data_file['exp_cfg/path_len'][()])
    #path_len = 2000
    
    for p in param_list:
        #print('param=%d; pkt loss rate=%.2f' % (p, data_file['data/'+p+'/pkt_loss_rate']) )
        #print('param=%d; goodput=%.2f' % (p, data_file['data/'+p+'/goodput']) )
        cmb_pwr_lvl = get_radio_data_hist(data_file, path_len, p)
        
        plt_radio_data_hist(cmb_pwr_lvl, fname=fname_base+param_fname_sfx[p]+'.pdf', num_runs=100, title=None, cbar=True)
        #plt_radio_data_hist(cmb_pwr_lvl, fname=None, num_runs=100, title=param_list_str[p])
       
    cmb_pwr_lvl_opt = get_radio_data_hist(data_file_opt, path_len, '0.50')
    plt_radio_data_hist(cmb_pwr_lvl_opt, fname=fname_base+'_opt.pdf', num_runs=1, title=None, cbar=False)


#gen_histograms(radio_data_file_adaptive_o, radio_data_file_opt_o, 'out/fig_momr_res_sim_o')
#gen_histograms(radio_data_file_adaptive_p, radio_data_file_opt_p, 'out/fig_momr_res_sim_p')
#gen_histograms(radio_data_file_adaptive_q, radio_data_file_opt_q, 'out/fig_momr_res_sim_q')


cmb_pwr_lvl_opt = get_radio_data_hist(radio_data_file_opt_o, 20000, '0.50')
plt_radio_data_hist(cmb_pwr_lvl_opt, fname='out/fig_momr_res_sim_o_opt.pdf', num_runs=1, title=None, cbar=True)

cmb_pwr_lvl_opt = get_radio_data_hist(radio_data_file_opt_p, 20000, '0.50')
plt_radio_data_hist(cmb_pwr_lvl_opt, fname='out/fig_momr_res_sim_p_opt.pdf', num_runs=1, title=None, cbar=True)

cmb_pwr_lvl_opt = get_radio_data_hist(radio_data_file_opt_q, 20000, '0.50')
plt_radio_data_hist(cmb_pwr_lvl_opt, fname='out/fig_momr_res_sim_q_opt.pdf', num_runs=1, title=None, cbar=True)