from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns

import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv, gen_confusion_matrix, csv_to_array
from thesis_util import *

import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


#plt.style.use('seaborn')
#plt.style.use('tex')

DATA_DIR = '../../exps/data/thesis_ch4/expl/'
DATA_DIR_CH3 = '../../exps/data/thesis_ch3/policies/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = 'ch4_policy_eval/'


ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']

param_list_str = {'0.00':'Decayed Exploration Rate',
                      '1.00':'Adaptive Exploration Rate',}


def save_actions_from_radio_data(data_list, param, runs, actions_fname_type):
    for rd in data_list:
        
        #sfx = rd['exp_cfg/env_name'][()]+'_'+str('%.2f'%float(rd['exp_cfg/w1_comp_val'][()]))
        sfx = rd['exp_cfg/env_name'][()]+'_pwr'
        print(sfx)
        use_trace = rd['radio_data/'+param+'/radio_use_trace']
        r0_pwr_trace = rd['radio_data/'+param+'/radio_0_pwr_trace']
        r1_pwr_trace = rd['radio_data/'+param+'/radio_1_pwr_trace']
        radio_data_to_action_csv(use_trace, r0_pwr_trace, r1_pwr_trace, 20000, runs, DATA_EXP_DIR+actions_fname_type+sfx+'.csv')
        


gm_agent_expl_01_desc_fname = DATA_DIR+'gm_agent_expl_sweep_w_01.csv'

gm_agent_expl_01_data_desc = load_res_data_descriptor(gm_agent_expl_01_desc_fname)

gm_agent_expl_01_data, gm_agent_expl_01_radio_data = load_res_data(DATA_DIR, gm_agent_expl_01_data_desc, 'gm_agent', 'expl_sweep_w_01', dt_idx=3)



rl_agent_adptv_desc_fname = DATA_DIR_CH3+'rl_adptv_eps/rl_agent_rw_sweep_1.csv'
opt_so_desc_fname = DATA_DIR_CH3+'opt/opt_agent_obj_sweep_1.csv'

rl_agent_adptv_data_desc = load_res_data_descriptor(rl_agent_adptv_desc_fname)
opt_so_data_desc = load_res_data_descriptor(opt_so_desc_fname)

rl_agent_adptv_data, rl_agent_adptv_radio_data = load_res_data(DATA_DIR_CH3+'rl_adptv_eps/', rl_agent_adptv_data_desc, 'rl_agent', 'rw_sweep', dt_idx=4)
opt_so_data, opt_so_radio_data = load_res_data(DATA_DIR_CH3+'opt/', opt_so_data_desc, 'opt_agent', 'obj_sweep', dt_idx=4)



opt_expl_01_fname = DATA_DIR+'radio_data_opt_agent_w1_01_09052021_1806.hdf5'
opt_p_w_01_fname = DATA_DIR+'radio_data_exp_optimal_front_09052021_2108.hdf5'
opt_o_w_01_fname = DATA_DIR+'radio_data_exp_optimal_front_09052021_2109.hdf5'
opt_q_w_01_fname = DATA_DIR+'radio_data_exp_optimal_front_09052021_2111.hdf5'

opt_p_w_01_radio_data = h5py.File(opt_p_w_01_fname, 'r')
opt_o_w_01_radio_data = h5py.File(opt_o_w_01_fname, 'r')
opt_q_w_01_radio_data = h5py.File(opt_q_w_01_fname, 'r')

opt_radio_data = [opt_p_w_01_radio_data,opt_o_w_01_radio_data, opt_q_w_01_radio_data]

#save_actions_from_radio_data(gm_agent_expl_01_radio_data, '0.00', 100, 'decayed_actions_')

#save_actions_from_radio_data(gm_agent_expl_01_radio_data, '1.00', 100, 'adaptive_actions_')

#save_actions_from_radio_data(opt_radio_data, '0.10', 1, 'optimal_actions_')


#save_actions_from_radio_data(opt_so_radio_data, '2.00', 1, 'opt_so_actions_')
#save_actions_from_radio_data(rl_agent_adptv_radio_data, '2.00', 100, 'rl_adptv_actions_')

save_actions_from_radio_data(opt_so_radio_data, '1.00', 1, 'opt_so_actions_')
#save_actions_from_radio_data(rl_agent_adptv_radio_data, '1.00', 100, 'rl_adptv_actions_')