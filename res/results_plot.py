
import numpy as np
import matplotlib.pyplot as plt

# Environment with Shadowing - PLE = 2.5, PL_SIGMA = 5
sarsa_reduced_state_outages_mean        = 227.2
sarsa_reduced_state_outages_std         = 38.088

sarsa_reduced_state_kbytes_tx_mean      = 47922.571
sarsa_reduced_state_kbytes_tx_std       = 1614.798
sarsa_reduced_state_kbytes_lost_mean    = 5199.2
sarsa_reduced_state_kbytes_lost_std     = 825.634


sarsa_rssi_state_outages_mean           = 485.12
sarsa_rssi_state_outages_std            = 32.057

sarsa_rssi_state_kbytes_tx_mean         = 51094.948
sarsa_rssi_state_kbytes_tx_std          = 2430.597
sarsa_rssi_state_kbytes_lost_mean       = 2425.6
sarsa_rssi_state_kbytes_lost_std        = 160.286

ql_reduced_state_outages_mean           = 214.16
ql_reduced_state_outages_std            = 35.264

ql_reduced_state_kbytes_tx_mean         = 47274.199
ql_reduced_state_kbytes_tx_std          = 1710.842
ql_reduced_state_kbytes_lost_mean       = 4957.6
ql_reduced_state_kbytes_lost_std        = 829.311

ql_rssi_state_outages_mean              = 475.86
ql_rssi_state_outages_std               = 34.126

ql_rssi_state_kbytes_tx_mean            = 50971.542
ql_rssi_state_kbytes_tx_std             = 2329.297
ql_rssi_state_kbytes_lost_mean          = 2379.3
ql_rssi_state_kbytes_lost_std           = 170.631


agent_types = ['SARSA\nRSSI-based State', 'Q-L\nRSSI-based State', 'SARSA\nReduced State', 'Q-L\nReduced State']
x_pos = np.arange(len(agent_types))

outages         = [sarsa_rssi_state_outages_mean,   ql_rssi_state_outages_mean,     sarsa_reduced_state_outages_mean,   ql_reduced_state_outages_mean]
outages_error   = [sarsa_rssi_state_outages_std,    ql_rssi_state_outages_std,      sarsa_reduced_state_outages_std,    ql_reduced_state_outages_std]

kbytes_tx       = [sarsa_rssi_state_kbytes_tx_mean, ql_rssi_state_kbytes_tx_mean,   sarsa_reduced_state_kbytes_tx_mean, ql_reduced_state_kbytes_tx_mean]
kbytes_tx_error = [sarsa_rssi_state_kbytes_tx_std,  ql_rssi_state_kbytes_tx_std,    sarsa_reduced_state_kbytes_tx_std,  ql_reduced_state_kbytes_tx_std]


kbytes_lost         = []
kbytes_lost_error   = []

fig, ax = plt.subplots()
ax.bar(x_pos, outages, yerr=outages_error, align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('No. of outages')
ax.set_xticks(x_pos)
ax.set_xticklabels(agent_types)
ax.set_title('Outages')
ax.yaxis.grid(True)

fig, ax = plt.subplots()
ax.bar(x_pos, kbytes_tx, yerr=kbytes_tx_error, align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('KB transferred')
ax.set_xticks(x_pos)
ax.set_xticklabels(agent_types)
ax.set_title('Throughput')
ax.yaxis.grid(True)

plt.tight_layout()




# Evironment without shadowing - PLE 2.5
sarsa_reduced_state_outages_mean        = 75.02
sarsa_reduced_state_outages_std         = 11.203

sarsa_reduced_state_kbytes_tx_mean      = 57281.7
sarsa_reduced_state_kbytes_tx_std       = 4579.108
sarsa_reduced_state_kbytes_lost_mean    = 1875.5
sarsa_reduced_state_kbytes_lost_std     = 280.067


sarsa_rssi_state_outages_mean           = 409.0
sarsa_rssi_state_outages_std            = 40.536

sarsa_rssi_state_kbytes_tx_mean         = 41655.307
sarsa_rssi_state_kbytes_tx_std          = 5394.736
sarsa_rssi_state_kbytes_lost_mean       = 2045.0
sarsa_rssi_state_kbytes_lost_std        = 202.682

ql_reduced_state_outages_mean           = 75.22
ql_reduced_state_outages_std            = 10.57

ql_reduced_state_kbytes_tx_mean         = 57708.9
ql_reduced_state_kbytes_tx_std          = 4540.250
ql_reduced_state_kbytes_lost_mean       = 1880.5
ql_reduced_state_kbytes_lost_std        = 264.258

ql_rssi_state_outages_mean              = 407.22
ql_rssi_state_outages_std               = 41.525

ql_rssi_state_kbytes_tx_mean            = 41097.421
ql_rssi_state_kbytes_tx_std             = 5268.193
ql_rssi_state_kbytes_lost_mean          = 2036.1
ql_rssi_state_kbytes_lost_std           = 207.625


agent_types = ['SARSA\nRSSI-based State', 'Q-L\nRSSI-based State', 'SARSA\nReduced State', 'Q-L\nReduced State']
x_pos = np.arange(len(agent_types))

outages         = [sarsa_rssi_state_outages_mean,   ql_rssi_state_outages_mean,     sarsa_reduced_state_outages_mean,   ql_reduced_state_outages_mean]
outages_error   = [sarsa_rssi_state_outages_std,    ql_rssi_state_outages_std,      sarsa_reduced_state_outages_std,    ql_reduced_state_outages_std]

kbytes_tx       = [sarsa_rssi_state_kbytes_tx_mean, ql_rssi_state_kbytes_tx_mean,   sarsa_reduced_state_kbytes_tx_mean, ql_reduced_state_kbytes_tx_mean]
kbytes_tx_error = [sarsa_rssi_state_kbytes_tx_std,  ql_rssi_state_kbytes_tx_std,    sarsa_reduced_state_kbytes_tx_std,  ql_reduced_state_kbytes_tx_std]

fig, ax = plt.subplots()
ax.bar(x_pos, outages, yerr=outages_error, align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('No. of outages')
ax.set_xticks(x_pos)
ax.set_xticklabels(agent_types)
ax.set_title('Outages')
ax.yaxis.grid(True)

fig, ax = plt.subplots()
ax.bar(x_pos, kbytes_tx, yerr=kbytes_tx_error, align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('KB transferred')
ax.set_xticks(x_pos)
ax.set_xticklabels(agent_types)
ax.set_title('Throughput')
ax.yaxis.grid(True)

plt.tight_layout()
plt.show()