import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import gym_grid_wireless.envs
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from util.visualisation import TrajectoryVisualiser

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_DIR = '../exps/data/'

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_12102020_1710.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5' # w1=0.5
#exp_exploration_fname = 'test_radio_data_exploration_15102020_1454.hdf5' # w1=0.45
#exp_exploration_fname = 'test_radio_data_exploration_16102020_1552.hdf5' # with a = a_prime change in algorithm

#exp_exploration_fname = 'radio_data_exp_adaptive_gm_agent_env_o_20102020_1957.hdf5'


#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_20102020_1645.hdf5'
exp_optimal_fname = 'radio_data_exp_optimal_front_19112020_1244.hdf5'
exp_optimal_data_fname = 'data_exp_optimal_front_19112020_1244.hdf5'

#data_file = h5py.File(DATA_DIR+exp_exploration_fname, 'r')
#data_file = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
data_file_optimal = h5py.File(DATA_DIR+exp_optimal_fname, 'r')
data_file_optimal_data = h5py.File(DATA_DIR+exp_optimal_data_fname, 'r')

#print('Radio data power level, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
#param_list = data_file['dist_step/dist_step']



#print(data_file['radio_data'].keys())
#radio_data_decay = [ data_file['radio_data/0/'+key][()] for key in data_file['radio_data/0'].keys() ]
#radio_data_adaptive = [ data_file['radio_data/1/'+key][()] for key in data_file['radio_data/1'].keys() ]
##radio_data = [ data_file['radio_data/0.8/'+key][()] for key in data_file['radio_data/0.8'].keys() ]
#test_data_radio_use_trace = data_file['radio_data/0.0/radio_use_trace'][()]
#test_data_outage_trace = data_file['radio_data/0.0/outage_trace'][()]
#radio_data_optimal = [ data_file_optimal['radio_data/0.9/'+key][()] for key in data_file_optimal['radio_data/0.9'].keys() ]
#RUN_NO = 8 # for env q for paper
#RUN_NO = 5

#print(data_file['radio_data/0.0/'].keys())
#print(radio_data)
path_len = data_file_optimal['exp_cfg/path_len'][()]
env_name = data_file_optimal['exp_cfg/env_name'][()]
env_spec = data_file_optimal['exp_cfg/env_spec'][()]
env_path_fname = env_name+'_'+path_len+'.csv'
env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
print(env_path_fname, env_fname)
tj = TrajectoryVisualiser(FILE_DIR, env_path_fname, env_fname)
#tj.plt_pwr_lvl_trace(radio_data_adaptive[1][RUN_NO], radio_data_adaptive[2][RUN_NO], dist_step, title='')
#plt.savefig('fig_adaptv_radio_pwr.png', dpi=600)
#tj.plt_pwr_lvl_trace(radio_data_decay[1][RUN_NO], radio_data_decay[2][RUN_NO], dist_step, title='')
#plt.savefig('fig_decay_radio_pwr.png', dpi=600)
#tj.plt_pwr_lvl_trace(radio_data[1][RUN_NO], radio_data[2][RUN_NO], dist_step, title='')
#tj.plt_pwr_lvl_trace(radio_data_optimal[1], radio_data_optimal[2], dist_step, title='')


for w in data_file_optimal['radio_data'].keys():
    rd = [ data_file_optimal['radio_data/'+w+'/'+key][()] for key in data_file_optimal['radio_data/'+w].keys() ]
    dist_step = data_file_optimal['dist_step/'+w][()]
    print(data_file_optimal_data['data/'+w+'/goodput'][()])
    print(data_file_optimal_data['data/'+w+'/tot_pwr'][()])
    tj.plt_pwr_lvl_trace(rd[1], rd[2], dist_step, title=str(w))
    plt.savefig('19112020_03_optimal_'+w+'.png', dpi=600)


data_file_optimal.close()
data_file_optimal_data.close()