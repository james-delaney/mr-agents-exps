import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd


DATA_DIR = '../exps/data/'
DATA_DIR = '../../../exp_data/'

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = 'comms_letters_11022021/'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']

exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'
exp_optimal_fname = 'radio_data_exp_optimal_front_12022021_1433.hdf5'

exp_adaptive_o = 'radio_data_exp_adaptive_gm_agent_exp_sweep_10032021_1009.hdf5' #radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1615.hdf5'
#exp_adaptive_o = 'radio_data_exp_adaptive_gm_agent_exp_sweep_17032021_1027.hdf5'
exp_adaptive_p = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1616.hdf5'
exp_optimal_o = 'radio_data_exp_optimal_front_15022021_1435.hdf5'
exp_optimal_p = 'radio_data_exp_optimal_front_15022021_1436.hdf5'

exp_data_adaptive_env_o = 'data_exp_adaptive_gm_agent_exp_sweep_10032021_1009.hdf5' #data_exp_adaptive_gm_agent_exp_sweep_11022021_1615.hdf5'
#exp_data_adaptive_env_o = 'data_exp_adaptive_gm_agent_exp_sweep_17032021_1027.hdf5'
exp_data_adaptive_env_p = 'data_exp_adaptive_gm_agent_exp_sweep_11022021_1616.hdf5'
exp_data_adaptive_env_q = 'data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'

policy_stats_fname = 'policy_stats_exp_comms_letters_11022021.hdf5'

data_file_adaptive = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
data_file_opt = h5py.File(DATA_DIR+exp_optimal_fname, 'r')

data_file_adaptive_o = h5py.File(DATA_DIR+exp_adaptive_o, 'r')
data_file_adaptive_p = h5py.File(DATA_DIR+exp_adaptive_p, 'r')
data_file_opt_o = h5py.File(DATA_DIR+exp_optimal_o, 'r')
data_file_opt_p = h5py.File(DATA_DIR+exp_optimal_p, 'r')

opt_use_trace = data_file_opt_o['radio_data/0.50/radio_use_trace']
opt_r0_pwr_trace = data_file_opt_o['radio_data/0.50/radio_0_pwr_trace']
opt_r1_pwr_trace = data_file_opt_o['radio_data/0.50/radio_1_pwr_trace']

adaptive_use_trace = data_file_adaptive_o['radio_data/1.00/radio_use_trace']
adaptive_r0_pwr_trace = data_file_adaptive_o['radio_data/1.00/radio_0_pwr_trace']
adaptive_r1_pwr_trace = data_file_adaptive_o['radio_data/1.00/radio_1_pwr_trace']

decayed_use_trace = data_file_adaptive_o['radio_data/0.00/radio_use_trace']
decayed_r0_pwr_trace = data_file_adaptive_o['radio_data/0.00/radio_0_pwr_trace']
decayed_r1_pwr_trace = data_file_adaptive_o['radio_data/0.00/radio_1_pwr_trace']


#radio_data_to_action_csv(opt_use_trace, opt_r0_pwr_trace, opt_r1_pwr_trace, 20000, 1, DATA_EXP_DIR+'opt_actions_o.csv')
#radio_data_to_action_csv(adaptive_use_trace, adaptive_r0_pwr_trace, adaptive_r1_pwr_trace, 20000, 100, DATA_EXP_DIR+'adaptive_actions_o.csv')
#radio_data_to_action_csv(decayed_use_trace, decayed_r0_pwr_trace, decayed_r1_pwr_trace, 20000, 100, DATA_EXP_DIR+'decayed_actions_o.csv')

def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]
'''
def get_radio_data_hist(data_file, path_len, param):
        r0 = data_file['radio_data/'+param+'/radio_0_pwr_trace']
        r1 = data_file['radio_data/'+param+'/radio_1_pwr_trace']
        
        r0_hist = []
        r1_hist = []
        for i in range(path_len):
            r0_hist.append(np.bincount(r0[:,i], minlength=6))
            r1_hist.append(np.bincount(r1[:,i], minlength=6))
            if i % 1000 == 0:
                sys.stdout.write('\r\tCalculating histogram done %d steps of %d' % (i, path_len))
                sys.stdout.flush()
                
        r0_hist = np.array(r0_hist)
        r1_hist = np.array(r1_hist)
        print('\tDone')
        print(r0_hist.shape)
        
        r0_hist_bin = []
        r1_hist_bin = []
        bin_size = 1
        for i in range(int(path_len/bin_size)):
            r0_hist_bin.append(np.mean(r0_hist[i*bin_size:(i*bin_size+bin_size), :], axis=0))
            r1_hist_bin.append(np.mean(r1_hist[i*bin_size:(i*bin_size+bin_size), :], axis=0))
        
        r0_hist_bin = np.array(r0_hist_bin)
        r1_hist_bin = np.array(r1_hist_bin)
        
    
        # stack up the histograms for the two radios' power levels (0-4 is radio 0, 5-9 is radio 1)
        cmb_pwr_lvl = np.vstack((r0_hist[:,1:].T, r1_hist[:,1:].T))
        return cmb_pwr_lvl

def plt_radio_data_hist(cmb_pwr_lvl_hist, fname=None, num_runs=100):
    fig = plt.figure()
    ax = sns.heatmap(cmb_pwr_lvl_hist, vmin=0, vmax=num_runs, cmap='rocket_r', cbar=False, xticklabels=2500)
    cbar = ax.figure.colorbar(ax.collections[0])
    step = num_runs/5
    ticks = np.arange(0,num_runs+step, step)
    cbar.set_ticks(ticks)
    #cbar.set_ticks([0, 20, 40, 60, 80, 100])
    ticks_str = ['%.2f'%(a) for a in ticks/num_runs]
    cbar.set_ticklabels(ticks_str)
    cbar.set_label('Probability of selection')
    ax.invert_yaxis()
    ax.set_xlabel('Steps')
    ax.set_ylabel('Action, (Power levels)')
    #ax.set_title('w1=%.2f\n \n' % (float(p)))
    if fname is not None:
        fig_fname = fname
        plt.savefig(fig_fname, dpi=600)
'''
def gen_histograms(data_file, data_file_opt):
    param_list = data_file['radio_data'].keys()
    
    param_list_str = {'0.00':'Decayed Exploration Rate',
                      '1.00':'Adaptive Exploration Rate',}
    
    
    exp_cfg_keys = data_file['exp_cfg'].keys()
    
    #for k in exp_cfg_keys:
    #    print(k, data_file['exp_cfg/'+k][()])
    
    path_len = int(data_file['exp_cfg/path_len'][()])
    #path_len = 2000
    
    for p in param_list:
        #print('param=%d; pkt loss rate=%.2f' % (p, data_file['data/'+p+'/pkt_loss_rate']) )
        #print('param=%d; goodput=%.2f' % (p, data_file['data/'+p+'/goodput']) )
        cmb_pwr_lvl = get_radio_data_hist(data_file, path_len, p)
        
        plt_radio_data_hist(cmb_pwr_lvl, fname='comms_letters_11022021/11022021_exp_%.2f_0304.pdf' % (float(p)), num_runs=100, title=None, cbar=True)
        #plt_radio_data_hist(cmb_pwr_lvl, fname=None, num_runs=100, title=param_list_str[p])
       
    #cmb_pwr_lvl_opt = get_radio_data_hist(data_file_opt, path_len, '0.50')
    #plt_radio_data_hist(cmb_pwr_lvl_opt, fname='comms_letters_11022021/11022021_opt_0304.pdf', num_runs=1, title=None, cbar=False)


def calc_policy_stats():
    # only calculate the rmse for all data unless the data file doesn't exist - it takes a while
    if not os.path.exists(DATA_EXP_DIR+policy_stats_fname):
        '''
        rmse_decayed_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '0.00', 20000)
        rmse_adaptive_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '1.00', 20000)
    
        rmse_decayed_o = opt_policy_rmse_at_step(data_file_opt_o['radio_data'], '0.50', data_file_adaptive_o['radio_data'], '0.00', 20000)
        rmse_adaptive_o = opt_policy_rmse_at_step(data_file_opt_o['radio_data'], '0.50', data_file_adaptive_o['radio_data'], '1.00', 20000)
        
        rmse_decayed_p = opt_policy_rmse_at_step(data_file_opt_p['radio_data'], '0.50', data_file_adaptive_p['radio_data'], '0.00', 20000)
        rmse_adaptive_p = opt_policy_rmse_at_step(data_file_opt_p['radio_data'], '0.50', data_file_adaptive_p['radio_data'], '1.00', 20000)
        '''
        
        rmse_decayed_q, mae_decayed_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '0.00', 20000)
        rmse_adaptive_q, mae_adaptive_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '1.00', 20000)
    
        rmse_decayed_o, mae_decayed_o = opt_policy_rmse_at_step(data_file_opt_o['radio_data'], '0.50', data_file_adaptive_o['radio_data'], '0.00', 20000)
        rmse_adaptive_o, mae_adaptive_o = opt_policy_rmse_at_step(data_file_opt_o['radio_data'], '0.50', data_file_adaptive_o['radio_data'], '1.00', 20000)
        
        rmse_decayed_p, mae_decayed_p = opt_policy_rmse_at_step(data_file_opt_p['radio_data'], '0.50', data_file_adaptive_p['radio_data'], '0.00', 20000)
        rmse_adaptive_p, mae_adaptive_p = opt_policy_rmse_at_step(data_file_opt_p['radio_data'], '0.50', data_file_adaptive_p['radio_data'], '1.00', 20000)
        
        file = h5py.File(DATA_EXP_DIR+policy_stats_fname, 'w')
        
        grp_rmse_q = file.create_group('rmse_q')
        grp_rmse_o = file.create_group('rmse_o')
        grp_rmse_p = file.create_group('rmse_p')
        
        grp_rmse_q.create_dataset('0.00', data=rmse_decayed_q)
        grp_rmse_q.create_dataset('1.00', data=rmse_adaptive_q)
        
        grp_rmse_o.create_dataset('0.00', data=rmse_decayed_o)
        grp_rmse_o.create_dataset('1.00', data=rmse_adaptive_o)
        
        grp_rmse_p.create_dataset('0.00', data=rmse_decayed_p)
        grp_rmse_p.create_dataset('1.00', data=rmse_adaptive_p)
        
        grp_mae_q = file.create_group('mae_q')
        grp_mae_o = file.create_group('mae_o')
        grp_mae_p = file.create_group('mae_p')
        
        grp_mae_q.create_dataset('0.00', data=mae_decayed_q)
        grp_mae_q.create_dataset('1.00', data=mae_adaptive_q)
        
        grp_mae_o.create_dataset('0.00', data=mae_decayed_o)
        grp_mae_o.create_dataset('1.00', data=mae_adaptive_o)
        
        grp_mae_p.create_dataset('0.00', data=mae_decayed_p)
        grp_mae_p.create_dataset('1.00', data=mae_adaptive_p)
    else:
        # data file for rmse already exists so we can just read it all back it
        print('reading policy stats data')
        file = h5py.File(DATA_EXP_DIR+policy_stats_fname, 'r')
        
        rmse_decayed_q = file['rmse_q/0.00'][()]
        rmse_adaptive_q = file['rmse_q/1.00'][()]
        
        rmse_decayed_o = file['rmse_o/0.00'][()]
        rmse_adaptive_o = file['rmse_o/1.00'][()]
        
        rmse_decayed_p = file['rmse_p/0.00'][()]
        rmse_adaptive_p = file['rmse_p/1.00'][()]
        
        mae_decayed_q = file['mae_q/0.00'][()]
        mae_adaptive_q = file['mae_q/1.00'][()]
        
        mae_decayed_o = file['mae_o/0.00'][()]
        mae_adaptive_o = file['mae_o/1.00'][()]
        
        mae_decayed_p = file['mae_p/0.00'][()]
        mae_adaptive_p = file['mae_p/1.00'][()]
    
    
    mpe_decayed =  [[np.mean(rmse_decayed_o), np.median(rmse_decayed_o), np.std(rmse_decayed_o), np.var(rmse_decayed_o)],
                    [np.mean(rmse_decayed_p), np.median(rmse_decayed_p), np.std(rmse_decayed_p), np.var(rmse_decayed_p)],
                    [np.mean(rmse_decayed_q), np.median(rmse_decayed_q), np.std(rmse_decayed_q), np.var(rmse_decayed_q)],]
    mpe_adaptive = [[np.mean(rmse_adaptive_o), np.median(rmse_adaptive_o), np.std(rmse_adaptive_o), np.var(rmse_adaptive_o)],
                    [np.mean(rmse_adaptive_p), np.median(rmse_adaptive_p), np.std(rmse_adaptive_p), np.var(rmse_adaptive_p)],
                    [np.mean(rmse_adaptive_q), np.median(rmse_adaptive_q), np.std(rmse_adaptive_q), np.var(rmse_adaptive_q)],]
    
    print('\t\tMPE (decayed exploration):')
    print('\t\t\to:%.4f\t\tmed=%.4f\t\tstd=%.4f\t\tvar=%.4f' % (mpe_decayed[0][0], mpe_decayed[0][1], mpe_decayed[0][2], mpe_decayed[0][3]))
    print('\t\t\tp:%.4f\t\tmed=%.4f\t\tstd=%.4f\t\tvar=%.4f' % (mpe_decayed[1][0], mpe_decayed[1][1], mpe_decayed[1][2], mpe_decayed[0][3]))
    print('\t\t\tq:%.4f\t\tmed=%.4f\t\tstd=%.4f\t\tvar=%.4f' % (mpe_decayed[2][0], mpe_decayed[2][1], mpe_decayed[2][2], mpe_decayed[0][3]))
    print('\t\tMPE (adaptive exploration):')
    print('\t\t\to:%.4f\t\tmed=%.4f\t\tstd=%.4f\t\tvar=%.4f' % (mpe_adaptive[0][0], mpe_adaptive[0][1], mpe_adaptive[0][2], mpe_adaptive[0][3]))
    print('\t\t\tp:%.4f\t\tmed=%.4f\t\tstd=%.4f\t\tvar=%.4f' % (mpe_adaptive[1][0], mpe_adaptive[1][1], mpe_adaptive[1][2], mpe_adaptive[1][3]))
    print('\t\t\tq:%.4f\t\tmed=%.4f\t\tstd=%.4f\t\tvar=%.4f' % (mpe_adaptive[2][0], mpe_adaptive[2][1], mpe_adaptive[2][2], mpe_adaptive[2][3]))
    
    
    mae_decayed =  [[np.mean(mae_decayed_o), np.std(mae_decayed_o), np.var(mae_decayed_o)],
                    [np.mean(mae_decayed_p), np.std(mae_decayed_p), np.var(mae_decayed_p)],
                    [np.mean(mae_decayed_q), np.std(mae_decayed_q), np.var(mae_decayed_q)],]
    mae_adaptive = [[np.mean(mae_adaptive_o), np.std(mae_adaptive_o), np.var(mae_adaptive_o)],
                    [np.mean(mae_adaptive_p), np.std(mae_adaptive_p), np.var(mae_adaptive_p)],
                    [np.mean(mae_adaptive_q), np.std(mae_adaptive_q), np.var(mae_adaptive_q)],]
    
    len_ = rmse_decayed_o.shape[0]
    print('stacking up data into dataframe')
    step = np.array(range(len_))    
    adaptv = np.full(len_, 'Adaptive')
    decay = np.full(len_, 'Decayed')
    env_o = np.full(len_, 'Long boundary')
    env_p = np.full(len_, 'Near return')
    env_q = np.full(len_, 'Linear return')
    
    rmse_decayed_q_dist = np.vstack((rmse_decayed_q, decay, env_q))
    rmse_adaptive_q_dist = np.vstack((rmse_adaptive_q, adaptv, env_q))
    rmse_decayed_o_dist = np.vstack((rmse_decayed_o, decay, env_o))
    rmse_adaptive_o_dist = np.vstack((rmse_adaptive_o, adaptv, env_o))
    rmse_decayed_p_dist = np.vstack((rmse_decayed_p, decay, env_p))
    rmse_adaptive_p_dist = np.vstack((rmse_adaptive_p, adaptv, env_p))
    
    mae_decayed_q_dist = np.vstack((mae_decayed_q*200, decay, env_q))
    mae_adaptive_q_dist = np.vstack((mae_adaptive_q*200, adaptv, env_q))
    mae_decayed_o_dist = np.vstack((mae_decayed_o*200, decay, env_o))
    mae_adaptive_o_dist = np.vstack((mae_adaptive_o*200, adaptv, env_o))
    mae_decayed_p_dist = np.vstack((mae_decayed_p*200, decay, env_p))
    mae_adaptive_p_dist = np.vstack((mae_adaptive_p*200, adaptv, env_p))
    
    
    rmse = np.hstack((rmse_decayed_q_dist, rmse_adaptive_q_dist, rmse_decayed_o_dist, rmse_adaptive_o_dist, rmse_decayed_p_dist, rmse_adaptive_p_dist))
    mae = np.hstack((mae_decayed_q_dist, mae_adaptive_q_dist, mae_decayed_o_dist, mae_adaptive_o_dist, mae_decayed_p_dist, mae_adaptive_p_dist))

    df = pd.DataFrame(data=rmse.T, columns=['rmse', 'exploration', 'env'])
    df.explode('rmse')
    df['rmse'] = df['rmse'].astype('float')
    plt.figure()
    
    
    df.explode('rmse')
    df['rmse'] = df['rmse'].astype('float')
    #print(df)
    ax = sns.boxplot(data=df, x='env', y='rmse', hue='exploration', showfliers=False)
    ax.set_xlabel('Environment')
    ax.set_ylabel('RMSE')
    ax.legend(loc='upper right').set_title('')
    #plt.savefig(DATA_EXP_DIR+'11022021_expl_dist_rmse_per_step.png', dpi=600)
    
    df = pd.DataFrame(data=mae.T, columns=['mae', 'exploration', 'env'])
    df.explode('mae')
    df['mae'] = df['mae'].astype('float')
    plt.figure()
    
    
    df.explode('mae')
    df['mae'] = df['mae'].astype('float')
    #print(df)
    ax = sns.boxplot(data=df, x='env', y='mae', hue='exploration', showfliers=False)
    ax.set_xlabel('Environment')
    ax.set_ylabel('MAE')
    ax.legend(loc='upper right').set_title('')
    #plt.savefig(DATA_EXP_DIR+'11022021_expl_dist_mae_per_step.png', dpi=600)
    
    
    '''
    rmse_time_q = np.hstack((np.vstack((step, rmse_decayed_q, decay)), np.vstack((step, rmse_adaptive_q, adaptv))))
    rmse_time_q_decay = np.vstack((step, rmse_decayed_q))
    rmse_time_q_adaptv = np.vstack((step, rmse_adaptive_q))
    
    plt.figure()
    df = pd.DataFrame(data=rmse_time_q.T, columns=['step', 'rmse', 'exploration'])
    df_decay = pd.DataFrame(data=rmse_time_q_decay.T, columns=['step', 'rmse'])
    df_adaptv = pd.DataFrame(data=rmse_time_q_adaptv.T, columns=['step', 'rmse'])
    #print(df)
    #sns.relplot(data=df, x='step', y='rmse', hue='exploration', kind='line')
    
    sns.lineplot(data=df_decay, x='step', y='rmse', label='decay')
    sns.lineplot(data=df_adaptv, x='step', y='rmse', label='adaptive')
    '''
    
    
    df_o = df[df['env'] == 'Long boundary']
    df_p = df[df['env'] == 'Near return']
    df_q = df[df['env'] == 'Linear return']
    
    
    sns.displot(df_o, x='mae', hue='exploration', kind='kde', multiple='stack')
    
    sns.displot(df_p, x='mae', hue='exploration', kind='kde',  multiple='stack')
    
    sns.displot(df_q, x='mae', hue='exploration', kind='kde',  multiple='stack')
    
    plt.figure()
    g = sns.FacetGrid(data=df, row='exploration', col='env')
    g.map(sns.histplot, 'mae', bins=range(-10,11))
    
    
    file.close()

print(data_file_adaptive_o['exp_cfg/alpha'][()])
print('Starting histogram calculation')
gen_histograms(data_file_adaptive, data_file_opt)
#gen_histograms(data_file_adaptive_o, data_file_opt_o)
#cmb_pwr_lvl = get_radio_data_hist(data_file_adaptive_o, 20000, '1.00')
#plt_radio_data_hist(cmb_pwr_lvl, fname=None, num_runs=100, title='Adaptive Exploration')


print()
print()
#print('Starting policy stats calculation')

#calc_policy_stats()

'''
rmse_decayed_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '0.00', 20000)
rmse_adaptive_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '1.00', 20000)


rmse_decayed_o = opt_policy_rmse_at_step(data_file_opt_o['radio_data'], '0.50', data_file_adaptive_o['radio_data'], '0.00', 20000)
rmse_adaptive_o = opt_policy_rmse_at_step(data_file_opt_o['radio_data'], '0.50', data_file_adaptive_o['radio_data'], '1.00', 20000)

rmse_decayed_p = opt_policy_rmse_at_step(data_file_opt_p['radio_data'], '0.50', data_file_adaptive_p['radio_data'], '0.00', 20000)
rmse_adaptive_p = opt_policy_rmse_at_step(data_file_opt_p['radio_data'], '0.50', data_file_adaptive_p['radio_data'], '1.00', 20000)

mpe_decayed = [ [np.mean(rmse_decayed_o), np.std(rmse_decayed_o)],
                [np.mean(rmse_decayed_p), np.std(rmse_decayed_p)],
                [np.mean(rmse_decayed_q), np.std(rmse_decayed_q)],]
mpe_adaptive = [[np.mean(rmse_adaptive_o), np.std(rmse_adaptive_o)],
                [np.mean(rmse_adaptive_p), np.std(rmse_adaptive_p)],
                [np.mean(rmse_adaptive_q), np.std(rmse_adaptive_q)],]

print('\t\tMPE (decayed exploration):')
print('\t\t\to:%.4f\t\tstd=%.4f' % (mpe_decayed[0][0], mpe_decayed[0][1]))
print('\t\t\tp:%.4f\t\tstd=%.4f' % (mpe_decayed[1][0], mpe_decayed[1][1]))
print('\t\t\tq:%.4f\t\tstd=%.4f' % (mpe_decayed[2][0], mpe_decayed[2][1]))
print('\t\tMPE (adaptive exploration):')
print('\t\t\to:%.4f\t\tstd=%.4f' % (mpe_adaptive[0][0], mpe_adaptive[0][1]))
print('\t\t\tp:%.4f\t\tstd=%.4f' % (mpe_adaptive[1][0], mpe_adaptive[1][1]))
print('\t\t\tq:%.4f\t\tstd=%.4f' % (mpe_adaptive[2][0], mpe_adaptive[2][1]))
'''


## load data files for environment comparison of data params
data_file_o = h5py.File(DATA_DIR+exp_data_adaptive_env_o, 'r')
data_file_p = h5py.File(DATA_DIR+exp_data_adaptive_env_p, 'r')
data_file_q = h5py.File(DATA_DIR+exp_data_adaptive_env_q, 'r')


def get_statistical_data(data_file):
    goodput = {}
    pkt_loss_rate = {}
    tot_pwr = {}
    df = {}
    param_list = data_file['data'].keys()
    for p in param_list:
        gp = data_file['data/'+p+'/goodput'][()]
        plr = data_file['data/'+p+'/pkt_loss_rate'][()]
        tp = data_file['data/'+p+'/tot_pwr'][()]
    
        goodput[p] = {}
        goodput[p]['mean'] = np.mean(gp)/1000000
        goodput[p]['std'] = np.std(gp)/1000000
        goodput[p]['var'] = np.var(gp)/1000000
        
        pkt_loss_rate[p] = {}
        pkt_loss_rate[p]['mean'] = np.mean(plr)*100
        pkt_loss_rate[p]['std'] = np.std(plr)*100
        pkt_loss_rate[p]['var'] = np.var(plr)*100
        
        tot_pwr[p] = {}
        tot_pwr[p]['mean'] = np.mean(tp)
        tot_pwr[p]['std'] = np.std(tp)
        tot_pwr[p]['var'] = np.var(tp)
        
        
        d = np.hstack((gp.squeeze(axis=2), plr, tp))
        print(d.shape)
        df[p] = pd.DataFrame(d, columns=['goodput', 'pkt_loss_rate', 'tot_pwr'])
    
    return goodput, pkt_loss_rate, tot_pwr, df



def plt_trajectory_fig(data_file, param, run):
    radio_data = [data_file['radio_data/'+param+'/radio_use_trace'][()][run],
                  data_file['radio_data/'+param+'/outage_trace'][()][run]]
    
    env_name = ENV_NAME[int(data_file['exp_cfg/env_name'][()])]
    env_spec = data_file['exp_cfg/env_spec'][()]
    path_len = data_file['exp_cfg/path_len'][()]
    
    env_fnames = get_env_fnames(env_name, env_spec, path_len)
    print(env_fnames)
    radio_data_l = [radio_data]
    
    tj = EnvTrajectoryPlotter(FILE_DIR, [env_fnames[0]], env_fnames[1])
    tj.plt_multi_fig_traj_pwr_lvl(radio_data_l, title=None, fig_name='comms_letters_11022021/fig_sample_env.pdf')


goodput_o, pkt_loss_rate_o, tot_pwr_o, df_o = get_statistical_data(data_file_o)
goodput_p, pkt_loss_rate_p, tot_pwr_p, df_p = get_statistical_data(data_file_p)    
goodput_q, pkt_loss_rate_q, tot_pwr_q, df_q = get_statistical_data(data_file_q)   


print()
print()
print('Statistical data:')
def print_stat(stat_name, data, names):
    print('%s\t\t\t\t'%(stat_name))
    for d, n in zip(data, names):
        print('%s:\t%.2f\t\t%.2f\t\t%.2f\t -\t %.2f\t\t%.2f\t\t%.2f' % (n,
                                                                    d['0.00']['mean'], 
                                                                    d['0.00']['std'], 
                                                                    d['0.00']['var'], 
                                                                    d['1.00']['mean'],
                                                                    d['1.00']['std'],
                                                                    d['1.00']['var'],))

print_stat('Goodput', [goodput_o, goodput_p, goodput_q], ['o', 'p', 'q'])
print_stat('Pkt loss rate', [pkt_loss_rate_o, pkt_loss_rate_p, pkt_loss_rate_q], ['o', 'p', 'q'])
print_stat('Tot pwr cons', [tot_pwr_o, tot_pwr_p, tot_pwr_q], ['o', 'p', 'q'])


#plt_trajectory_fig(data_file_adaptive, '1.00', 0)

'''

#plt_trajectory_fig(data_file_adaptive, '1.00', 0)

#plt_radio_data_hist_at_step(data_file_adaptive, '1.00', 100, 1000)

#print('Starting MPE calculation')
#rmse_decayed_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '0.00', 10)
#rmse_adaptive_q = opt_policy_rmse_at_step(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '1.00', 10)

#print(np.transpose(rmse_decayed_q))

rmse_q = np.vstack((rmse_decayed_q.T, rmse_adaptive_q.T))
#print(rmse_q.T)
df = pd.DataFrame(data=rmse_q.T, columns=['decayed', 'adaptive'])
#print(df)
sns.boxplot(data=df, showfliers=False)
'''

'''
sns.displot(df_o['0.00'], x='pkt_loss_rate')
sns.displot(df_o['1.00'], x='pkt_loss_rate')

sns.displot(df_p['0.00'], x='pkt_loss_rate')
sns.displot(df_p['1.00'], x='pkt_loss_rate')

sns.displot(df_q['0.00'], x='pkt_loss_rate')
sns.displot(df_q['1.00'], x='pkt_loss_rate')
'''

'''
rmse_decayed_q = opt_policy_rmse_per_run(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '0.00', 2000)
rmse_adaptive_q = opt_policy_rmse_per_run(data_file_opt['radio_data'], '0.50', data_file_adaptive['radio_data'], '1.00', 2000)

print('stacking up data into dataframe')
step = np.array(range(100))    
adaptv = np.full(100, 'Adaptive')
decay = np.full(100, 'Decayed')
env_o = np.full(100, 'Long boundary')
env_p = np.full(100, 'Near return')
env_q = np.full(100, 'Linear return')

rmse_decayed_q_dist = np.vstack((rmse_decayed_q, decay, env_q))
rmse_adaptive_q_dist = np.vstack((rmse_adaptive_q, adaptv, env_q))

rmse = np.hstack((rmse_decayed_q_dist, rmse_adaptive_q_dist))

df = pd.DataFrame(data=rmse.T, columns=['rmse', 'exploration', 'env'])
df.explode('rmse')
df['rmse'] = df['rmse'].astype('float')
plt.figure()


df.explode('rmse')
df['rmse'] = df['rmse'].astype('float')
#print(df)
ax = sns.boxplot(data=df, x='env', y='rmse', hue='exploration', showfliers=False)
ax.set_xlabel('Environment')
ax.set_ylabel('RMSE')
ax.legend().set_title('')

df_q = df[df['env'] == 'Linear return']

plt.figure()
sns.displot(df_q, x='rmse', col='exploration', multiple='dodge')
'''
data_file_adaptive.close()
data_file_o.close()
data_file_p.close()
data_file_q.close()
