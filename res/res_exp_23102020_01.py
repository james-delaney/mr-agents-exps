
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

'''
Experiment result generation script: 23/10/2020 - 01
    Impact of alpha on policy behavior under repeated (reset) learning
    - comparing the change of % of optimal policy for a given objective weighting
'''

DATA_DIR = '../exps/data/'

data_set = {'alpha_sweep' : [ {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '20102020_1645'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1407'}
                              },
                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1449'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1450'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1452'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1547'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1548'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1549'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1606'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '26102020_1632'}
                              },
                            ],
            'optimal': {
                'data_fnames': {
                    'exp_script': 'optimal_front',
                    #'datetime': '22102020_1623',
                    #'datetime': '17112020_1311',
                    #'datetime': '18112020_1021',
                    #'datetime': '19112020_1244'
                    'datetime': '26112020_1634'
                },
            }
}

alpha_sweep_17112020 =      [ {'gamma': 0.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1348'}
                              },
                              {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1452'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1530'}
                              },

                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1607'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1645'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1722'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1759'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1836'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1913'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_1951'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '17112020_2028'}
                              },
                              
                            ]

alpha_sweep_18112020 =      [ {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1120'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1127'}
                              },
                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1127'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1139'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1145'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1151'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1157'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1203'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1209'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent',
                                   'datetime': '18112020_1216'}
                              },
                            ]


alpha_sweep_24112020 =      [ {'gamma': 0.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.00',
                                   'datetime': '24112020_2151'}
                              },
                              {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.10',
                                   'datetime': '24112020_2151'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.20',
                                   'datetime': '24112020_2151'}
                              },
                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.30',
                                   'datetime': '24112020_2151'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.40',
                                   'datetime': '24112020_2151'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.50',
                                   'datetime': '24112020_1748'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.60',
                                   'datetime': '24112020_1748'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.70',
                                   'datetime': '24112020_1748'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.80',
                                   'datetime': '24112020_1748'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.90',
                                   'datetime': '24112020_1748'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_1.00',
                                   'datetime': '24112020_2158'}
                              },
                            ]

alpha_sweep_27112020_w1_03 =[ {'gamma': 0.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.00',
                                   'datetime': '27112020_1442'}
                              },
                              {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.10',
                                   'datetime': '27112020_1516'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.20',
                                   'datetime': '27112020_1551'}
                              },
                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.30',
                                   'datetime': '27112020_1625'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.40',
                                   'datetime': '27112020_1701'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.50',
                                   'datetime': '27112020_1736'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.60',
                                   'datetime': '27112020_1811'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.70',
                                   'datetime': '27112020_1846'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.80',
                                   'datetime': '27112020_1921'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.90',
                                   'datetime': '27112020_1957'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_1.00',
                                   'datetime': '27112020_2033'}
                              },
                            ]

alpha_sweep_27112020_w1_00 =[ {'gamma': 0.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.00',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.10',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.20',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.30',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.40',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.50',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.60',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.70',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.80',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.90',
                                   'datetime': '30112020_1523'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_1.00',
                                   'datetime': '30112020_1523'}
                              },
                            ]


alpha_sweep_27112020_w1_05 =[ {'gamma': 0.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.00',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.1,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.10',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.2,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.20',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.3,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.30',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.4,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.40',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.5,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.50',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.6,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.60',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.7,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.70',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.8,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.80',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 0.9,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_0.90',
                                   'datetime': '30112020_1355'}
                              },
                              {'gamma': 1.0,
                               'data_fnames': {
                                   'exp_script': 'adaptive_gm_agent_1.00',
                                   'datetime': '30112020_1355'}
                              },
                            ]

data_set['alpha_sweep'] = alpha_sweep_27112020_w1_00

def load_data_file(data_dir, data_set, data_name, data_set_type, data_type):
    if data_set_type == 'single':
        if data_type == 'data':
            return h5py.File(data_dir+'data_exp_'+data_set[data_name]['data_fnames']['exp_script']+'_'+data_set[data_name]['data_fnames']['datetime']+'.hdf5', 'r')
        elif data_type == 'radio_data':
            return h5py.File(data_dir+'radio_data_exp_'+data_set[data_name]['data_fnames']['exp_script']+'_'+data_set[data_name]['data_fnames']['datetime']+'.hdf5', 'r')
    elif data_set_type == 'array':
        if data_type == 'data':
            return [h5py.File(data_dir+'data_exp_'+data_set['data_fnames']['exp_script']+'_'+data_set['data_fnames']['datetime']+'.hdf5', 'r') for data_set in data_set[data_name]]
        elif data_type == 'radio_data':
            return [h5py.File(data_dir+'radio_data_exp_'+data_set['data_fnames']['exp_script']+'_'+data_set['data_fnames']['datetime']+'.hdf5', 'r') for data_set in data_set[data_name]]
        

optimal_front_data = load_data_file(DATA_DIR, data_set, 'optimal','single', 'data')
optimal_front_radio_data = load_data_file(DATA_DIR, data_set, 'optimal', 'single', 'radio_data')

test_data = load_data_file(DATA_DIR, data_set, 'alpha_sweep', 'array', 'data')
test_radio_data = load_data_file(DATA_DIR, data_set, 'alpha_sweep', 'array', 'radio_data')


weight_param_list = ['%.2f'%p for p in optimal_front_data['exp_cfg/param_list'][()]]

#print(weight_param_list)
#print(alpha_param_list)
print(optimal_front_radio_data['radio_data'].keys())


print(optimal_front_data['exp_cfg/param_list'][()])


optimal_radio_use_trace = np.array([optimal_front_radio_data['radio_data/'+p+'/radio_use_trace'][()][0] for p in optimal_front_radio_data['radio_data'].keys()])
optimal_r0_pwr_trace = np.array([optimal_front_radio_data['radio_data/'+p+'/radio_0_pwr_trace'][()][0] for p in optimal_front_radio_data['radio_data'].keys()])
optimal_r1_pwr_trace = np.array([optimal_front_radio_data['radio_data/'+p+'/radio_1_pwr_trace'][()][0] for p in optimal_front_radio_data['radio_data'].keys()])




#print(test_radio_use_trace.shape, optimal_radio_use_trace.shape)

w = '0.00'

optimal_radio_use_trace = optimal_front_radio_data['radio_data/'+w+'/radio_use_trace'][()][0]
optimal_r0_pwr_trace = optimal_front_radio_data['radio_data/'+w+'/radio_0_pwr_trace'][()][0]
optimal_r1_pwr_trace = optimal_front_radio_data['radio_data/'+w+'/radio_1_pwr_trace'][()][0]

res_list = []
for g in range(len(data_set['alpha_sweep'])):
    print('gamma=%.2f' % (data_set['alpha_sweep'][g]['gamma']))

    num_runs = int(test_data[g]['exp_cfg/num_runs'][()])
    path_len = int(test_data[g]['exp_cfg/path_len'][()])

    
    alpha_param_list = ['%.2f'%p for p in test_data[g]['exp_cfg/param_list'][()]]
    
    test_radio_use_trace = np.array([test_radio_data[g]['radio_data/'+p+'/radio_use_trace'][()] for p in test_radio_data[g]['radio_data'].keys()])
    test_r0_pwr_trace = np.array([test_radio_data[g]['radio_data/'+p+'/radio_0_pwr_trace'][()] for p in test_radio_data[g]['radio_data'].keys()])
    test_r1_pwr_trace = np.array([test_radio_data[g]['radio_data/'+p+'/radio_1_pwr_trace'][()] for p in test_radio_data[g]['radio_data'].keys()])
    
    alpha_res_list = []
    for a in range(len(alpha_param_list)):
        #print('Alpha=%s' % alpha_param_list[a])
        pc_optimal_runs = 0
        for r in range(num_runs):
            
            run_count = 0
            for i in range(path_len):
                if test_radio_use_trace[a, r, i] == optimal_radio_use_trace[i]:
                    if test_radio_use_trace[a,r,i] == 0:
                        if test_r0_pwr_trace[a,r,i] == optimal_r0_pwr_trace[i]:        
                            run_count += 1
                    elif test_radio_use_trace[a,r,i] == 1:
                        if test_r1_pwr_trace[a,r,i] == optimal_r1_pwr_trace[i]:        
                            run_count += 1
                    elif test_radio_use_trace[a,r,i] == 2:
                        if (test_r0_pwr_trace[a,r,i] == optimal_r0_pwr_trace[i]) and (test_r1_pwr_trace[a,r,i] == optimal_r1_pwr_trace[i]):
                            run_count += 1
    
            pc_optimal = (run_count/path_len)
            #print('Run no. %d  \t%.2f%%' % (r, pc_optimal))
            pc_optimal_runs += pc_optimal
        
        #print('\t%.2f%%' % (pc_optimal_runs/num_runs))
        alpha_res_list.append(pc_optimal_runs/num_runs)
        print('Alpha=%s  \t%.2f%%' % (alpha_param_list[a], (pc_optimal_runs/num_runs)*100))
    
    res_list.append(alpha_res_list)
    

plt.figure()

for g in range(len(data_set['alpha_sweep'])):
    plt.scatter(alpha_param_list, res_list[g], label='g=%.2f'%(data_set['alpha_sweep'][g]['gamma']))
    

plt.xlabel('Alpha')
plt.ylabel('% of optimal')
plt.title('Alpha/Gamma sweep, env o, w1='+w)
plt.legend(loc="upper left", bbox_to_anchor=(1,1))

optimal_front_data.close()
optimal_front_radio_data.close()


for g in range(len(data_set['alpha_sweep'])):
    test_data[g].close()
    test_radio_data[g].close()
    
plt.savefig('res_exp_26112020_01.png', dpi=600)
plt.show()