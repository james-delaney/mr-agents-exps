import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv, gen_confusion_matrix, csv_to_array, get_statistical_data
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize

DATA_DIR = '../exps/data/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = '24032021_01/'


ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


exp_optimal_fname = 'radio_data_exp_optimal_front_15022021_1435.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_24032021_1435.hdf5' # eps_min = 0.1
exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_24032021_1532.hdf5' # eps_min = 0.05
exp_adaptive_data_fname = 'data_exp_adaptive_gm_agent_exp_sweep_24032021_1532.hdf5'


data_file_adaptive = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
data_file_opt = h5py.File(DATA_DIR+exp_optimal_fname, 'r')
data_file_adaptive_data = h5py.File(DATA_DIR+exp_adaptive_data_fname, 'r')

opt_use_trace = data_file_opt['radio_data/0.50/radio_use_trace']
opt_r0_pwr_trace = data_file_opt['radio_data/0.50/radio_0_pwr_trace']
opt_r1_pwr_trace = data_file_opt['radio_data/0.50/radio_1_pwr_trace']

decayed_use_trace = data_file_adaptive['radio_data/0.00/radio_use_trace']
decayed_r0_pwr_trace = data_file_adaptive['radio_data/0.00/radio_0_pwr_trace']
decayed_r1_pwr_trace = data_file_adaptive['radio_data/0.00/radio_1_pwr_trace']


opt_actions_fname = 'opt_actions.csv'
decayed_actions_fname = 'decayed_actions.csv'

if not os.path.exists(DATA_EXP_DIR+opt_actions_fname):
    print('writing optimal radio data to action csv')
    radio_data_to_action_csv(opt_use_trace, opt_r0_pwr_trace, opt_r1_pwr_trace, 20000, 1, DATA_EXP_DIR+'opt_actions.csv')

if not os.path.exists(DATA_EXP_DIR+decayed_actions_fname):
    print('writing decayed radio data to action csv')
    radio_data_to_action_csv(decayed_use_trace, decayed_r0_pwr_trace, decayed_r1_pwr_trace, 20000, 50, DATA_EXP_DIR+'decayed_actions.csv')
    

opt_actions = csv_to_array(DATA_EXP_DIR+opt_actions_fname, int)
decayed_actions = csv_to_array(DATA_EXP_DIR+decayed_actions_fname, int)

print('generating confusion matrix')
d, d_report, d_mcc = gen_confusion_matrix(opt_actions[0], decayed_actions, 11)

d = (d/np.sum(d, axis=1).reshape(-1,1))*100
d = np.nan_to_num(d)

df_d = pd.DataFrame(d, range(11), range(11))

labels = StringIO(u'''0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,1,0,0,0,0,0
                      0,0,0,0,0,0,1,0,0,0,0
                      0,0,0,0,0,0,0,1,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      ''')                      
                      
labels = pd.read_csv(labels, header=None)
annotations = labels.astype(str)
annot_a = np.where(annotations=='1', 'N/A', '      ')
annot_d = np.where(annotations=='1', 'N/A', '      ')



opt_action_mask = np.array([1,1,1,1,1,0,0,0,1,1,1]).reshape(-1,1)
d *= opt_action_mask

for i in range(11):
    for j in range(11):
        #if a[i,j] > 0.0:
        #    annot_a[i,j] = '%.1f%%' % (a[i,j])
        
        if d[i,j] > 0.0:
            annot_d[i,j] = '%.1f%%' % (d[i,j])
            
            
plt.figure()
ax2 = sns.heatmap(df_d, annot=annot_d, fmt='s', vmin=0, vmax=100, cbar=False, cmap='rocket', annot_kws={"size":8})
for i in range(11):
    ax2.axhline(i, color='white', lw=2)
plt.title('Decayed exploration')
plt.xlabel('Learned Actions')
plt.ylabel('Optimal Actions')
plt.gca().invert_yaxis()
cbar = ax2.figure.colorbar(ax2.collections[0])
cbar.set_ticks([0,20,40,60,80,100])
cbar.set_label('Rate of selection, %')
#plt.savefig('comms_letters_11022021/conf_decayed.png', dpi=600)



d_precision = []
d_recall = []
for i in range(11):
    d_precision.append(d[i,i]/np.sum(d[:,i]))
    d_recall.append(d[i,i]/np.sum(d[i,:]))


d_precision = d_precision*opt_action_mask.T
d_recall = d_recall*opt_action_mask.T


mean_d_p = np.mean(d_precision[d_precision>0.0])
mean_d_r = np.mean(d_recall[d_recall>0.0])
f1_d = (2 * (mean_d_p*mean_d_r))/(mean_d_p+mean_d_r)

print('Decayed stats:')
print('Precision:', mean_d_p)
print('Recall: ', mean_d_r)
print('F1-score: ', f1_d)


decayed_hist = get_radio_data_hist(data_file_adaptive, 20000, '0.00')
#adaptive_hist = get_radio_data_hist(data_file, path_len, '1.00')

plt_radio_data_hist(decayed_hist, None, num_runs=5, title='Decayed Exploration Rate')

goodput, pkt_loss_rate, tot_pwr, df = get_statistical_data(data_file_adaptive_data)

print(goodput)
print(pkt_loss_rate)
print(tot_pwr)