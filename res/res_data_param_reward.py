
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

DATA_DIR = '../exps/data/'

# for environment q
exp_data_fname = 'data_exp_optimal_front_09122020_1523.hdf5'

# for environment o
#exp_exploration_fname = 'data_exp_adaptive_gm_agent_13102020_1324.hdf5'
#exp_optimal_front_fname = 'data_exp_optimal_front_13102020_1448.hdf5'


data_file = h5py.File(DATA_DIR+exp_data_fname, 'r')
param_list = data_file['exp_cfg/param_list'][()]
#print(param_list)
#print(data_file_test_front['exp_cfg/env_name'][()], data_file_opt_front['exp_cfg/env_name'][()])
count = 0
print('Cumulative reward for each param in list: ', param_list)
for p in param_list:
    cml_reward = np.sum(data_file['data/'+str('%.2f'%p)+'/multi_obj_reward'][()][0])
    print('%s   %.2f' %(p, cml_reward))