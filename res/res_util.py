import sys
import numpy as np
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import sklearn.metrics as metrics
import copy
import csv

import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': False,
    'font.family': 'Times',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})

def get_policy_value(radio_use_trace, r0_pwr_trace, r1_pwr_trace, run_no):
    total_val = 0
    
    if not len(r0_pwr_trace) == len(r1_pwr_trace) == len(radio_use_trace):
        raise AssertionError()
        
    use_trace = radio_use_trace[run_no]
    r0 = r0_pwr_trace[run_no]
    r1 = r1_pwr_trace[run_no]
        
    for i in range(len(use_trace)):
        if use_trace[i] == 0:
            total_val += (r0[i]+1)
        elif use_trace[i] == 1:
            total_val += (r1[i]+6)
        else:
            total_val += 0
    
    return total_val

# this reorders actions so 802.11 are a_0 - a_4 and 802.15.4 are a_5 to a_9
def get_action_from_radio_data(radio_id, r0_pwr, r1_pwr):
    action = 0
    if radio_id == 0:
        #action = r0_pwr
        action = (r0_pwr + 4)
    elif radio_id == 1:
        #action = (r1_pwr + 4)
        action = (r1_pwr - 1)
    else:
        action = 10

    #print(radio_id, r0_pwr, r1_pwr, action)
    return action

def opt_policy_rmse_at_step(radio_data_opt, opt_param, radio_data_test, test_param, path_len):
    opt_use_trace = radio_data_opt[opt_param+'/radio_use_trace']
    opt_r0_pwr_trace = radio_data_opt[opt_param+'/radio_0_pwr_trace']
    opt_r1_pwr_trace = radio_data_opt[opt_param+'/radio_1_pwr_trace']
    
    test_use_trace = radio_data_test[test_param+'/radio_use_trace']
    test_r0_pwr_trace = radio_data_test[test_param+'/radio_0_pwr_trace']
    test_r1_pwr_trace = radio_data_test[test_param+'/radio_1_pwr_trace']
    
    num_runs = test_use_trace.shape[0]
    rmse = []
    mae = []
    for i in range(path_len):
        if i % 100 == 0:
            sys.stdout.write('\rcalc rmse for step %d\r' %(i))
            sys.stdout.flush()
        opt_action = get_action_from_radio_data(opt_use_trace[0,i], opt_r0_pwr_trace[0,i], opt_r1_pwr_trace[0,i])
        #print('opt: ', opt_use_trace[0,i], opt_r0_pwr_trace[0,i], opt_r1_pwr_trace[0,i], opt_action)
        sq_err = 0
        test_action = []
        for r in range(num_runs):
            test_action.append(get_action_from_radio_data(test_use_trace[r,i], test_r0_pwr_trace[r,i], test_r1_pwr_trace[r,i]))
            #print('test: ', test_use_trace[r,i], test_r0_pwr_trace[r,i], test_r1_pwr_trace[r,i], test_action[-1])
        
        test_action = np.array(test_action)
        #sq_err += np.power((opt_action-test_action), 2)
        sq_err = np.sum(np.power((opt_action-test_action), 2))
        #sq_err += np.sqrt(sq_err)
        #print(sq_err)
        rmse.append(np.sqrt(sq_err/num_runs))
        #rmse.append(sq_err/num_runs)
        
        me = np.sum(opt_action-test_action)/num_runs
        mae.append(me)
        
    sys.stdout.write('\n')
    
    return np.array(rmse), np.array(mae)
    #print('mean rmse for param ' + test_param + ': ' + str(np.mean(rmse)) + ' std: ' + str(np.std(rmse)))

def opt_policy_rmse_per_run(radio_data_opt, opt_param, radio_data_test, test_param, path_len):
    opt_use_trace = radio_data_opt[opt_param+'/radio_use_trace']
    opt_r0_pwr_trace = radio_data_opt[opt_param+'/radio_0_pwr_trace']
    opt_r1_pwr_trace = radio_data_opt[opt_param+'/radio_1_pwr_trace']
    
    test_use_trace = radio_data_test[test_param+'/radio_use_trace']
    test_r0_pwr_trace = radio_data_test[test_param+'/radio_0_pwr_trace']
    test_r1_pwr_trace = radio_data_test[test_param+'/radio_1_pwr_trace']
    
    print('getting optimal trajectory')
    # get optimal trajectory of actions
    opt_actions = [get_action_from_radio_data(opt_use_trace[0,i], opt_r0_pwr_trace[0,i], opt_r1_pwr_trace[0,i]) for i in range(path_len)]
    
    
    num_runs = test_use_trace.shape[0]
    rmse = []
    mae = []
    for r in range(num_runs):
        sys.stdout.write('\rcalc rmse for run %d\r' %(r))
        sys.stdout.flush()
        # get trajectory of actions selected for this run
        test_action = [get_action_from_radio_data(test_use_trace[r,i], test_r0_pwr_trace[r,i], test_r1_pwr_trace[r,i]) for i in range(path_len)]
        test_action = np.array(test_action)
        
        sq_err = np.sum(np.power((opt_actions-test_action), 2))
        rmse.append(np.sqrt(sq_err/path_len))
        
        me = np.sum(opt_actions-test_action)/path_len
        mae.append(me)
        
    return np.array(rmse), np.array(mae)
        
        

def get_radio_data_hist(data_file, path_len, param):
    r = data_file['radio_data/'+param+'/radio_use_trace']
    r0 = data_file['radio_data/'+param+'/radio_0_pwr_trace']
    r1 = data_file['radio_data/'+param+'/radio_1_pwr_trace']
    print('path len ', path_len)
    
    print('Calculating r_off hist')
    r_off_hist = [np.bincount(r[:,i], minlength=3)[2] for i in range(path_len)]
    print('Calculating r0 hist')
    r0_hist = [np.bincount(r0[:,i], minlength=6) for i in range(path_len)]
    print('Calculating r1 hist')
    r1_hist = [np.bincount(r1[:,i], minlength=6) for i in range(path_len)]
    
    '''
    for i in range(path_len):        
        #r_off_hist.append(np.bincount(r[:,i], minlength=3)[2])
        #r0_hist.append(np.bincount(r0[:,i], minlength=6))
        #r1_hist.append(np.bincount(r1[:,i], minlength=6))
        if i % 1000 == 0:
            sys.stdout.write('\r\tCalculating histogram done %d steps of %d' % (i, path_len))
            sys.stdout.flush()
    '''
    r_off_hist = np.array(r_off_hist)
    r0_hist = np.array(r0_hist)
    r1_hist = np.array(r1_hist)
    print('\tDone')
    print(r1_hist.shape)
    
    '''
    r0_hist_bin = []
    r1_hist_bin = []
    bin_size = 1
    for i in range(int(path_len/bin_size)):
        r0_hist_bin.append(np.mean(r0_hist[i*bin_size:(i*bin_size+bin_size), :], axis=0))
        r1_hist_bin.append(np.mean(r1_hist[i*bin_size:(i*bin_size+bin_size), :], axis=0))
    
    r0_hist_bin = np.array(r0_hist_bin)
    r1_hist_bin = np.array(r1_hist_bin)
    '''

    # stack up the histograms for the two radios' power levels (0-4 is radio 0, 5-9 is radio 1)
    #cmb_pwr_lvl = np.vstack((r0_hist[:,1:].T, r1_hist[:,1:].T))
    
    # stack up  histograms so 802.11 is 0-4 and 802.15.4 is 5-9
    cmb_pwr_lvl = np.vstack((r1_hist[:,1:].T, r0_hist[:,1:].T, r_off_hist.T))
    print(cmb_pwr_lvl.shape)
    return cmb_pwr_lvl

def plt_radio_data_hist(cmb_pwr_lvl_hist, fname=None, num_runs=100, title=None, cbar=True):
    #fig = plt.figure(figsize=[5.15,4.75])
    #fig = plt.figure(figsize=[4.85,4.45]) #opt
    fig = plt.figure(figsize=[5.6, 4.4]) #other
    #fig = plt.figure()
    ax = sns.heatmap(cmb_pwr_lvl_hist, vmin=0, vmax=num_runs, cmap='rocket_r', cbar=False, xticklabels=1000)
    
    if cbar:
        cbar = ax.figure.colorbar(ax.collections[0], pad=0.01)
        step = num_runs/5
        ticks = np.arange(0,num_runs+step, step)
        cbar.set_ticks(ticks)
        #cbar.set_ticks([0, 20, 40, 60, 80, 100])
        ticks_str = ['%.2f'%(a) for a in ticks/num_runs]
        cbar.set_ticklabels(ticks_str)
        cbar.set_label('Probability of selection')
    ax.invert_yaxis()
    ax.set_xlabel('Steps')
    ax.set_ylabel('Action, (Power levels)')
    if title is not None:
        ax.set_title('%s' % (title))
    
    plt.tight_layout(pad=0.4)
    if fname is not None:
        fig_fname = fname
        #plt.savefig(fig_fname, dpi=600)
        #plt.savefig(fig_fname, format='pdf', bbox_inches='tight')
        plt.savefig(fig_fname, backend='pgf')
        
        
def plt_radio_data_hist_at_step(data_file, param, num_runs, step_no):
    r = data_file['radio_data/'+param+'/radio_use_trace']
    r0 = data_file['radio_data/'+param+'/radio_0_pwr_trace']
    r1 = data_file['radio_data/'+param+'/radio_1_pwr_trace']
    
    print(r[:,step_no])
    print(r0[:,step_no])
    print(r1[:,step_no])
    
    a = np.where(r[:,step_no]==0,r0[:,step_no], r1[:,step_no])
    print(a)
    for i in range(num_runs):
        if r[i,step_no] == 2:
            a[i] = 10
    
    print(a)
    
    
    df = pd.DataFrame(data =a, columns=['action'])
    print(df)
    
    sns.histplot(data=df, x='action', discrete=True, bins=11, binrange=(0,11))
    
    
def radio_data_to_action_csv(radio_use_trace, r0_pwr_trace, r1_pwr_trace, path_len, runs, fname):
    if runs == 1:
        actions = [get_action_from_radio_data(radio_use_trace[0,i], r0_pwr_trace[0,i], r1_pwr_trace[0,i]) for i in range(path_len)]
    else:
        actions = []
        for r in range(runs):
            print('getting actions for run %d' % (r))
            actions.append([get_action_from_radio_data(radio_use_trace[r,i], r0_pwr_trace[r,i], r1_pwr_trace[r,i]) for i in range(path_len)])
    
    #actions = [[0,0,0], [1,2,3], [3,3,3]]
    actions = np.array(actions)
    
    #actions.tofile(fname, sep=',', format='%d')
    
    with open(fname, 'w') as file:
        if runs == 1:
            print('writing run 1 to file')
            for i in range(path_len):
                file.write('%d,' % (actions[i]))
            file.write('\n')
        else:
            for r in range(runs):
                print('writing run %d to file' % (r))
                for i in range(path_len):
                    file.write('%d,' % (actions[r, i]))
                file.write('\n')

def radio_data_to_actions(radio_use_trace, r0_pwr_trace, r1_pwr_trace, path_len, runs):
    if runs == 1:
        actions = [get_action_from_radio_data(radio_use_trace[0,i], r0_pwr_trace[0,i], r1_pwr_trace[0,i]) for i in range(path_len)]
    else:
        actions = []
        for r in range(runs):
            print('getting actions for run %d' % (r))
            actions.append([get_action_from_radio_data(radio_use_trace[r,i], r0_pwr_trace[r,i], r1_pwr_trace[r,i]) for i in range(path_len)])
    
    #actions = [[0,0,0], [1,2,3], [3,3,3]]
    actions = np.array(actions)
    return actions


def csv_to_array(fname, dtype):
    arr = []
    with open(fname) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            arr.append(np.array(row[:20000]).astype(dtype))
            
    return np.array(arr)

def gen_confusion_matrix(opt, data, num_classes):
    # create an empty array with dimensions equal to the number of discrete values or categories
    #cm = np.empty([num_classes, num_classes])
    cm_agg = None
    '''
    for i in range(data.shape[0]):
        #print('gen confusion matrix for run %d' % (i)) 
        cm = metrics.confusion_matrix(opt, data[i])
        if cm_agg is None:
            cm_agg = cm
        else:
            cm_agg += cm
    '''
    #temp_cm_agg = copy.copy(cm_agg)
    
    cm = metrics.confusion_matrix(np.tile(opt, data.shape[0]), data.flatten(), normalize=None)
    cm_report = metrics.classification_report(np.tile(opt, data.shape[0]), data.flatten())
    cm_matthews = metrics.matthews_corrcoef(np.tile(opt, data.shape[0]), data.flatten())
    '''
    tp = np.array(num_classes)
    tn = np.array(num_classes)
    fp = np.array(num_classes)
    fn = np.array(num_classes)
    for i in range(num_classes):
        tp += cm_agg[i,i]
        temp_cm_agg[i,i] = 0
        
    fp = np.sum(temp_cm_agg)
    '''  
    
    
    #return cm_agg, float(tp), float(fp)
    return cm, cm_report, cm_matthews
    
def get_statistical_data(data_file):
    goodput = {}
    pkt_loss_rate = {}
    tot_pwr = {}
    df = {}
    param_list = data_file['data'].keys()
    for p in param_list:
        gp = data_file['data/'+p+'/goodput'][()]
        plr = data_file['data/'+p+'/pkt_loss_rate'][()]
        tp = data_file['data/'+p+'/tot_pwr'][()]
    
        goodput[p] = {}
        goodput[p]['mean'] = np.mean(gp)/1000000
        goodput[p]['std'] = np.std(gp)/1000000
        goodput[p]['var'] = np.var(gp)/1000000
        
        pkt_loss_rate[p] = {}
        pkt_loss_rate[p]['mean'] = np.mean(plr)*100
        pkt_loss_rate[p]['std'] = np.std(plr)*100
        pkt_loss_rate[p]['var'] = np.var(plr)*100
        
        tot_pwr[p] = {}
        tot_pwr[p]['mean'] = np.mean(tp)
        tot_pwr[p]['std'] = np.std(tp)
        tot_pwr[p]['var'] = np.var(tp)
        
        
        d = np.hstack((gp.squeeze(axis=2), plr, tp))
        print(d.shape)
        df[p] = pd.DataFrame(d, columns=['goodput', 'pkt_loss_rate', 'tot_pwr'])
    
    return goodput, pkt_loss_rate, tot_pwr, df

    