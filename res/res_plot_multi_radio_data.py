
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from visualisation import TrajectoryVisualiser, EnvTrajectoryPlotter

FILE_DIR = 'gym_grid_wireless/envs/'
DATA_DIR = 'data/'

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_12102020_1710.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5' # w1=0.5
exp_fname_env_q = 'test_radio_data_exploration_15102020_1454.hdf5' # w1=0.45
exp_fname_env_o = 'radio_data_exp_adaptive_gm_agent_env_o_15102020_1658.hdf5'

data_file_env_q = h5py.File(DATA_DIR+exp_fname_env_q, 'r')
data_file_env_o = h5py.File(DATA_DIR+exp_fname_env_o, 'r')

#print('Radio data power level, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
#param_list = data_file['dist_step/dist_step']

#dist_step = data_file['dist_step/dist_step'][()]

#radio_data_decay = [ data_file['radio_data/0/'+key][()] for key in data_file['radio_data/0'].keys() ]
#radio_data_adaptive = [ data_file['radio_data/1/'+key][()] for key in data_file['radio_data/1'].keys() ]

radio_data_env_q = [data_file_env_q['radio_data/1/radio_use_trace'][()],
                    data_file_env_q['radio_data/1/outage_trace'][()]]
radio_data_env_o = [data_file_env_o['radio_data/0.45/radio_use_trace'][()],
                    data_file_env_o['radio_data/0.45/outage_trace'][()]]


radio_data_env_q = np.array([run[8] for run in radio_data_env_q])
radio_data_env_o = [run[6] for run in radio_data_env_o]
print(radio_data_env_q.shape)
#RUN_NO = 8 # for env q for paper
RUN_NO = 6

path_len_q = data_file_env_q['exp_cfg/path_len'][()]
env_name_q = data_file_env_q['exp_cfg/env_name'][()]
env_spec_q = data_file_env_q['exp_cfg/env_spec'][()]
env_path_fname_q = env_name_q+'_'+path_len_q+'.csv'
env_fname_q = 'polys_'+env_name_q+'_'+env_spec_q+'.csv'

path_len_o = data_file_env_o['exp_cfg/path_len'][()]
env_name_o = data_file_env_o['exp_cfg/env_name'][()]
env_path_fname_o = env_name_o+'_'+path_len_o+'.csv'

env_path_fnames = [env_path_fname_q, env_path_fname_o]
radio_data = [radio_data_env_q, radio_data_env_o]

tj = EnvTrajectoryPlotter(FILE_DIR, env_path_fnames, env_fname_q)
#tj.plt_pwr_lvl_trace(radio_data_adaptive[1][RUN_NO], radio_data_adaptive[2][RUN_NO], dist_step, title='')
#tj.plt_pwr_lvl_trace(radio_data_decay[1][RUN_NO], radio_data_decay[2][RUN_NO], dist_step, title='')
#tj.plt_pwr_lvl_trace(radio_data[1][RUN_NO], radio_data[2][RUN_NO], dist_step, title='')
tj.plt_multi_fig_traj_pwr_lvl(radio_data, '', 'sample_env.png')
data_file_env_q.close()
data_file_env_o.close()