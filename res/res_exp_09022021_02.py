
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

DATA_DIR = '../exps/data/'
ENV_NAME = ['o', 'p', 'q']

# for environment o
exp_rl_o_fname = 'data_exp_adaptive_gm_agent_w_sweep_09022021_1741.hdf5'
exp_opt_o_fname = 'data_exp_optimal_front_05022021_1052.hdf5'

# for environment p
exp_rl_p_fname = 'data_exp_adaptive_gm_agent_w_sweep_09022021_1747.hdf5'
exp_opt_p_fname = 'data_exp_optimal_front_05022021_1055.hdf5'

# for environment q
exp_rl_q_fname = 'data_exp_adaptive_gm_agent_w_sweep_09022021_1748.hdf5'
exp_opt_q_fname = 'data_exp_optimal_front_05022021_1101.hdf5'


data_file_rl_o = h5py.File(DATA_DIR+exp_rl_o_fname, 'r')
data_file_opt_o = h5py.File(DATA_DIR+exp_opt_o_fname, 'r')

data_file_rl_p = h5py.File(DATA_DIR+exp_rl_p_fname, 'r')
data_file_opt_p = h5py.File(DATA_DIR+exp_opt_p_fname, 'r')

data_file_rl_q = h5py.File(DATA_DIR+exp_rl_q_fname, 'r')
data_file_opt_q = h5py.File(DATA_DIR+exp_opt_q_fname, 'r')

'''
param_list = data_file_test_front['exp_cfg/param_list'][()]
print(param_list)
print(data_file_test_front['exp_cfg/env_name'][()], data_file_opt_front['exp_cfg/env_name'][()])
'''

count = 0

def load_front_data(data_file):
    global count
    data = None
    x_err = []
    y_err = []
    for key in data_file['data'].keys():
        #mean_gp = np.mean(data_file['data/'+key+'/goodput'][()])
        #int_data = np.hstack((data_file['data/'+key+'/goodput'][()], data_file['data/'+key+'/tot_pwr'][()]))
        #int_data[:,0] = mean_gp
        int_data = np.hstack((np.mean(data_file['data/'+key+'/goodput'][()]), np.mean(data_file['data/'+key+'/tot_pwr'][()])))
        x_err.append(np.std((data_file['data/'+key+'/goodput'][()])))
        y_err.append(np.std((data_file['data/'+key+'/tot_pwr'][()])))
        if data is None:
            data = int_data
        else:
            #print(data.shape, int_data.shape)
            data = np.vstack((data, int_data))
    
    name_col = np.full([len(data),1],count)
    data = np.hstack((data, name_col))
    count += 1
    return data, x_err, y_err

def find_non_dominated_set(data_points):
    num_pts = len(data_points)
    
    undom_set = []
    for i in range(num_pts):
        pt_i = data_points[i]
        for j in range(num_pts):
            if (pt_i[0] >= data_points[j,0]) and (pt_i[1] <= data_points[j,1]):
                if pt_i[0] > data_points[j,0] or pt_i[1] < data_points[i,1]:
                    continue
            break
 
def keep_efficient(pts):
    #print(pts)
    'returns Pareto efficient row subset of pts'
    # sort points by decreasing sum of coordinates
    pts = pts[pts.sum(1).argsort()[::-1]]
    # initialize a boolean mask for undominated points
    # to avoid creating copies each iteration
    undominated = np.ones(pts.shape[0], dtype=bool)
    for i in range(pts.shape[0]):
        # process each point in turn
        n = pts.shape[0]
        if i >= n:
            break
        # find all points not dominated by i
        # since points are sorted by coordinate sum
        # i cannot dominate any points in 1,...,i-1
        undominated[i+1:n] = (pts[i+1:] > pts[i]).any(1) 
        
        # keep points undominated so far
        pts = pts[undominated[:n]]
    
    #print(pts)
    return pts



def plot_opt_and_test_front(data_opt_front, data_test_front, title='', opt_lbl='', test_lbl=''):
    test_front_data, test_x_err, test_y_err = load_front_data(data_test_front)
    opt_front_data, opt_x_err, opt_y_err = load_front_data(data_opt_front)
    
    test_front_data[:,0] = -1*test_front_data[:,0]/1000000
    opt_front_data[:,0] = -1*opt_front_data[:, 0]/1000000
    front_test_front_data = keep_efficient(test_front_data[:,:2])

    #data = pd.DataFrame(np.vstack((front_test_front_data, opt_front_data)), columns=['x','y','file'])
    #print(data)
    
    fig = plt.figure()
    #sns.lineplot(x=front_test_front_data[:,0], y=front_test_front_data[:,1], sort=True, err_style='band')
    #sns.pointplot(x=front_test_front_data[:,0], y=front_test_front_data[:,1])
    #sns.pointplot(x=opt_front_data[:,0], y=opt_front_data[:,1])
    #sns.pointplot(x='x', y='y', hue='file', data=data, dodge=True, join=False)
    #sns.lineplot(x='x', y='y', hue='file', data=data)
    #sns.lineplot(x=opt_front_data[:,0], y=opt_front_data[:,1], sort=True)
    
    #plt.scatter(-1*test_front_data[:,0], test_front_data[:,1])
    plt.scatter(-1*front_test_front_data[:,0], front_test_front_data[:,1])
    plt.scatter(-1*opt_front_data[:,0], opt_front_data[:,1])
    #plt.plot(-1*test_front_data[:,0], test_front_data[:,1])
    plt.plot(-1*front_test_front_data[:,0], front_test_front_data[:,1], label='Adaptive RL Agent')
    plt.plot(-1*opt_front_data[:,0], opt_front_data[:,1], label='Optimal Agent')
    
    '''
    for x,y,a, in zip(-1*opt_front_data[:,0], opt_front_data[:,1], param_list):
        label = '%.2f'%(a)
        
        plt.annotate(label, (x,y), textcoords="offset points", xytext=(0,120*a), ha='center')
    '''
    
    ax = fig.gca()
    ax.invert_xaxis()
    
    ax.set_xlabel('Mean Goodput, MB')
    ax.set_ylabel('Mean Power Consumption, Wh')
    ax.set_title(title)
    ax.legend()
    
    fig_fname = '09022021_02/09020201_02_%s.png' % (title)
    plt.tight_layout()
    plt.savefig(fig_fname, dpi=600)
    
    
plot_opt_and_test_front(data_file_opt_o, data_file_rl_o, 'Trajectory '+ENV_NAME[data_file_rl_o['exp_cfg/env_name'][()]], 'Optimal Agent', 'MORL Agent')
plot_opt_and_test_front(data_file_opt_p, data_file_rl_p, 'Trajectory '+ENV_NAME[data_file_rl_p['exp_cfg/env_name'][()]], 'Optimal Agent', 'MORL Agent')
plot_opt_and_test_front(data_file_opt_q, data_file_rl_q, 'Trajectory '+ENV_NAME[data_file_rl_q['exp_cfg/env_name'][()]], 'Optimal Agent', 'MORL Agent')

data_file_rl_o.close()
data_file_opt_o.close()
data_file_rl_p.close()
data_file_opt_p.close()
data_file_rl_q.close()
data_file_opt_q.close()