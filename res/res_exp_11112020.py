import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
    
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import gym_grid_wireless.envs
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from util.visualisation import TrajectoryVisualiser

'''
Experiment result generation script: 23/10/2020 - 01
    LoRa and WiFi radios and varied environment walk paths
    - comparing radio and power level selection for w1=0
'''

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_DIR = '../exps/data/'

data_set = { 'env_sweep_lora': [{
                                'w1':0.45,
                                'data_fnames': {
                                       'exp_script': 'lora_wifi_agent',
                                       'datetime': '11112020_1156'}
                               },
                                {
                                'w1':0.00,
                                'data_fnames': {
                                       'exp_script': 'lora_wifi_agent',
                                       'datetime': '12112020_0934'}
                               }],
             'env_sweep_zb': [{
                                'w1':0.00,
                                'data_fnames': {
                                       'exp_script': 'zb_wifi_agent',
                                       'datetime': '12112020_0936'}
                               },
                                {
                                'w1':0.45,
                                'data_fnames': {
                                       'exp_script': 'zb_wifi_agent',
                                       'datetime': '12112020_0940'}
                               }]
                            
                           
            }


def load_data_file(data_dir, data_set, data_name, data_set_type, data_type):
    if data_set_type == 'single':
        if data_type == 'data':
            return h5py.File(data_dir+'data_exp_'+data_set[data_name]['data_fnames']['exp_script']+'_'+data_set[data_name]['data_fnames']['datetime']+'.hdf5', 'r')
        elif data_type == 'radio_data':
            return h5py.File(data_dir+'radio_data_exp_'+data_set[data_name]['data_fnames']['exp_script']+'_'+data_set[data_name]['data_fnames']['datetime']+'.hdf5', 'r')
    elif data_set_type == 'array':
        if data_type == 'data':
            return [h5py.File(data_dir+'data_exp_'+data_set['data_fnames']['exp_script']+'_'+data_set['data_fnames']['datetime']+'.hdf5', 'r') for data_set in data_set[data_name]]
        elif data_type == 'radio_data':
            return [h5py.File(data_dir+'radio_data_exp_'+data_set['data_fnames']['exp_script']+'_'+data_set['data_fnames']['datetime']+'.hdf5', 'r') for data_set in data_set[data_name]]
        


test_data_lora = load_data_file(DATA_DIR, data_set, 'env_sweep_lora', 'array', 'data')
test_radio_data_lora = load_data_file(DATA_DIR, data_set, 'env_sweep_lora', 'array', 'radio_data')

test_data_zb = load_data_file(DATA_DIR, data_set, 'env_sweep_zb', 'array', 'data')
test_radio_data_zb = load_data_file(DATA_DIR, data_set, 'env_sweep_zb', 'array', 'radio_data')    



for w in range(len(data_set['env_sweep_lora'])):
    test_radio_data = test_radio_data_lora[w]
    test_data = test_data_lora[w]
    envs = list(test_radio_data['radio_data/'].keys())
    print(envs)
    for e in range(len(envs)):
        print(test_radio_data['dist_step/'].keys(), envs[e])
        dist_step = test_radio_data['dist_step/'+str(envs[e])][()]
        radio_use_trace = test_radio_data['radio_data/'+envs[e]+'/radio_use_trace'][2]
        radio_outage_trace = test_radio_data['radio_data/'+envs[e]+'/outage_trace'][2]
        radio_0_pwr_trace = test_radio_data['radio_data/'+envs[e]+'/radio_0_pwr_trace'][2]
        radio_1_pwr_trace = test_radio_data['radio_data/'+envs[e]+'/radio_1_pwr_trace'][2]
        
        path_len = test_data['exp_cfg/path_len'][()]
        env_name = envs[e]
        env_spec = test_data['exp_cfg/env_spec'][()]
        env_path_fname = env_name+'_'+path_len+'.csv'
        env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
        
        print('alpha', test_data['exp_cfg/alpha'][()])
        print('gamma', test_data['exp_cfg/gamma'][()])
        print('w1', test_data['exp_cfg/w1_comp_val'][()], data_set['env_sweep_lora'][w])
        print('data tx=', test_data['data/'+envs[e]+'/goodput'][()][2])
        print('pkt loss rate=', test_data['data/'+envs[e]+'/pkt_loss_rate'][()][2])
        print('tot pwr=', test_data['data/'+envs[e]+'/tot_pwr'][()][2])
        print()
        tj = TrajectoryVisualiser(FILE_DIR, env_path_fname, env_fname)
        title = 'w1='+str(data_set['env_sweep_lora'][w]['w1'])
        tj.plt_fig_traj_pwr_lvl(radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step, title=title, r0_lbl='LoRa')
        tj.save('res_exp_11112020_01_lora_'+envs[e]+'_'+title+'.png', 600)

for w in range(len(data_set['env_sweep_zb'])):
    test_radio_data = test_radio_data_zb[w]
    test_data = test_data_zb[w]
    envs = list(test_radio_data['radio_data/'].keys())
    print(envs)
    for e in range(len(envs)):
        print(test_radio_data['dist_step/'].keys(), envs[e])
        dist_step = test_radio_data['dist_step/'+str(envs[e])][()]
        radio_use_trace = test_radio_data['radio_data/'+envs[e]+'/radio_use_trace'][19]
        radio_outage_trace = test_radio_data['radio_data/'+envs[e]+'/outage_trace'][19]
        radio_0_pwr_trace = test_radio_data['radio_data/'+envs[e]+'/radio_0_pwr_trace'][19]
        radio_1_pwr_trace = test_radio_data['radio_data/'+envs[e]+'/radio_1_pwr_trace'][19]
        
        path_len = test_data['exp_cfg/path_len'][()]
        env_name = envs[e]
        env_spec = test_data['exp_cfg/env_spec'][()]
        env_path_fname = env_name+'_'+path_len+'.csv'
        env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
        
        print('alpha', test_data['exp_cfg/alpha'][()])
        print('gamma', test_data['exp_cfg/gamma'][()])
        print('w1', test_data['exp_cfg/w1_comp_val'][()], data_set['env_sweep_zb'][w])
        print('data tx=', test_data['data/'+envs[e]+'/goodput'][()][19])
        print('pkt loss rate=', test_data['data/'+envs[e]+'/pkt_loss_rate'][()][19])
        print('tot pwr=', test_data['data/'+envs[e]+'/tot_pwr'][()][19])
        print()
        tj = TrajectoryVisualiser(FILE_DIR, env_path_fname, env_fname)
        title = 'w1='+str(data_set['env_sweep_zb'][w]['w1'])
        tj.plt_fig_traj_pwr_lvl(radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step, title=title)
        tj.save('res_exp_11112020_01_zb_'+envs[e]+'_'+title+'.png', 600)


for w in range(len(data_set['env_sweep_lora'])):
    test_data_lora[w].close()
    test_radio_data_lora[w].close()

for w in range(len(data_set['env_sweep_zb'])):
    test_data_zb[w].close()
    test_radio_data_zb[w].close()