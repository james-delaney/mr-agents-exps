
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import scipy.stats as st

DATA_DIR = '../exps/data/'



exp_rl_fname = 'data_exp_adaptive_rl_agent_env_sweep_04022021_1619.hdf5'
exp_fuzzy_fname = 'data_exp_fuzzy_agent_env_sweep_04022021_1021.hdf5'
exp_fixed_min_fname = 'data_exp_fixed_policy_agent_env_sweep_08022021_1554.hdf5'
exp_fixed_max_fname = 'data_exp_fixed_policy_agent_env_sweep_08022021_1606.hdf5'
exp_morl_fname = 'data_exp_adaptive_gm_agent_env_sweep_05022021_1304.hdf5'
exp_fuzzy_arb_fname = 'data_exp_fuzzy_agent_env_sweep_17022021_1624.hdf5'


data_file_rl = h5py.File(DATA_DIR+exp_rl_fname, 'r')
data_file_fuzzy = h5py.File(DATA_DIR+exp_fuzzy_fname, 'r')
data_file_fixed_min = h5py.File(DATA_DIR+exp_fixed_min_fname, 'r')
data_file_fixed_max = h5py.File(DATA_DIR+exp_fixed_max_fname, 'r')
data_file_morl = h5py.File(DATA_DIR+exp_morl_fname, 'r')
data_file_fuzzy_arb = h5py.File(DATA_DIR+exp_fuzzy_arb_fname, 'r')

print(data_file_rl['data'].keys())


params = []

goodput_rl = []
goodput_rl_err = []
pkt_loss_rate_rl = []
pkt_loss_rate_rl_err = []
tot_pwr_rl = []
tot_pwr_rl_err = []


goodput_fuzzy = []
goodput_fuzzy_err = []
pkt_loss_rate_fuzzy = []
pkt_loss_rate_fuzzy_err = []
tot_pwr_fuzzy = []
tot_pwr_fuzzy_err = []

goodput_fixed_min = []
goodput_fixed_min_err = []
pkt_loss_rate_fixed_min = []
pkt_loss_rate_fixed_min_err = []
tot_pwr_fixed_min = []
tot_pwr_fixed_min_err = []

goodput_fixed_max = []
goodput_fixed_max_err = []
pkt_loss_rate_fixed_max = []
pkt_loss_rate_fixed_max_err = []
tot_pwr_fixed_max = []
tot_pwr_fixed_max_err = []

goodput_morl = []
goodput_morl_err = []
pkt_loss_rate_morl = []
pkt_loss_rate_morl_err = []
tot_pwr_morl = []
tot_pwr_morl_err = []


goodput_fuzzy_arb = []
goodput_fuzzy_arb_err = []
pkt_loss_rate_fuzzy_arb = []
pkt_loss_rate_fuzzy_arb_err = []
tot_pwr_fuzzy_arb = []
tot_pwr_fuzzy_arb_err = []

param_list = data_file_rl['data'].keys()

for p in param_list:
    gp_rl = data_file_rl['data/'+p+'/goodput'][()]
    plr_rl = data_file_rl['data/'+p+'/pkt_loss_rate'][()]
    tp_rl = data_file_rl['data/'+p+'/tot_pwr'][()]
    
    gp_fuzzy = data_file_fuzzy['data/'+p+'/goodput'][()]
    plr_fuzzy = data_file_fuzzy['data/'+p+'/pkt_loss_rate'][()]
    tp_fuzzy = data_file_fuzzy['data/'+p+'/tot_pwr'][()]

    gp_f_min = data_file_fixed_min['data/'+p+'/goodput'][()]
    plr_f_min = data_file_fixed_min['data/'+p+'/pkt_loss_rate'][()]
    tp_f_min = data_file_fixed_min['data/'+p+'/tot_pwr'][()]

    gp_f_max = data_file_fixed_max['data/'+p+'/goodput'][()]
    plr_f_max = data_file_fixed_max['data/'+p+'/pkt_loss_rate'][()]
    tp_f_max = data_file_fixed_max['data/'+p+'/tot_pwr'][()]
    
    gp_morl = data_file_morl['data/'+p+'/goodput'][()]
    plr_morl = data_file_morl['data/'+p+'/pkt_loss_rate'][()]
    tp_morl = data_file_morl['data/'+p+'/tot_pwr'][()]
    
    gp_fuzzy_arb = data_file_fuzzy_arb['data/'+p+'/goodput'][()]
    plr_fuzzy_arb = data_file_fuzzy_arb['data/'+p+'/pkt_loss_rate'][()]
    tp_fuzzy_arb = data_file_fuzzy_arb['data/'+p+'/tot_pwr'][()]
    
    goodput_rl.append(np.mean(gp_rl))
    #goodput_rl_err.append(st.norm.interval(alpha=0.95, loc=np.mean(gp_rl), scale=st.sem(gp_rl)))
    goodput_rl_err.append(np.std(gp_rl))
    pkt_loss_rate_rl.append(np.mean(plr_rl))
    pkt_loss_rate_rl_err.append(np.std(plr_rl))
    tot_pwr_rl.append(np.mean(tp_rl))
    tot_pwr_rl_err.append(np.std(tp_morl))
    
    goodput_fuzzy.append(np.mean(gp_fuzzy))
    #goodput_fuzzy_err.append(st.norm.interval(alpha=0.95, loc=np.mean(gp_fuzzy), scale=st.sem(gp_fuzzy)))
    goodput_fuzzy_err.append(np.std(gp_fuzzy))
    pkt_loss_rate_fuzzy.append(np.mean(plr_fuzzy))
    pkt_loss_rate_fuzzy_err.append(np.std(plr_fuzzy))
    tot_pwr_fuzzy.append(np.mean(tp_fuzzy))
    tot_pwr_fuzzy_err.append(np.std(tp_fuzzy))
    
    goodput_fixed_min.append(np.mean(gp_f_min))
    #goodput_fixed_min_err.append(st.norm.interval(alpha=0.95, loc=np.mean(gp_rl), scale=st.sem(gp_rl)))
    goodput_fixed_min_err.append(np.std(gp_f_min))
    pkt_loss_rate_fixed_min.append(np.mean(plr_f_min))
    pkt_loss_rate_fixed_min_err.append(np.std(plr_f_min))
    tot_pwr_fixed_min.append(np.mean(tp_f_min))
    tot_pwr_fixed_min_err.append(np.std(tp_f_min))
    
    goodput_fixed_max.append(np.mean(gp_f_max))
    #goodput_fixed_min_err.append(st.norm.interval(alpha=0.95, loc=np.mean(gp_f_min), scale=st.sem(gp_f_min)))
    goodput_fixed_max_err.append(np.std(gp_f_max))
    pkt_loss_rate_fixed_max.append(np.mean(plr_f_max))
    pkt_loss_rate_fixed_max_err.append(np.std(plr_f_max))
    tot_pwr_fixed_max.append(np.mean(tp_f_max))
    tot_pwr_fixed_max_err.append(np.std(tp_f_max))
    
    goodput_morl.append(np.mean(gp_morl))
    #goodput_morl_err.append(st.norm.interval(alpha=0.95, loc=np.mean(gp_morl), scale=st.sem(gp_morl)))
    goodput_morl_err.append(np.std(gp_morl))
    pkt_loss_rate_morl.append(np.mean(plr_morl))
    pkt_loss_rate_morl_err.append(np.std(plr_morl))
    tot_pwr_morl.append(np.mean(tp_morl))
    tot_pwr_morl_err.append(np.std(tp_morl))
    
    goodput_fuzzy_arb.append(np.mean(gp_fuzzy_arb))
    #goodput_fuzzy_err.append(st.norm.interval(alpha=0.95, loc=np.mean(gp_fuzzy), scale=st.sem(gp_fuzzy)))
    goodput_fuzzy_arb_err.append(np.std(gp_fuzzy_arb))
    pkt_loss_rate_fuzzy_arb.append(np.mean(plr_fuzzy_arb))
    pkt_loss_rate_fuzzy_arb_err.append(np.std(plr_fuzzy_arb))
    tot_pwr_fuzzy_arb.append(np.mean(tp_fuzzy_arb))
    tot_pwr_fuzzy_arb_err.append(np.std(tp_fuzzy_arb))
    
print(goodput_rl, pkt_loss_rate_rl, tot_pwr_rl)
print(goodput_fuzzy, pkt_loss_rate_fuzzy, tot_pwr_fuzzy)
print(goodput_rl_err, goodput_fuzzy_err, goodput_fixed_min_err, goodput_fixed_max_err, goodput_morl_err)

goodput_rl = np.array(goodput_rl)

goodput_rl_ci = st.norm.interval(alpha=0.95, loc=np.mean(goodput_rl), scale=st.sem(goodput_rl))
print(goodput_rl_ci)
goodput_rl_err = np.array(goodput_rl_err)
goodput_rl_std = np.std(goodput_rl)
goodput_fuzzy = np.array(goodput_fuzzy)
goodput_fuzzy_std = np.std(goodput_fuzzy)
goodput_fuzzy_err = np.array(goodput_fuzzy_err)
goodput_morl = np.array(goodput_morl)
goodput_morl_std = np.std(goodput_morl)
goodput_morl_err = np.array(goodput_morl_err)
goodput_fuzzy_arb = np.array(goodput_fuzzy_arb)
goodput_fuzzy_arb_std = np.std(goodput_fuzzy_arb)
goodput_fuzzy_arb_err = np.array(goodput_fuzzy_arb_err)


pkt_loss_rate_rl = np.array(pkt_loss_rate_rl)
pkt_loss_rate_rl_err = np.array(pkt_loss_rate_rl_err)
pkt_loss_rate_fuzzy = np.array(pkt_loss_rate_fuzzy)
pkt_loss_rate_fuzzy_err = np.array(pkt_loss_rate_fuzzy_err)
pkt_loss_rate_morl = np.array(pkt_loss_rate_morl)
pkt_loss_rate_morl_err = np.array(pkt_loss_rate_morl_err)
pkt_loss_rate_fuzzy_arb = np.array(pkt_loss_rate_fuzzy_arb)
pkt_loss_rate_fuzzy_arb_err = np.array(pkt_loss_rate_fuzzy_arb_err)



tot_pwr_rl = np.array(tot_pwr_rl)
tot_pwr_rl_err = np.array(tot_pwr_rl_err)
tot_pwr_fuzzy = np.array(tot_pwr_fuzzy)
tot_pwr_fuzzy_err = np.array(tot_pwr_fuzzy_err)
tot_pwr_morl = np.array(tot_pwr_morl)
tot_pwr_morl_err = np.array(tot_pwr_morl_err)
tot_pwr_fuzzy_arb = np.array(tot_pwr_fuzzy_arb)
tot_pwr_fuzzy_arb_err = np.array(tot_pwr_fuzzy_arb_err)


goodput_fixed_min = np.array(goodput_fixed_min)
goodput_fixed_min_std = np.std(goodput_fixed_min)
goodput_fixed_min_err = np.array(goodput_fixed_min_err)
pkt_loss_rate_fixed_min = np.array(pkt_loss_rate_fixed_min)
pkt_loss_rate_fixed_min_err = np.array(pkt_loss_rate_fixed_min_err)
tot_pwr_fixed_min = np.array(tot_pwr_fixed_min)
tot_pwr_fixed_min_err = np.array(tot_pwr_fixed_min_err)

goodput_fixed_max = np.array(goodput_fixed_max)
goodput_fixed_max_std = np.std(goodput_fixed_max)
goodput_fixed_max_err = np.std(goodput_fixed_max_err)
pkt_loss_rate_fixed_max = np.array(pkt_loss_rate_fixed_max)
pkt_loss_rate_fixed_max_err = np.array(pkt_loss_rate_fixed_max_err)
tot_pwr_fixed_max = np.array(tot_pwr_fixed_max)
tot_pwr_fixed_max_err = np.array(tot_pwr_fixed_max_err)

plt.figure()

barWidth = 0.15
r1 = np.arange(3)
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
r4 = [x + barWidth for x in r3]
r5 = [x + barWidth for x in r4]

print(goodput_rl_std, goodput_fuzzy_std, goodput_fixed_max_std, goodput_fixed_min_std, goodput_morl_std)

plt.bar(r1, goodput_rl/1000000, yerr=goodput_rl_err/1000000, capsize=3, width=barWidth, label='RL')
plt.bar(r2, goodput_fuzzy_arb/1000000, yerr=goodput_fuzzy_arb_err/1000000, capsize=3, width=barWidth, label='Fuzzy')
plt.bar(r3, goodput_fuzzy/1000000, yerr=goodput_fuzzy_err/1000000, capsize=3, width=barWidth, label='Fuzzy - tuned')
plt.bar(r4, goodput_fixed_min/1000000, yerr=goodput_fixed_min_err/1000000, capsize=3, width=barWidth, label='Fixed MIN')
plt.bar(r5, goodput_fixed_max/1000000, yerr=goodput_fixed_max_err/1000000, capsize=3, width=barWidth, label='Fixed MAX')
#plt.bar(r5, goodput_morl/1000000, yerr=goodput_morl_err/1000000, width=barWidth, label='MORL')


plt.xlabel('Trajectory')
plt.xticks([r + barWidth/2 for r in range(3)], ['o', 'p', 'q'])

plt.legend()
plt.ylabel('Data, Mb')
plt.title('Total Data Transferred')
plt.savefig('04022021_02/04022021_gp_1.png', dpi=600)

plt.figure()
plt.bar(r1, pkt_loss_rate_rl*100, yerr=pkt_loss_rate_rl_err*100, capsize=3, width=barWidth, label='RL')
plt.bar(r2, pkt_loss_rate_fuzzy_arb*100, yerr=pkt_loss_rate_fuzzy_arb_err*100, capsize=3, width=barWidth, label='Fuzzy')
plt.bar(r3, pkt_loss_rate_fuzzy*100, yerr=pkt_loss_rate_fuzzy_err*100, capsize=3, width=barWidth, label='Fuzzy - tuned')
plt.bar(r4, pkt_loss_rate_fixed_min*100, yerr=pkt_loss_rate_fixed_min_err*100, capsize=3, width=barWidth, label='Fixed MIN')
plt.bar(r5, pkt_loss_rate_fixed_max*100, yerr=pkt_loss_rate_fixed_max_err*100, capsize=3, width=barWidth, label='Fixed MAX')
#plt.bar(r5, pkt_loss_rate_morl*100, yerr=pkt_loss_rate_morl_err*100, width=barWidth, label='MORL')


plt.xlabel('Trajectory')
plt.xticks([r + barWidth/1.5 for r in range(3)], ['o', 'p', 'q'])
plt.ylim([0,100])
plt.legend()
plt.ylabel('Packet Loss Rate, %')
plt.title('Packet Loss Rate')
plt.savefig('04022021_02/04022021_plr_1.png', dpi=600)

plt.figure()
plt.bar(r1, tot_pwr_rl, yerr=tot_pwr_rl_err, capsize=3, width=barWidth, label='RL')
plt.bar(r2, tot_pwr_fuzzy_arb, yerr=tot_pwr_fuzzy_arb_err, capsize=3, width=barWidth, label='Fuzzy')
plt.bar(r3, tot_pwr_fuzzy, yerr=tot_pwr_fuzzy_err, capsize=3, width=barWidth, label='Fuzzy - tuned')
plt.bar(r4, tot_pwr_fixed_min, yerr=tot_pwr_fixed_min_err, capsize=3, width=barWidth, label='Fixed MIN')
plt.bar(r5, tot_pwr_fixed_max, yerr=tot_pwr_fixed_max_err, capsize=3, width=barWidth, label='Fixed MAX')
#plt.bar(r5, tot_pwr_morl, yerr=tot_pwr_morl_err, width=barWidth, label='MORL')


plt.xlabel('Trajectory')
plt.xticks([r + barWidth/2 for r in range(3)], ['o', 'p', 'q'])

plt.legend()
plt.ylabel('Power Consumption, Wh')
plt.title('Power Consumption')
plt.savefig('04022021_02/04022021_tp_1.png', dpi=600)