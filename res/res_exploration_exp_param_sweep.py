import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

DATA_DIR = 'data/exploration/'

param_list = np.linspace(0,0,1)
lbl = ['%.2f' % (p) for p in param_list]

data_files = [h5py.File(DATA_DIR+'data_exp_exploration_20102020_0955_'+param+'.hdf5', 'r') for param in lbl]


def t_test(data_a, data_b):
    
    var_a = np.var(data_a)
    var_b = np.var(data_b)
    
    std_dev = np.sqrt((var_a + var_b)/2)
    
    t_val = (np.mean(data_a) + np.mean(data_b))/(std_dev*np.sqrt(2/len(data_a)))
    
    df = 2*len(data_a) - 2
    
    p_val = 1 - stats.t.cdf(t_val, df=df)
    
    print(t_val,df, var_a, var_b, p_val)
    return t_val, p_val


#pkt_loss_ttest = [t_test(a,b) for data_files[p]['data/0/pkt_loss_rate'][()], data_files[p]['data/1/pkt_loss_rate'][()]) in len(param_list)]

pkt_loss_ttest = []
#for p in range(len(param_list)):
for p in range(1):
    a = data_files[p]['data/0/pkt_loss_rate'][()]
    b = data_files[p]['data/1/pkt_loss_rate'][()]
    
    pkt_loss_ttest.append([a.mean(), b.mean(), t_test(a, b)])
    print(stats.ttest_rel(a,b))
    
print(pkt_loss_ttest)

for d in data_files:
    d.close()