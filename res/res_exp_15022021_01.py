import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from res_util import get_radio_data_hist, plt_radio_data_hist, opt_policy_rmse_at_step

DATA_DIR = '../exps/data/'

exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_11022021_1440.hdf5'
exp_optimal_fname = 'radio_data_exp_optimal_front_12022021_1433.hdf5'

data_file_adaptive = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
data_file_optimal = h5py.File(DATA_DIR+exp_optimal_fname, 'r')

path_len = int(data_file_adaptive['exp_cfg/path_len'][()])


decayed_hist = get_radio_data_hist(data_file_adaptive, path_len, '0.00')
adaptive_hist = get_radio_data_hist(data_file_adaptive, path_len, '1.00')
opt_hist = get_radio_data_hist(data_file_optimal, path_len, '0.50')

plt_radio_data_hist(decayed_hist, '15022021_01/15022021_01_decay_hist.png', num_runs=100, title='Decayed Exploration Rate')
plt_radio_data_hist(adaptive_hist, '15022021_01/15022021_01_adptv_hist.png', num_runs=100, title='Adaptive Exploration Adaptive')
plt_radio_data_hist(opt_hist, '15022021_01/15022021_01_opt_hist.png', num_runs=1, title='Optimal Policy')


print()
print()
print('Starting MPE calculation')
rmse_decayed_q = opt_policy_rmse_at_step(data_file_optimal['radio_data'], '0.50', data_file_adaptive['radio_data'], '0.00', 20000)
rmse_adaptive_q = opt_policy_rmse_at_step(data_file_optimal['radio_data'], '0.50', data_file_adaptive['radio_data'], '1.00', 20000)

mpe_decayed = [np.mean(rmse_decayed_q), np.std(rmse_decayed_q)]
mpe_adaptive = [np.mean(rmse_adaptive_q), np.std(rmse_adaptive_q)]

print('\t\tMPE (decayed exploration):')
print('\t\t\to:%.4f\t\tstd=%.4f' % (mpe_decayed[0], mpe_decayed[1]))
print('\t\tMPE (adaptive exploration):')
print('\t\t\to:%.4f\t\tstd=%.4f' % (mpe_adaptive[0], mpe_adaptive[1]))