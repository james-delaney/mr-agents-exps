
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from res_util import get_radio_data_hist, plt_radio_data_hist

DATA_DIR = '../exps/data/'


import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_1454.hdf5' # for env q, w1=0.45
#exp_exploration_fname = 'test_radio_data_exploration_16102020_1552.hdf5' # with a = a_prime change in algorithm
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_19112020_1735.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_w_sweep_01122020_1244.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_env_sweep_26032021_1231.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_w_sweep_09022021_1530.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_19022021_1224.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_24032021_1435.hdf5'
#exp_optimal_fname = 'radio_data_exp_optimal_front_26032021_1240.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_env_sweep_26032021_1247.hdf5'
#exp_adaptive_fname = 'radio_data_exp_fuzzy_agent_rule_sweep_30032021_1109.hdf5'
exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_30032021_1523.hdf5'
#exp_adaptive_fname = 'radio_data_exp_optimal_front_so_30032021_1458.hdf5'
exp_adaptive_fname = 'radio_data_exp_testbed_gm_agent_exp_sweep_23042021_1200.hdf5'


#exp_adaptive_fname = 'thesis_ch3/radio_data_rl_agent_app_sweep_27042021_0944.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_rw_sweep_28042021_1312.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_exp_sweep_28042021_1619.hdf5'

#exp_adaptive_fname = 'radio_data_exp_fixed_policy_agent_env_sweep_30042021_1200.hdf5'
exp_adaptive_fname = 'thesis_ch4/param/radio_data_opt_agent_w1_08052021_1554.hdf5'
exp_adaptive_fname = 'radio_data_exp_optimal_front_09052021_2109.hdf5'

exp_adaptive_fname = 'radio_data_exp_testbed_gm_agent_exp_sweep_14052021_1552.hdf5'

data_file = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
#data_file = h5py.File(DATA_DIR+exp_optimal_fname, 'r')

param_list = data_file['radio_data'].keys()

param_list_str = {'0.00':'Maximised Bitrate Reward',
                  '1.00':'Minimised Power Consumption Reward',
                  '2.00':'Combined max Bitrate, min Power Consumption Reward',}

#print('Radio data histogram, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
exp_cfg_keys = data_file['exp_cfg'].keys()


path_len = int(data_file['exp_cfg/path_len'][()])
#print(data_file['exp_cfg/env_name'][()])

#decayed_hist = get_radio_data_hist(data_file, path_len, '0.00')
#adaptive_hist = get_radio_data_hist(data_file, path_len, '1.00')

#plt_radio_data_hist(decayed_hist, None, num_runs=5, title='Decayed Exploration Rate')
#plt_radio_data_hist(adaptive_hist, None, num_runs=5, title='Adaptive Exploration Adaptive')

for p in param_list:
    hist = get_radio_data_hist(data_file, path_len, p)
    plt_radio_data_hist(hist, None, num_runs=50, title=None)
    

data_file.close()