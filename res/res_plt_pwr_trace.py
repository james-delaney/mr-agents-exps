import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import gym_grid_wireless.envs
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from util.visualisation import TrajectoryVisualiser
import res_util

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_DIR = '../exps/data/'

#sfx = 'exp_fuzzy_agent_08012021_1149.hdf5'
sfx = 'exp_optimal_front_14022021_1538.hdf5'
#sfx = 'exp_fuzzy_agent_10122020_1210.hdf5'
#sfx = 'exp_adaptive_gm_agent_env_sweep_05022021_1144.hdf5'
#sfx = 'exp_fixed_policy_agent_env_sweep_08022021_1606.hdf5'
exp_radio_data_fname = 'radio_data_'+sfx
exp_data_fname = 'data_'+sfx
#exp_radio_data_fname = 'exploration/radio_data_exp_exploration_18112020_1154.hdf5'
#exp_data_fname = 'exploration/data_exp_exploration_18112020_1154.hdf5'

#data_file = h5py.File(DATA_DIR+exp_exploration_fname, 'r')
data_file = h5py.File(DATA_DIR+exp_data_fname, 'r')
radio_data_file = h5py.File(DATA_DIR+exp_radio_data_fname, 'r')


RUN_NO = 0

ENV_NAME = ['o', 'p', 'q']

#print(data_file['radio_data/0.0/'].keys())
#print(radio_data)




for w in radio_data_file['radio_data'].keys():
    rd = [ radio_data_file['radio_data/'+w+'/'+key][()] for key in radio_data_file['radio_data/'+w].keys() ]
    dist_step = radio_data_file['dist_step/'+w][()]
    print(radio_data_file['radio_data/'+w].keys())
    #print(data_file['data/'+w+'/goodput'][()])
    #print(data_file['data/'+w+'/tot_pwr'][()])
    print(np.array(rd).shape)
    print(rd[1][0])
    
    path_len = data_file['exp_cfg/path_len'][()]
    env_name = data_file['exp_cfg/env_name'][()]
    env_spec = data_file['exp_cfg/env_spec'][()]
    if (data_file['exp_cfg/exp_param'][()] == 'env_name'):
        env_name = ENV_NAME[int(float(w))]
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    print('path='+env_path_fname)
    tj = TrajectoryVisualiser(FILE_DIR, env_path_fname, env_fname)
    tj.plt_fig_traj_pwr_lvl(rd[3][RUN_NO], rd[0][RUN_NO], rd[1][RUN_NO], rd[2][RUN_NO], dist_step, title=str('Optimal Agent\nw1=%.2f, Policy value=%d\n' % (float(w), res_util.get_policy_value(rd[0], rd[1], rd[2], RUN_NO))))
    #tj.plt_fig_traj_pwr_lvl(rd[3][RUN_NO], rd[0][RUN_NO], rd[1][RUN_NO], rd[2][RUN_NO], dist_step, title='Fuzzy Multi-Radio Agent\nPolicy value=%d' % (res_util.get_policy_value(rd[0], rd[1], rd[2], RUN_NO)))
    plt.savefig('opt_rl_env_'+env_name+'/pwr_trace_%.2f.png'%(float(w)), dpi=600)
    print(res_util.get_policy_value(rd[0], rd[1], rd[2], RUN_NO))
    
'''    
    #tj.plt_pwr_lvl_trace(rd[1][RUN_NO], rd[2][RUN_NO], dist_step, title=str(w))
    #plt.savefig('pwr_trace_'+w+'.png', dpi=600)
    
    plt.savefig('10122020_07.png', dpi=600)
'''

data_file.close()
radio_data_file.close()
