from __future__ import unicode_literals
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from res_util import opt_policy_rmse_at_step, opt_policy_rmse_per_run, plt_radio_data_hist, plt_radio_data_hist_at_step, get_radio_data_hist, radio_data_to_action_csv, gen_confusion_matrix
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append('../../gym_grid_wireless/')
#print(sys.path)
import gym_grid_wireless.envs
from util.visualisation import EnvTrajectoryPlotter
import pandas as pd
import csv
from io import StringIO
from sklearn.preprocessing import normalize


import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})


#plt.style.use('seaborn')
#plt.style.use('tex')

DATA_DIR = '../exps/data/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
DATA_EXP_DIR = 'comms_letters_11022021/'


ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']

opt_fname = 'opt_actions.csv'
adaptive_fname = 'adaptive_actions.csv'
decayed_fname = 'decayed_actions.csv'


def csv_to_array(fname, dtype):
    arr = []
    with open(fname) as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            arr.append(np.array(row[:20000]).astype(dtype))
            
    return np.array(arr)


def get_actions(env_name):
    opt_fname = 'opt_actions_'+env_name+'.csv'
    adaptive_fname = 'adaptive_actions_'+env_name+'.csv'
    decayed_fname = 'decayed_actions_'+env_name+'.csv'
    
    opt_actions = csv_to_array(DATA_EXP_DIR+opt_fname, int)
    adaptive_actions = csv_to_array(DATA_EXP_DIR+adaptive_fname, int)
    decayed_actions = csv_to_array(DATA_EXP_DIR+decayed_fname, int)
    
    return opt_actions, adaptive_actions, decayed_actions

def aggregate_actions():
    
    env_str = { 'o': 'Long boundary',
                'p': 'Near return',
                'q': 'Linear return'}
    
    error = None
    rmse = None
    for e in ENV_NAME:
        opt, adaptive, decayed = get_actions(e)
        
        adaptive_error = opt-adaptive
        decayed_error = opt-decayed
        
        #adaptive_rmse = np.sqrt(np.sum(np.power(adaptive_error, 2), axis=0)/adaptive_error.shape[0])
        #decayed_rmse = np.sqrt(np.sum(np.power(decayed_error, 2), axis=0)/decayed_error.shape[0])
        
        adaptive_rmse = np.sqrt(np.power(adaptive_error,2).mean(axis=0))
        decayed_rmse = np.sqrt(np.power(decayed_error,2).mean(axis=0))
        
        print(adaptive_rmse.shape)
        
        adaptive_error = adaptive_error.flatten()
        decayed_error = decayed_error.flatten()
        adaptive_rmse = adaptive_rmse.flatten()
        decayed_rmse = decayed_rmse.flatten()
        
        
        len_error = adaptive_error.shape[0]
        len_rmse = adaptive_rmse.shape[0]
        
        env = np.full(len_error, env_str[e])
        expl_decayed = np.full(len_error, 'Decayed')
        expl_adaptive = np.full(len_error, 'Adaptive')
        
        adaptive_error = np.vstack((adaptive_error, env, expl_adaptive))
        decayed_error = np.vstack((decayed_error, env, expl_decayed))
        
        decayed_rmse = np.vstack((decayed_rmse, np.full(len_rmse, env_str[e]), np.full(len_rmse, 'Decayed')))
        adaptive_rmse = np.vstack((adaptive_rmse, np.full(len_rmse, env_str[e]), np.full(len_rmse, 'Adaptive')))
        
        
        if error is not None:
            error = np.hstack((error, adaptive_error, decayed_error))
            print(error.shape)
        else:
            error = np.hstack((adaptive_error, decayed_error))
            print(error.shape)
    
        if rmse is not None:
            rmse = np.hstack((rmse, adaptive_rmse, decayed_rmse))
        else:
            rmse = np.hstack((adaptive_rmse, decayed_rmse))
    
    return error, rmse
'''  
len_ = 2000000
step = np.array(range(len_))    
adaptv = np.full(len_, 'Adaptive')
decay = np.full(len_, 'Decayed')
env_o = np.full(len_, 'Long boundary')
env_p = np.full(len_, 'Near return')
env_q = np.full(len_, 'Linear return')

adaptive_error = opt_actions-adaptive_actions
decayed_error = opt_actions-decayed_actions
agg_adaptive_error =  adaptive_error.flatten()
agg_decayed_error = decayed_error.flatten()

abs_adaptive_error = np.sqrt(np.power(opt_actions-adaptive_actions, 2))
abs_decayed_error = np.sqrt(np.power(opt_actions-decayed_actions, 2))
agg_abs_adaptive_error = abs_adaptive_error.flatten()
agg_abs_decayed_error = abs_decayed_error.flatten()

agg_adaptive_error = np.vstack((agg_adaptive_error, adaptv))
agg_decayed_error = np.vstack((agg_decayed_error, decay))

agg_error = np.hstack((agg_adaptive_error, agg_decayed_error))
df = pd.DataFrame(data=agg_error.T, columns=['error', 'exploration'])
df.explode('error')
df['error'] = df['error'].astype('int')

sns.catplot(data=df, y='error', x='exploration', kind='box', showfliers=False)
'''


'''
ax1 = sns.displot(agg_adaptive_error, bins=range(-10,11))
sns.boxplot(agg_adaptive_error)
#ax1.set_title('Adaptive error')
ax2 = sns.displot(agg_decayed_error, bins=range(-10,11))
sns.boxplot(agg_decayed_error)
#ax2.set_title('Decayed error')
'''
#ax3 = sns.displot(agg_abs_adaptive_error, bins=range(0,11))
#ax3.set_title('Absoulute adaptive error')
#ax4 = sns.displot(agg_abs_decayed_error, bins=range(0,11))
#ax4.set_title('Absolute decayed error')

'''
raw_error, rmse = aggregate_actions()
df = pd.DataFrame(data=raw_error.T, columns=['error', 'env', 'exploration'])
df.explode('error')
df['error'] = df['error'].astype('int')
sns.catplot(data=df, y='error', x='env', hue='exploration', kind='box', showfliers=False)

d = df[df['env'] == 'Long boundary']
dd = d[d['exploration'] == 'Adaptive']
print(d)
sns.displot(data=d)
sns.displot(data=d[d['exploration'] == 'Decayed'])
print(d[d['exploration'] == 'Decayed'])

df = pd.DataFrame(data=rmse.T, columns=['rmse', 'env', 'exploration'])
df.explode('rmse')
df['rmse'] = df['rmse'].astype('float')
sns.catplot(data=df, y='rmse', x='env', hue='exploration', kind='box', showfliers=False)
#print(rmse.T)
'''

opt, adaptive, decayed = get_actions('q')
#print(opt.shape, adaptive.shape)
a, a_report, a_mcc = gen_confusion_matrix(opt[0], adaptive, 11)
d, d_report, d_mcc = gen_confusion_matrix(opt[0], decayed, 11)

opt, adaptive, decayed = get_actions('o')
#print(opt.shape, adaptive.shape)
a_o, a_report_o, a_mcc_o = gen_confusion_matrix(opt[0], adaptive, 11)
d_o, d_report_o, d_mcc_o = gen_confusion_matrix(opt[0], decayed, 11)

opt, adaptive, decayed = get_actions('p')
#print(opt.shape, adaptive.shape)
a_p, a_report_p, a_mcc_p = gen_confusion_matrix(opt[0], adaptive, 11)
d_p, d_report_p, d_mcc_p = gen_confusion_matrix(opt[0], decayed, 11)



'''
# normalize whole matrices
a = a - a.mean()
a = a/np.abs(a).max()

d = d - d.mean()
d = d / np.abs(d).max()
'''
a += a_o
a += a_p

d += d_o
d += d_p


a = (a/np.sum(a, axis=1).reshape(-1,1))*100
a = np.nan_to_num(a)

d = (d/np.sum(d, axis=1).reshape(-1,1))*100
d = np.nan_to_num(d)

print(a)
print(d)

df_a = pd.DataFrame(a, range(11), range(11))
df_d = pd.DataFrame(d, range(11), range(11))




#print('Precision: a=%.4f, d=%.4f' % ())

#print()
#print(df_d)



labels = StringIO(u'''0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,1,0,0,0,0,0
                      0,0,0,0,0,0,1,0,0,0,0
                      0,0,0,0,0,0,0,1,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,1,0
                      0,0,0,0,0,0,0,0,0,0,1
                      ''')
                      
labels = StringIO(u'''0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,1,0,0,0,0,0
                      0,0,0,0,0,0,1,0,0,0,0
                      0,0,0,0,0,0,0,1,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      0,0,0,0,0,0,0,0,0,0,0
                      ''')                      
                      
labels = pd.read_csv(labels, header=None)
annotations = labels.astype(str)
annot_a = np.where(annotations=='1', 'N/A', '      ')
annot_d = np.where(annotations=='1', 'N/A', '      ')



opt_action_mask = np.array([1,1,1,1,1,0,0,0,1,1,1]).reshape(-1,1)
a *= opt_action_mask
d *= opt_action_mask

for i in range(11):
    for j in range(11):
        if a[i,j] > 0.0:
            #annot_a[i,j] = r'$\textbf{{{:.1f}}}$'.format(a[i,j])
            #print(r'$\textbf{{{:.1f}}}$'.format(a[i,j]))
            annot_a[i,j] = '%.1f' % (a[i,j])
        
        if d[i,j] > 0.0:
            #annot_d[i,j] = r'$\textbf{{{:.1f}}}$'.format(d[i,j])
            #annot_d[i,j] = r'$\\textbf{abc}$'
            annot_d[i,j] = '%.1f' % (d[i,j])


plt.figure()
ax1 = sns.heatmap(df_a, annot=annot_a, fmt='s', vmin=0, vmax=100, cbar=False, cmap='rocket', annot_kws={"size":14, "fontweight":'bold'})
for i in range(11):
    ax1.axhline(i, color='white', lw=2)
#plt.title('Adaptive exploration')
plt.xlabel('Learned Actions')
plt.ylabel('Optimal Actions')
plt.gca().invert_yaxis()
cbar = ax1.figure.colorbar(ax1.collections[0])
cbar.set_ticks([0,20,40,60,80,100])
cbar.set_label('Rate of selection, \%')
#plt.savefig('comms_letters_11022021/conf_adaptive.png', dpi=1200)
plt.tight_layout(pad=0.4)
plt.savefig('comms_letters_11022021/conf_adaptive.pdf', format='pdf', bbox_inches='tight')


plt.figure()
ax2 = sns.heatmap(df_d, annot=annot_d, fmt='s', vmin=0, vmax=100, cbar=False, cmap='rocket', annot_kws={"size":14, "fontweight":'bold'})
for i in range(11):
    ax2.axhline(i, color='white', lw=2)
#plt.title('Decayed exploration')
plt.xlabel('Learned Actions')
plt.ylabel('Optimal Actions')
plt.gca().invert_yaxis()
cbar = ax2.figure.colorbar(ax2.collections[0])
cbar.set_ticks([0,20,40,60,80,100])
cbar.set_label('Rate of selection, \%')
#plt.savefig('comms_letters_11022021/conf_decayed.png', dpi=1200)
plt.tight_layout(pad=0.4)
plt.savefig('comms_letters_11022021/conf_decayed.pdf', format='pdf', bbox_inches='tight')

a_precision = []
a_recall = []
d_precision = []
d_recall = []
for i in range(11):
    a_precision.append(a[i,i]/np.sum(a[:,i]))
    a_recall.append(a[i,i]/np.sum(a[i,:]))
    d_precision.append(d[i,i]/np.sum(d[:,i]))
    d_recall.append(d[i,i]/np.sum(d[i,:]))

a_precision = a_precision*opt_action_mask.T
d_precision = d_precision*opt_action_mask.T
a_recall = a_recall*opt_action_mask.T
d_recall = d_recall*opt_action_mask.T

mean_a_p = np.mean(a_precision[a_precision>0.0])
mean_a_r = np.mean(a_recall[a_recall>0.0])
mean_d_p = np.mean(d_precision[d_precision>0.0])
mean_d_r = np.mean(d_recall[d_recall>0.0])
f1_a = (2 * (mean_a_p*mean_a_r))/(mean_a_p+mean_a_r)
f1_d = (2 * (mean_d_p*mean_d_r))/(mean_d_p+mean_d_r)

print('Adaptive stats:')
print('Precision:', mean_a_p)
print('Recall: ', mean_a_r)
print('F1-score: ', f1_a)
print('Decayed stats:')
print('Precision:', mean_d_p)
print('Recall: ', mean_d_r)
print('F1-score: ', f1_d)



print('Adaptive matthews coef:')
print(a_mcc)
print('Decayed matthews coef:')
print(d_mcc)