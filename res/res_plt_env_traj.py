import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import gym_grid_wireless.envs
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from util.visualisation import EnvTrajectoryPlotter

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
#DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]


env = ENV_NAME[2]
spec = ENV_SPEC[2]
plen = PATH_LEN[0]

fnames = get_env_fnames(env, spec, plen)

tj = EnvTrajectoryPlotter(FILE_DIR, [fnames[0]], fnames[1])
tj.plt_multi_traj_env('Trajectory ' + env)
fig_name = 'traj_'+env+'_'+spec+'_'+plen+'.png'
plt.tight_layout()
plt.savefig(fig_name, dpi=600)
