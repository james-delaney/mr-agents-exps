
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns


DATA_DIR = '../exps/data/'

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_1454.hdf5' # for env q, w1=0.45
#exp_exploration_fname = 'test_radio_data_exploration_16102020_1552.hdf5' # with a = a_prime change in algorithm
exp_adaptive_fname = 'data_exp_adaptive_gm_agent_19112020_1735.hdf5'
exp_adaptive_fname_1 = 'data_exp_adaptive_gm_agent_w_sweep_0112020_1723.hdf5'
exp_optimal_fname = 'data_exp_optimal_front_26112020_1020.hdf5'
exp_optimal_fname_1 = 'data_exp_optimal_front_01122020_1040.hdf5'



exp_adaptive_fname_1 = 'data_exp_adaptive_gm_agent_w_sweep_09122020_1517.hdf5'
exp_optimal_fname_1 = 'data_exp_optimal_front_09122020_1523.hdf5'

data_file = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
data_file_1 = h5py.File(DATA_DIR+exp_adaptive_fname_1, 'r')
data_file_optimal = h5py.File(DATA_DIR+exp_optimal_fname, 'r')
data_file_optimal_1 = h5py.File(DATA_DIR+exp_optimal_fname_1, 'r')
param_list = data_file['data'].keys()
param_list_1 = data_file_1['data'].keys()
#print('Radio data histogram, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
print(data_file_1['data'].keys())

#path_len = int(data_file['exp_cfg/path_len'][()])
#path_len = 2000


params = []
goodput = []
pkt_loss_rate = []
tot_pwr = []
goodput_1 = []
pkt_loss_rate_1 = []
tot_pwr_1 = []

goodput_opt = []
pkt_loss_rate_opt = []
tot_pwr_opt = []

goodput_opt_1 = []
pkt_loss_rate_opt_1 = []
tot_pwr_opt_1 = []


for p, p_1 in zip(param_list, param_list_1):
    gp = data_file['data/'+p+'/goodput'][()]
    plr = data_file['data/'+p+'/pkt_loss_rate'][()]
    tp = data_file['data/'+p+'/tot_pwr'][()]
    
    gp_1 = data_file_1['data/'+p_1+'/goodput'][()]
    plr_1 = data_file_1['data/'+p_1+'/pkt_loss_rate'][()]
    tp_1 = data_file_1['data/'+p_1+'/tot_pwr'][()]
    
    gp_opt = data_file_optimal['data/'+p+'/goodput'][()]
    plr_opt = data_file_optimal['data/'+p+'/pkt_loss_rate'][()]
    tp_opt = data_file_optimal['data/'+p+'/tot_pwr'][()] 
    
    gp_opt_1 = data_file_optimal_1['data/'+p_1+'/goodput'][()]
    plr_opt_1 = data_file_optimal_1['data/'+p_1+'/pkt_loss_rate'][()]
    tp_opt_1 = data_file_optimal_1['data/'+p_1+'/tot_pwr'][()] 
    
    params.append(float(p))
    goodput.append(np.mean(gp))
    pkt_loss_rate.append(np.mean(plr))
    tot_pwr.append(np.mean(tp))
    goodput_1.append(np.mean(gp_1))
    pkt_loss_rate_1.append(np.mean(plr_1))
    tot_pwr_1.append(np.mean(tp_1))

    goodput_opt.append(np.mean(gp_opt))
    pkt_loss_rate_opt.append(np.mean(plr_opt))
    tot_pwr_opt.append(np.mean(tp_opt))
    
    goodput_opt_1.append(np.mean(gp_opt_1))
    pkt_loss_rate_opt_1.append(np.mean(plr_opt_1))
    tot_pwr_opt_1.append(np.mean(tp_opt_1))

goodput = np.array(goodput)
goodput_1 = np.array(goodput_1)
pkt_loss_rate = np.array(pkt_loss_rate)
pkt_loss_rate_1 = np.array(pkt_loss_rate_1)
tot_pwr = np.array(tot_pwr)
tot_pwr_1 = np.array(tot_pwr_1)

goodput_opt = np.array(goodput_opt)
pkt_loss_rate_opt = np.array(pkt_loss_rate_opt)
tot_pwr_opt = np.array(tot_pwr_opt)


goodput_opt_1 = np.array(goodput_opt)
pkt_loss_rate_opt_1 = np.array(pkt_loss_rate_opt)
tot_pwr_opt_1 = np.array(tot_pwr_opt)

plt.figure()
plt.scatter(params, goodput/1000000, c='r', label='Power-based')
plt.scatter(params, goodput_1/1000000, c='b', label='Index-based')
plt.scatter(params, goodput_opt/1000000, label='Power-based optimal')
plt.scatter(params, goodput_opt_1/1000000, label='Index-based optimal')
plt.xlabel('w1 Value')
plt.ylabel('Avg. Data Tx, Mb')
plt.title('Avg. Data Transferred vs. w1 Value')
plt.legend()
plt.savefig('03122020_avg_dtx.png', dpi=600)


plt.figure()
plt.scatter(params, pkt_loss_rate, c='r', label='Power-based')
plt.scatter(params, pkt_loss_rate_1, c='b', label='Index-based')
plt.scatter(params, pkt_loss_rate_opt, label='Power-based optimal')
plt.scatter(params, pkt_loss_rate_opt_1, label='Index-based optimal')
plt.xlabel('w1 Value')
plt.ylabel('Packet Loss Rate')
plt.title('Avg. Packet Loss Rate vs. w1 Value')
plt.legend()
plt.savefig('03122020_avg_pkt_loss.png', dpi=600)

plt.figure()
plt.scatter(params, tot_pwr, c='r', label='Power-based')
plt.scatter(params, tot_pwr_1, c='b', label='Index-based')
plt.scatter(params, tot_pwr_opt, label='Power-based optimal')
plt.scatter(params, tot_pwr_opt, label='Index-based optimal')
plt.xlabel('w1 Value')
plt.ylabel('Total Power Consumed, Wh')
plt.title('Avg. Total Power Consumed vs. w1 Value')
plt.legend()
plt.savefig('03122020_avg_pwr_cons.png', dpi=600)


data_file.close()
data_file_1.close()
data_file_optimal.close()