# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 09:23:28 2021

@author: James
"""
import numpy as  np
import h5py 
import sys

DATA_DIR = '../exps/data/'

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_1454.hdf5' # for env q, w1=0.45
#exp_exploration_fname = 'test_radio_data_exploration_16102020_1552.hdf5' # with a = a_prime change in algorithm
exp_adaptive_fname = 'radio_data_exp_optimal_front_26112020_1020.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_rw_sweep_04022021_1111.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_rw_sweep_12012021_1652.hdf5'
#exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_rw_sweep_02022021_1300.hdf5'


data_file = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')

param_list = data_file['radio_data'].keys()
exp_cfg_keys = data_file['exp_cfg'].keys()

for k in exp_cfg_keys:
    print(k, data_file['exp_cfg/'+k][()])