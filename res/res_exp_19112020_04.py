
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns


DATA_DIR = '../exps/data/'

#exp_exploration_fname = 'test_radio_data_exploration.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_0910.hdf5'
#exp_exploration_fname = 'test_radio_data_exploration_15102020_1454.hdf5' # for env q, w1=0.45
#exp_exploration_fname = 'test_radio_data_exploration_16102020_1552.hdf5' # with a = a_prime change in algorithm
exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_19112020_1735.hdf5'


exp_optimal_fname = 'radio_data_exp_optimal_front_26112020_1020.hdf5'

data_file = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')
data_file_optimal = h5py.File(DATA_DIR+exp_optimal_fname, 'r')

param_list = data_file['radio_data'].keys()

print('Radio data histogram, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
print(data_file.keys())

path_len = int(data_file['exp_cfg/path_len'][()])
#path_len = 2000

def radio_data_to_hist(r0, r1):
    r0_hist = []
    r1_hist = []
    for i in range(path_len):
        r0_hist.append(np.bincount(r0[:,i], minlength=6))
        r1_hist.append(np.bincount(r1[:,i], minlength=6))
        if i % 1000 == 0:
            sys.stdout.write('\r\tCalculating histogram done %d steps of %d' % (i, path_len))
            sys.stdout.flush()
            
    r0_hist = np.array(r0_hist)
    r1_hist = np.array(r1_hist)
    print('\tDone')
    print(r0_hist.shape)
    
    r0_hist_bin = []
    r1_hist_bin = []
    for i in range(int(path_len/50)):
        r0_hist_bin.append(np.mean(r0_hist[i*50:(i*50+50), :], axis=0))
        r1_hist_bin.append(np.mean(r1_hist[i*50:(i*50+50), :], axis=0))
    
    r0_hist_bin = np.array(r0_hist_bin)
    r1_hist_bin = np.array(r1_hist_bin)
    
    # stack up the histograms for the two radios' power levels (0-4 is radio 0, 5-9 is radio 1)
    cmb_pwr_lvl = np.vstack((r0_hist[:,1:].T, r1_hist[:,1:].T))
    return cmb_pwr_lvl

for p in param_list:
    #print('param=%d; pkt loss rate=%.2f' % (p, data_file['data/'+p+'/pkt_loss_rate']) )
    #print('param=%d; goodput=%.2f' % (p, data_file['data/'+p+'/goodput']) )
    r0 = data_file['radio_data/'+p+'/radio_0_pwr_trace']
    r1 = data_file['radio_data/'+p+'/radio_1_pwr_trace']
    
    cmb_pwr_lvl = radio_data_to_hist(r0, r1)
    
    opt_r0 = data_file_optimal['radio_data/'+p+'/radio_0_pwr_trace']
    opt_r1 = data_file_optimal['radio_data/'+p+'/radio_1_pwr_trace']
    
    opt_cmb_pwr_lvl = radio_data_to_hist(opt_r0, opt_r1)
    
    l = []
    for a in range(path_len):
        l.append(np.max(cmb_pwr_lvl[:,a]))
    print(np.array(l))
    print(np.mean(np.array(l)))
    
    fig, (ax1, ax2, axcb) = plt.subplots(1, 3, gridspec_kw={'width_ratios':[1,1,0.08]})
    ax1.get_shared_y_axes().join(ax2)
    
    #print(cmb_pwr_lvl.shape)
    #axcb= ax.figure.colorbar(ax.collections[0])
    h1 = sns.heatmap(cmb_pwr_lvl, cmap='rocket_r', cbar=False, xticklabels=2500, ax=ax1)
    h1.invert_yaxis()
    h1.set_xlabel('Steps')
    h1.set_ylabel('Action, (Power levels)')
    h1.set_title('RL Agent')
    
    
    h2 = sns.heatmap(opt_cmb_pwr_lvl, cmap='rocket_r', xticklabels=2500, ax=ax2, cbar_ax = axcb)
    h2.invert_yaxis()
    h2.set_xlabel('Steps')
    #h2.set_ylabel('Action, (Power levels)')
    h2.set_title('Optimal')
    
    axcb.set_yticks([0, 20, 40, 60, 80, 100])
    axcb.set_yticklabels(['0', '0.2', '0.4', '0.6', '0.8', '1.0'])
    axcb.set_ylabel('Probability of selection')
    
    fig.suptitle('w1=%.2f, w2=%.2f' % (float(p), 1.0-float(p)))
    
    fig_fname = '19112020_04/19112020_4_o_%.2f.png' % (float(p))
    plt.tight_layout()
    plt.savefig(fig_fname, dpi=600)
data_file.close()
data_file_optimal.close()