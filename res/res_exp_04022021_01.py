import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import numpy as  np
import h5py 
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import gym_grid_wireless.envs
from util.visualisation import TrajectoryVisualiser

DATA_DIR = '../exps/data/'
FILE_DIR = os.path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]





#exp_adaptive_fname = 'radio_data_exp_adaptive_gm_agent_w_sweep_09022021_1530.hdf5'
exp_adaptive_fname = 'radio_data_exp_adaptive_rl_agent_rw_sweep_04022021_1111.hdf5'

data_file = h5py.File(DATA_DIR+exp_adaptive_fname, 'r')

param_list = data_file['radio_data'].keys()

param_list_str = {'0.00':'Maximised Bitrate Reward',
                  '1.00':'Minimised Power Consumption Reward',
                  '2.00':'Combined max Bitrate, min Power Consumption Reward',}

#print('Radio data histogram, w1='+str(data_file['exp_cfg/w1_comp_val'][()]))
exp_cfg_keys = data_file['exp_cfg'].keys()

for k in exp_cfg_keys:
    print(k, data_file['exp_cfg/'+k][()])
'''
path_len = int(data_file['exp_cfg/path_len'][()])
#path_len = 2000
for p in param_list:
    #print('param=%d; pkt loss rate=%.2f' % (p, data_file['data/'+p+'/pkt_loss_rate']) )
    #print('param=%d; goodput=%.2f' % (p, data_file['data/'+p+'/goodput']) )
    r0 = data_file['radio_data/'+p+'/radio_0_pwr_trace']
    r1 = data_file['radio_data/'+p+'/radio_1_pwr_trace']
    
    r0_hist = []
    r1_hist = []
    for i in range(path_len):
        r0_hist.append(np.bincount(r0[:,i], minlength=6))
        r1_hist.append(np.bincount(r1[:,i], minlength=6))
        if i % 1000 == 0:
            sys.stdout.write('\r\tCalculating histogram done %d steps of %d' % (i, path_len))
            sys.stdout.flush()
            
    r0_hist = np.array(r0_hist)
    r1_hist = np.array(r1_hist)
    print('\tDone')
    print(r0_hist.shape)
    
    r0_hist_bin = []
    r1_hist_bin = []
    for i in range(int(path_len/50)):
        r0_hist_bin.append(np.mean(r0_hist[i*50:(i*50+50), :], axis=0))
        r1_hist_bin.append(np.mean(r1_hist[i*50:(i*50+50), :], axis=0))
    
    r0_hist_bin = np.array(r0_hist_bin)
    r1_hist_bin = np.array(r1_hist_bin)

    # stack up the histograms for the two radios' power levels (0-4 is radio 0, 5-9 is radio 1)
    cmb_pwr_lvl = np.vstack((r0_hist[:,1:].T, r1_hist[:,1:].T))
    
    fig = plt.figure()
    ax = sns.heatmap(cmb_pwr_lvl, vmin=0, vmax=100, cmap='rocket_r', cbar=False, xticklabels=2500)
    cbar = ax.figure.colorbar(ax.collections[0])
    cbar.set_ticks([0, 20, 40, 60, 80, 100])
    cbar.set_ticklabels(['0', '0.2', '0.4', '0.6', '0.8', '1.0'])
    cbar.set_label('Probability of selection')
    ax.invert_yaxis()
    ax.set_xlabel('Steps')
    ax.set_ylabel('Action, (Power levels)')
    ax.set_title('w1=%.2f\n \n' % (float(p)))
    #fig_fname = '04022021_01/04022021_decay_rw_%.2f.png' % (float(p))
    #plt.savefig(fig_fname, dpi=600)
'''    
 
env = data_file['exp_cfg/env_name'][()]
spec = ENV_SPEC[2]
plen = PATH_LEN[0]

fnames = get_env_fnames(env, spec, plen)

print(data_file['radio_data/'].keys())
radio_use_trace = data_file['radio_data/2.00/radio_use_trace'][()]
outage_trace = data_file['radio_data/2.00/outage_trace'][()]
radio_0_pwr_trace = data_file['radio_data/2.00/radio_0_pwr_trace'][()]
radio_1_pwr_trace = data_file['radio_data/2.00/radio_1_pwr_trace'][()]
dist_step = data_file['dist_step/2.00'][()]

RUN_NO = 54

tj = TrajectoryVisualiser(FILE_DIR, fnames[0], fnames[1])
tj.plt_fig_traj_pwr_lvl(radio_use_trace[RUN_NO], outage_trace[RUN_NO], radio_0_pwr_trace[RUN_NO], radio_1_pwr_trace[RUN_NO], dist_step)
data_file.close()