import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import numpy as np

DST_NODE_X = 100
DST_NODE_Y = 100

import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 14 ,
    'font.size': 14,
    'legend.fontsize': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
})

class TrajectoryVisualiser():

    def __init__(self, file_dir, path_fname, env_fname):
        self.file_dir = file_dir
        self.path_fname = path_fname
        self.env_fname = env_fname

        self.env_polygons = []
        self.env_PLE = []

        self.path_points = []
        self.path_points_x = []
        self.path_points_y = []

        #print('init trajectory viz')
        print('\tloading path: ' + str(self.path_fname))
        with open(self.file_dir+self.path_fname, 'r', newline='') as file:
            lines = csv.reader(file, delimiter=',')
            for line in lines:
                #print([int(line[0]), int(line[1])])
                self.path_points.append([int(line[0]), int(line[1])])
                self.path_points_x.append(int(line[0]))
                self.path_points_y.append(int(line[1]))
    
    
        print('\tloading polygons: ' + str(self.env_fname))
        with open(self.file_dir+self.env_fname, 'r') as file:
            lines = csv.reader(file, delimiter=',')
            env = next(lines)
            
            line=next(lines)
            while line:
            #for line in lines:
                p = []
                
                for i in range(1, len(line), 2):
                    p.append([float(line[i]), float(line[i+1])])
                poly  = Polygon(p)
                
                self.env_polygons.append(poly)
                self.env_PLE.append(float(line[0]))

                line = next(lines, None)

    # Plot a combined trajectory w/adaptive radio selection and power level vs. distance plot
    def plt_fig_traj_pwr_lvl(self, radio_use_trace, outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step, title=None, r0_lbl='802.15.4', r0_color='m', r1_lbl='802.11', r1_color='c'):
        #fig, axs = plt.subplots(1,2, )
        fig = plt.figure(figsize=(12.4, 4.8))
        if title is not None:
            fig.suptitle(title)
        axs = []
        axs.append(plt.subplot2grid((16, 40), (0, 16), colspan=1, rowspan=16))
        axs.append(plt.subplot2grid((16, 40), (0, 0), colspan=16, rowspan=16))
        axs.append(plt.subplot2grid((16, 40), (0, 22), colspan=23, rowspan=16))

        norm = mpl.colors.Normalize(vmin=3.0, vmax=5.0, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in self.env_polygons:
            axs[1].fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))

        cb1 = mpl.colorbar.ColorbarBase(axs[0], cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})
            
        r0_points_x = []
        r0_points_y = []
        r1_points_x = []
        r1_points_y = []
        nr_points_x = []
        nr_points_y = []

        r0_dn_points_x = []
        r0_dn_points_y = []
        r1_dn_points_x = []
        r1_dn_points_y = []
        
        for i in range(0, len(self.path_points)):
            #if i % 1000 == 0:
                #print("count: " + str(i)) 
            if radio_use_trace[i] == 0:
                #print("num " + str(i) + "r0")
                r0_points_x.append(self.path_points_x[i])
                r0_points_y.append(self.path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='y')
            elif radio_use_trace[i] == 1:
                #print("num " + str(i) + "r1")
                r1_points_x.append(self.path_points_x[i])
                r1_points_y.append(self.path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='c')
            else:
                #print("num " + str(i) + "nr")
                nr_points_x.append(self.path_points_x[i])
                nr_points_y.append(self.path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='r')

            if outage_trace[i] == 1:
                r0_dn_points_x.append(self.path_points_x[i])
                r0_dn_points_y.append(self.path_points_y[i])
            elif outage_trace[i] == 2:
                r1_dn_points_x.append(self.path_points_x[i])
                r1_dn_points_y.append(self.path_points_y[i])
        
        #print(len(r0_dn_points_x), len(r1_dn_points_x))
        #print(radio_use_trace)

        axs[1].plot(r0_points_x, r0_points_y, 'om', markersize=1, c=r0_color, label=r0_lbl+' Radio')
        axs[1].plot(r1_points_x, r1_points_y, 'oc', markersize=1, c=r1_color, label=r1_lbl+' Radio')
        
        axs[1].plot(r0_dn_points_x, r0_dn_points_y, 'ob', markersize=0.5, c='b', label=r0_lbl+' Radio down')
        axs[1].plot(r1_dn_points_x, r1_dn_points_y, 'og', markersize=0.5, c='g', label=r1_lbl+' Radio down')
        axs[1].plot(nr_points_x, nr_points_y, 'or', markersize=0.5, c='r', label='Radio off')

        axs[1].plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")

        axs[1].set_xlabel('X coordinate, m')
        axs[1].set_ylabel('Y coordinate, m')
        #ax2.set_xticks([])
        #ax2.set_yticks([])
        lg = axs[1].legend(prop={'size':8}, loc=4)
        lg.legendHandles[0]._legmarker.set_markersize(5)
        lg.legendHandles[1]._legmarker.set_markersize(5)
        lg.legendHandles[2]._legmarker.set_markersize(5)
        lg.legendHandles[3]._legmarker.set_markersize(5)
        lg.legendHandles[4]._legmarker.set_markersize(5)
        lg.legendHandles[5]._legmarker.set_markersize(5)
        axs[1].set_xlim(0, 500)
        axs[1].set_ylim(0, 500)
        
        #plt.axis([0,500,0,500])
        axs[1].set_title('Mobile Node Trajectory and Adaptive Radio Selection')


        axs[2].set_xlabel('Steps')

        

        ax2 = axs[2].twinx()
        l2 = ax2.plot(range(0,len(self.path_points)), dist_step, c='r', label='Node distance')

        ax2.set_ylim([0, 450])
        ax2.set_ylabel('Node distance, m')
        #axs[2].legend(loc="upper right", bbox_to_anchor=(1,1), bbox_transform=axs[2].transAxes, markerscale=3)
        
        r0 = axs[2].scatter(range(0,len(self.path_points)), radio_0_pwr_trace, c=r0_color, s=1.5, label=r0_lbl+' Radio')
        r1 = axs[2].scatter(range(0,len(self.path_points)), radio_1_pwr_trace, c=r1_color, s=1.5, label=r1_lbl+' Radio')
        axs[2].set_ylim([0.5,5.5])
        axs[2].set_ylabel('Radio Power Level')
        
        axs[2].set_zorder(ax2.get_zorder()+1)
        axs[2].patch.set_visible(False)
        
        axs[2].set_title('Radio Power Level and Node Distance vs. Steps')

    def plt_pwr_lvl_trace(self, radio_0_pwr_trace, radio_1_pwr_trace, dist_step, title=''):
        #fig = plt.figure()
        #### Plot Radio Power level and distance vs no. of steps
        fig, ax1 =plt.subplots()
        plt.xlabel('Steps')
        print(len(self.path_points), len(radio_0_pwr_trace))
        r0 = ax1.scatter(range(0,len(self.path_points)), radio_0_pwr_trace, c='m', s=1.5, label='802.15.4 Radio')
        r1 = ax1.scatter(range(0,len(self.path_points)), radio_1_pwr_trace, c='c', s=1.5, label='802.11 Radio')
        ax1.set_ylim([0.5,5.5])
        ax1.set_ylabel('Radio Power Level')
        
        ax2 = ax1.twinx()
        l2 = ax2.plot(range(0,len(self.path_points)), dist_step, c='r', label='Node distance')
        
        ax2.set_ylim([0, 400])
        ax2.set_ylabel('Node distance, m')
    
        
    
        fig.suptitle('Radio Power Level and Node Distance vs. Steps\n'+title)
        ax1.plot(np.nan, c='r', label='Node distance')
        ax1.legend(loc='lower right', markerscale=4)
        #fig.legend(loc="lower right", bbox_to_anchor=(0,1), bbox_transform=ax1.transAxes, markerscale=3)

    def plt_mean_pwr_use(self, radio_0_pwr_trace, radio_1_pwr_trace, num_runs, p_len, title=''):
        r0_pwr_use = np.array([np.bincount(radio_0_pwr_trace[r]) for r in range(num_runs)])
        r1_pwr_use = np.array([np.bincount(radio_1_pwr_trace[r]) for r in range(num_runs)])
        #print(r0_pwr_use)
        #print(r1_pwr_use)
        #total_on_steps = [np.sum(r0_pwr_use[r][1:6])+np.sum(r1_pwr_use[r][1:6]) for r in range(num_runs)]
        #print(total_on_steps)
        r0_pwr_use_pc = np.array([[r0_pwr_use[r,l]/p_len for l in range(6)] for r in range(num_runs)])
        r1_pwr_use_pc = np.array([[r1_pwr_use[r,l]/p_len for l in range(6)] for r in range(num_runs)])
        mean_r0_pwr_use = np.mean(r0_pwr_use_pc, axis=0)
        std_r0_pwr_use = np.std(r0_pwr_use_pc, axis=0)
        mean_r1_pwr_use = np.mean(r1_pwr_use_pc, axis=0)
        std_r1_pwr_use = np.std(r1_pwr_use_pc, axis=0)

        #print(r0_pwr_use_pc)
        #print(r1_pwr_use_pc)
        #print(mean_r0_pwr_use, mean_r1_pwr_use)

        lbl = ['OFF', '1', '2', '3', '4', '5']
        x = np.arange(len(lbl))
        bar_width = 0.35

        fig, ax = plt.subplots()
        rects1 = ax.bar(x - bar_width/2, mean_r0_pwr_use*100, bar_width, color='m', yerr=std_r0_pwr_use, label='802.15.4 Radio')
        rects2 = ax.bar(x + bar_width/2, mean_r1_pwr_use*100, bar_width, color='c', yerr=std_r1_pwr_use, label='802.11 Radio')

        #err1 = ax.errorbar(x-bar_width/2, mean_r0_pwr_use, std_r0_pwr_use)
        #err2 = ax.errorbar(x-bar_width/2, mean_r1_pwr_use, std_r1_pwr_use)

        ax.set_ylabel('% time in Power State')
        ax.set_xlabel('Power States')
        ax.set_xticks(x)
        ax.set_ylim((0,100))
        ax.set_title(title)
        ax.set_xticklabels(lbl)
        ax.legend()
        
    def save(self, fname, dpi):
        plt.savefig(fname, dpi=dpi)

        
class EnvTrajectoryPlotter():
    def __init__(self, file_dir, path_fnames, env_fname):
        self.file_dir = file_dir
        self.path_fnames = path_fnames
        self.env_fname = env_fname

        self.env_polygons = []
        self.env_PLE = []

        self.path_points_x = []
        self.path_points_y = []

        print('init trajectory viz')
        for path_fname in path_fnames:
            pts_x = []
            pts_y = []
            print('\tloading path: ' + str(path_fname))
            with open(self.file_dir+path_fname, 'r', newline='') as file:
                lines = csv.reader(file, delimiter=',')
                for line in lines:
                    pts_x.append(int(line[0])) 
                    pts_y.append(int(line[1]))
                    
            self.path_points_x.append(pts_x)
            self.path_points_y.append(pts_y)
    
    
        print('\tloading polygons: ' + str(self.env_fname))
        with open(self.file_dir+self.env_fname, 'r') as file:
            lines = csv.reader(file, delimiter=',')
            env = next(lines)
            
            line=next(lines)
            while line:
            #for line in lines:
                p = []
                
                for i in range(1, len(line), 2):
                    p.append([float(line[i]), float(line[i+1])])
                poly  = Polygon(p)
                
                self.env_polygons.append(poly)
                self.env_PLE.append(float(line[0]))

                line = next(lines, None)
        
    def plt_multi_traj_env(self, title):
        fig = plt.figure(figsize=(5, 5))
        if title is not None:
            fig.suptitle(title)
        axs = []
        axs.append(plt.subplot2grid((16, 17), (0, 16), colspan=1, rowspan=16))
        axs.append(plt.subplot2grid((16, 17), (0, 0), colspan=16, rowspan=16))

        norm = mpl.colors.Normalize(vmin=3.5, vmax=5, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in self.env_polygons:
            axs[1].fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))

        cb1 = mpl.colorbar.ColorbarBase(axs[0], cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})
        
        count = 0
        colors = ['orange', 'r']
        for pts_x, pts_y, fname in zip(self.path_points_x, self.path_points_y, self.path_fnames):
            #print(pts_y)
            axs[1].plot(pts_x, pts_y, colors[count], label='Trajectory: '+fname[0])
            count += 1

        axs[1].plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")
        
        axs[1].set_xlabel('X coordinate, m')
        axs[1].set_ylabel('Y coordinate, m')
        axs[1].set_xlim(0, 500)
        axs[1].set_ylim(0, 500)
        
        lg = axs[1].legend(prop={'size':8}, loc=4)
        
        
    def plt_multi_fig_traj_pwr_lvl(self, radio_data, title=None, fig_name=None):
        fig = plt.figure()
        if title is not None:
            fig.suptitle(title)
        axs = []
        axs.append(plt.subplot2grid((16, 17), (0, 16), colspan=1, rowspan=16))
        axs.append(plt.subplot2grid((16, 17), (0, 0), colspan=16, rowspan=16))

        norm = mpl.colors.Normalize(vmin=3.5, vmax=5, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in self.env_polygons:
            axs[1].fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))

        cb1 = mpl.colorbar.ColorbarBase(axs[0], cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})
        
        count = 0
        r0_points_x = []
        r0_points_y = []
        r1_points_x = []
        r1_points_y = []
        nr_points_x = []
        nr_points_y = []

        r0_dn_points_x = []
        r0_dn_points_y = []
        r1_dn_points_x = []
        r1_dn_points_y = []
        
        for pts_x, pts_y, radio_data in zip(self.path_points_x, self.path_points_y, radio_data):
            #print(pts_y)
            print(radio_data[0])
            for i in range(0, len(pts_x)):
                #if i % 1000 == 0:
                    #print("count: " + str(i)) 
                
                if radio_data[0][i] == 0:
                    #print("num " + str(i) + "r0")
                    r0_points_x.append(pts_x[i])
                    r0_points_y.append(pts_y[i])
                    #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='y')
                elif radio_data[0][i] == 1:
                    #print("num " + str(i) + "r1")
                    r1_points_x.append(pts_x[i])
                    r1_points_y.append(pts_y[i])
                    #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='c')
                else:
                    #print("num " + str(i) + "nr")
                    nr_points_x.append(pts_x[i])
                    nr_points_y.append(pts_y[i])
                    #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='r')
    
                if radio_data[1][i] == 1:
                    r0_dn_points_x.append(pts_x[i])
                    r0_dn_points_y.append(pts_y[i])
                elif radio_data[1][i] == 2:
                    r1_dn_points_x.append(pts_x[i])
                    r1_dn_points_y.append(pts_y[i])
            
            #print(len(r0_dn_points_x), len(r1_dn_points_x))
            #print(radio_use_trace)
    
        axs[1].plot(r0_points_x, r0_points_y, 'om', markersize=1, c='m', label='802.15.4 Radio')
        axs[1].plot(r1_points_x, r1_points_y, 'oc', markersize=1, c='c', label='802.11 Radio')
        
        axs[1].plot(r0_dn_points_x, r0_dn_points_y, 'ob', markersize=0.5, c='b', label='802.15.4 Radio down')
        axs[1].plot(r1_dn_points_x, r1_dn_points_y, 'og', markersize=0.5, c='g', label='802.11 Radio down')
        axs[1].plot(nr_points_x, nr_points_y, 'or', markersize=0.5, c='r', label='Radio off')

        axs[1].plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")
        
        axs[1].set_xlabel('X coordinate, m')
        axs[1].set_ylabel('Y coordinate, m')
        axs[1].set_xlim(0, 500)
        axs[1].set_ylim(0, 500)
        
        lg = axs[1].legend(prop={'size':14}, loc=1)
        lg.legendHandles[0]._legmarker.set_markersize(5)
        lg.legendHandles[1]._legmarker.set_markersize(5)
        lg.legendHandles[2]._legmarker.set_markersize(5)
        lg.legendHandles[3]._legmarker.set_markersize(5)
        lg.legendHandles[4]._legmarker.set_markersize(5)
        lg.legendHandles[5]._legmarker.set_markersize(5)
        
        
        plt.tight_layout(pad=0.4)
        
        if fig_name is not None:
            #plt.savefig(fig_name, dpi=600)
            plt.savefig(fig_name, format='pdf', bbox_inches='tight')

    def plt_traj_on_ax(self, ax, title=None, min=3.5, max=5):
        norm = mpl.colors.Normalize(vmin=min, vmax=max, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in self.env_polygons:
            ax.fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))

        count = 0
        colors = ['deeppink', 'r']
        for pts_x, pts_y, fname in zip(self.path_points_x, self.path_points_y, self.path_fnames):
            #print(pts_y)
            ax.plot(pts_x, pts_y, colors[count], label='MN trajectory')
            count += 1

        ax.plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")
        
        ax.set_xlabel('X coordinate, m')
        ax.set_ylabel('Y coordinate, m')
        ax.set_xlim(0, 500)
        ax.set_ylim(0, 500)
        
        lg = ax.legend(prop={'size':12}, loc=4)
        
        if title is not None:
            ax.title.set_text(title)
        
    def plt_cbar_on_ax(self, ax, min=3.5, max=5):
        norm = mpl.colors.Normalize(vmin=min, vmax=max, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        
        cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 12})
        
        
class StepBasedVisualiser():

    def __init__(self):
        pass

    # Scatter plot a single figure showing a single run of time-based array data
    # 'y_lim' parameter should be None for default limits or a tuple of two numbers in the form (lower_limit, upper_limit)
    # 'scale' parameter multiplies each data value by a scalar - can be used to multiply/divide and change the units of the data plotted
    def plt_fig_single_run(self, y_data, y_label=None, title=None, series_label=None, markersize=1, y_lim=None, scale=None):
        fig = plt.figure()

        if scale is None:
            scale = 1

        plt.scatter(range(0, len(y_data)), y_data*scale, s=markersize, label=series_label)
        plt.xlabel('Steps')
        
        if title is not None:
            plt.title(title)

        if y_label is not None:
            plt.ylabel(y_label)
            plt.legend(markerscale=3)
        
        if y_label is not None:
            if (y_lim is not None) and (len(y_lim) == 2):
                plt.ylim(y_lim[0], y_lim[1])
            else:
                print('y_lim isn\'t a len 2 tuple, using default limits')
        


        

    def plt_fig_multi_run(self, y_data_arr, y_label, series_labels, title=None, markersize=1, y_lim=None, scale=None):
        fig = plt.figure()

        if scale is None:
            scale = 1

        if len(y_data_arr) == len(series_labels):
            for data, label in zip(y_data_arr, series_labels):
                plt.scatter(range(0, len(data)), data*scale, s=markersize, label=label)
        else:
            print('ERROR plotting multi-run figure: y data array and series labels lists are not the same length, %d != %d' % (len(y_data_arr), len(series_labels)))
            return
        
        plt.xlabel('Steps')
        
        if title is not None:
            plt.title(title)
        
        if y_label is not None:
            plt.ylabel(y_label)
            plt.legend(markerscale=3)
        
        if y_label is not None:
            if (y_lim is not None) and (len(y_lim) == 2):
                plt.ylim(y_lim[0], y_lim[1])
            else:
                print('y_lim isn\'t a len 2 tuple, using default limits')






#
#
#    
# For future ref
'''
fig = plt.figure()
for i in range(len(param_list)):
    #print(param_pwr_cons[i])
    #param_pwr_cons_i = np.apply_along_axis(np.bincount, axis=0, arr=param_pwr_cons[i], minlength=np.max(param_pwr_cons[i])+1)
    param_pwr_cons_i = np.mean(param_pwr_cons[i], axis=0)
    plt.scatter(range(0, PATH_LEN), param_pwr_cons_i, s=1, label=str(param_list[i])+'mA')
plt.xlabel('Steps')
plt.ylabel('Avg. Power Consumption, mA')
plt.title('Avg. Instantaneous Power Consumption vs. Steps')
plt.ylim(0, 250)
lg = plt.legend(markerscale=3)
#ax = plt.gca()
#lg = ax.legend(prop={'size':8})
#for i in range(0,11):
#    lg.legendHandles[i]._legmarker.set_markersize(5)


fig = plt.figure()
for i in range(len(param_list)):
    param_bitrate_i = np.mean(param_bitrate[i], axis=0)
    plt.scatter(range(0, PATH_LEN), param_bitrate_i/1000, s=1, label=str(param_list[i])+'mA')
plt.xlabel('Steps')
plt.ylabel('Avg. Bitrate, Kbps')
plt.title('Avg. Instantaneous Bitrate vs. Steps')
plt.ylim(0, 1000)
lg = plt.legend(markerscale=3)
#ax = plt.gca()
#lg = ax.legend(prop={'size':8})
#for i in range(0,11):
#    lg.legendHandles[i]._legmarker.set_markersize(5)
'''

'''
fig = plt.figure()
plt.scatter(range(0, PATH_LEN), param_inst_pwr_cons[0], s=1, label='w1='+str(param_list[0]))
plt.xlabel('Steps')
plt.ylabel('Power Consumption, mA')
plt.title('Instantaneous Power Consumption vs. Steps')
lg = plt.legend(markerscale=3)

fig = plt.figure()
plt.scatter(range(0, PATH_LEN), param_inst_bitrate[0]/1000, s=1, label='w1='+str(param_list[0]))
plt.xlabel('Steps')
plt.ylabel('Bitrate, Kbps')
plt.title('Instantaneous Bitrate vs. Steps')
lg = plt.legend(markerscale=3)
'''

'''
fig = plt.figure()
plt.scatter(range(0, PATH_LEN), cum_step_reward[RESULT_RUN], s=1)
plt.xlabel('Steps')
plt.ylabel('Reward')
plt.title('Agent Reward vs. Steps')
'''

'''
fig = plt.figure()
plt.scatter(range(0, PATH_LEN), weights0_step, s=1, label='w1')
plt.scatter(range(0, PATH_LEN), weights1_step, s=1, label='w2')
plt.xlabel('Steps')
plt.ylabel('Weight value')
plt.title('Adaptive Reward Function Weights')
plt.legend(markerscale=3)
'''