#import gym
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from util.visualisation import EnvTrajectoryPlotter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 12 ,
    'font.size': 12,
    'legend.fontsize': 12,
    'xtick.labelsize': 12,
    'ytick.labelsize': 12,
})

import os
import gym_grid_wireless.envs

FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
print(FILE_DIR)
ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3_4.5', '3.5_5','4_6']
PATH_LEN = ['20000']

def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]


env_name_o = ENV_NAME[0]
env_name_p = ENV_NAME[1]
env_name_q = ENV_NAME[2]
env_spec_easy = ENV_SPEC[1]
env_spec_hard = ENV_SPEC[2]
env_spec_extrahard = ENV_SPEC[3]
path_len = PATH_LEN[0]

env_fnames_o_hard = get_env_fnames(env_name_o, env_spec_hard, path_len)
env_fnames_p_hard = get_env_fnames(env_name_p, env_spec_hard, path_len)
env_fnames_q_hard = get_env_fnames(env_name_q, env_spec_hard, path_len)

env_fnames_o_easy = get_env_fnames(env_name_o, env_spec_easy, path_len)
env_fnames_p_easy = get_env_fnames(env_name_p, env_spec_easy, path_len)
env_fnames_q_easy = get_env_fnames(env_name_q, env_spec_easy, path_len)

env_fnames_o_extrahard = get_env_fnames(env_name_o, env_spec_extrahard, path_len)
env_fnames_p_extrahard = get_env_fnames(env_name_p, env_spec_extrahard, path_len)
env_fnames_q_extrahard = get_env_fnames(env_name_q, env_spec_extrahard, path_len)

tj_o = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_o_hard[0]], env_fnames_o_hard[1])
tj_p = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_p_hard[0]], env_fnames_p_hard[1])
tj_q = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_q_hard[0]], env_fnames_q_hard[1])

'''
fig = plt.figure(figsize=(12, 9.6))
axs = []
axs.append(plt.subplot2grid((35, 42), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (0,17), colspan=1, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (0,24), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (0,41), colspan=1, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (20,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (20,17), colspan=1, rowspan=16))

tj_o.plt_traj_on_ax(axs[0], title='Long boundary', min=3, max=5)
tj_o.plt_cbar_on_ax(axs[1], min=3, max=5)
tj_p.plt_traj_on_ax(axs[2], title='Linear return', min=3, max=5)
tj_p.plt_cbar_on_ax(axs[3], min=3, max=5)
tj_q.plt_traj_on_ax(axs[4], title='Near return', min=3, max=5)
tj_q.plt_cbar_on_ax(axs[5], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_hard_sq.pdf', format='pdf', bbox_inches='tight')


# easy environments
tj_o = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_o_easy[0]], env_fnames_o_easy[1])
tj_p = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_p_easy[0]], env_fnames_p_easy[1])
tj_q = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_q_easy[0]], env_fnames_q_easy[1])

fig = plt.figure(figsize=(12, 9.6))
axs = []
axs.append(plt.subplot2grid((35, 42), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (0,17), colspan=1, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (0,24), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (0,41), colspan=1, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (20,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((35, 42), (20,17), colspan=1, rowspan=16))

tj_o.plt_traj_on_ax(axs[0], title='Long boundary', min=3, max=5)
tj_o.plt_cbar_on_ax(axs[1], min=3, max=5)
tj_p.plt_traj_on_ax(axs[2], title='Linear return', min=3, max=5)
tj_p.plt_cbar_on_ax(axs[3], min=3, max=5)
tj_q.plt_traj_on_ax(axs[4], title='Near return', min=3, max=5)
tj_q.plt_cbar_on_ax(axs[5], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_easy_sq.pdf', format='pdf', bbox_inches='tight')
'''
#
#
#
#
'''
tj_o = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_o_hard[0]], env_fnames_o_hard[1])
tj_p = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_p_hard[0]], env_fnames_p_hard[1])
tj_q = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_q_hard[0]], env_fnames_q_hard[1])


fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_o.plt_traj_on_ax(axs[0], min=3, max=5)
tj_o.plt_cbar_on_ax(axs[1], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_o_hard.pdf', format='pdf', bbox_inches='tight')

fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_q.plt_traj_on_ax(axs[0], min=3, max=5)
tj_q.plt_cbar_on_ax(axs[1], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_q_hard.pdf', format='pdf', bbox_inches='tight')


fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_p.plt_traj_on_ax(axs[0], min=3, max=5)
tj_p.plt_cbar_on_ax(axs[1], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_p_hard.pdf', format='pdf', bbox_inches='tight')

# easy environments
tj_o = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_o_easy[0]], env_fnames_o_easy[1])
tj_p = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_p_easy[0]], env_fnames_p_easy[1])
tj_q = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_q_easy[0]], env_fnames_q_easy[1])


fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_o.plt_traj_on_ax(axs[0], min=3, max=5)
tj_o.plt_cbar_on_ax(axs[1], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_o_easy.pdf', format='pdf', bbox_inches='tight')

fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_q.plt_traj_on_ax(axs[0], min=3, max=5)
tj_q.plt_cbar_on_ax(axs[1], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_q_easy.pdf', format='pdf', bbox_inches='tight')


fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_p.plt_traj_on_ax(axs[0], min=3, max=5)
tj_p.plt_cbar_on_ax(axs[1], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_p_easy.pdf', format='pdf', bbox_inches='tight')
'''


'''
fig = plt.figure(figsize=(16, 4.8))
axs = []
axs.append(plt.subplot2grid((16, 55), (0, 0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 55), (0, 19), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 55), (0, 38), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 55), (0, 54), colspan=1, rowspan=16))

#tj.plt_multi_fig_traj_pwr_lvl(radio_data_l, title=None, fig_name='comms_letters_11022021/fig_sample_env.pdf')
tj_o.plt_traj_on_ax(axs[0], title='Long boundary', min=3, max=5)
tj_p.plt_traj_on_ax(axs[1], title='Linear return', min=3, max=5)
tj_q.plt_traj_on_ax(axs[2], title='Near return', min=3, max=5)
tj_q.plt_cbar_on_ax(axs[3], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_hard.pdf', format='pdf', bbox_inches='tight')


# easy environments
tj_o = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_o_easy[0]], env_fnames_o_easy[1])
tj_p = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_p_easy[0]], env_fnames_p_easy[1])
tj_q = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_q_easy[0]], env_fnames_q_easy[1])

fig = plt.figure(figsize=(16, 4.8))
axs = []
axs.append(plt.subplot2grid((16, 55), (0, 0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 55), (0, 19), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 55), (0, 38), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 55), (0, 54), colspan=1, rowspan=16))

#tj.plt_multi_fig_traj_pwr_lvl(radio_data_l, title=None, fig_name='comms_letters_11022021/fig_sample_env.pdf')
tj_o.plt_traj_on_ax(axs[0], title='Long boundary', min=3, max=5)
tj_p.plt_traj_on_ax(axs[1], title='Linear return', min=3, max=5)
tj_q.plt_traj_on_ax(axs[2], title='Near return', min=3, max=5)
tj_q.plt_cbar_on_ax(axs[3], min=3, max=5)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_easy.pdf', format='pdf', bbox_inches='tight')
'''


tj_o = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_o_extrahard[0]], env_fnames_o_extrahard[1])
tj_p = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_p_extrahard[0]], env_fnames_p_extrahard[1])
tj_q = EnvTrajectoryPlotter(FILE_DIR, [env_fnames_q_extrahard[0]], env_fnames_q_extrahard[1])


fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_o.plt_traj_on_ax(axs[0], min=3, max=6)
tj_o.plt_cbar_on_ax(axs[1], min=3, max=6)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_o_extrahard.pdf', format='pdf', bbox_inches='tight')

fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_q.plt_traj_on_ax(axs[0], min=3, max=6)
tj_q.plt_cbar_on_ax(axs[1], min=3, max=6)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_q_extrahard.pdf', format='pdf', bbox_inches='tight')


fig = plt.figure(figsize=(6,4.8))
axs = []
axs.append(plt.subplot2grid((16, 18), (0,0), colspan=16, rowspan=16))
axs.append(plt.subplot2grid((16, 18), (0,17), colspan=1, rowspan=16))
tj_p.plt_traj_on_ax(axs[0], min=3, max=6)
tj_p.plt_cbar_on_ax(axs[1], min=3, max=6)

plt.tight_layout()

plt.savefig('out/fig_imr_envs_p_extrahard.pdf', format='pdf', bbox_inches='tight')
