import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]

#import gym
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.fuzzy_agent_v3 import FuzzyMultiRadioAgent
from agents.fuzzy_ctl import FuzzyController, FuzzySet, MultiRadioFuzzySet, RadioPwrCtlFuzzySet

import skfuzzy as fuzz
from skfuzzy.control.visualization import FuzzyVariableVisualizer
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use("pgf")
matplotlib.rcParams.update({
    'text.usetex': True,
    'font.family': 'Times New Roman',
    'axes.labelsize': 10 ,
    'font.size': 10,
    'legend.fontsize': 10,
    'xtick.labelsize': 10,
    'ytick.labelsize': 10,
})


def plt_two_same_vars(vars, var_names, title=None, xlabels=None, xlim=None, fname=None, bbox_anchor_x=1):
    fig, ax = plt.subplots(2,1)
    fvv = []
    for i in range(len(vars)):
        fvv.append(FuzzyVariableVisualizer(vars[i]))
        plt.close()
        
        fvv[i].fig = fig
        fvv[i].ax = ax[i]

        
    #for i in range(len(vars)):
        fvv[i].view()
    
    #for i in range(len(vars)):
        if xlabels is not None:
            ax[i].set_xlabel(xlabels[i])
            
        if xlim is not None:
            ax[i].set_xlim(xlim)
            
        ax[i].legend().set_visible(False)
        
    
    
    if title is not None:
        fig.suptitle(title)
    
    #plt.tight_layout()
    fig.legend(labels=var_names, bbox_to_anchor=(bbox_anchor_x,0.7),loc=1)
    fig.tight_layout()
    fig.subplots_adjust(right=0.77)
    
    if fname is not None:
        plt.savefig(fname, format='pdf', bbox_inches='tight')

mr_fuzzy_set = MultiRadioFuzzySet(0)


input_vars_snr = [mr_fuzzy_set.inputs['snr_80211'], mr_fuzzy_set.inputs['snr_802154']]
input_vars_snr_xlbl = ['SNR (IEEE802.11 Radio)', 'SNR (IEEE802.15.4 Radio)']
input_vars_snr_mf_lbl = ['None', 'Low', 'Low-Med', 'Med', 'Med-High', 'High']

input_vars_ptx = [mr_fuzzy_set.inputs['tx_pow_80211'], mr_fuzzy_set.inputs['tx_pow_802154']]
input_vars_ptx_xlbl = [r'$P_{tx}$ level (IEEE802.11 Radio)', r'$P_{tx}$ level (IEEE802.15.4 Radio)']
input_vars_ptx_mf_lbl = ['Off', '1', '2', '3', '4', '5']


output_vars_ctl = [mr_fuzzy_set.outputs['tx_pow_ctl_80211'], mr_fuzzy_set.outputs['tx_pow_ctl_802154']]
output_vars_ctl_xlbl = [r'$P_{tx}$ control action (IEEE802.11 Radio)', r'$P_{tx}$ control action (IEEE802.15.4 Radio)']
output_vars_ctl_mf_lbl = ['Off', 'Increase', 'Decrease', 'No Change', 'Switch Radio']

plt_two_same_vars(input_vars_snr, input_vars_snr_mf_lbl, title='SNR', xlabels=input_vars_snr_xlbl, xlim=[-10,50], fname='fig_fuzzy_set_inp_snr.pdf', bbox_anchor_x=1.001)
plt_two_same_vars(input_vars_ptx, input_vars_ptx_mf_lbl, title=r'Transmit Power Level', xlabels=input_vars_ptx_xlbl, xlim=[0,6], fname='fig_fuzzy_set_inp_ptx.pdf', bbox_anchor_x=1)
plt_two_same_vars(output_vars_ctl, output_vars_ctl_mf_lbl, title='Radio and Power Control Output', xlabels=output_vars_ctl_xlbl, xlim=[0,5.5], fname='fig_fuzzy_set_out_ctl.pdf', bbox_anchor_x=1)

'''
fig, ax = plt.subplots(2,1)

fvv = FuzzyVariableVisualizer(mr_fuzzy_set.inputs['snr_802154'])
fvv.fig = fig
fvv.ax = ax[0]

fvv2 = FuzzyVariableVisualizer(mr_fuzzy_set.inputs['snr_80211'])
fvv2.fig = fig
fvv2.ax = ax[1]

fvv.view()
fvv2.view()

ax[0].set_xlim([-10,50])
ax[0].set_xlabel('SNR (IEEE802.15.4 Radio)')
ax[1].set_xlim([-10,50])
ax[1].set_xlabel('SNR (IEEE802.11 Radio)')

#ax[0].legend(labels=['a','b','c','d','e','f'])
#ax[1].legend(labels=['a','b','c','d','e','f'])
ax[0].legend().set_visible(False)
ax[1].legend().set_visible(False)
fig.legend(labels=['a','b','c','d','e','f'])
fig.suptitle('SNR')

fig.tight_layout()
'''



#plt.show()
