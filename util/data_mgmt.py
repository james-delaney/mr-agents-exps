import os
import csv
import json
import h5py
from os import path

#DATA_EXP_DIR = path.dirname(path.dirname(path.abspath(__file__)))+'\data\'
DATA_EXP_DIR = '../data/'

def export_cfg_results_data(fname, exp_cfg, data):
    '''
    exp_cfg['results'] = data
    
    with open(DATA_EXP_DIR+fname+str('.json'), 'w') as file:
        json.dump(str(exp_cfg), file)
    '''
    file = h5py.File(fname+'.hdf5','w')
    grp_exp_cfg = file.create_group('exp_cfg')
    grp_data = file.create_group('data')
    
    for key in exp_cfg.keys():
        try:
            grp_exp_cfg.create_dataset(key, data=exp_cfg[key])
        except:
            grp_exp_cfg.create_dataset(key, data=str(exp_cfg[key]))
    
    for p in exp_cfg['param_list']:
        '''
        if type(p) is float:
            grp = grp_data.create_group('%.2f'%(p))    
        else:
            grp = grp_data.create_group(str(p))
        '''
        grp = grp_data.create_group('%.2f'%(p))    
        for key in data[p].keys():    
            grp.create_dataset(key, data=data[p][key])
        
        
    print(file.keys())
    print(grp_exp_cfg.keys())
    print(grp_data.keys())
    file.close()
        
# fname = filename string, will have .json appended and be stored in data/
# exp_cfg = a str(exp_cfg) conversion of experiment configuration
# radio_data = [radio_use_trace, outage_trace, radio_0_pwr_trace, radio_1_pwr_trace]
def export_cfg_radio_data(fname, exp_cfg, radio_data, dist_step):
    '''
    results = {
        cfg_item: 'xyz',
        ...,
        ...,
        ...,
        'path_len':X,
        'num_runs':Y,
        'dist_step': [...], (len=path_len)
        results: 
            [
                ['radio_use_trace': [0,0,1,2,...,1], (len=path_len)
                 'outage_trace': [0,0,0,0,...,1], (len=path_len)
                 'radio_0_pwr_trace': [...] (len=path_len)
                 'radio_1_pwr_trace': [...] (len=path_len)],
                [...]
            ] (len=num_runs)
        }
    
    '''
    file = h5py.File(fname+'.hdf5', 'w')
    grp_exp_cfg = file.create_group('exp_cfg')
    grp_dist_step = file.create_group('dist_step')
    grp_radio_data = file.create_group('radio_data')
    
    for key in exp_cfg.keys():
        grp_exp_cfg.create_dataset(key, data=str(exp_cfg[key]))
        
    #grp_dist_step.create_dataset('dist_step', data=dist_step)
    
    i = 0
    for p in exp_cfg['param_list']:
        '''
        if type(p) is float:
            grp = grp_radio_data.create_group('%.2f'%(p))
            grp_dist_step.create_dataset('%.2f'%(p), data=radio_data[i][4])
        else:
            grp = grp_radio_data.create_group(str(p))
            grp_dist_step.create_dataset(str(p), data=radio_data[i][4])
        '''    
        grp = grp_radio_data.create_group('%.2f'%(p))
        grp_dist_step.create_dataset('%.2f'%(p), data=radio_data[i][4])
        
        grp.create_dataset('radio_use_trace', data=radio_data[i][0])
        grp.create_dataset('outage_trace', data=radio_data[i][1])
        grp.create_dataset('radio_0_pwr_trace', data=radio_data[i][2])
        grp.create_dataset('radio_1_pwr_trace', data=radio_data[i][3])
        
        #grp_dist_step.create_dataset('dist_step', data=radio-data[i][4])
        i += 1
        
    print(file.keys())
    print(grp_exp_cfg.keys())
    print(grp_dist_step.keys())
    print(grp_radio_data.keys())
    
    file.close()