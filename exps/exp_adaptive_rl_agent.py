
import gym
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
    
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
#from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV1 as pw1
from gym_grid_wireless.envs.path_power_wireless_env_v1 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates, PowerStates, MultiRadioPowerState, ExtPowerStates, MultiRadioExtPowerState, RadioSNRStates, MultiRadioSNRState
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
from gym_grid_wireless.node import *
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import json
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.rl_agent import RLAgent
from util.visualisation import TrajectoryVisualiser, StepBasedVisualiser
import util.data_mgmt as data_mgmt
from agents.policies import *

# un comment this when runnign thesis exps, comment when running this script directly
import gym_grid_wireless.__init__

FILE_DIR = 'gym_grid_wireless/envs/'
DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


TIME_STEP_INTERVAL = 0.2
RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [10,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

RADIO_CFG_WIFI_LORA = [Radio(0, 915000000, 250000, -120, 7812, [-6,0,5,10,12], [11,16,20,25,29], 'lora', 8),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

ACTION_SPACE     = spaces.Discrete(len(Actions))
STATE_SPACE      = States

EXP_CFG = { 'rl_alg': 'sarsa',
            #'state_space': States,
            'state_space': PowerStates,
            #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
            'states_obj':MultiRadioPowerState(RADIO_CFG),
            'policy': AdaptiveEGreedyPolicy(ACTION_SPACE, len(PowerStates), 0.7, 0.1),
            'rw_fn': RW_FN_PWR_CONS,
            'alpha': 0.7,
            'gamma': 0.1,
            'epsilon_schedule': RLAgent.ADPTV_EPS,
            'rand_agent_expl':False,
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'pkt_sim_cfg': LowFreqPktSimCfg(),
            'shd_cfg': [0, 0.7],
            'env_name': ENV_NAME[0],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 5,
            'exp_param':'rw_fn',
            'param_list': [RW_FN_PWR_CONS, RW_FN_PWR_EFF, RW_FN_PWR_EFF_CONS],
}

EXP_CFG_EPS = {        'rl_alg': 'sarsa',
                        #'state_space': States,
                        'state_space': PowerStates,
                        #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
                        'states_obj':MultiRadioPowerState(RADIO_CFG),
                        'num_components': 2,
                        'w1_comp_val': 0,
                        'rw_fn': RW_FN_PWR_CONS,
                        'alpha': 0.7,
                        'gamma': 0.1,
                        'epsilon_schedule': RLAgent.ADPTV_EPS,
                        'rand_agent_expl':False,
                        'action_space': Actions,
                        'radio_cfg': RADIO_CFG,
                        'env_name': ENV_NAME[2],
                        'env_spec': ENV_SPEC[2],
                        'path_len': PATH_LEN[0],
                        'num_runs': 1000,
                        'exp_param':'epsilon_schedule',
                        'param_list':[RLAgent.DECAY_EPS, RLAgent.ADPTV_EPS],
}

EXP_CFG_ENV = {         'rl_alg': 'sarsa',
                        #'state_space': States,
                        'state_space': PowerStates,
                        #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
                        'states_obj':MultiRadioPowerState(RADIO_CFG),
                        'num_components': 2,
                        'w1_comp_val': 0,
                        'rw_fn': RW_FN_PWR_EFF_CONS,
                        'alpha': 0.7,
                        'gamma': 0.1,
                        'epsilon_schedule': RLAgent.ADPTV_EPS,
                        'rand_agent_expl':False,
                        'action_space': Actions,
                        'radio_cfg': RADIO_CFG,
                        'env_name': ENV_NAME[2],
                        'env_spec': ENV_SPEC[2],
                        'path_len': PATH_LEN[0],
                        'num_runs': 100,
                        'exp_param':'env_name',
                        'param_list':[0, 1, 2],
}

EXP_CFG_EXT_STATE = {   'rl_alg': 'sarsa',
                        'state_space': PowerStates,
                        'states_obj':MultiRadioPowerState(RADIO_CFG),
                        'num_components': 2,
                        'w1_comp_val': 0.05,
                        'alpha': 0.7,
                        'gamma': 0.1,
                        'epsilon_schedule': RLAgent.ADPTV_EPS,
                        'rand_agent_expl':False,
                        'action_space': Actions,
                        'radio_cfg': RADIO_CFG,
                        'env_name': ENV_NAME[2],
                        'env_spec': ENV_SPEC[2],
                        'path_len': PATH_LEN[0],
                        'num_runs': 5,
                        'exp_param':'w1_comp_val',
                        'param_list':[0, 0.3, 0.5],
}

EXP_CFG_LORA = {    'rl_alg': 'sarsa',
                    #'state_space': States,
                    'state_space': PowerStates,
                    #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
                    'states_obj':MultiRadioPowerState(RADIO_CFG_WIFI_LORA),
                    'num_components': 2,
                    'w1_comp_val': 0,
                    'alpha': 0.7,
                    'gamma': 0.1,
                    'epsilon_schedule': RLAgent.ADPTV_EPS,
                    'rand_agent_expl':False,
                    'action_space': Actions,
                    'radio_cfg': RADIO_CFG_WIFI_LORA,
                    'env_name': ENV_NAME[0],
                    'env_spec': ENV_SPEC[2],
                    'path_len': PATH_LEN[0],
                    'num_runs': 100,
                    'exp_param':'w1_comp_val',
                    'param_list':[0.05],
}

EXP_CFG_SNR_STATE = {   'rl_alg': 'sarsa',
                        #'state_space': States,
                        'state_space': RadioSNRStates,
                        #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
                        'states_obj':MultiRadioSNRState(RADIO_CFG),
                        'num_components': 2,
                        'w1_comp_val': 0,
                        'alpha': 0.7,
                        'gamma': 0.1,
                        'epsilon_schedule': RLAgent.ADPTV_EPS,
                        'rand_agent_expl':False,
                        'action_space': Actions,
                        'radio_cfg': RADIO_CFG,
                        'env_name': ENV_NAME[0],
                        'env_spec': ENV_SPEC[2],
                        'path_len': PATH_LEN[0],
                        'num_runs': 20,
                        'exp_param':'w1_comp_val',
                        'param_list': [0.05],
}

#EXP_CFG = EXP_CFG_MAX_MIN_W

def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]

def init_env(env_files, env_cfg):
    env = gym.make("path-power-wireless-v1", 
                    update_interval             = TIME_STEP_INTERVAL, 
                    radio_cfg                   = env_cfg['radio_cfg'], 
                    agent_path_fname            = env_files[0], 
                    env_fname                   = env_files[1], 
                    ple_fname                   = env_files[2], 
                    shd_fname                   = env_files[3], 
                    god_agent                   = False,
                    pkt_sim_cfg                 = env_cfg['pkt_sim_cfg'], 
                    shd_cfg                     = env_cfg['shd_cfg'])
    
    env.action_space = spaces.Discrete(len(env_cfg['action_space']))
    env.action_enum = env_cfg['action_space']
    
    env.set_reward_fn(env_cfg['rw_fn'])

    return env

def init_agent(env, agent_cfg):
    agent = RLAgent(env)

    agent.cfg_agent(agent_cfg['rl_alg'], 
                    agent_cfg['state_space'],
                    agent_cfg['states_obj'], 
                    agent_cfg['alpha'], 
                    agent_cfg['gamma'], 
                    agent_cfg['epsilon_schedule'],
                    agent_cfg['rand_agent_expl'],
                    agent_cfg['policy'])

    return agent


def run_exp(cfg):
    radio_data = []
    data = {}
    
    num_runs = cfg['num_runs']
    p_len = int(cfg['path_len'])
    param = cfg['exp_param']
    param_list = cfg['param_list']
    #param_list = [0, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2]
    #param_list = [0]

    

    print('Starting experiment with ctl param=\''+param+'\'')
    #print('  Params will be: '+str([float('%.2f' % (p)) for p in param_list]))

    for p in range(len(param_list)):
        
        cfg[param] =  param_list[p]
        env_files = get_env_fnames(cfg['env_name'], cfg['env_spec'], cfg['path_len'])

        env = init_env(env_files, cfg)

        agent = init_agent(env, cfg)

        print('\tRunning experiment with \''+param+'\'='+str('%.2f' % (p)) + ' for ' + str(num_runs) + ' runs')
        for i in range(num_runs):
            #print('Run ' + str(i+1) + ' of ' + str(num_runs),end='\r', flush=True)
            sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
            sys.stdout.flush()
            agent.simulate_run(re_init=True)
        
        print()
        # extract and save radio data for this group of parameter runs
        radio_use_trace = env.recorder.data['radio_use_trace']
        radio_outage_trace = env.recorder.data['outage_trace']
        radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
        radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
        dist_step = env.recorder.data['dist_step'][0]
        radio_data.append([radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step])
        
        pkt_loss_rate = env.recorder.data['pkt_loss_rate']
        cum_power = env.recorder.data['tot_pwr']
        cum_step_reward = env.recorder.data['cum_step_reward']
        avg_bitrate = env.recorder.data['avg_bitrate']
        avg_pwr_cons = env.recorder.data['avg_pwr_cons']
        inst_bitrate = env.recorder.data['inst_bitrate']
        inst_pwr_cons = env.recorder.data['inst_pwr_cons']
        goodput = env.recorder.data['goodput']

        pkt_loss_rate = np.array(pkt_loss_rate)
        tot_power = np.array(cum_power)
        avg_bitrate = np.array(avg_bitrate)
        avg_pwr_cons = np.array(avg_pwr_cons)[num_runs-1,-1]

        data[param_list[p]] = {'pkt_loss_rate': pkt_loss_rate,
                               'tot_pwr': cum_power,
                               'goodput': goodput
                              }
    return radio_data, dist_step, data
    

'''
cfg = EXP_CFG
radio_data, dist_step, data = run_exp(cfg)




curr_time = time.localtime()
    
ts = str('%02d' % (curr_time.tm_hour))+str('%02d' % (curr_time.tm_min))
data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data_exp_adaptive_rl_agent_rw_sweep_28042021_'+ts, cfg, data)
data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data_exp_adaptive_rl_agent_rw_sweep_28042021_'+ts, cfg, radio_data, dist_step)
'''