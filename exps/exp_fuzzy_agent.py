import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]

import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
#from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates, PowerStates, MultiRadioPowerState, ExtPowerStates, MultiRadioExtPowerState
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import json
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.fuzzy_agent_v3 import FuzzyMultiRadioAgent
from agents.fuzzy_ctl import FuzzyController, FuzzySet, MultiRadioFuzzySet, RadioPwrCtlFuzzySet
from util.visualisation import TrajectoryVisualiser, StepBasedVisualiser
import util.data_mgmt as data_mgmt
import skfuzzy as fuzz

FILE_DIR = 'gym_grid_wireless/envs/'
DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


TIME_STEP_INTERVAL = 0.2
RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [10,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

RADIO_CFG_WIFI_LORA = [Radio(0, 915000000, 250000, -120, 7812, [-6,0,5,10,12], [11,16,20,25,29], 'lora', 8),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]



BALANCED_FUZZY_SET = MultiRadioFuzzySet(0.00)
MIN_PWR_FUZZY_SET = MultiRadioFuzzySet(1.00, 'power')
MAX_BR_FUZZY_SET = MultiRadioFuzzySet(2.00, 'bitrate')
'''
FUZZY_SET.inputs['snr_80211']['none'] = fuzz.trapmf(FUZZY_SET.inputs['snr_80211'].universe, [-40, -40, 1, 1])
FUZZY_SET.inputs['snr_80211']['low'] = fuzz.trapmf(FUZZY_SET.inputs['snr_80211'].universe, [1, 2, 4, 5])
FUZZY_SET.inputs['snr_80211']['low_med'] = fuzz.trimf(FUZZY_SET.inputs['snr_80211'].universe, [4, 7, 10])
FUZZY_SET.inputs['snr_80211']['med'] = fuzz.trimf(FUZZY_SET.inputs['snr_80211'].universe, [10, 15, 20])
FUZZY_SET.inputs['snr_80211']['med_high'] = fuzz.trimf(FUZZY_SET.inputs['snr_80211'].universe, [15, 20, 25])
FUZZY_SET.inputs['snr_80211']['high'] = fuzz.trapmf(FUZZY_SET.inputs['snr_80211'].universe, [24, 29, 100, 100])

FUZZY_SET.setup_rules()
'''

EXP_CFG = { 'fuzzy_set': MIN_PWR_FUZZY_SET,
            'input_map': {
                    'snr_802154': 'radios[0].get_snr()',
                    'snr_80211': 'radios[1].get_snr()',
                    'tx_pow_802154': 'radios[0].get_tx_pow_step()+1',
                    'tx_pow_80211': 'radios[1].get_tx_pow_step()+1'
            },
            'action_space': ControlActions,
            'radio_cfg': RADIO_CFG,
            'env_name': 0,
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'shd_cfg': [10, 0.7],
            'num_runs': 1,
            'exp_param':'fuzzy_set',
            'param_list': [BALANCED_FUZZY_SET, MIN_PWR_FUZZY_SET, MAX_BR_FUZZY_SET],
}

EXP_CFG_ENV = { 'fuzzy_set': BALANCED_FUZZY_SET,
                'input_map': {
                    'snr_802154': 'radios[0].get_snr()',
                    'snr_80211': 'radios[1].get_snr()',
                    'tx_pow_802154': 'radios[0].get_tx_pow_step()+1',
                    'tx_pow_80211': 'radios[1].get_tx_pow_step()+1'
            },
            'action_space': ControlActions,
            'radio_cfg': RADIO_CFG,
            'env_name': ENV_NAME[0],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 2,
            'exp_param':'env_name',
            'param_list': [0,1,2],
}

def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]

def init_env(env_files, env_cfg):
    env = gym.make("path-power-wireless-v2", 
                    update_interval             = TIME_STEP_INTERVAL, 
                    radio_cfg                   = env_cfg['radio_cfg'], 
                    agent_path_fname            = env_files[0], 
                    env_fname                   = env_files[1], 
                    ple_fname                   = env_files[2], 
                    shd_fname                   = env_files[3], 
                    god_agent                   = True,
                    pkt_sim_cfg                 = env_cfg['pkt_sim_cfg'],
                    shd_cfg                     = env_cfg['shd_cfg'])
    
    env.action_space = spaces.Discrete(len(env_cfg['action_space']))
    env.action_enum = env_cfg['action_space']

    return env

def init_agent(env, agent_cfg):
    agent = FuzzyMultiRadioAgent(env)

    agent.cfg_agent(agent_cfg['fuzzy_set'],
                    agent_cfg['input_map'])

    return agent

def run_exp_weight_param_var(cfg):
    radio_data = []
    data = {}
    
    num_runs = cfg['num_runs']
    p_len = int(cfg['path_len'])
    param = cfg['exp_param']
    param_list = cfg['param_list']

    print('Starting experiment with ctl param=\''+param+'\'')
    print('  Params will be: '+str([float('%.2f' % (p)) for p in param_list]))

    for p in range(len(param_list)):
        
        cfg[param] =  param_list[p]
        env_files = get_env_fnames(cfg['env_name'], cfg['env_spec'], cfg['path_len'])

        env = init_env(env_files, cfg)

        agent = init_agent(env, cfg)

        print('\tRunning experiment with \''+param+'\'='+str('%.2f' % (param_list[p])) + ' for ' + str(num_runs) + ' runs')
        for i in range(num_runs):
            #print('Run ' + str(i+1) + ' of ' + str(num_runs),end='\r', flush=True)
            sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
            sys.stdout.flush()
            agent.simulate_run(re_init=True)
    
    
        # extract and save radio data for this group of parameter runs
        radio_use_trace = env.recorder.data['radio_use_trace']
        radio_outage_trace = env.recorder.data['outage_trace']
        radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
        radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
        dist_step = env.recorder.data['dist_step'][0]
        radio_data.append([radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step])
        
        pkt_loss_rate = env.recorder.data['pkt_loss_rate']
        cum_power = env.recorder.data['tot_pwr']
        cum_step_reward = env.recorder.data['cum_step_reward']
        avg_bitrate = env.recorder.data['avg_bitrate']
        avg_pwr_cons = env.recorder.data['avg_pwr_cons']
        inst_bitrate = env.recorder.data['inst_bitrate']
        inst_pwr_cons = env.recorder.data['inst_pwr_cons']
        goodput = env.recorder.data['goodput']
        
        pkt_loss_rate = np.array(pkt_loss_rate)
        tot_power = np.array(cum_power)
        avg_bitrate = np.array(avg_bitrate)
        avg_pwr_cons = np.array(avg_pwr_cons)[num_runs-1,-1]
        
        data[param_list[p]] = {'pkt_loss_rate': pkt_loss_rate,
                               'tot_pwr': cum_power,
                               'goodput': goodput
                              }
    
    return radio_data, dist_step, data

'''
cfg = EXP_CFG
radio_data, dist_step, data = run_exp_weight_param_var(cfg)


curr_time = time.localtime()
    
ts = str('%02d' % (curr_time.tm_hour))+str('%02d' % (curr_time.tm_min))
fname_sfx = '_exp_fuzzy_agent_rule_sweep_30032021_'+ts
data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data'+fname_sfx, cfg, data)
data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data'+fname_sfx, cfg, radio_data, dist_step)
print('Data saved to: ', fname_sfx+'.hdf5')

'''