import gym


# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]
import gym

from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
from gym_grid_wireless.envs import PathPowerWirelessEnvV1 as pw1
#from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v1 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates, PowerStates, MultiRadioPowerState
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import json
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
#print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.optimum_agent import OptimalAgent, OptimalSOAgent
from util.visualisation import TrajectoryVisualiser, StepBasedVisualiser
import util.data_mgmt as data_mgmt

# un comment this when runnign thesis exps, comment when running this script directly
#import gym_grid_wireless.__init__

FILE_DIR = 'gym_grid_wireless/envs/'
DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


TIME_STEP_INTERVAL = 0.2
RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [10,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

RADIO_CFG_WIFI_LORA = [Radio(0, 915000000, 250000, -120, 7812, [-6,0,5,10,12], [11,16,20,25,29], 'lora', 8),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]


ACTION_SPACE     = Actions
STATE_SPACE      = States

EXP_CFG = { 'env_id':'path-power-wireless-v3',
            'num_components': 2,
            'component_weights': [0,1],
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'env_name': ENV_NAME[2],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'shd_cfg': [0,0.7],   # shadowing cfg, 0=std dev, 1=correlation coeff
            'num_runs': 1,
            'exp_param':'component_weights',
            #'param_list':np.linspace(0.2,0.3,11),
            'param_list':[0.10]
}

EXP_CFG_SO = {
    'env_id':'path-power-wireless-v1',
    'rw_fn': RW_FN_PWR_EFF_CONS,
    'action_space': Actions,
    'radio_cfg': RADIO_CFG,
    'env_name': 0,
    'env_spec': ENV_SPEC[2],
    'path_len': PATH_LEN[0],
    'shd_cfg': [6,0.7],   # shadowing cfg, 0=std dev, 1=correlation coeff
    'num_runs': 100,
    'exp_param':'rw_fn',
    'param_list': [RW_FN_PWR_CONS, RW_FN_PWR_EFF_CONS, RW_FN_PWR_EFF]
    }

def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]

def init_env(env_files, env_cfg):
    
    env = gym.make(env_cfg['env_id'],
                   update_interval = TIME_STEP_INTERVAL, 
                   radio_cfg                   = env_cfg['radio_cfg'], 
                   agent_path_fname            = env_files[0], 
                   env_fname                   = env_files[1], 
                   ple_fname                   = env_files[2], 
                   shd_fname                   = env_files[3], 
                   god_agent                   = True, 
                   pkt_sim_cfg                 = DefaultPktSimCfg(),
                   shd_cfg                     = env_cfg['shd_cfg'])
    
    env.action_space = spaces.Discrete(len(env_cfg['action_space']))
    env.action_enum = env_cfg['action_space']

    return env

def init_agent(env, agent_cfg):
    agent = OptimalAgent(env)

    agent.cfg_agent(agent_cfg['num_components'], 
                    agent_cfg['component_weights'])

    return agent

# experiment to run optimal multi-objective agent to generate a pareto front
def run_exp_gen_front(cfg):
    radio_data = []
    data = {}
    
    num_runs = cfg['num_runs']
    p_len = int(cfg['path_len'])
    param = cfg['exp_param']
    param_list = cfg['param_list']
    
    env_files = get_env_fnames(cfg['env_name'], cfg['env_spec'], cfg['path_len'])

    print('Starting experiment with ctl param=\''+param+'\'')
    print('  Params will be: '+str([float('%.2f' % (p)) for p in param_list]))
    
    for p in range(len(param_list)):
        cfg[param] = [param_list[p], 1-param_list[p]]
        
        env = init_env(env_files, cfg)
        agent = init_agent(env, cfg)
        
        print('\tRunning opt front experiment with \''+param+'\'='+str('[%.2f, %.2f]' % (param_list[p], 1-param_list[p])) + ' for ' + str(num_runs) + ' runs')
        for i in range(num_runs):
            sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
            sys.stdout.flush()
            agent.simulate_run(re_init=True)
    
        # extract and save radio data for this group of parameter runs
        radio_use_trace = env.recorder.data['radio_use_trace']
        radio_outage_trace = env.recorder.data['outage_trace']
        radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
        radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
        dist_step = env.recorder.data['dist_step'][0]
        
        
        pkt_loss_rate = env.recorder.data['pkt_loss_rate']
        cum_power = env.recorder.data['tot_pwr']
        cum_step_reward = env.recorder.data['cum_step_reward']
        avg_bitrate = env.recorder.data['avg_bitrate']
        avg_pwr_cons = env.recorder.data['avg_pwr_cons']
        inst_bitrate = env.recorder.data['inst_bitrate']
        inst_pwr_cons = env.recorder.data['inst_pwr_cons']
        goodput = env.recorder.data['goodput']
        mo_reward = env.recorder.data['multi_obj_reward']

        pkt_loss_rate = np.array(pkt_loss_rate)
        tot_power = np.array(cum_power)
        avg_bitrate = np.array(avg_bitrate)
        avg_pwr_cons = np.array(avg_pwr_cons)[num_runs-1,-1]

        inst_bitrate = np.array(inst_bitrate)
        inst_pwr_cons = np.array(inst_pwr_cons)

        #mean_pkt_loss = np.mean(pkt_loss_rate)
        #std_pkt_loss = np.std(pkt_loss_rate)
        #mean_power = np.mean(tot_power)
        #std_power = np.std(tot_power)

        #mean_avg_bitrate = np.mean(avg_bitrate[:,p_len-1])
        #std_avg_bitrate = np.std(avg_bitrate[:,p_len-1])
        
        #mean_goodput = np.mean(goodput)
        #std_goodput = np.std(goodput)

        radio_data.append([radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step])
        #data.append([mean_pkt_loss, std_pkt_loss, mean_power, std_power, mean_avg_bitrate, std_avg_bitrate, mean_goodput, std_goodput])
        '''
        data[param_list[p]] = {'mean_pkt_loss':     env.recorder.mean_across_runs('pkt_loss_rate'),
                               'std_pkt_loss':      env.recorder.std_across_runs('pkt_loss_rate'),
                               'mean_power':        env.recorder.mean_across_runs('tot_pwr'),
                               'std_power':         env.recorder.std_across_runs('tot_pwr'),
                               'mean_goodput':      env.recorder.mean_across_runs('goodput'),
                               'std_goodput':       env.recorder.std_across_runs('goodput')
            }
        '''
        data[param_list[p]] = {'pkt_loss_rate': pkt_loss_rate,
                               'tot_pwr': cum_power,
                               'goodput': goodput,
                               'multi_obj_reward': mo_reward,
                              }
        
    
    return radio_data, dist_step, data

def run_exp_single_obj(cfg):
    radio_data = []
    data = {}
    
    num_runs = cfg['num_runs']
    p_len = int(cfg['path_len'])
    param = cfg['exp_param']
    param_list = cfg['param_list']
    #param_list = [0, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2]
    #param_list = [0]

    

    print('Starting experiment with ctl param=\''+param+'\'')
    print('  Params will be: '+str([float('%.2f' % (p)) for p in param_list]))

    for p in range(len(param_list)):
        
        cfg[param] =  param_list[p]
        env_files = get_env_fnames(cfg['env_name'], cfg['env_spec'], cfg['path_len'])

        env = init_env(env_files, cfg)
        env.set_reward_fn(cfg['rw_fn'])
        agent = OptimalSOAgent(env)
        agent.cfg_agent()

        print('\tRunning experiment with \''+param+'\'='+str('%.2f' % (param_list[p])) + ' for ' + str(num_runs) + ' runs')
        for i in range(num_runs):
            #print('Run ' + str(i+1) + ' of ' + str(num_runs),end='\r', flush=True)
            sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
            sys.stdout.flush()
            agent.simulate_run(re_init=False)
        
        print()
        # extract and save radio data for this group of parameter runs
        radio_use_trace = env.recorder.data['radio_use_trace']
        radio_outage_trace = env.recorder.data['outage_trace']
        radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
        radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
        dist_step = env.recorder.data['dist_step'][0]
        radio_data.append([radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step])
        
        pkt_loss_rate = env.recorder.data['pkt_loss_rate']
        cum_power = env.recorder.data['tot_pwr']
        cum_step_reward = env.recorder.data['cum_step_reward']
        avg_bitrate = env.recorder.data['avg_bitrate']
        avg_pwr_cons = env.recorder.data['avg_pwr_cons']
        inst_bitrate = env.recorder.data['inst_bitrate']
        inst_pwr_cons = env.recorder.data['inst_pwr_cons']
        goodput = env.recorder.data['goodput']

        pkt_loss_rate = np.array(pkt_loss_rate)
        tot_power = np.array(cum_power)
        avg_bitrate = np.array(avg_bitrate)
        avg_pwr_cons = np.array(avg_pwr_cons)[num_runs-1,-1]

        data[param_list[p]] = {'pkt_loss_rate': pkt_loss_rate,
                               'tot_pwr': cum_power,
                               'goodput': goodput
                              }
    return radio_data, dist_step, data


def run_exp_w_optimal(w1_val):
    num_runs = EXP_CFG['num_runs']
    p_len = int(EXP_CFG['path_len'])
    #param = EXP_CFG['exp_param']
    param = 'component_weights'
    param_list = EXP_CFG['param_list']
    
    env_files = get_env_fnames(EXP_CFG['env_name'], EXP_CFG['env_spec'], EXP_CFG['path_len'])

    print('Starting experiment with ctl param=\''+param+'\'')
    print('  Params will be: '+str(w1_val))
    
    
    EXP_CFG[param] = [w1_val, 1-w1_val]
    
    env = init_env(env_files, EXP_CFG)
    agent = init_agent(env, EXP_CFG)
    
    print('\tRunning experiment with \''+param+'\'='+str('[%.2f, %.2f]' % (w1_val, 1-w1_val)) + ' for ' + str(num_runs) + ' runs')
    for i in range(num_runs):
        sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
        sys.stdout.flush()
        agent.simulate_run(re_init=True)
        

EXP_CFG = EXP_CFG
radio_data, dist_step, data = run_exp_gen_front(EXP_CFG)
#radio_data, dist_step, data = run_exp_single_obj(EXP_CFG)

#print(data)


curr_time = time.localtime()
    
ts = str('%02d' % (curr_time.tm_hour))+str('%02d' % (curr_time.tm_min))
fname_sfx = '_exp_optimal_front_09052021_'+ts
data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data'+fname_sfx, EXP_CFG, data)
data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data'+fname_sfx, EXP_CFG, radio_data, dist_step)
print('Data saved to: ', fname_sfx)


'''
param_list = EXP_CFG['param_list']
for p in range(len(param_list)):
    print(data[param_list[p]]['mean_goodput'][0])
    print(data[param_list[p]]['mean_power'][0])
    plt.scatter(data[param_list[p]]['mean_goodput'][0]/1000000, data[param_list[p]]['mean_power'][0], s=20, label='w1=%.2f' % (param_list[p]))
    #plt.annotate('%.2f, %.2f' % (param_list[p], 1-param_list[p]), (data[p][6]/1000000, data[p][2]))
'''


'''
ax = plt.gca()
ax.invert_xaxis()
plt.title('Objective Performance per Objective Preferences')
plt.xlabel('Mean Goodput, MB')
plt.ylabel('Mean Power Consumption, Wh')
plt.legend()
'''  
    
    
#run_exp_w_optimal(0.25)   