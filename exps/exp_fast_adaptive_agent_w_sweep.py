import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]

#import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
#from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates, PowerStates, MultiRadioPowerState, ExtPowerStates, MultiRadioExtPowerState
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import json
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.gm_rl_agent import GmAgent
from util.visualisation import TrajectoryVisualiser, StepBasedVisualiser
import util.data_mgmt as data_mgmt
from multiprocessing import Pool
from copy import deepcopy

FILE_DIR = 'gym_grid_wireless/envs/'
DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\'

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


TIME_STEP_INTERVAL = 0.2

RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [10,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

RADIO_CFG_WIFI_LORA = [Radio(0, 915000000, 250000, -120, 7812, [-6,0,5,10,12], [11,16,20,25,29], 'lora', 8),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

ACTION_SPACE     = Actions
STATE_SPACE      = States

EXP_CFG = { 'rl_alg': 'sarsa',
            #'state_space': States,
            'state_space': PowerStates,
            #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
            'states_obj':MultiRadioPowerState(RADIO_CFG),
            'num_components': 2,
            'w1_comp_val': 0.5,
            'alpha': 0.7,
            'gamma': 0.1,
            'epsilon_schedule': GmAgent.ADPTV_EPS,
            'rand_agent_expl':False,
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'env_name': ENV_NAME[0],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 1,
            'exp_param':'alpha',
            'param_list':np.linspace(0,1,11),
}


EXP_CFG_LORA = {    'rl_alg': 'sarsa',
                    #'state_space': States,
                    'state_space': PowerStates,
                    #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
                    'states_obj':MultiRadioPowerState(RADIO_CFG),
                    'num_components': 2,
                    'w1_comp_val': 0,
                    'alpha': 0.7,
                    'gamma': 0.1,
                    'epsilon_schedule': GmAgent.ADPTV_EPS,
                    'rand_agent_expl':False,
                    'action_space': Actions,
                    'radio_cfg': RADIO_CFG_WIFI_LORA,
                    'env_name': ENV_NAME[0],
                    'env_spec': ENV_SPEC[2],
                    'path_len': PATH_LEN[0],
                    'num_runs': 100,
                    'exp_param':'w1_comp_val',
                    'param_list':np.linspace(0,1,21),
}

#EXP_CFG = EXP_CFG_MAX_MIN_W


def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]

def init_env(env_files, env_cfg):
    env = gym.make("path-power-wireless-v2", 
                    update_interval             = TIME_STEP_INTERVAL, 
                    radio_cfg                   = env_cfg['radio_cfg'], 
                    agent_path_fname            = env_files[0], 
                    env_fname                   = env_files[1], 
                    ple_fname                   = env_files[2], 
                    shd_fname                   = env_files[3], 
                    god_agent                   = False)
    
    env.action_space = spaces.Discrete(len(env_cfg['action_space']))
    env.action_enum = env_cfg['action_space']

    return env

def init_agent(env, agent_cfg):
    agent = GmAgent(env)

    agent.cfg_agent(agent_cfg['rl_alg'], 
                    agent_cfg['state_space'],
                    agent_cfg['states_obj'], 
                    agent_cfg['num_components'], 
                    agent_cfg['w1_comp_val'], 
                    agent_cfg['alpha'], 
                    agent_cfg['gamma'], 
                    agent_cfg['epsilon_schedule'],
                    agent_cfg['rand_agent_expl'])

    return agent

def exp_worker(agent, cfg, param, param_val, num_runs):
     print('\tRunning experiment with \''+param+'\'='+str('%.2f' % (param_val) + ' for ' + str(num_runs) + ' runs')
        for i in range(num_runs):
            #print('Run ' + str(i+1) + ' of ' + str(num_runs),end='\r', flush=True)
            sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
            sys.stdout.flush()
            agent.simulate_run(re_init=True)


def run_exp_weight_param_var(cfg):
    radio_data = []
    data = {}
    
    num_runs = cfg['num_runs']
    p_len = int(cfg['path_len'])
    param = cfg['exp_param']
    param_list = cfg['param_list']
    #param_list = [0, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2]
    #param_list = [0]

    env_files = get_env_fnames(cfg['env_name'], cfg['env_spec'], cfg['path_len'])

    print('Starting experiment with ctl param=\''+param+'\'')
    print('  Params will be: '+str([float('%.2f' % (p)) for p in param_list]))

    envs = []
    agents = []
    cfgs = []
    params = []
    param_vals = []
    
    for p in range(len(param_list)):
        
        cfg_p = deepcopy(cfg)
        
        cfg_p[param] =  param_list[p]


        envs.append(init_env(env_files, cfg_p))

        agents.append(init_agent(envs[p], cfg_p))
        
        cfgs.append(cfg_p)
        params.append(param)
        param_vals.append(param_list[p])

    with Pool(len(gamma_list)) as p:
        envs = p.map_async(exp_worker, args)        
    
    
    for env in envs:
        # extract and save radio data for this group of parameter runs
        radio_use_trace = env.recorder.data['radio_use_trace']
        radio_outage_trace = env.recorder.data['outage_trace']
        radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
        radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
        dist_step = env.recorder.data['dist_step'][0]
        radio_data.append([radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step])
        
        pkt_loss_rate = env.recorder.data['pkt_loss_rate']
        cum_power = env.recorder.data['tot_pwr']
        cum_step_reward = env.recorder.data['cum_step_reward']
        avg_bitrate = env.recorder.data['avg_bitrate']
        avg_pwr_cons = env.recorder.data['avg_pwr_cons']
        inst_bitrate = env.recorder.data['inst_bitrate']
        inst_pwr_cons = env.recorder.data['inst_pwr_cons']
        goodput = env.recorder.data['goodput']

        pkt_loss_rate = np.array(pkt_loss_rate)
        tot_power = np.array(cum_power)
        avg_bitrate = np.array(avg_bitrate)
        avg_pwr_cons = np.array(avg_pwr_cons)[num_runs-1,-1]

        inst_bitrate = np.array(inst_bitrate)
        inst_pwr_cons = np.array(inst_pwr_cons)

        #mean_pkt_loss = np.mean(pkt_loss_rate)
        #std_pkt_loss = np.std(pkt_loss_rate)
        #mean_power = np.mean(tot_power)
        #std_power = np.std(tot_power)

        #mean_avg_bitrate = np.mean(avg_bitrate[:,p_len-1])
        #std_avg_bitrate = np.std(avg_bitrate[:,p_len-1])
        
        #mean_goodput = np.mean(goodput)
        #std_goodput = np.std(goodput)

        #data.append([mean_pkt_loss, std_pkt_loss, mean_power, std_power, mean_avg_bitrate, std_avg_bitrate, mean_goodput, std_goodput])
        '''
        data[param_list[p]] = {'mean_pkt_loss':     env.recorder.mean_across_runs('pkt_loss_rate'),
                               'std_pkt_loss':      env.recorder.std_across_runs('pkt_loss_rate'),
                               'mean_power':        env.recorder.mean_across_runs('tot_pwr'),
                               'std_power':         env.recorder.std_across_runs('tot_pwr'),
                               'mean_goodput':      env.recorder.mean_across_runs('goodput'),
                               'std_goodput':       env.recorder.std_across_runs('goodput')
            }
        '''
        data[param_list[p]] = {'pkt_loss_rate': pkt_loss_rate,
                               'tot_pwr': cum_power,
                               'goodput': goodput
                              }
    return radio_data, dist_step, data, cfg


def run_exp_env_w_var():
    data = []
    envs = ['p']
    param_list = EXP_CFG['param_list']
    
    for e in range(len(envs)):
        EXP_CFG['env_name'] = envs[e]
        
        data.append(run_exp_weight_param_var(EXP_CFG))
    
    plt.figure()
    
    count = 0
    markers = ['o', 'D', 's']
    for d in data:
        for p in range(len(param_list)):
            plt.scatter(d[p][6]/1000000, d[p][2], s=20, marker=markers[count], label='w1=%.2f' % (param_list[p]))
            #plt.annotate('%.2f, %.2f' % (param_list[p], 1-param_list[p]), (data[p][6]/1000000, data[p][2]))
        count += 1

    ax = plt.gca()
    ax.invert_xaxis()
    plt.title('Objective Performance per Objective Preferences')
    plt.xlabel('Mean Goodput, MB')
    plt.ylabel('Mean Power Consumption, Wh')
    plt.legend()
    
    
        
    
    return

'''
cfg = EXP_CFG_ENV
#radio_data, dist_step, data = run_exp_weight_param_var(EXP_CFG)
radio_data, dist_step, data = run_exp_weight_param_var(cfg)
#run_exp_env_param_var()
data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data_exp_adaptive_gm_agent_26102020_1632', cfg, data)
#with open(DATA_EXP_DIR + 'data_exp_adaptive_gm_agent.json', 'w') as file:
#    json.dump(data, file)
data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data_exp_adaptive_gm_agent_26102020_1632', cfg, radio_data, dist_step)
'''

#run_exp_env_w_var()
'''
param_list = EXP_CFG['param_list']
for p in range(len(param_list)):
    print(data[param_list[p]]['mean_goodput'][0])
    print(data[param_list[p]]['mean_power'][0])
    plt.scatter(data[param_list[p]]['mean_goodput'][0]/1000000, data[param_list[p]]['mean_power'][0], s=20, label='w1=%.2f' % (param_list[p]))
    #plt.annotate('%.2f, %.2f' % (param_list[p], 1-param_list[p]), (data[p][6]/1000000, data[p][2]))
'''

'''
ax = plt.gca()
ax.invert_xaxis()
plt.title('Objective Performance per Objective Preferences')
plt.xlabel('Mean Goodput, MB')
plt.ylabel('Mean Power Consumption, Wh')
plt.legend()


plt.show()
'''


### Uncomment for alpha/gamma sweep
'''
cfg = EXP_CFG
print('w1=',cfg['w1_comp_val'])
gamma_list = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
for g in gamma_list:
    cfg['gamma'] = g
    
    radio_data, dist_step, data = run_exp_weight_param_var(cfg)
    curr_time = time.localtime()
    
    ts = str(curr_time.tm_hour)+str('%02d' % (curr_time.tm_min))
    gm = str('%.2f' % g)
    data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data_exp_adaptive_gm_agent_'+gm+'_27112020_'+ts, cfg, data)
    data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data_exp_adaptive_gm_agent_'+gm+'_27112020_'+ts, cfg, radio_data, dist_step)
    
    print('saved gamma='+str(g)+' at '+ts)
'''


'''
cfg = EXP_CFG
radio_data, dist_step, data = run_exp_weight_param_var(cfg)
curr_time = time.localtime()
    
ts = str('%02d' % (curr_time.tm_hour))+str('%02d' % (curr_time.tm_min))
data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data_exp_adaptive_gm_agent_19112020_'+ts, cfg, data)
data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data_exp_adaptive_gm_agent_19112020_'+ts, cfg, radio_data, dist_step)
'''


def run_exp_fast_adaptive_agent_param_sweep():
    #gamma_list = [0,0.1,0.2,0.3,0.4]
    #gamma_list = [0.5, 0.6, 0.7, 0.8, 0.9]
    #gamma_list = [0.1, 0.2]
    w1_list = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    data = []
    args = []
    for g in w1_list:
        #cfg['gamma'] = g
        args.append(g)
        
        
    
    with Pool(len(gamma_list)) as p:
        result = p.map_async(run_exp_weight_param_var, args)
        radio_data, dist_step, data, cfg = zip(*result.get())
        
    
    
    print(np.array(radio_data).shape)
    print(np.array(dist_step).shape)
    print(np.array(data).shape)
    
    p.close()
    p.join()
    
    curr_time = time.localtime()
    ts = str('%02d' % (curr_time.tm_hour))+str('%02d' % (curr_time.tm_min))
    
    for rd, ds, d, c, g in zip(radio_data, dist_step, data, cfg, w1_list):   
        gm = str('%.2f' % g)
        print(np.array(rd[0][0]).shape)
        print(np.array(ds).shape)
        print(np.array(d).shape)
        data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data_exp_adaptive_gm_agent_'+gm+'_30112020_'+ts, c, d)
        data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data_exp_adaptive_gm_agent_'+gm+'_30112020_'+ts, c, rd, ds)

    
if __name__ == '__main__':
    #cfg = EXP_CFG
    run_exp_fast_adaptive_agent_param_sweep()


