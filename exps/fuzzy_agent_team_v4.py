import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]


import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
import gym_grid_wireless
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw
from gym_grid_wireless.envs.path_power_wireless_env import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States
from gym_grid_wireless.multi_radio_actions import Actions, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

#Things to do
# -Add Transmission 'Abysmal' Power
# -Keep working on Fuzzy Computing
# -Add environments biased to: Data Rate, Balanced, Efficient Energy Consumption
# -Get some more graphs for report.
# -Turn off radio if abysmal.
# -Make goals of Pkt Loses/Pwr Cons and design a system around.

high_check = 0

def increment():
    global high_check
    high_check = high_check+1
    #print (high_check)

def check():
    #print ('This is the check:', high_check)
    return high_check

def reset():
    global high_check
    high_check = high_check-250
    #print (high_check)


TIME_STEP_INTERVAL = 0.2
PATH_LEN = 20000

#FILE_DIR = 'gym_grid_wireless/envs/'#os.path.dirname(__file__) + '/'
FILE_DIR = path.dirname(os.path.abspath(gym_grid_wireless.envs.__file__))+'/'
#                          #Freq     #Bandwidth                 dBm (Power)        mA (Current)
RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,17.2,20.2,30.7,33.4], 'psk', 4),
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]

#RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,17.2,20.2,30.7,33.4], 'psk', 4),
               # Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]


#RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [7,15,18,21,24], [4,27,54,107,215], 'psk', 4), #Radio(0, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]

#RADIO_CFG = [   Radio(0, 2400, -84, 1000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240]),
#               Radio(1, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215])]
                


#RADIO_CFG = [   Radio(0, 915, -101, 200, [7,7,7,7,7], [4,4,4,4,4]), #Radio(0, 915, -101, 200, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400, -84, 1000, [5,5,5,5,5], [7.8,7.82,7.82,7.82,7.82])]


DST_NODE_X = 100
DST_NODE_Y = 100

#                                               CHANGE THESE FOR ALTERNATE ENVIRONMENTS
#_____________________________________________________________________________________________________
#_o_ _p_ _q_, numbers after the letters path loss exponents

# ------This one does not show a figure but gives pkt loss and pwr cons---------
#PATH_FNAME = 'm_7500.csv'#'center_20000.csv'
#ENV_FNAME = 'polys_n_3.2_4.5.csv'#'polys_c_2.0_3.5.csv'
#PLE_FNAME = 'PLE_m_7500_polys_n_3.2_4.5.csv'#'PLE_center_20000_polys_c_2.0_3.5.csv'
#SHD_FNAME = 'SHD_m_7500_polys_n_3.2_4.5.csv'

# ----Preferring this environment ----------
PATH_FNAME = 'o_20000.csv' #o_7500.csv
ENV_FNAME = 'polys_o_3.0_4.5.csv' #polys_q_3.5_5.csv     ||||||         polys_o_3.0_4.5.csv   |||| polys_q_3.5_5.csv
PLE_FNAME = 'PLE_o_20000_polys_o_3.0_4.5.csv' # PLE_o_20000_polys_o_3.5_5.csv
SHD_FNAME = 'SHD_o_20000_polys_o_3.0_4.5.csv' # PLE_o_20000_polys_o_3.5_5.csv


# -----Drop Out Environment -------
#PATH_FNAME = 'o_20000.csv'
#ENV_FNAME = 'polys_o_3.5_5.csv'
#PLE_FNAME = 'PLE_o_20000_polys_o_3.5_5.csv'
#SHD_FNAME = 'SHD_o_20000_polys_o_3.5_5.csv'

# -----Shorter Environment ------
#PATH_FNAME = 'q_20000.csv'      #'center_20000.csv'
#ENV_FNAME = 'polys_q_3.0_4.5.csv'       #'polys_c_2.0_3.5.csv'
#PLE_FNAME = 'PLE_q_20000_polys_q_3.0_4.5.csv'       #'PLE_center_20000_polys_c_2.0_3.5.csv'
#SHD_FNAME = 'SHD_q_20000_polys_q_3.0_4.5.csv'

#_____________________________________________________________________________________________________

#env = gym.make("grid-wireless-v0", update_interval=0.5, god_agent=False)
#env = gym.make("linear-wireless-v0", update_interval=0.5, god_agent=False)
env = gym.make("path-power-wireless-v2", update_interval=TIME_STEP_INTERVAL, radio_cfg=RADIO_CFG, agent_path_fname=PATH_FNAME, env_fname=ENV_FNAME, ple_fname=PLE_FNAME, shd_fname=SHD_FNAME, god_agent=True)
#env = gym.make("path-power-wireless-v0", update_interval=0.5, radio_cfg=RADIO_CFG, agent_path_fname='a_1.csv', env_fname='polys_1.5_4.5.csv', god_agent=False)

#state = env.reset()
#renderer = env.render('human')


NUM_RUNS = 1
##This defines the simulation walking path
def plt_env_path(radio_use_trace, outage_trace):
    env_polygons = []
    env_PLE = []
    path_points = []
    path_count = 0
    path_len = 0
    env_PLE = []
    PLE_points = []
    path_points_x = []
    path_points_y = []

    print('loading path')
    with open(FILE_DIR+PATH_FNAME, 'r', newline='') as file:
        lines = csv.reader(file, delimiter=',')
        for line in lines:
            #print([int(line[0]), int(line[1])])
            path_points.append([int(line[0]), int(line[1])])
            path_points_x.append(int(line[0]))
            path_points_y.append(int(line[1]))
    
    
    print('loading polygons')
    with open(FILE_DIR+ENV_FNAME, 'r') as file:
        lines = csv.reader(file, delimiter=',')
        env = next(lines)
        
        line=next(lines)
        while line:
        #for line in lines:
            p = []
            
            for i in range(1, len(line), 2):
                p.append([float(line[i]), float(line[i+1])])
            poly  = Polygon(p)
            
            env_polygons.append(poly)
            env_PLE.append(float(line[0]))

            line = next(lines, None)
        
        # Comment out plot of environment and path
        
        fig = plt.figure(1)
        fig.suptitle('Mobile Node Trajectory and Adaptive Radio Selection\nFuzzy Power Control')
        gs = gridspec.GridSpec(16, 10)

        ax = plt.subplot2grid((16,20), (0,17), colspan=1, rowspan=16)
        ax2 = plt.subplot2grid((16,20), (0,0), colspan=16, rowspan=16)
    
        norm = mpl.colors.Normalize(vmin=2.0, vmax=6.0, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in env_polygons:
            ax2.fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(env_PLE[env_polygons.index(p)]))


        cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})



        #ax2.plot(path_points_x, path_points_y, 'r', label="Mobile node path")
        
        r0_points_x = []
        r0_points_y = []
        r1_points_x = []
        r1_points_y = []
        nr_points_x = []
        nr_points_y = []

        r0_dn_points_x = []
        r0_dn_points_y = []
        r1_dn_points_x = []
        r1_dn_points_y = []
        
        for i in range(0, PATH_LEN-1):
            #if i % 1000 == 0:
            #    print("count: " + str(i)) 
            if radio_use_trace[i] == 0:
                #print("num " + str(i) + "r0")
                r0_points_x.append(path_points_x[i])
                r0_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='y')
            elif radio_use_trace[i] == 1:
                #print("num " + str(i) + "r1")
                r1_points_x.append(path_points_x[i])
                r1_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='c')
            else:
                #print("num " + str(i) + "nr")
                nr_points_x.append(path_points_x[i])
                nr_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='r')

            if outage_trace[i] == 1:
                r0_dn_points_x.append(path_points_x[i])
                r0_dn_points_y.append(path_points_y[i])
            elif outage_trace[i] == 2:
                r1_dn_points_x.append(path_points_x[i])
                r1_dn_points_y.append(path_points_y[i])
        
        print(len(r0_dn_points_x), len(r1_dn_points_x))
        #print(radio_use_trace)

        ax2.plot(r0_points_x, r0_points_y, 'om', markersize=1, c='m', label='802.15.4 Radio')
        ax2.plot(r1_points_x, r1_points_y, 'oc', markersize=1, c='c', label='802.11 Radio')
        
        ax2.plot(r0_dn_points_x, r0_dn_points_y, 'ob', markersize=1, c='b', label='802.15.4 Radio down')
        ax2.plot(r1_dn_points_x, r1_dn_points_y, 'og', markersize=1, c='g', label='802.11 Radio down')
        ax2.plot(nr_points_x, nr_points_y, 'or', markersize=1, c='r', label='Radio off')

        ax2.plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")

        ax2.set_xlabel('X coordinate, m')
        ax2.set_ylabel('Y coordinate, m')
        #ax2.set_xticks([])
        #ax2.set_yticks([])
        lg = ax2.legend(prop={'size':8}, loc=4)
        lg.legendHandles[0]._legmarker.set_markersize(5)
        lg.legendHandles[1]._legmarker.set_markersize(5)
        lg.legendHandles[2]._legmarker.set_markersize(5)
        lg.legendHandles[3]._legmarker.set_markersize(5)
        lg.legendHandles[4]._legmarker.set_markersize(5)
        lg.legendHandles[5]._legmarker.set_markersize(5)
        plt.xlim(0, 500)
        plt.ylim(0, 500)
        
        plt.axis([0,500,0,500])
##
## _________________________________________________________________________________________________________________________________________________________________________
##                                                                    DO NOT CHANGE ABOVE CODE YET
## _________________________________________________________________________________________________________________________________________________________________________
#Insert Function later
#Weights are determined by users
#w1(1-norm_power)+w2norm_data_rate
##

#Work in Matlab, and get similar results.
#
class FuzzyMultiRadioAgent():
    #Initialise Fuzzy Set
    def __init__(self):
        self.init_fuzzy_set()

    #Defining the triangles
    def init_fuzzy_set(self):
        #SNR, estimating distance based on SNR
        #Goes from 0 to 1 in Y, but X is -20 to 100.
                     #Zigbee
        self.sig_qual_802154 = ctrl.Antecedent(np.arange(-40,100,1), 'snr_802154')
                     #WiFi
        self.sig_qual_80211 = ctrl.Antecedent(np.arange(-40,100,1), 'snr_80211')

        #TX_Power
        #Goes from 0 to 1 in Y, but X is 0 to 3.
                    #Zigbee
        tx_pow_802154 = ctrl.Antecedent(np.arange(0,3,1), 'tx_pow_802154')
                     #WiFi
        tx_pow_80211 = ctrl.Antecedent(np.arange(0,3,1), 'tx_pow_80211')

        #Radio Selection
        #Depending where the membership of above values fall will result in the radio selection below.
        #Goes from 0 to 1 in Y, but X is 0 to 1.1
                     #Zigbee
        radio_sel_802154 = ctrl.Consequent(np.arange(0,1.1,0.05), 'radio_802154')
                     #WiFi
        radio_sel_80211 = ctrl.Consequent(np.arange(0,1.1,0.05), 'radio_80211')
        
        #Assign membership function of radio power level to Zigbee and WiFi functions.
        #The result of the Antecedent becomes the Consequent. 
        #Results in a POWER value of 0 to 1, falling within a range of 3.
                 #Zigbee
        #self.radio_pwr_lvl_802154 = ctrl.Consequent(np.arange(0,3,1), 'pwr_lvl_802154')
                    #WiFi
        #self.radio_pwr_lvl_80211 = ctrl.Consequent(np.arange(0,3,1), 'pwr_lvl_80211')
                    #Zigbee
        self.radio_pwr_lvl_802154 = ctrl.Consequent(np.arange(-1,3,0.25), 'pwr_lvl_802154')
                    #WiFi
        self.radio_pwr_lvl_80211 = ctrl.Consequent(np.arange(-1,3,0.25), 'pwr_lvl_80211')

        #Defining the Low/Med/High of Triangles
        #Trapezoidal Membership Functon Generator
        #               -20,100,1
                        #Zigbee
        self.sig_qual_802154['abysmal'] = fuzz.trimf(self.sig_qual_802154.universe, [-40, -40, 0])            
        self.sig_qual_802154['low'] = fuzz.trapmf(self.sig_qual_802154.universe, [-20, -20, -2, 8])
        self.sig_qual_802154['med'] = fuzz.trimf(self.sig_qual_802154.universe, [5, 8, 23])
        self.sig_qual_802154['high'] = fuzz.trapmf(self.sig_qual_802154.universe, [8, 23, 100, 100])
        #               -20,100,1
        #               WiFi
        #'''
        #sig_qual_80211['low'] = fuzz.trapmf(sig_qual_80211.universe, [-20, -20, -5, 14])
       # sig_qual_80211['med'] = fuzz.trimf(sig_qual_80211.universe, [-5, 14, 29])
        #sig_qual_80211['high'] = fuzz.trapmf(sig_qual_80211.universe, [14, 29, 100, 100])
        #'''
        #Defining the Low/Med/High of Triangles
                         #WiFi
        self.sig_qual_80211['abysmal'] = fuzz.trimf(self.sig_qual_80211.universe, [-40, -40, -20])
        self.sig_qual_80211['low'] = fuzz.trapmf(self.sig_qual_80211.universe, [-20, -20, 8, 18])
        self.sig_qual_80211['med'] = fuzz.trimf(self.sig_qual_80211.universe, [8, 18, 29])
        self.sig_qual_80211['high'] = fuzz.trapmf(self.sig_qual_80211.universe, [18, 29, 100, 100])
        
        #Transmission Power of Zigbee
        #Consider adding more crisp words
        #Adding a way to turn if off
        #_________________________________________
        tx_pow_802154['low'] = fuzz.trimf(tx_pow_802154.universe, [0, 0, 1])
        tx_pow_802154['med'] = fuzz.trimf(tx_pow_802154.universe, [1, 1, 2])
        tx_pow_802154['high'] = fuzz.trimf(tx_pow_802154.universe, [1, 2, 2])
        
        #Transmission Power of WiFi
        #Consider adding more crisp words
        tx_pow_80211['low'] = fuzz.trimf(tx_pow_80211.universe, [0, 0, 1])
        tx_pow_80211['med'] = fuzz.trimf(tx_pow_80211.universe, [1, 1, 2])
        tx_pow_80211['high'] = fuzz.trimf(tx_pow_80211.universe, [1,2,2])

        #Selection of Zigbee
        #Consider adding more crisp words
        radio_sel_802154['abysmal'] = fuzz.trimf(radio_sel_80211.universe, [0,0.05,0.20])
        radio_sel_802154['bad'] = fuzz.trimf(radio_sel_802154.universe, [0.15,0.2,0.4])
        radio_sel_802154['not bad'] = fuzz.trimf(radio_sel_802154.universe, [0.2,0.4,0.6])
        radio_sel_802154['not good'] = fuzz.trimf(radio_sel_802154.universe, [0.4,0.6,0.8])
        radio_sel_802154['good'] = fuzz.trimf(radio_sel_802154.universe, [0.6,0.8,1.0])

        #Selection of WiFi
        #Consider adding more crisp words
        radio_sel_80211['abysmal'] = fuzz.trimf(radio_sel_80211.universe, [0,0.05,0.20])
        radio_sel_80211['bad'] = fuzz.trimf(radio_sel_80211.universe, [0.15,0.2,0.4])
        radio_sel_80211['not bad'] = fuzz.trimf(radio_sel_80211.universe, [0.2,0.4,0.6])
        radio_sel_80211['not good'] = fuzz.trimf(radio_sel_80211.universe, [0.4,0.6,0.8])
        radio_sel_80211['good'] = fuzz.trimf(radio_sel_80211.universe, [0.6,0.8,1.0])

        #Power Level of Zigbee
        self.radio_pwr_lvl_802154['abysmal'] = fuzz.trimf(self.radio_pwr_lvl_802154.universe, [-1,-1,0.5])
        self.radio_pwr_lvl_802154['low'] = fuzz.trimf(self.radio_pwr_lvl_802154.universe, [0,0,1.5])
        self.radio_pwr_lvl_802154['med'] = fuzz.trimf(self.radio_pwr_lvl_802154.universe, [0.25,1,1.5])
        self.radio_pwr_lvl_802154['high'] = fuzz.trimf(self.radio_pwr_lvl_802154.universe, [1,2,3])

        #Power Level of WiFi
        self.radio_pwr_lvl_80211['abysmal'] = fuzz.trimf(self.radio_pwr_lvl_80211.universe, [-1,-1,0.5])
        self.radio_pwr_lvl_80211['low'] = fuzz.trimf(self.radio_pwr_lvl_80211.universe, [0,0,1.5])
        self.radio_pwr_lvl_80211['med'] = fuzz.trimf(self.radio_pwr_lvl_80211.universe, [0.25,1,1.5])
        self.radio_pwr_lvl_80211['high'] = fuzz.trimf(self.radio_pwr_lvl_80211.universe, [1,2,3])


        # rules for 802.15.4 output, Zigbee
        rule1 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['low'], radio_sel_802154['not good'])
        rule2 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['med'], radio_sel_802154['abysmal'])
        rule3 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['high'], radio_sel_802154['abysmal'])
        rule4 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['low'], radio_sel_802154['not good'])
        rule5 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['med'], radio_sel_802154['abysmal']) 
        rule6 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['high'], radio_sel_802154['abysmal'])
        rule7 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['low'], radio_sel_802154['good'])
        rule8 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['med'], radio_sel_802154['abysmal'])
        rule9 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['high'], radio_sel_802154['abysmal'])
        rule10 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['abysmal'], radio_sel_802154['good'])
        rule11 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['abysmal'], radio_sel_802154['good'])
        rule12 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['abysmal'], radio_sel_802154['good'])
        rule13 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['abysmal'], radio_sel_802154['abysmal'])
        rule14 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['high'], radio_sel_802154['abysmal'])
        rule15 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['med'], radio_sel_802154['abysmal'])
        rule16 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['low'], radio_sel_802154['abysmal'])
        


        # rules for 802.11 output, WiFi
        rule17 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['low'], radio_sel_80211['not bad'])
        rule18 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['med'], radio_sel_80211['good'])
        rule19 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['high'], radio_sel_80211['good'])
        rule20 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['low'], radio_sel_80211['bad'])
        rule21 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['med'], radio_sel_80211['not good']) 
        rule22 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['high'], radio_sel_80211['good'])
        rule23 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['low'], radio_sel_80211['abysmal'])
        rule24 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['med'], radio_sel_80211['not bad'])
        rule25 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['high'], radio_sel_80211['good'])
        rule26 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['high'], radio_sel_80211['good'])
        rule27 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['med'], radio_sel_80211['good'])
        rule28 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['low'], radio_sel_80211['good'])
        rule29 = ctrl.Rule(self.sig_qual_802154['abysmal'] & self.sig_qual_80211['abysmal'], radio_sel_80211['abysmal'])
        rule30 = ctrl.Rule(self.sig_qual_802154['high'] & self.sig_qual_80211['abysmal'], radio_sel_80211['abysmal'])
        rule31 = ctrl.Rule(self.sig_qual_802154['med'] & self.sig_qual_80211['abysmal'], radio_sel_80211['abysmal'])
        rule32 = ctrl.Rule(self.sig_qual_802154['low'] & self.sig_qual_80211['abysmal'], radio_sel_80211['abysmal'])

        #-----Explain rules to figure-------
        #Transmission Power depending on Signal Quality for Zigbee
        rule33 = ctrl.Rule(self.sig_qual_802154['low'] & tx_pow_802154['low'], self.radio_pwr_lvl_802154['med'])
        rule34 = ctrl.Rule(self.sig_qual_802154['low'] & tx_pow_802154['med'], self.radio_pwr_lvl_802154['high'])
        rule35 = ctrl.Rule(self.sig_qual_802154['low'] & tx_pow_802154['high'], self.radio_pwr_lvl_802154['high'])
        rule36 = ctrl.Rule(self.sig_qual_802154['med'] & tx_pow_802154['low'], self.radio_pwr_lvl_802154['low'])
        rule37 = ctrl.Rule(self.sig_qual_802154['med'] & tx_pow_802154['med'], self.radio_pwr_lvl_802154['high'])
        rule38 = ctrl.Rule(self.sig_qual_802154['med'] & tx_pow_802154['high'], self.radio_pwr_lvl_802154['med'])
        rule39 = ctrl.Rule(self.sig_qual_802154['high'] & tx_pow_802154['low'], self.radio_pwr_lvl_802154['low'])
        rule40 = ctrl.Rule(self.sig_qual_802154['high'] & tx_pow_802154['med'], self.radio_pwr_lvl_802154['low'])
        rule41 = ctrl.Rule(self.sig_qual_802154['high'] & tx_pow_802154['high'], self.radio_pwr_lvl_802154['med'])
        rule42 = ctrl.Rule(self.sig_qual_802154['abysmal'] & tx_pow_802154['high'], self.radio_pwr_lvl_802154['abysmal'])
        rule43 = ctrl.Rule(self.sig_qual_802154['abysmal'] & tx_pow_802154['med'], self.radio_pwr_lvl_802154['high'])
        rule44 = ctrl.Rule(self.sig_qual_802154['abysmal'] & tx_pow_802154['low'], self.radio_pwr_lvl_802154['med'])
       

        #Transmission Power depending on Signal Quality for WiFi
        rule45 = ctrl.Rule(self.sig_qual_80211['low'] & tx_pow_80211['low'], self.radio_pwr_lvl_80211['med'])
        rule46 = ctrl.Rule(self.sig_qual_80211['low'] & tx_pow_80211['med'], self.radio_pwr_lvl_80211['high'])
        rule47 = ctrl.Rule(self.sig_qual_80211['low'] & tx_pow_80211['high'], self.radio_pwr_lvl_80211['high'])
        rule48 = ctrl.Rule(self.sig_qual_80211['med'] & tx_pow_80211['low'], self.radio_pwr_lvl_80211['med'])
        rule49 = ctrl.Rule(self.sig_qual_80211['med'] & tx_pow_80211['med'], self.radio_pwr_lvl_80211['high'])
        rule50 = ctrl.Rule(self.sig_qual_80211['med'] & tx_pow_80211['high'], self.radio_pwr_lvl_80211['med'])
        rule51 = ctrl.Rule(self.sig_qual_80211['high'] & tx_pow_80211['low'], self.radio_pwr_lvl_80211['low'])
        rule52 = ctrl.Rule(self.sig_qual_80211['high'] & tx_pow_80211['med'], self.radio_pwr_lvl_80211['med'])
        rule53 = ctrl.Rule(self.sig_qual_80211['high'] & tx_pow_80211['high'], self.radio_pwr_lvl_80211['med'])
        rule54 = ctrl.Rule(self.sig_qual_80211['abysmal'] & tx_pow_80211['high'], self.radio_pwr_lvl_80211['abysmal'])
        rule55 = ctrl.Rule(self.sig_qual_80211['abysmal'] & tx_pow_80211['med'], self.radio_pwr_lvl_80211['high'])
        rule56 = ctrl.Rule(self.sig_qual_80211['abysmal'] & tx_pow_80211['low'], self.radio_pwr_lvl_80211['med'])
    

        #Additional rules

        #rule1.view()
        self.radio_ctrl_sys = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, 
                                        rule10, rule11, rule12, rule13,rule14, rule15, rule16, rule17, rule18,
                                        rule19, rule20, rule21, rule22, rule23, rule24, rule25, rule26, rule27,
                                        rule28, rule29, rule30, rule31, rule32, rule33, rule34, rule35, rule36, rule37, rule38, rule39, rule40, rule41, rule42, rule43, 
                                        rule44, rule45, rule46, rule47, rule48, rule49, rule50, rule51, rule52, rule53, rule54, rule55,
                                        rule56])

        self.radio_sel = ctrl.ControlSystemSimulation(self.radio_ctrl_sys)

#________________________________________________________________________________________________________________________________________________________________________
#                                                           FUZZY COMPUTING INPUT ENDS HERE
#_________________________________________________________________________________________________________________________________________________________________________

    def compute_output(self, snr_0, snr_1, tx_pow_0, tx_pow_1):
        
        self.radio_sel.input['snr_802154'] = snr_0
        self.radio_sel.input['snr_80211'] = snr_1
        
        self.radio_sel.input['tx_pow_802154'] = tx_pow_0
        self.radio_sel.input['tx_pow_80211'] = tx_pow_1

        
        #print(snr_0, tx_pow_0)
        #print(snr_1, tx_pow_1)
 
        self.radio_sel.compute()
        #print(snr_0, snr_1)
        #print(self.radio_sel.output['radio_802154'], self.radio_sel.output['radio_80211'])
       
       #If it chose 802.15.4 over 802.11 do these options
       #more to add later
        if (self.radio_sel.output['radio_802154'] > self.radio_sel.output['radio_80211']):
            a=fuzz.interp_membership(self.radio_pwr_lvl_802154.universe, self.radio_pwr_lvl_802154['low'].mf, self.radio_sel.output['pwr_lvl_802154'])
            b=fuzz.interp_membership(self.radio_pwr_lvl_802154.universe, self.radio_pwr_lvl_802154['med'].mf, self.radio_sel.output['pwr_lvl_802154'])
            c=fuzz.interp_membership(self.radio_pwr_lvl_802154.universe, self.radio_pwr_lvl_802154['high'].mf, self.radio_sel.output['pwr_lvl_802154'])
            d=fuzz.interp_membership(self.sig_qual_802154.universe, self.sig_qual_802154['low'].mf, self.radio_sel.output['radio_802154'])
            e=fuzz.interp_membership(self.sig_qual_802154.universe, self.sig_qual_802154['med'].mf, self.radio_sel.output['radio_802154'])
            f=fuzz.interp_membership(self.sig_qual_802154.universe, self.sig_qual_802154['high'].mf, self.radio_sel.output['radio_802154'])
            g=fuzz.interp_membership(self.sig_qual_802154.universe, self.sig_qual_802154['abysmal'].mf, self.radio_sel.output['radio_802154'])
            z=fuzz.interp_membership(self.radio_pwr_lvl_802154.universe, self.radio_pwr_lvl_802154['abysmal'].mf, self.radio_sel.output['pwr_lvl_802154'])
            #if (a == b):
            #print(e,f,g)
            if (a > b and a > c):
                d = 1
                #print('this works 1')
            elif (b > a and a >= c):
                d = 1.5
            elif (b > a and b > c):
                d = 1.75
            elif (b >= a and c == b and check() <= 3500):
                increment()
                d = 2.5
                #print(b,c)
            elif (c > b and c > a):
                d = 3
                #print('reset')
                reset() #reset the high_check
                #print('this works 3')
            elif (z > a and z >b and z >c):
                d = 0
                print ("Z function, Zig")
            else:
                d = -1
                print('else statement Zig')
            return [0,d]
        #If it chose 802.11 over 802.15.4 do these options
        #more to add later
        else:
            a=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['low'].mf, self.radio_sel.output['pwr_lvl_80211'])
            b=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['med'].mf, self.radio_sel.output['pwr_lvl_80211'])
            c=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['high'].mf, self.radio_sel.output['pwr_lvl_80211'])
            d=fuzz.interp_membership(self.sig_qual_80211.universe, self.sig_qual_80211['low'].mf, self.radio_sel.output['radio_80211'])
            e=fuzz.interp_membership(self.sig_qual_80211.universe, self.sig_qual_80211['med'].mf, self.radio_sel.output['radio_80211'])
            f=fuzz.interp_membership(self.sig_qual_80211.universe, self.sig_qual_80211['high'].mf, self.radio_sel.output['radio_80211'])
            g=fuzz.interp_membership(self.sig_qual_80211.universe, self.sig_qual_80211['abysmal'].mf, self.radio_sel.output['radio_80211'])
            z=fuzz.interp_membership(self.radio_pwr_lvl_80211.universe, self.radio_pwr_lvl_80211['abysmal'].mf, self.radio_sel.output['pwr_lvl_80211'])
            #print(d,e,f,g)
            if (a > b and a > c):
                d = 1
            elif (a > b and b > c):
                d = 1.25
                #print('this works 1.5')
            elif (b > a and a > c):
                d = 1.5
            elif (b > a and b > c):
                d = 2
            elif (b > a and c > b): 
                d = 2.5
            elif (c > b and c > a):
                d = 3
            elif (z > a and z >b and z >c):
                d = 0
                print ("Z function, Wifi")
            else:
                d = -1
                print('else statement wifi')
            return [1,d]

 
def run_fuzzy_agent():
 
    agent = FuzzyMultiRadioAgent()
 
    action_1 = Actions.RADIO_1_TX_POW_0
    
    #Reset the environment
    env.reset()
 
    done = False
    while not done:
        obs, reward, done, info = env.step(action_1)
        
        #print('reward ' + str(reward))
        if (obs is not None):
            curr_radio_id = obs.get_curr_radio_id()
            radios = obs.get_radios()    
        else:
            pass
        
       
        
        #
        #   Change this to affect some of the decision making.
        #
        #print(radios[0].get_tx_pow_step())
        #print(radios[1].get_tx_pow_step())

        if (0 < radios[0].get_tx_pow_step() == 0):
            tx_pow_0 = 0
        elif (radios[0].get_tx_pow_step() == 1):
            tx_pow_0 = 0.5
        elif (1.5 < radios[0].get_tx_pow_step() <= 2):
            tx_pow_0 = 1
        elif (2.5 < radios[0].get_tx_pow_step() <= 3):
            tx_pow_0 = 1.5
        elif radios[0].get_tx_pow_step() > 3:
            tx_pow_0 = 2
        else:
            tx_pow_0 = -1
 
        if (0 < radios[1].get_tx_pow_step() == 0):
            tx_pow_1 = 0
        elif (radios[1].get_tx_pow_step() == 1):
            tx_pow_1 = 0.5
        elif (1.5 < radios[1].get_tx_pow_step() <= 2):
            tx_pow_1 = 1
        elif (2.5 < radios[1].get_tx_pow_step() <= 3):
            tx_pow_1 = 1.5
        elif radios[1].get_tx_pow_step() > 3:
            tx_pow_1 = 2
        else:
            tx_pow_1 = -1
 
        #tx_pow_0 = -1
        #tx_pow_1 = -1

        radio_select = agent.compute_output(radios[0].get_snr(), radios[1].get_snr(), tx_pow_0, tx_pow_1)
        #print (radio_select)
        
        radio_pwr = radio_select[1]
        #print (radio_pwr)

        #print(radios[0].get_snr())
    



        #if radios[1].get_link_status():    
        #if radio_select[0] == 1:
            #radio_select = 1
        #else:
            #radio_select = 0
 
        if (radio_select[0] == 0):
            #print('802.15.4', radio_pwr)
            if (-1 < radio_pwr <= 0):
                action_1 = Actions.NO_TX
            elif (radio_pwr == 1):
                action_1 = Actions.RADIO_0_TX_POW_0
            elif (1 < radio_pwr <= 1.5):
                action_1 = Actions.RADIO_0_TX_POW_1
            elif (1.5 < radio_pwr <= 2):
                action_1 = Actions.RADIO_0_TX_POW_2 
            elif (2 < radio_pwr <= 2.5):
                action_1 = Actions.RADIO_0_TX_POW_3
            elif (2.5 < radio_pwr <= 3):
                #action_1 = Actions.NO_TX
                action_1 = Actions.RADIO_0_TX_POW_4
            else:
                action_1 = Actions.NO_TX
            
            #action_1 = Actions.RADIO_0_TX_POW_4
            #802.11
        else:
            #print('802.11', radio_pwr)
            if (-1 < radio_pwr <= 0 or radios[1].get_snr() < -6.8):
                action_1 = Actions.NO_TX
            elif (radio_pwr == 1):
                action_1 = Actions.RADIO_1_TX_POW_0
            elif (1 < radio_pwr <= 1.5):
                action_1 = Actions.RADIO_1_TX_POW_1
            elif (1.5 < radio_pwr <= 2):
                action_1 = Actions.RADIO_1_TX_POW_2
            elif (2 < radio_pwr <= 2.5):
                #print(1,4)
                action_1 = Actions.RADIO_1_TX_POW_3
            elif (2.5 < radio_pwr <= 3):
                #action_1 = Actions.NO_TX
                action_1 = Actions.RADIO_1_TX_POW_4
            else:
                action_1 = Actions.NO_TX



PLE_list = [2.5,2.75,3.0,3.25,3.5,3.75,4.0,4.25,4.5,4.75,5.0]
PLE_list = [0]
data = []
for i in range(0, len(PLE_list)):
    env = gym.make("path-power-wireless-v2", update_interval=TIME_STEP_INTERVAL, radio_cfg=RADIO_CFG, agent_path_fname=PATH_FNAME, env_fname=ENV_FNAME, ple_fname=PLE_FNAME, shd_fname=SHD_FNAME, god_agent=True)
    env.PLE = PLE_list[i]
 
    print('Running fuzzy agent for ' + str(NUM_RUNS) + ' runs at PLE = ' + str(PLE_list[i]))
 
    for i in range(0, NUM_RUNS):
        print('Run ' + str(i+1) + ' of ' + str(NUM_RUNS),end='\r', flush=True)
        run_fuzzy_agent()
        
    #Record the data
    pkt_loss_rate = env.recorder.data['pkt_loss_rate']
    cum_power = env.recorder.data['tot_pwr']
    step_reward = env.recorder.data['step_reward']
    good_put = env.recorder.data['goodput']
    avg_pwr_cons = env.recorder.data['avg_pwr_cons']
    avg_bitrate = env.recorder.data['avg_bitrate']

    
    
    pkt_loss_rate = np.array(pkt_loss_rate)
    tot_power = np.array(cum_power)
    mean_pkt_loss = np.mean(pkt_loss_rate)
    pkt_loss_std = np.std(pkt_loss_rate)
    mean_power = np.mean(tot_power)
    power_std = np.std(tot_power)
    
 
    print('mean pkt loss: ' + str(mean_pkt_loss) + ' std: ' + str(pkt_loss_std))
    print('mean pwr cons: ' + str(mean_power) + ' std: ' + str(power_std))
    print('Throughput (Bytes)', good_put)
    #print('Avg Power Cons:', avg_pwr_cons)
    #print('Average Bitrate: ', avg_bitrate)
    data.append([mean_pkt_loss, pkt_loss_std, mean_power, power_std])
 
print(data)
#___________________________________________________________________
#Count this later for data links and outages
#___________________________________________________________________
#print(env.recorder.data['radio_use_trace'])
#print(env.recorder.data['outage_trace'][0])
radio_trace_ = env.recorder.data['radio_use_trace']
outage_trace_ = env.recorder.data['outage_trace']
plt_env_path(radio_trace_[0], outage_trace_[0])
 
radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
dist_step = env.recorder.data['dist_step'][0]
 
#Figure 2
fig, ax1 =plt.subplots()
plt.xlabel('Steps')
r0 = ax1.scatter(range(0,PATH_LEN), radio_0_pwr_trace[NUM_RUNS-1], c='m', s=1.5, label='802.15.4 Radio')
r1 = ax1.scatter(range(0,PATH_LEN), radio_1_pwr_trace[NUM_RUNS-1], c='c', s=1.5, label='802.11 Radio')
plt.legend(loc ="best", markerscale = 10 )
ax1.set_ylim([0.5,5.5])
ax1.set_ylabel('Radio Power Level')
 
ax2 = ax1.twinx()
l2 = ax2.plot(range(0,PATH_LEN), dist_step, c='r', label='Node distance')
 
ax2.set_ylim([0, 400])
ax2.set_ylabel('Node distance, m')
 
 
fig.suptitle('Radio Power Level and Node Distance vs. Steps')
 
#Figure 3
#--Add a legend---
fig2, ax1 =plt.subplots()
plt.xlabel('Steps')
r0 = ax1.scatter(range(0,PATH_LEN), avg_pwr_cons, c='g', s=1.5, label='Avg Power Consumption')
ax1.set_ylabel('Avg Power Consumption (mA)')
plt.legend(loc ="best", markerscale = 10 )
ax2 = ax1.twinx()
l2 = ax2.plot(range(0,PATH_LEN), dist_step, c='r', label='Node distance')
 

ax2.set_ylabel('Node distance, m')
 
 
fig2.suptitle('Avg Power Consumption vs. Steps')

#Figure 4
#--Add a legend---
fig3 = plt.figure(3)
fig3, ax1 =plt.subplots()
plt.xlabel('Steps')
r0 = ax1.scatter(range(0,PATH_LEN), outage_trace_, c='b', s=1.5, label='Outage Trace')
ax1.set_ylim([-0.25,1.25])
ax1.set_ylabel('Outage Trace, 0 = reachable, 1 = unreachable')
plt.legend(loc ="best", markerscale = 10 )
ax2 = ax1.twinx()
l2 = ax2.plot(range(0,PATH_LEN), dist_step, c='r', label='Node distance')
 

ax2.set_ylabel('Node distance, m')
 
 
fig3.suptitle('Outage Trace vs. Steps')

plt.show()
 

