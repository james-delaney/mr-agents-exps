import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]

#import gym
from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
from gym_grid_wireless.envs import PathPowerWirelessEnvV3 as pw3
from gym_grid_wireless.envs.path_power_wireless_env_v2 import *
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates, PowerStates, MultiRadioPowerState
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly
import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
#print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.gm_rl_agent import GmAgent
from util.visualisation import TrajectoryVisualiser, StepBasedVisualiser
import util.data_mgmt as data_mgmt

FILE_DIR = 'gym_grid_wireless/envs/'
DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\'

    
ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3.0_4.5', '3.5_5']
PATH_LEN = ['20000']


TIME_STEP_INTERVAL = 0.2
RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [10,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

ACTION_SPACE     = Actions
STATE_SPACE      = States

EXP_CFG = { 'rl_alg': 'sarsa',
            #'state_space': States,
            'state_space': PowerStates,
            #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
            'states_obj':MultiRadioPowerState(RADIO_CFG),
            'num_components': 2,
            'w1_comp_val': 0,
            'alpha': 0.7,
            'gamma': 0.1,
            'epsilon_schedule': GmAgent.DECAY_EPS,
            'rand_agent_expl':True,
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'env_name': ENV_NAME[2],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 5,
            'exp_param':'w1_comp_val',
            'param_list':[0, 0.05, 0.1],
}

EXP_CFG_EXPL = { 'rl_alg': 'sarsa',
            #'state_space': States,
            'state_space': PowerStates,
            #'states_obj': MultiRadioState(RADIO_CFG, 10, 25, 35, 45, 55, 65, 75),
            'states_obj':MultiRadioPowerState(RADIO_CFG),
            'num_components': 2,
            'w1_comp_val': 0.45,
            'alpha': 0.7,
            'gamma': 0.1,
            'epsilon_schedule': GmAgent.ADPTV_EPS,
            'rand_agent_expl':False,
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'env_name': ENV_NAME[2],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 100,
            'exp_param':'epsilon_schedule',
            'param_list':[GmAgent.ADPTV_EPS, GmAgent.DECAY_EPS],
}

#EXP_CFG = EXP_CFG_EXPL

def get_env_fnames(env_name, env_spec, path_len):
    env_path_fname = env_name+'_'+path_len+'.csv'
    env_fname = 'polys_'+env_name+'_'+env_spec+'.csv'
    env_ple_fname = 'PLE_'+env_name+'_'+path_len+'_'+env_fname
    env_shd_fname = 'SHD_'+env_name+'_'+path_len+'_'+env_fname

    return [env_path_fname, env_fname, env_ple_fname, env_shd_fname]

def init_env(env_files, env_cfg):
    env = gym.make("path-power-wireless-v2", 
                    update_interval             = TIME_STEP_INTERVAL, 
                    radio_cfg                   = env_cfg['radio_cfg'], 
                    agent_path_fname            = env_files[0], 
                    env_fname                   = env_files[1], 
                    ple_fname                   = env_files[2], 
                    shd_fname                   = env_files[3], 
                    god_agent                   = False)
    
    env.action_space = spaces.Discrete(len(env_cfg['action_space']))
    env.action_enum = env_cfg['action_space']

    return env

def init_agent(env, agent_cfg):
    agent = GmAgent(env)

    agent.cfg_agent(agent_cfg['rl_alg'], 
                    agent_cfg['state_space'],
                    agent_cfg['states_obj'], 
                    agent_cfg['num_components'], 
                    agent_cfg['w1_comp_val'], 
                    agent_cfg['alpha'], 
                    agent_cfg['gamma'], 
                    agent_cfg['epsilon_schedule'],
                    agent_cfg['rand_agent_expl'])

    return agent

def run_exp_exploration(exp_cfg):
    radio_data = []
    data = {}
    
    num_runs = exp_cfg['num_runs']
    p_len = int(exp_cfg['path_len'])
    param = exp_cfg['exp_param']
    param_list = exp_cfg['param_list']
    #param_list = [0, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2]
    #param_list = [0]

    env_files = get_env_fnames(exp_cfg['env_name'], exp_cfg['env_spec'], exp_cfg['path_len'])

    print('Starting experiment with ctl param=\''+param+'\'')
    print('  Params will be: '+str([float('%.2f' % (p)) for p in param_list]))

    for p in range(len(param_list)):
        
        exp_cfg[param] =  param_list[p]
        if (param_list[p] == GmAgent.DECAY_EPS):
            exp_cfg['rand_agent_expl'] = True
        else:
            exp_cfg['rand_agent_expl'] = False

        env = init_env(env_files, exp_cfg)

        agent = init_agent(env, exp_cfg)

        print('\tRunning experiment with \''+param+'\'='+str('%.2f' % (param_list[p])) + ' for ' + str(num_runs) + ' runs')
        for i in range(num_runs):
            sys.stdout.write('\r\tRun %d of %d' % (i+1, num_runs))
            sys.stdout.flush()
            agent.simulate_run(re_init=False)
        
        
        # extract and save radio data for this group of parameter runs
        radio_use_trace = env.recorder.data['radio_use_trace']
        radio_outage_trace = env.recorder.data['outage_trace']
        radio_0_pwr_trace = env.recorder.data['radio_pwr_lvl_r0']
        radio_1_pwr_trace = env.recorder.data['radio_pwr_lvl_r1']
        dist_step = env.recorder.data['dist_step'][0]
        radio_data.append([radio_use_trace, radio_outage_trace, radio_0_pwr_trace, radio_1_pwr_trace, dist_step])
        
        pkt_loss_rate = env.recorder.data['pkt_loss_rate']
        cum_power = env.recorder.data['tot_pwr']
        cum_step_reward = env.recorder.data['cum_step_reward']
        avg_bitrate = env.recorder.data['avg_bitrate']
        avg_pwr_cons = env.recorder.data['avg_pwr_cons']
        inst_bitrate = env.recorder.data['inst_bitrate']
        inst_pwr_cons = env.recorder.data['inst_pwr_cons']
        goodput = env.recorder.data['goodput']
        tot_pkts = env.recorder.data['tot_pkts']
        pkts_lost = env.recorder.data['pkts_lost']

        pkt_loss_rate = np.array(pkt_loss_rate)
        tot_power = np.array(cum_power)
        avg_bitrate = np.array(avg_bitrate)
        avg_pwr_cons = np.array(avg_pwr_cons)[num_runs-1,-1]

        inst_bitrate = np.array(inst_bitrate)
        inst_pwr_cons = np.array(inst_pwr_cons)

        tot_pkts = np.array(tot_pkts)
        pkts_lost = np.array(pkts_lost)

        mean_pkt_loss = np.mean(pkt_loss_rate)
        std_pkt_loss = np.std(pkt_loss_rate)
        mean_power = np.mean(tot_power)
        std_power = np.std(tot_power)

        mean_avg_bitrate = np.mean(avg_bitrate[:,p_len-1])
        std_avg_bitrate = np.std(avg_bitrate[:,p_len-1])
        
        mean_goodput = np.mean(goodput)
        std_goodput = np.std(goodput)
        
        mean_tot_pkts = np.mean(tot_pkts)
        std_tot_pkts = np.std(tot_pkts)
        mean_pkts_lost = np.mean(pkts_lost)
        std_pkts_lost = np.std(pkts_lost)

        #data.append([mean_pkt_loss, std_pkt_loss, mean_power, std_power, mean_avg_bitrate, std_avg_bitrate, mean_goodput, std_goodput, mean_tot_pkts, std_tot_pkts, mean_pkts_lost, std_pkts_lost])
        '''
        data[param_list[p]] = {'mean_pkt_loss':     env.recorder.mean_across_runs('pkt_loss_rate'),
                               'std_pkt_loss':      env.recorder.std_across_runs('pkt_loss_rate'),
                               'mean_power':        env.recorder.mean_across_runs('tot_pwr'),
                               'std_power':         env.recorder.std_across_runs('tot_pwr'),
                               'mean_goodput':      env.recorder.mean_across_runs('goodput'),
                               'std_goodput':       env.recorder.std_across_runs('goodput')
            }
        '''
        data[param_list[p]] = {'pkt_loss_rate': pkt_loss_rate,
                               'tot_pwr': cum_power,
                               'goodput': goodput,
                               #'cum_step_reward':cum_step_reward
                              }
    '''    
    tj = TrajectoryVisualiser(FILE_DIR, env_files[0], env_files[1])
    step_vis = StepBasedVisualiser()


    for p in range(len(param_list)):
        radio_use = np.array([np.bincount(radio_data[p][0][r]) for r in range(num_runs)])
        radio_use_pc = [[radio_use[r,0]/p_len, radio_use[r,1]/p_len] for r in range(num_runs)]
        mean_radio_use_pc = np.mean(radio_use_pc, axis=0)

        if param_list[p] == GmAgent.ADPTV_EPS:
            lbl = r'Adaptive $\epsilon-greedy'
        elif param_list[p] == GmAgent.DECAY_EPS:
            lbl = r'Decayed $\epsilon-greedy with periodic reset'

        tj.plt_mean_pwr_use(radio_data[p][2], radio_data[p][3], num_runs, p_len, title='Radio Power State Distribution\n'+lbl)
        #print(np.sum(mean_r0_pwr_use), np.sum(mean_r1_pwr_use))
        lbl = ''
        
        
        tj.plt_fig_traj_pwr_lvl(radio_data[p][0][0], radio_data[p][1][0], radio_data[p][2][0], radio_data[p][3][0], radio_data[p][4], title=lbl)


    plt.figure()
    for p in range(len(param_list)):
        plt.scatter(data[p][6]/1000000, data[p][2], s=20, label='w1='+str('%.2f' % (param_list[p])))
        #plt.annotate('%.2f, %.2f' % (param_list[p], 1-param_list[p]), (data[p][6]/1000000, data[p][2]))

    ax = plt.gca()
    ax.invert_xaxis()
    plt.title('Objective Performance per Objective Preferences')
    plt.xlabel('Mean Goodput, MB')
    plt.ylabel('Mean Power Consumption, Wh')
    plt.legend()

    print('Radio use:')
    #for p in range(len(param_list)):
    radio_use = np.array([np.bincount(radio_data[p][0][r]) for r in range(num_runs)])
    radio_use_pc = [[radio_use[r,0]/p_len, radio_use[r,1]/p_len] for r in range(num_runs)]
    #mean_radio_use_pc = np.mean(radio_use_pc, axis=0)
    #print(radio_use)
    #print(radio_use_pc)
    #print(mean_radio_use_pc)
    #print('\tparam %.2f: r0=%.2f%%, r1=%.2f%%' % (param_list[p], mean_radio_use_pc[0]*100, mean_radio_use_pc[1]*100))
    '''


    '''
    for r in range(num_runs):
        if r%1==0:
            tj.plt_fig_traj_pwr_lvl(radio_use_trace[r], radio_outage_trace[r], radio_0_pwr_trace[r], radio_1_pwr_trace[r], dist_step, 'Run No. ' + str(r))


    td_err_ = env.recorder.data['td_err']
    for i in range(len(td_err_)):
        step_vis.plt_fig_single_run(td_err_[i], 'TD-Error', 'TD-Error vs. Steps, Run No. ' + str(i))
    '''
    
    '''
    tot_pkts = env.recorder.data['tot_pkts']
    pkts_lost = env.recorder.data['pkts_lost']
    goodput = env.recorder.data['goodput']

    mean_tot_pkts = np.mean(tot_pkts)
    std_tot_pkts = np.std(tot_pkts)
    mean_pkts_lost = np.mean(pkts_lost)
    std_pkts_lost = np.std(pkts_lost)
    mean_goodput = np.mean(goodput)
    std_goodput = np.std(goodput)
    '''
    '''
    for p in range(len(param_list)):
        mean_tot_pkts = data[p][8]
        std_tot_pkts = data[p][9]
        mean_pkts_lost = data[p][10]
        std_pkts_lost = data[p][11]
        mean_goodput = data[p][6]
        std_goodput = data[p][7]
        mean_pwr_cons = data[p][2]
        std_pwr_cons = data[p][3]
        
        
        print('Param='+str(param_list[p]))
        
        
        
        print('mean tot pkts: %.2f, std: %.2f' % (mean_tot_pkts, std_tot_pkts))
        print('mean pkts lost: %.2f, std: %.2f' % (mean_pkts_lost, std_pkts_lost))
        print('mean goodput: %.2f bytes, std: %.2f' % (mean_goodput, std_goodput))
        print('mean pwr cons: %.2f Wh, std: %.2f' % (mean_pwr_cons, std_pwr_cons))
    '''
    return radio_data, dist_step, data

'''
# comparing reward function and exploration types
def run_exp_reward_func_expl_cmp():

    param_list = [['complex']]

    env_files = get_env_fnames(EXP_CFG['env_name'], EXP_CFG['env_spec'], EXP_CFG['path_len'])
'''

'''
radio_data, dist_step, data = run_exp_exploration()

data_mgmt.export_cfg_results_data('test_data_exp_exploration_16102020_1552', EXP_CFG, data)
data_mgmt.export_cfg_radio_data('test_radio_data_exploration_16102020_1552', EXP_CFG, radio_data, dist_step)
#tp = EnvTrajectoryPlotter(FILE_DIR, ['o_20000.csv', 'q_20000.csv'], 'polys_o_3.5_5.csv')
#tp.plt_multi_traj_env('Agent Trajectories')
'''

## Uncomment to sweep w1 params with each exploration type
'''
param_list = np.linspace(0.5,1,6)
print('Starting run of experiments with w1 params: ' + str(param_list))
for p in param_list:
    EXP_CFG_EXPL['w1_comp_val'] = p
    radio_data, dist_step, data = run_exp_exploration(EXP_CFG_EXPL)
    
    lbl = '%.2f' % (p)
    data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'/exploration/data_exp_exploration_18112020_1154_'+lbl, EXP_CFG_EXPL, data)
    data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'exploration/radio_data_exp_exploration_18112020_1154_'+lbl, EXP_CFG_EXPL, radio_data, dist_step)

plt.show()
'''
radio_data, dist_step, data = run_exp_exploration(EXP_CFG)
data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'/exploration/data_exp_exploration_18112020_1154', EXP_CFG, data)
data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'exploration/radio_data_exp_exploration_18112020_1154', EXP_CFG, radio_data, dist_step)