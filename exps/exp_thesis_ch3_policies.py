
import gym

# this is a hack to make gym custom env work in Spyder
env_dict = gym.envs.registration.registry.env_specs.copy()
print(env_dict)
for env in env_dict:
    #print("Remove {} from registry".format(env))
    del gym.envs.registration.registry.env_specs[env]

#import gym

from gym import error, spaces, utils
from gym.utils import seeding
#from gym_grid_wireless.envs import LinearWirelessEnv as lw
#from gym_grid_wireless.envs.linear_wireless_env import *
#from gym_grid_wireless.envs.grid_wireless_env import *
#from gym_grid_wireless.envs import GridWirelessEnv as gw
#from gym_grid_wireless.envs import PathPowerWirelessEnv as pw
#from gym_grid_wireless.envs import PathPowerWirelessEnvV2 as pw2
from gym_grid_wireless.envs.path_power_wireless_env_v1 import *

from gym_grid_wireless.radio import Radio
from gym_grid_wireless.multi_radio_state import MultiRadioState, States, RadioPowerStates, PowerStates, MultiRadioPowerState, ExtPowerStates, MultiRadioExtPowerState, RadioSNRStates, MultiRadioSNRState
from gym_grid_wireless.multi_radio_actions import Actions, ActionsSimple, LowPowerActions, HighPowerActions, Radio0ActionsOnly, Radio1ActionsOnly, ControlActions
from gym_grid_wireless.node import *

import time
from numpy import array
from numpy import zeros
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
import matplotlib
import os
import csv
import json
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
print(path.dirname(path.dirname(path.abspath(__file__))))
from agents.rl_agent import RLAgent
from agents.fuzzy_agent_v3 import FuzzyMultiRadioAgent
from agents.fuzzy_ctl import FuzzyController, FuzzySet, MultiRadioFuzzySet, RadioPwrCtlFuzzySet
from agents.fixed_policy_agent import FixedPolicyAgent
from util.visualisation import TrajectoryVisualiser, StepBasedVisualiser
import util.data_mgmt as data_mgmt
from agents.policies import *
from copy import deepcopy


from exps.exp_fuzzy_agent import run_exp_weight_param_var as run_fuzzy_exp
from exps.exp_fixed_policy_agent import run_exp as run_fixed_exp
from exps.exp_adaptive_rl_agent import run_exp as run_rl_exp
from exps.exp_optimal_front import run_exp_single_obj as run_opt_exp

import gym_grid_wireless.__init__

DATA_EXP_DIR = path.dirname(path.abspath(__file__))+'\\data\\thesis_ch3\\policies\\rl_adptv_eps\\05052021\\'

TIME_STEP_INTERVAL = 0.2
RADIO_CFG = [   Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [10,17.2,20.2,30.7,33.4], 'psk', 4),
                #Radio(0, 915000000, 2000000, -110, 250000, [-6,0,5,10,12], [17,25,35,45,55], 'psk', 4),
                #Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [1.82,7.8,31,83,240], 'psk', 2)]
                Radio(1, 2400000000, 20000000, -97, 1000000, [-1,5,11,15,19.5], [147.2440,149.73,159.6287,179.6139,240], 'psk', 2)]

ENV_NAME = ['o', 'p', 'q']
ENV_SPEC = ['2.5_4', '3_4.5', '3.5_5']
PATH_LEN = ['20000']
ENV_SHD_SPEC = [0, 4, 7, 12]

ACTION_SPACE     = spaces.Discrete(len(Actions))
STATE_SPACE      = States

BALANCED_FUZZY_SET = MultiRadioFuzzySet(0.00)
MIN_PWR_FUZZY_SET = MultiRadioFuzzySet(1.00, 'power')
MAX_BR_FUZZY_SET = MultiRadioFuzzySet(2.00, 'bitrate')

'''
        Experiment script for thesis Ch. 3 - Policy performance
        
            The purpose of this script is to initialise and run all experiments 
            for the three agent types considered in Ch. 3 of the thesis. Results 
            data for these thesis scripts will be saved in dedicated folders 
            separate from the regular scripts.

'''


###
###  Experiment sub-script configurations
###     param-var = pkt_sim_cfg
EXP_CFG_RL = {
            'rl_alg': 'sarsa',
            'state_space': PowerStates,
            'states_obj':MultiRadioPowerState(RADIO_CFG),
            'policy': AdaptiveEGreedyPolicy(ACTION_SPACE, len(PowerStates), 0.7, 0.1),#ScheduleEGreedyPolicy(ACTION_SPACE, 0.95, 0.05, DECAY_PWR_STEPS),#
            'rw_fn': RW_FN_PWR_EFF_CONS,
            'alpha': 0.7,
            'gamma': 0.1,
            'epsilon_schedule': RLAgent.ADPTV_EPS,
            'rand_agent_expl':False,
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'pkt_sim_cfg': DefaultPktSimCfg(),
            'shd_cfg':[0,0],
            'env_name': ENV_NAME[2],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 100,
            'exp_param':'rw_fn',
            'param_list': [RW_FN_PWR_EFF, RW_FN_PWR_CONS, RW_FN_PWR_EFF_CONS]
    }


EXP_CFG_FUZZY = { 
            'fuzzy_set': BALANCED_FUZZY_SET,
            'input_map': {
                'snr_802154': 'radios[0].get_snr()',
                'snr_80211': 'radios[1].get_snr()',
                'tx_pow_802154': 'radios[0].get_tx_pow_step()+1',
                'tx_pow_80211': 'radios[1].get_tx_pow_step()+1'
             },
            'action_space': ControlActions,
            'radio_cfg': RADIO_CFG,
            'pkt_sim_cfg': DefaultPktSimCfg(),
            'shd_cfg': [0, 0.7],
            'env_name': ENV_NAME[2],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 100,
            'exp_param':'fuzzy_set',
            'param_list': [BALANCED_FUZZY_SET, MIN_PWR_FUZZY_SET, MAX_BR_FUZZY_SET],
}


EXP_CFG_FIXED = {
            'policy': FixedPolicyAgent.BALANCE,
            'action_space': Actions,
            'radio_cfg': RADIO_CFG,
            'pkt_sim_cfg': DefaultPktSimCfg(),
            'shd_cfg':[10, 0.7],
            'env_name': ENV_NAME[2],
            'env_spec': ENV_SPEC[2],
            'path_len': PATH_LEN[0],
            'num_runs': 50,
            'exp_param': 'pkt_sim_cfg',
            'param_list': [HighDrPktSimCfg(), LowFreqPktSimCfg()],
}


EXP_CFG_OPT = {
    'env_id':'path-power-wireless-v1',
    'rw_fn': RW_FN_PWR_EFF_CONS,
    'action_space': Actions,
    'radio_cfg': RADIO_CFG,
    'env_name': 0,
    'env_spec': ENV_SPEC[2],
    'path_len': PATH_LEN[0],
    'shd_cfg': [0,0.7],   # shadowing cfg, 0=std dev, 1=correlation coeff
    'num_runs': 1,
    'exp_param':'rw_fn',
    'param_list': [RW_FN_PWR_CONS, RW_FN_PWR_EFF_CONS, RW_FN_PWR_EFF]
}

###
###
###
def save_data(cfg, radio_data, dist_step, data, script_name, name_sfx, datetime):
    data_mgmt.export_cfg_results_data(DATA_EXP_DIR+'data_'+script_name+'_'+name_sfx+'_'+datetime, cfg, data)
    data_mgmt.export_cfg_radio_data(DATA_EXP_DIR+'radio_data_'+script_name+'_'+name_sfx+'_'+datetime, cfg, radio_data, dist_step)
    

    
###
###
###
def gen_agent_cfg(env_paths, env_PLEs, env_SHDs, exp_cfg):
    cfg = []
    for path in env_paths:
        for ple in env_PLEs:
            for shd in env_SHDs:
                c = deepcopy(exp_cfg)
                c['env_name'] = path
                c['env_spec'] = ple
                c['shd_cfg'] = [shd, 0.7]
                cfg.append(c)
    
    return cfg

paths = ['o', 'p', 'q']
ples = ['3_4.5', '3.5_5']
shds = [0]


def run_rl_agent_exps():
    exps = []
    script_name = 'rl_agent'
    name_sfx = 'app_sweep'
    
    cfg_rl = gen_agent_cfg(paths, ples, shds, EXP_CFG_RL)
    for cfg in cfg_rl:
        radio_data, dist_step, data = run_rl_exp(cfg)
        
        curr_time = time.localtime() 
        date = '%02d%02d%4d' % (curr_time.tm_mday, curr_time.tm_mon, curr_time.tm_year)
        ts = '%02d%02d' % (curr_time.tm_hour, curr_time.tm_min)
        datetime = date + '_' + ts
        
        save_data(cfg, radio_data, dist_step, data, script_name, name_sfx, datetime)
        exps.append([cfg['env_name'], cfg['env_spec'], cfg['shd_cfg'], datetime])
        
    with open(DATA_EXP_DIR+script_name+'_'+name_sfx+'.csv', 'w') as file:
        for e in exps:
            for i in e:
                file.write('%s,' % (str(i)))
            file.write('\n')
        file.close()
            
        
    
#run_rl_agent_exps() 


def run_agent_exps(cfg, script_name, name_sfx, run_func):
    exps = []
    #script_name = 'rl_agent'
    #name_sfx = 'app_sweep'
    
    cfg = gen_agent_cfg(paths, ples, shds, cfg)
    print('Running exps: ', len(cfg))
    for c in cfg:
        radio_data, dist_step, data = run_func(c)
        
        curr_time = time.localtime() 
        date = '%02d%02d%4d' % (curr_time.tm_mday, curr_time.tm_mon, curr_time.tm_year)
        ts = '%02d%02d' % (curr_time.tm_hour, curr_time.tm_min)
        datetime = date + '_' + ts
        
        save_data(c, radio_data, dist_step, data, script_name, name_sfx, datetime)
        exps.append([c['env_name'], c['env_spec'], c['shd_cfg'], datetime])
        
    with open(DATA_EXP_DIR+script_name+'_'+name_sfx+'.csv', 'w') as file:
        for e in exps:
            for i in e:
                file.write('%s,' % (str(i)))
            file.write('\n')
        file.close()
        
run_agent_exps(EXP_CFG_RL, 'rl_agent', 'rw_sweep', run_rl_exp)
#run_agent_exps(EXP_CFG_FUZZY, 'fuzzy_agent', 'set_sweep', run_fuzzy_exp)
#run_agent_exps(EXP_CFG_FIXED, 'fixed_agent', 'app_sweep', run_fixed_exp)
#run_agent_exps(EXP_CFG_OPT, 'opt_agent', 'obj_sweep', run_opt_exp)

